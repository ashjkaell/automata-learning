# Article: Automata Learning, A Categorical Perspective

Some remarks concerning this [article](./articles/categoricalPerspective.pdf#page=400)

[Here](index.html#some-notes-on-category-theory) are a few notes on category

### Factorization of maps in the "Sets" category

In this category (and many others !) any map $f : A \rightarrow B$ (which is a function by def of **Sets**) can be written as $f = (A \twoheadrightarrow X \rightarrowtail B)$.

**Note:** In the case of the **Sets** category:

* An *epimorphism* ($\twoheadrightarrow$) is a surjection.
* A *monomorphism* ($\rightarrowtail$) is an injection.

We can view $\text{row}(s)$ in an observation table as a function $S \rightarrow 2^E$ by defining $\text{row}(s)$ more abstractly as: 
$$
U,V \subseteq A^{*},\ \text{row}(s): U \rightarrow 2^V,\ \text{row}(u)(v) = \mathcal{L}(uv)
$$
This is notation for:
$$
\text{row}(u) = \{v \in V |\ uv \in \mathcal{L}\}
$$
Here $\mathcal{L}$ is the language we are trying to learn. This is essentially the same definition as in Angluin's paper, now written as a function to a subset of *extentions* $v \in E$ that make the start $u$ in $\mathcal{L}$

### Lemma 2
[link to lemma](./articles/categoricalPerspective.pdf#page=402)

This formalization allows to write and prove `Lemma 2`.

An interesting insight is the use of the factorization $(A \twoheadrightarrow X \rightarrowtail B)$ for the function $\text{row}(s)$. The intermediate $X$ in this case is a sort of compressed (because it's reached via a surjective mapping) representation of words in $S \cup S\cdot A$. (Note: The $X$ is potentially not the same in both diagrams !)

**Question:** Is $X$ the state-space of the automaton the algorithm is considering ? TODO

**Closed-ness**: The existence of the mapping $i$ shows that ??? TODO
**Complete-ness**: The existence of $j$ shows ??? TODO
