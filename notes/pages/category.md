# Some notes on Category Theory

## Definition

**Definition:** A *category* is a labeled directed graph.

* The nodes are called *objects*
* The directed edges are called *arrows* or *morphisms*

Arrows need to follow the following properties:

1. We can compose arrows associatively
2. For each object there exists an *identity arrow* (From object to self)

## The category of sets

1. **objects:** Sets
2. **arrows:** functions from one set to another

## Properties of Morphisms

Let $f : a \rightarrow b$ be a morphism. It can have the following properties; $f$ is a:

1. [monomorphism](https://en.wikipedia.org/wiki/Monomorphism) (or *monic*) if $\forall g_1, g_2 : x \rightarrow a, f \circ g_1 = f \circ g_2$ implies $g_1 = g_2$.
2. [epimorphism](https://en.wikipedia.org/wiki/Epimorphism) (or *epic*) if $\forall g_1, g_2 : a \rightarrow x, g_1 \circ f = g_2 \circ f$ implies $g_1 = g_2$.
3. bimorphism if $f$ is both *epic* and *monic*.
4. [isomorphism](https://en.wikipedia.org/wiki/Isomorphism) if there exists a morphism $g : b \rightarrow a$ s.t. $f \circ g = 1_b$ and $g \circ f = 1_a$.
5. [endomorphism](https://en.wikipedia.org/wiki/Endomorphism) if $a = b$.
6. [retraction](https://en.wikipedia.org/wiki/Retract_(category_theory)) if a right inverse of $f$ exists.
7. [section](https://en.wikipedia.org/wiki/Section_(category_theory)) if a left inverse of $f$ exists

### Notation

* We write a *monomorphism* by $\rightarrowtail$
* We write a *epimorphism* by $\twoheadrightarrow$

### Hom-Classes or Hom-Sets

The set $hom(X,Y)$ is defined as the set of all morphisms between objects $X$ and $Y$.

*Note:* We might have problems with sets vs classes; **locally small categories** are those whose hom-classes
are *actually* sets and not *classes*.

## Functors

Let $C$ and $D$ be categories. A functor $F$ from $C$ to $D$ is a mapping that:

  * associates to each object $X$ in $C$ an object $F(X)$ in $D$,
  * associates to each morphism in $C$ a morphism in $D$, such that identity and composition are preserved (to the manner of an isomorphism, etc...)

### Two important properties of functors:

We can prove:

  * F transforms each commutative diagram in C into a commutative diagram in D;
  * if f is an isomorphism in C, then F(f) is an isomorphism in D.
  
## Initial and Terminal objects

**Definition:** An *initial object* $I$ of a category C is such that there exists precisely one morphism $I \rightarrow X$ for all objects $X$ in $C$.

**Definition:** An *terminal object* $T$ of a category C is such that there exists precisely one morphism $X \rightarrow T$ for all objects $X$ in $C$.

An object *both initial and terminal* is called a *zero object* or *null object*.

## F-Algebras

The idea of an *F-Algebra* is to generalize *algebraic* structures by rewriting the laws in terms of morphisms.

### Definition

Let C be a category and $F: C \rightarrow C$ be an *endofunctor* of C. An *F-Algebra* is a tuple $(A,\alpha)$ where A is an *object* of C and $\alpha$ is a morphism $F(A) \rightarrow A$. (Well defined since $F$ is an *endo*functor).

The general idea is to replace *elements* of an algebraic structure (ex: group) by 
