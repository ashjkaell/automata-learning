# Meeting 2 notes:

## Oracle.

We can use `SOME.` to model non deterministic choice

It would also be helpful to use a `Locale` for both L* and
the oracle's properties:

```text
locale L* = fixes L :

assume oracle d = ... <-> L(d) = L
```
