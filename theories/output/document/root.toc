\contentsline {section}{\numberline {1}Finite Sets and Lemmas}{4}{section.1}% 
\contentsline {section}{\numberline {2}Functions on finite sets}{4}{section.2}% 
\contentsline {subsection}{\numberline {2.1}Union of finite sets}{4}{subsection.2.1}% 
\contentsline {subsection}{\numberline {2.2}NubBy function on finite sets}{5}{subsection.2.2}% 
\contentsline {subsection}{\numberline {2.3}Definition of a safe-head function}{10}{subsection.2.3}% 
\contentsline {section}{\numberline {3}Moore machines}{11}{section.3}% 
\contentsline {section}{\numberline {4}Main definitions and lemmas}{11}{section.4}% 
\contentsline {section}{\numberline {5}Well-definiteness}{12}{section.5}% 
\contentsline {paragraph}{Elimination rules for wdef moore machine}{13}{section*.2}% 
\contentsline {section}{\numberline {6}Output of the machine on words}{13}{section.6}% 
\contentsline {paragraph}{Remark on the definition of output}{13}{section*.3}% 
\contentsline {section}{\numberline {7}Number of states}{14}{section.7}% 
\contentsline {section}{\numberline {8}Moore-machine Isomorphisms}{14}{section.8}% 
\contentsline {subsection}{\numberline {8.1}Into and elim rules for isomorphisms}{14}{subsection.8.1}% 
\contentsline {subsection}{\numberline {8.2}Relation to definition in the paper.}{16}{subsection.8.2}% 
\contentsline {subsection}{\numberline {8.3}Properties of dfa-Isomorphisms}{17}{subsection.8.3}% 
\contentsline {section}{\numberline {9}Order relation on DFAs}{20}{section.9}% 
\contentsline {section}{\numberline {10}Minimal acceptors}{21}{section.10}% 
\contentsline {section}{\numberline {11}Product Automaton}{22}{section.11}% 
\contentsline {subsection}{\numberline {11.1}Lemmas about the product automaton}{23}{subsection.11.1}% 
\contentsline {section}{\numberline {12}Basic Definitions}{24}{section.12}% 
\contentsline {subsection}{\numberline {12.1}Some facts about regular languages}{24}{subsection.12.1}% 
\contentsline {subsubsection}{\numberline {12.1.1}Prefixes and Suffixes of words}{24}{subsubsection.12.1.1}% 
\contentsline {subsubsection}{\numberline {12.1.2}Prefix and Suffix closedness}{25}{subsubsection.12.1.2}% 
\contentsline {subsubsection}{\numberline {12.1.3}Function to compute the set of all prefixes}{28}{subsubsection.12.1.3}% 
\contentsline {subsection}{\numberline {12.2}Misc Theorems and Defs.}{30}{subsection.12.2}% 
\contentsline {section}{\numberline {13}Datatypes for Observation Tables}{31}{section.13}% 
\contentsline {subsection}{\numberline {13.1}The main datatype}{31}{subsection.13.1}% 
\contentsline {paragraph}{Equality on rows}{32}{section*.4}% 
\contentsline {subsection}{\numberline {13.2}The "filled" invariant}{32}{subsection.13.2}% 
\contentsline {subsection}{\numberline {13.3}Wellformedness of Tables}{33}{subsection.13.3}% 
\contentsline {subsection}{\numberline {13.4}Closedness and Consistency}{36}{subsection.13.4}% 
\contentsline {paragraph}{Definition of Closedness}{36}{section*.5}% 
\contentsline {paragraph}{Definition of Consistency}{36}{section*.6}% 
\contentsline {subsection}{\numberline {13.5}Getting Closedness and Consistency Problems}{37}{subsection.13.5}% 
\contentsline {paragraph}{Closedness problems}{37}{section*.7}% 
\contentsline {paragraph}{Consistency problems}{38}{section*.8}% 
\contentsline {section}{\numberline {14}Implementation of Dana Angluin's LStar Learner algorithm}{41}{section.14}% 
\contentsline {section}{\numberline {15}Converting Tables to Automata}{42}{section.15}% 
\contentsline {subsection}{\numberline {15.1}Preliminary steps}{42}{subsection.15.1}% 
\contentsline {subsection}{\numberline {15.2}Making the states of the acceptor}{45}{subsection.15.2}% 
\contentsline {subsection}{\numberline {15.3}Initial state.}{46}{subsection.15.3}% 
\contentsline {subsection}{\numberline {15.4}Accepting states}{46}{subsection.15.4}% 
\contentsline {subsection}{\numberline {15.5}Definition of the transition function}{47}{subsection.15.5}% 
\contentsline {subsection}{\numberline {15.6}Making a conjecture}{48}{subsection.15.6}% 
\contentsline {subsection}{\numberline {15.7}Welldefined conjecture}{52}{subsection.15.7}% 
\contentsline {section}{\numberline {16}Main lemmas}{52}{section.16}% 
\contentsline {subsection}{\numberline {16.1}Proof of theorem 1}{52}{subsection.16.1}% 
\contentsline {subsubsection}{\numberline {16.1.1}Lemma 2}{52}{subsubsection.16.1.1}% 
\contentsline {subsubsection}{\numberline {16.1.2}Lemma 3}{53}{subsubsection.16.1.2}% 
\contentsline {paragraph}{Notion of compatibility dfa - table}{53}{section*.9}% 
\contentsline {paragraph}{Proof of lemma 3}{54}{section*.10}% 
\contentsline {subsubsection}{\numberline {16.1.3}Lemma 4}{57}{subsubsection.16.1.3}% 
\contentsline {paragraph}{Converting states of the other acceptor to rows}{57}{section*.11}% 
\contentsline {paragraph}{Obtaining rows of the table as functions.}{57}{section*.12}% 
\contentsline {paragraph}{Proof of lemma 4}{58}{section*.13}% 
\contentsline {subsubsection}{\numberline {16.1.4}Theorem 1}{65}{subsubsection.16.1.4}% 
\contentsline {section}{\numberline {17}Implementation of Dana Angluin's LStar algorithm for learning DFAs (Modified for moore-machines)}{66}{section.17}% 
\contentsline {section}{\numberline {18}Datatypes for MAT - oracles}{66}{section.18}% 
\contentsline {section}{\numberline {19}The L* algorithm}{66}{section.19}% 
\contentsline {subsection}{\numberline {19.1}Necessary subroutines}{66}{subsection.19.1}% 
\contentsline {paragraph}{Looking up with alternatives.}{67}{section*.14}% 
\contentsline {subsubsection}{\numberline {19.1.1}Extending tables via membership queries}{67}{subsubsection.19.1.1}% 
\contentsline {subsubsection}{\numberline {19.1.2}Fixing Closedness and Consistency problems}{68}{subsubsection.19.1.2}% 
\contentsline {subsubsection}{\numberline {19.1.3}Wrapping observation tables with logs}{69}{subsubsection.19.1.3}% 
\contentsline {paragraph}{Definition of maximum-str-length}{69}{section*.15}% 
\contentsline {paragraph}{Incrementing the function-call counts.}{69}{section*.16}% 
\contentsline {paragraph}{Updating the max seen quantities.}{70}{section*.17}% 
\contentsline {subsubsection}{\numberline {19.1.4}Looping until we can make a conjecture.}{70}{subsubsection.19.1.4}% 
\contentsline {subsubsection}{\numberline {19.1.5}Making the initial table}{71}{subsubsection.19.1.5}% 
\contentsline {subsubsection}{\numberline {19.1.6}Learning from counter-examples}{71}{subsubsection.19.1.6}% 
\contentsline {subsection}{\numberline {19.2}Main algorithm loop}{72}{subsection.19.2}% 
\contentsline {section}{\numberline {20}Implementation of Dana Angluin's LStar algorithm for learning DFAs. (Modified for Moore machines)}{73}{section.20}% 
\contentsline {section}{\numberline {21}Needed Notions}{75}{section.21}% 
\contentsline {subsection}{\numberline {21.1}Number of different rows (Termination measure)}{75}{subsection.21.1}% 
\contentsline {subsection}{\numberline {21.2}Lemma 5}{77}{subsection.21.2}% 
\contentsline {subsection}{\numberline {21.3}Using lemma 5 on minimum acceptor}{79}{subsection.21.3}% 
\contentsline {subsection}{\numberline {21.4}Improvement of the \emph {\sfcode 42 1000 \sfcode 63 1000 \sfcode 33 1000 \sfcode 58 1000 \sfcode 59 1000 \sfcode 44 1000 \normalfont \itshape follows{\unhbox \voidb@x \hbox {-}}orcm} lemma}{81}{subsection.21.4}% 
\contentsline {section}{\numberline {22}Correctness of table access functions}{82}{section.22}% 
\contentsline {paragraph}{Main invariant}{82}{section*.18}% 
\contentsline {paragraph}{Some helpful lemmas using the invariant}{83}{section*.19}% 
\contentsline {subsection}{\numberline {22.1}The make-initial-table function respects the invariant}{84}{subsection.22.1}% 
\contentsline {subsection}{\numberline {22.2}The 'extend' functions respect the invariant}{86}{subsection.22.2}% 
\contentsline {subsection}{\numberline {22.3}Properties of safe-lookup}{86}{subsection.22.3}% 
\contentsline {subsubsection}{\numberline {22.3.1}ExtendS}{87}{subsubsection.22.3.1}% 
\contentsline {paragraph}{Effect on A}{87}{section*.20}% 
\contentsline {paragraph}{Effect on E}{87}{section*.21}% 
\contentsline {paragraph}{Effect on S}{87}{section*.22}% 
\contentsline {paragraph}{Effect on SA}{88}{section*.23}% 
\contentsline {paragraph}{The type of queries extendS will perform}{89}{section*.24}% 
\contentsline {paragraph}{Parts of the invariant}{90}{section*.25}% 
\contentsline {subsubsection}{\numberline {22.3.2}ExtendE}{98}{subsubsection.22.3.2}% 
\contentsline {paragraph}{Effect on A}{98}{section*.26}% 
\contentsline {paragraph}{Effect on S and SA2}{98}{section*.27}% 
\contentsline {paragraph}{Effect on E}{99}{section*.28}% 
\contentsline {paragraph}{The types of queries extendE will perform}{99}{section*.29}% 
\contentsline {paragraph}{Parts of the invariant}{100}{section*.30}% 
\contentsline {subsection}{\numberline {22.4}The 'fix' functions respect the invariant}{105}{subsection.22.4}% 
\contentsline {subsection}{\numberline {22.5}The make-closed/make-consistent functions respect the invariant.}{107}{subsection.22.5}% 
\contentsline {subsection}{\numberline {22.6}The 'learn from counter-examples function' respects the invariant}{108}{subsection.22.6}% 
\contentsline {paragraph}{learn-from-cexample preserves the invariant}{108}{section*.31}% 
\contentsline {subsection}{\numberline {22.7}Further properties.}{109}{subsection.22.7}% 
\contentsline {paragraph}{make-consistent}{109}{section*.32}% 
\contentsline {paragraph}{make-closed}{109}{section*.33}% 
\contentsline {paragraph}{Learn from counter-example}{110}{section*.34}% 
\contentsline {section}{\numberline {23}Termination of the L* Algorithm}{110}{section.23}% 
\contentsline {subsection}{\numberline {23.1}Properties of make-cc}{110}{subsection.23.1}% 
\contentsline {subsubsection}{\numberline {23.1.1}Variant for make-closed}{110}{subsubsection.23.1.1}% 
\contentsline {subsubsection}{\numberline {23.1.2}Variant for make-consistent}{113}{subsubsection.23.1.2}% 
\contentsline {subsubsection}{\numberline {23.1.3}Generalizing to the make-cc-step function}{117}{subsubsection.23.1.3}% 
\contentsline {subsection}{\numberline {23.2}Termination Proof of make-cc}{118}{subsection.23.2}% 
\contentsline {subsection}{\numberline {23.3}Helper proof rules (makeCC)}{119}{subsection.23.3}% 
\contentsline {subsection}{\numberline {23.4}Further properties of make-cc.}{120}{subsection.23.4}% 
\contentsline {subsection}{\numberline {23.5}Properties for Termination of lStarLearner}{122}{subsection.23.5}% 
\contentsline {subsubsection}{\numberline {23.5.1}Lemmas about extending the table}{123}{subsubsection.23.5.1}% 
\contentsline {paragraph}{Extending the row-labels (S union SA)}{123}{section*.35}% 
\contentsline {paragraph}{Extending the set E}{125}{section*.36}% 
\contentsline {subsubsection}{\numberline {23.5.2}Properties of extensions of tables.}{127}{subsubsection.23.5.2}% 
\contentsline {subsection}{\numberline {23.6}Termination of the lstar-learner-loop}{129}{subsection.23.6}% 
\contentsline {subsection}{\numberline {23.7}Helper proof rule (lstar-learner-loop)}{135}{subsection.23.7}% 
\contentsline {section}{\numberline {24}Correctness of the L* Algorithm}{139}{section.24}% 
\contentsline {section}{\numberline {25}Analysis of the time/space complexity}{143}{section.25}% 
\contentsline {subsection}{\numberline {25.1}Proofs about log access functions}{143}{subsection.25.1}% 
\contentsline {subsection}{\numberline {25.2}Invariant for the logged iterations.}{146}{subsection.25.2}% 
\contentsline {subsection}{\numberline {25.3}Counting the number of function calls via the log.}{148}{subsection.25.3}% 
\contentsline {subsection}{\numberline {25.4}Bounding the max size of sets S and E}{152}{subsection.25.4}% 
\contentsline {subsection}{\numberline {25.5}Bounding the max size of strings in S and E}{164}{subsection.25.5}% 
\contentsline {subsection}{\numberline {25.6}Propagating the iteration log invariant}{176}{subsection.25.6}% 
\contentsline {section}{\numberline {26}Instantiation of locales used}{182}{section.26}% 
\contentsline {section}{\numberline {27}Definition and Properties of Nerode's relation}{182}{section.27}% 
\contentsline {subsection}{\numberline {27.1}Right congruence}{184}{subsection.27.1}% 
\contentsline {section}{\numberline {28}Definition of the Myhill-Nerode's minimal acceptor}{184}{section.28}% 
\contentsline {subsection}{\numberline {28.1}Definition of the statespace}{184}{subsection.28.1}% 
\contentsline {subsection}{\numberline {28.2}Initial state}{184}{subsection.28.2}% 
\contentsline {subsection}{\numberline {28.3}Definition of the transition function}{185}{subsection.28.3}% 
\contentsline {subsection}{\numberline {28.4}Definition of the final function}{186}{subsection.28.4}% 
\contentsline {section}{\numberline {29}Myhill-Nerode}{187}{section.29}% 
\contentsline {subsection}{\numberline {29.1}Helper definitions and lemmas}{187}{subsection.29.1}% 
\contentsline {subsection}{\numberline {29.2}Minimal acceptor and wellformedness}{187}{subsection.29.2}% 
\contentsline {subsection}{\numberline {29.3}Minimality of the constructed acceptor.}{191}{subsection.29.3}% 
\contentsline {subsection}{\numberline {29.4}Assembling lemmas to get main theorem}{197}{subsection.29.4}% 
\contentsline {section}{\numberline {30}Defining Oracles based on the Myhill-Nerode acceptor}{198}{section.30}% 
\contentsline {section}{\numberline {31}Instantiation of the Moore locale}{203}{section.31}% 
