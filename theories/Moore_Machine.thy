section \<open> Moore machines \<close>

theory Moore_Machine
  imports
    Main
    Finite_Sets
    Automata_Common
begin
 
text \<open> Contains basic definition of moore machines. The only practical difference
       between a Moore machine and a DFA is the final predicate, which becomes a
       function to a finite output alphabet in the case of the Moore machine.
       
       Angluin's LStar Learner algorithm can be directly adapted for Moore machines,
       which is what we do in these theories. \<close>

section \<open> Main definitions and lemmas \<close>

text \<open> Note that the Moore machine is defined over the fset type (finite set) as statespace. \<close>
record ('a,'s,'o) moore_machine =
     \<comment> \<open> For flexibility, we make the alphabet known to the machine.\<close>
    get_alphabet :: "'a fset"
     \<comment> \<open> The output set of the moore machine is implicitly datatype 'o \<close>
    \<comment> \<open> The statespace is a finite set\<close>
    get_statespace :: "'s fset "
    \<comment> \<open> Initial state\<close>
    get_initial :: "'s"
    \<comment> \<open> Transition function \<close>
    get_transition :: "'a \<Rightarrow> 's \<Rightarrow> 's"
    \<comment> \<open> The only difference between a dfa and a moore machine is the output function !\<close>
    get_finalf :: "'s \<Rightarrow> 'o"


text \<open> The transition function extended to work on words
       The mixfix annotation still allows for partial application \<close>
definition delta :: "('a,'s,'o) moore_machine \<Rightarrow> 'a list \<Rightarrow> 's \<Rightarrow> 's"
  ("\<delta>\<langle>_\<rangle>") where
 " delta A = foldl2 (get_transition A) "

lemma delta_Nil[simp]: "\<delta>\<langle>A\<rangle> [] s = s"
  by(simp add:delta_def)

lemma delta_Cons[simp]: "\<delta>\<langle>A\<rangle> (w@[a]) s = (get_transition A) a (\<delta>\<langle>A\<rangle> w s)"
  by(simp add:delta_def foldl2_def)

lemma delta_append[simp]: "\<delta>\<langle>A\<rangle> (xs@ys) q = \<delta>\<langle>A\<rangle> ys (\<delta>\<langle>A\<rangle> xs q)"
  apply(induction xs)
  apply(simp_all add: delta_def foldl2_def)
  done

(* It's not a good simp rule, since we could keep decomposing *)
lemma delta_Cons_head: "\<delta>\<langle>A\<rangle> (x#xs) q = \<delta>\<langle>A\<rangle> xs (\<delta>\<langle>A\<rangle> [x] q)"
proof -
  have "\<delta>\<langle>A\<rangle> (x#xs) q = \<delta>\<langle>A\<rangle> ([x]@xs) q" by(simp)
  also have "    ... = \<delta>\<langle>A\<rangle> xs (\<delta>\<langle>A\<rangle> [x] q)" by(simp only: delta_append)
  finally show ?thesis .
qed

(* This is an attempt at making it easier to work with *)
lemma delta_ConsI: "\<lbrakk>\<delta>\<langle>A\<rangle> [x] q = r; \<delta>\<langle>A\<rangle> xs r = s\<rbrakk> \<Longrightarrow> \<delta>\<langle>A\<rangle> (x#xs) q = s"
  by (meson delta_Cons_head)

section \<open> Well-definiteness \<close>
text \<open> Note that nothing actually insures that the delta function stays in the statespace !
       We'll need this for further notions, thus we define a 'welldefiniteness' predicate. \<close>

definition welldefined_dfa_p :: " ('a, 's, 'o) moore_machine \<Rightarrow> bool " where
  " welldefined_dfa_p machine \<equiv>
    \<comment> \<open> The initial state is in the statespace\<close>
    ((get_initial machine) \<in> set (get_statespace machine)) \<and> 
    \<comment> \<open> The transition function stays in the statespace \<close>
    (\<forall>s \<in> set (get_statespace machine). \<forall>a \<in> (set (get_alphabet machine)).
     (get_transition machine) a s \<in> set (get_statespace machine))"

lemma welldefined_dfa_pI[intro]:
  "\<lbrakk>((get_initial machine) \<in> set (get_statespace machine));
     \<And>s a. \<lbrakk>s \<in> set (get_statespace machine); a \<in> (set (get_alphabet machine))\<rbrakk> 
       \<Longrightarrow> (get_transition machine) a s \<in> set (get_statespace machine)\<rbrakk> 
     \<Longrightarrow> welldefined_dfa_p machine"
  by(auto simp add: welldefined_dfa_p_def)

text \<open> If the transition function stays in the statespace, we can extend this to the 
       delta function as well. \<close>
lemma welldefined_dfa_p_imp_delta_in_statespace:
  "welldefined_dfa_p dfa \<Longrightarrow> 
   (\<forall>s \<in> set (get_statespace dfa). \<forall>w \<in> lists (set (get_alphabet dfa)).
     \<delta>\<langle>dfa\<rangle> w s \<in> set (get_statespace dfa))"
proof(rule ballI, rule ballI)
  fix s w
  assume wdef: "welldefined_dfa_p dfa" and
         defs: "s \<in> set (get_statespace dfa)" and
         defw: "w \<in> lists (set (get_alphabet dfa))"
  then show "\<delta>\<langle>dfa\<rangle> w s \<in> set (get_statespace dfa)"
  proof(induction w arbitrary: s)
  case Nil
    then show ?case by(simp)
  next
  case (Cons a w)
    then show ?case by (simp add: delta_def welldefined_dfa_p_def)
  qed
qed

paragraph \<open> Elimination rules for wdef moore machine \<close>


lemma welldefined_dfa_p_initialE[elim]:
  "welldefined_dfa_p machine \<Longrightarrow> (get_initial machine) \<in> set (get_statespace machine)"
  by(simp add: welldefined_dfa_p_def)

lemma welldefined_dfa_p_deltaE[elim]: 
  "\<lbrakk>welldefined_dfa_p machine; s \<in> set (get_statespace machine);
   w \<in> lists (set (get_alphabet machine))\<rbrakk>
   \<Longrightarrow> \<delta>\<langle>machine\<rangle> w s \<in> set (get_statespace machine)"
  by(simp add: welldefined_dfa_p_imp_delta_in_statespace)

section \<open> Output of the machine on words \<close>

paragraph \<open> Remark on the definition of output \<close>

text \<open> In some cases, one can view the output of a moore machine on a word
       as the sequence of output symbols the machine goes through when processing
       the word. In our definitions we only consider the last symbol. This is not
       a restriction since the L* algorithm learns an automaton isomorph to the minimal
       acceptor, and by determinism the sequence of states and output symbols must match
       in both. \<close>

text \<open> Gets the output of the moore machine on some word; \<close>
definition output_on :: "('a,'s,'o) moore_machine \<Rightarrow> 'a list \<Rightarrow> 'o" where
  " output_on m word = (get_finalf m) (\<delta>\<langle>m\<rangle> word (get_initial m))"         

text \<open> Predicate checking if the accepted language is equal to another language.
       We have to restrict the input words to words in the alphabet ! \<close>
definition accepted_language_eq_p ::
  " ('a,'s,'o) moore_machine \<Rightarrow> 'a fset \<Rightarrow> ('a list \<Rightarrow> 'o) \<Rightarrow> bool" where
  " accepted_language_eq_p m \<Sigma> L \<equiv>
      (set (get_alphabet m) = set \<Sigma>) \<and>
      (\<forall>w \<in> lists (set (get_alphabet m)). output_on m w = L w)"

lemma accepted_language_eq_pI[intro]:
  "\<lbrakk>(set (get_alphabet m) = set \<Sigma>); \<And>w. w \<in> lists (set (get_alphabet m))
     \<Longrightarrow> output_on m w = L w\<rbrakk>
    \<Longrightarrow> accepted_language_eq_p m \<Sigma> L"
  by(auto simp add: accepted_language_eq_p_def)

lemma accepted_language_eq_p_alphE[elim]:
  "accepted_language_eq_p M \<Sigma> L \<Longrightarrow> (set (get_alphabet M) = set \<Sigma>)"
  by(auto simp add: accepted_language_eq_p_def)

lemma accepted_language_eq_pE[elim]:
  "\<lbrakk>accepted_language_eq_p M \<Sigma> L; w \<in> lists (set (get_alphabet M))\<rbrakk> \<Longrightarrow> 
     output_on M w = L w"
  by(auto simp add: accepted_language_eq_p_def)

section \<open> Number of states \<close>

abbreviation num_states :: " ('a,'s,'o) moore_machine \<Rightarrow> nat " where
  " num_states machine \<equiv> card (set (get_statespace machine)) "

section \<open> Moore-machine Isomorphisms \<close>

text \<open> An isomorphism between Moore machines is simply a bijection between their statespace which also:
       \<^enum> maps the initials states to each other
       \<^enum> respects the transition function
       \<^enum> respects the output function
       We also require the input and output alphabets to match
       (for simplicity we don't model morphisms on alphabets)\<close>

definition iso_betw_p ::
  "('s \<Rightarrow> 't) \<Rightarrow> ('a,'s,'o) moore_machine \<Rightarrow> ('a,'t,'o) moore_machine \<Rightarrow> bool " where
  " iso_betw_p f M Q \<equiv>
    \<comment> \<open> The alphabets are equal \<close>
    (set (get_alphabet M) = set (get_alphabet Q)) \<and>
    \<comment> \<open> f is a bijection \<close>
    bij_betw f (set (get_statespace M)) (set (get_statespace Q)) \<and>
    \<comment> \<open> f maps q0 to q0' \<close>
    f (get_initial M) = get_initial Q \<and>
    \<comment> \<open> f respects the trans. function \<close>
   (\<forall>s \<in> set (get_statespace M).
    \<forall>t \<in> set (get_statespace M).
    \<forall>a \<in> set (get_alphabet M).
     (get_transition M a) s = t \<longleftrightarrow> (get_transition Q a) (f s) = f t) \<and>
    \<comment> \<open> f respects the output function \<close>
   (\<forall>s \<in> set (get_statespace M). (get_finalf M) s = (get_finalf Q) (f s))"

subsection \<open> Into and elim rules for isomorphisms \<close>

lemma iso_betw_pI[intro]: 
  "\<lbrakk>(set (get_alphabet M) = set (get_alphabet Q));
    bij_betw f (set (get_statespace M)) (set (get_statespace Q));
    f (get_initial M) = get_initial Q;
    \<And>s t a. 
     \<lbrakk>s \<in> set (get_statespace M);
      t \<in> set (get_statespace M);
      a \<in> set (get_alphabet M)\<rbrakk> \<Longrightarrow>
     (get_transition M a) s = t \<longleftrightarrow> (get_transition Q a) (f s) = f t;
   (\<And>s. s \<in> set (get_statespace M) \<Longrightarrow> (get_finalf M) s = (get_finalf Q) (f s))\<rbrakk> \<Longrightarrow> iso_betw_p f M Q"
  by(auto simp add: iso_betw_p_def)


text \<open>We 'augment' the elimination rule to get that f respects the delta function by extending the statement to full words \<close>

lemma iso_betw_p_alph_equalE[elim]:
  "iso_betw_p f M Q \<Longrightarrow> set (get_alphabet M) = set (get_alphabet Q)"
  by(simp add: iso_betw_p_def)

lemma iso_betw_p_bijE[elim]:
  "iso_betw_p f M Q
   \<Longrightarrow> bij_betw f (set (get_statespace M)) (set (get_statespace Q))"
  by(simp add: iso_betw_p_def)

lemma iso_betw_p_initStatesE[elim]:
  "iso_betw_p f M Q \<Longrightarrow> f (get_initial M) = get_initial Q"
  by(simp add: iso_betw_p_def)

lemma iso_betw_p_transitionE[elim]:
  "\<lbrakk>iso_betw_p f M Q; s \<in> set (get_statespace M); t \<in> set (get_statespace M);
   a \<in> set (get_alphabet M)\<rbrakk>
    \<Longrightarrow> (get_transition M a) s = t \<longleftrightarrow> (get_transition Q a) (f s) = f t"
  by(simp add: iso_betw_p_def)

text \<open> This lemma "enhances" the fact by lifting it to the delta function \<close>
lemma iso_betw_p_imp_respects_delta:
  "welldefined_dfa_p M \<Longrightarrow> iso_betw_p f M Q \<Longrightarrow> 
  (\<forall>s \<in> set (get_statespace M).
   \<forall>t \<in> set (get_statespace M).
   \<forall>w \<in> lists (set (get_alphabet M)).
     \<delta>\<langle>M\<rangle> w s = t \<longleftrightarrow> \<delta>\<langle>Q\<rangle> w (f s) = f t)"
proof(rule ballI, rule ballI, rule ballI)
  fix s t w
  assume wdef: "welldefined_dfa_p M" and
         iso: "iso_betw_p f M Q" and
         defs: "s \<in> set (get_statespace M)" and
         deft: "t \<in> set (get_statespace M)" and
         defw: "w \<in> lists (set (get_alphabet M))"
  \<comment> \<open> We have the statement, but only for 1 letter words \<close>
  from iso have respects1: 
    "(\<forall>s \<in> set (get_statespace M). \<forall>t \<in> set (get_statespace M). \<forall>a \<in> set (get_alphabet M).
     (get_transition M) a s = t \<longleftrightarrow> (get_transition Q) a (f s) = f t)"
    by(simp add: iso_betw_p_def)
  \<comment> \<open> We also know that f is a bijection \<close>
  from iso have fbij: "bij_betw f (set (get_statespace M)) (set (get_statespace Q))"
    by(rule iso_betw_p_bijE)
  from fbij defs deft defw show "(\<delta>\<langle>M\<rangle> w s = t) = (\<delta>\<langle>Q\<rangle> w (f s) = f t)"
  proof(induction w arbitrary: t rule: rev_induct)
  case Nil
    \<comment> \<open> This case follows from injectivity of f \<close>
    then show ?case proof(simp only: delta_Nil)
      qed(drule bij_betw_imp_inj_on, auto simp add: inj_onD)
  next
    case (snoc x xs)
    \<comment> \<open> The IH gives us: \<close>
    from snoc have ih:
      "\<And>t. t \<in> set (get_statespace M) \<Longrightarrow> (\<delta>\<langle>M\<rangle> xs s = t) = (\<delta>\<langle>Q\<rangle> xs (f s) = f t)"
      by(simp)
    show ?case
      using ih
      by (metis append_in_lists_conv delta_Cons listsE respects1 snoc.prems(2)
          snoc.prems(3) snoc.prems(4) wdef welldefined_dfa_p_deltaE)
  qed
qed

text \<open> An elimination rule giving the generalized fact \<close>
lemma iso_betw_p_respects_deltaE: 
  "\<lbrakk>welldefined_dfa_p M; iso_betw_p f M Q; s \<in> set (get_statespace M);
    t \<in> set (get_statespace M); w \<in> lists (set (get_alphabet M))\<rbrakk> \<Longrightarrow>
     \<delta>\<langle>M\<rangle> w s = t \<longleftrightarrow> \<delta>\<langle>Q\<rangle> w (f s) = f t"
  by(auto simp add: iso_betw_p_imp_respects_delta)

lemma iso_betw_p_finalpE[elim]:
  "\<lbrakk>iso_betw_p f M Q; s \<in> set (get_statespace M)\<rbrakk>
   \<Longrightarrow> (get_finalf M) s = (get_finalf Q) (f s)"
  by(auto simp add: iso_betw_p_def)

subsection \<open> Relation to definition in the paper. \<close>

text \<open> In Angluin's paper, isomorphisms respecting the transition function is formalized as f commutes
       with delta. We show that the notion above is equivalent \<close>

text \<open> Note, for convenience we also generalized this to words (instead of single letters) \<close>
lemma iso_betw_p_imp_delta_commute[simp]:
  "\<lbrakk>welldefined_dfa_p M; iso_betw_p f M Q; s \<in> set (get_statespace M);
    w \<in> lists (set (get_alphabet M))\<rbrakk> \<Longrightarrow> 
   f (\<delta>\<langle>M\<rangle> w s) = \<delta>\<langle>Q\<rangle> w (f s)"
  using iso_betw_p_respects_deltaE welldefined_dfa_p_deltaE by fastforce

lemma delta_commute_imp_delta_respects: 
           "\<lbrakk>welldefined_dfa_p M; set (get_alphabet M) = set (get_alphabet Q); bij_betw f (set (get_statespace M)) (set (get_statespace Q))\<rbrakk> \<Longrightarrow>
           (\<forall>s \<in> set (get_statespace M). \<forall>a \<in> set (get_alphabet M). f (\<delta>\<langle>M\<rangle> [a] s) = \<delta>\<langle>Q\<rangle> [a] (f s)) \<Longrightarrow>
           (\<forall>s \<in> set (get_statespace M). \<forall>a \<in> set (get_alphabet M). \<forall>t \<in> set (get_statespace M). (get_transition M a) s = t \<longleftrightarrow> (get_transition Q a) (f s) = f t)"
proof -
  assume wdefined: "welldefined_dfa_p M" and
         sameAlph: "set (get_alphabet M) = set (get_alphabet Q)" and
         fbij: "bij_betw f (set (get_statespace M)) (set (get_statespace Q))" and
         fcommute: "(\<forall>s \<in> set (get_statespace M). \<forall>a \<in> set (get_alphabet M). f (\<delta>\<langle>M\<rangle> [a] s) = \<delta>\<langle>Q\<rangle> [a] (f s))"
  show "(\<forall>s \<in> set (get_statespace M). \<forall>a \<in> set (get_alphabet M). \<forall>t \<in> set (get_statespace M). (get_transition M a) s = t \<longleftrightarrow> (get_transition Q a) (f s) = f t)"
  proof (rule ballI, rule ballI, rule ballI)
    fix s a t
    assume defs: "s \<in> set (get_statespace M)" and defa: "a \<in> set (get_alphabet M)" and deft: "t \<in> set (get_statespace M)"
    from fbij have finj: "inj_on f (set (get_statespace M))" by(simp add: bij_betw_imp_inj_on)
    from wdefined defa defs have **: "(\<delta>\<langle>M\<rangle> [a] s) \<in> set (get_statespace M)" by(auto intro: welldefined_dfa_p_imp_delta_in_statespace)
    then show "(get_transition M a s = t) = (get_transition Q a (f s) = f t)" proof -
      from fcommute and defs defa have *: "f (\<delta>\<langle>M\<rangle> [a] s) = \<delta>\<langle>Q\<rangle> [a] (f s)" by(simp)
      \<comment> \<open> We replace get transition by delta of one element for simplicity first \<close>
      then have "\<delta>\<langle>M\<rangle> [a] s = t \<longleftrightarrow> \<delta>\<langle>Q\<rangle> [a] (f s) = f t" proof -
        have "(\<delta>\<langle>Q\<rangle> [a] (f s) = f t) \<longleftrightarrow> f (\<delta>\<langle>M\<rangle> [a] s) = f t" using * by(simp)
        also have "... \<longleftrightarrow> (\<delta>\<langle>M\<rangle> [a] s) = t" using ** finj
          by (meson deft inj_onD)
        finally show ?thesis by(simp)
      qed
      then show ?thesis by (metis append_Nil delta_Cons delta_Nil)
    qed
  qed
qed

subsection \<open> Properties of dfa-Isomorphisms \<close>

text \<open> Having an isomorphism implies trivially that the number of states is equal \<close>
lemma iso_betw_p_imp_eq_num_states:
  "iso_betw_p f M Q \<Longrightarrow> num_states M = num_states Q"
  using bij_betw_same_card iso_betw_p_bijE by blast

text \<open> Most importantly; Isomorphic machines give the same outputs !
       Note that we need the machines(s) to be well defined (isomorphism carries welldefiniteness !) \<close>
lemma iso_betw_p_imp_eq_acceptsP:
  assumes
    iso:  " iso_betw_p f M Q " and
    wdef:  " welldefined_dfa_p M " and
    defw:  " w \<in> (lists (set (get_alphabet M))) "
  shows " output_on M w = output_on Q w "
using assms proof(induction w)
  case Nil
  then show ?case using assms
    by (metis (no_types, lifting) delta_Nil iso_betw_p_finalpE iso_betw_p_initStatesE output_on_def welldefined_dfa_p_initialE)
next
  case (Cons a w)
  then show ?case unfolding output_on_def proof -
    have *: "get_finalf M (\<delta>\<langle>M\<rangle> (a # w) (get_initial M))
           = get_finalf M (\<delta>\<langle>M\<rangle> w (\<delta>\<langle>M\<rangle> [a] (get_initial M)))"
      by(metis delta_Cons_head)
    have **: "get_finalf Q (\<delta>\<langle>Q\<rangle> (a # w) (get_initial Q))
            = get_finalf Q (\<delta>\<langle>Q\<rangle> w (\<delta>\<langle>Q\<rangle> [a] (get_initial Q)))"
      by(metis delta_Cons_head)
    have "get_finalf M (\<delta>\<langle>M\<rangle> w (\<delta>\<langle>M\<rangle> [a] (get_initial M)))
        = get_finalf Q (\<delta>\<langle>Q\<rangle> w (\<delta>\<langle>Q\<rangle> [a] (get_initial Q)))"
    proof -
      from Cons have "a \<in> set (get_alphabet M)" by(simp)
      moreover from Cons have "w \<in> lists (set (get_alphabet M))" by(simp)
      ultimately have "f (\<delta>\<langle>M\<rangle> [a] (get_initial M)) = (\<delta>\<langle>Q\<rangle> [a] (get_initial Q))"
        using Cons.prems iso_betw_p_initStatesE welldefined_dfa_p_initialE
        by fastforce
      then have "(\<delta>\<langle>Q\<rangle> w (f (\<delta>\<langle>M\<rangle> [a] (get_initial M))))
                 = (\<delta>\<langle>Q\<rangle> w (\<delta>\<langle>Q\<rangle> [a] (get_initial Q)))" by simp
      then have "f (\<delta>\<langle>M\<rangle> w (\<delta>\<langle>M\<rangle> [a] (get_initial M)))
                 = (\<delta>\<langle>Q\<rangle> w (\<delta>\<langle>Q\<rangle> [a] (get_initial Q)))"        
        by (metis (no_types, lifting) Cons.prems(3) delta_Cons_head iso
            iso_betw_p_imp_delta_commute iso_betw_p_initStatesE
            wdef welldefined_dfa_p_initialE)
      then show ?thesis using Cons.prems
        by (metis (mono_tags, lifting) delta_Cons_head iso_betw_p_finalpE
            welldefined_dfa_p_deltaE welldefined_dfa_p_initialE)
    qed
    then show "get_finalf M (\<delta>\<langle>M\<rangle> (a # w) (get_initial M))
             = get_finalf Q (\<delta>\<langle>Q\<rangle> (a # w) (get_initial Q))" using * ** by(simp)
  qed
qed

text \<open>Existence of an isomorphism is a transitive property\<close>
lemma iso_betw_p_ex_trans:
  "iso_betw_p f A B \<Longrightarrow> iso_betw_p g B C \<Longrightarrow> iso_betw_p (g \<circ> f) A C" proof(rule iso_betw_pI)
  assume iso1: "iso_betw_p f A B" and iso2: "iso_betw_p g B C"
  from iso1 iso2 show " set (get_alphabet A) = set (get_alphabet C)"
    by (simp add: iso_betw_p_alph_equalE)
  from iso1 iso2 show "bij_betw (g \<circ> f) (set (get_statespace A)) (set (get_statespace C))"
    by (meson bij_betw_trans iso_betw_p_bijE)
  from iso1 iso2 show "(g \<circ> f) (get_initial A) = get_initial C"
    by (simp add: iso_betw_p_initStatesE)
  from iso1 iso2 show "\<And>s t a.
       \<lbrakk>s \<in> set (get_statespace A); t \<in> set (get_statespace A); a \<in> set (get_alphabet A)\<rbrakk>
       \<Longrightarrow> (get_transition A a s = t) = (get_transition C a ((g \<circ> f) s) = (g \<circ> f) t)" proof -
    fix s t a
    assume assms: "s \<in> set (get_statespace A)"
                  "t \<in> set (get_statespace A)"
                  "a \<in> set (get_alphabet A)"
    from iso1 assms have "get_transition A a s = t \<longleftrightarrow> get_transition B a (f s) = (f t)"
      by(rule iso_betw_p_transitionE)
    also from iso2 have "... \<longleftrightarrow> get_transition C a (g (f s)) = (g (f t))"
    proof(rule iso_betw_p_transitionE)
      from assms iso1 show "f s \<in> set (get_statespace B)"
        using bij_betwE iso_betw_p_bijE by blast
      from assms iso1 show "f t \<in> set (get_statespace B)"
        using bij_betwE iso_betw_p_bijE by blast
      from assms iso1 show "a \<in> set (get_alphabet B)"
        using iso_betw_p_alph_equalE by auto
    qed
    finally show "(get_transition A a s = t) = 
                  (get_transition C a ((g \<circ> f) s) = (g \<circ> f) t)" by simp
  qed
  show "\<And>s. s \<in> set (get_statespace A) \<Longrightarrow> get_finalf A s = get_finalf C ((g \<circ> f) s)" proof -
    fix s
    assume defs: "s \<in> set (get_statespace A)"
    from iso1 defs have "get_finalf A s = get_finalf B (f s)" by auto
    also from iso2 have "... = get_finalf C (g (f s))" proof(rule iso_betw_p_finalpE)
      from defs iso1 show "f s \<in> set (get_statespace B)"
        using bij_betwE iso_betw_p_bijE by blast
    qed
    finally show "get_finalf A s = get_finalf C ((g \<circ> f) s)" by simp
  qed
qed
    
lemma iso_betw_p_accepted_language_eq_p: 
  assumes 
    iso:  "iso_betw_p f M Q" and 
    wdef: "welldefined_dfa_p M"
  shows  "accepted_language_eq_p M \<Sigma> L = accepted_language_eq_p Q \<Sigma> L"
proof -
  from assms iso_betw_p_imp_eq_acceptsP have
    "\<And>w. w \<in> lists (set (get_alphabet M)) \<Longrightarrow> output_on M w = output_on Q w"
    by(auto)
  then show ?thesis unfolding accepted_language_eq_p_def
    using iso iso_betw_p_alph_equalE by metis
qed

section \<open> Order relation on DFAs \<close>
text \<open> With number of states and isomorphisms, we define 'smaller number of states; up to an isomorphism'
       as an order relation on DFAs \<close>

definition less_states_than :: " ('a,'s,'o) moore_machine \<Rightarrow> ('a,'t,'o) moore_machine \<Rightarrow> bool"
  ("(_) \<sqsubseteq> (_)" [64,64]63) where
  " less_states_than M Q \<equiv> (num_states M < num_states Q) \<or> (\<exists>f. iso_betw_p f M Q)"

lemma less_states_thanI:
  "(num_states Q \<le> num_states M \<Longrightarrow> \<exists>f. iso_betw_p f M Q)
  \<Longrightarrow> M \<sqsubseteq> Q" unfolding less_states_than_def
  using not_less by blast

lemma less_states_thanE[elim]:
  "\<lbrakk>less_states_than M Q; num_states M < num_states Q \<Longrightarrow> P M Q;
   \<And>f. iso_betw_p f M Q \<Longrightarrow> P M Q\<rbrakk> \<Longrightarrow> P M Q"
  by(auto simp add: less_states_than_def)

lemma less_states_than_isoD: "less_states_than M Q \<Longrightarrow> num_states M \<ge> num_states Q
  \<Longrightarrow> \<exists>f. iso_betw_p f M Q" by auto

lemma less_states_than_num_states:
  "less_states_than M Q \<Longrightarrow> num_states M \<le> num_states Q"
  by(auto simp add: iso_betw_p_imp_eq_num_states)

text \<open>Transitivity of the less-states-than relation\<close>
lemma less_states_than_trans[simp]: "A \<sqsubseteq> B \<Longrightarrow> B \<sqsubseteq> C \<Longrightarrow> A \<sqsubseteq> C"
proof(rule less_states_thanI)
  assume *: "A \<sqsubseteq> B" and **: "B \<sqsubseteq> C" and ***: "num_states C \<le> num_states A"
  from * have "num_states A \<le> num_states B" by(rule less_states_than_num_states)
  moreover from ** have "num_states B \<le> num_states C" by(rule less_states_than_num_states)
  ultimately have "num_states A \<le> num_states C" by simp
  from this and *** have "num_states A = num_states C" by auto
  then have "num_states A = num_states B" and "num_states B = num_states C"
    using \<open>num_states A \<le> num_states B\<close> \<open>num_states B \<le> num_states C\<close> by(linarith, linarith)
  then have "\<exists>f. iso_betw_p f A B" and "\<exists>g. iso_betw_p g B C"
    using * ** less_states_than_isoD by auto
  then obtain f g where "iso_betw_p f A B" and "iso_betw_p g B C" by auto
  then have "iso_betw_p (g \<circ> f) A C"
    using iso_betw_p_ex_trans by blast
  then show "\<exists>f. iso_betw_p f A C" by auto
qed

text \<open>Antisymmetry of the less-sattes-than relation\<close>
lemma less_states_than_antisym: "A \<sqsubseteq> B \<Longrightarrow> B \<sqsubseteq> A \<Longrightarrow> \<exists>f. iso_betw_p f A B"
  unfolding less_states_than_def
  using iso_betw_p_imp_eq_num_states by fastforce

section \<open> Minimal acceptors \<close>
text \<open> We have all the tools needed to define the concept of a 'minimal' acceptor; An acceptor
       for a weighted language L with minimal number of states (up to isomorphism).\<close>

definition follows_prop_p :: "('a,'s,'o) moore_machine \<Rightarrow> ('o \<Rightarrow> bool) \<Rightarrow> bool" where
  " follows_prop_p m prop \<equiv> \<forall>s \<in> set (get_statespace m).
      prop (get_finalf m s)"

text \<open> Note: Compared to a dfa, the language L is changed from a set to a weighted language with
       type @{typ "'a list \<Rightarrow> 'o"}.\<close>
text \<open> A slight technical problem is: We would need to universally quantify the type of the 
       Moore machine M in the following definition. Indeed, we want to argue minimality for
       every other acceptor, no matter the statespace. However, this is not easily possible.
       Since the statespace does not really matter as long as it is on an infinite type, we
       use lists of letters of the alphabet (since the alphabet is assumed to be non empty
       for our problem, or everything is trivial).\<close>
definition minimal_acceptor_p ::
  "('a,'s,'o) moore_machine \<Rightarrow> 'a fset \<Rightarrow> ('a list \<Rightarrow> 'o) \<Rightarrow> ('o \<Rightarrow> bool) \<Rightarrow> bool" where
   " minimal_acceptor_p dfa \<Sigma> L L_prop \<equiv>
                             accepted_language_eq_p dfa \<Sigma> L \<and>
                             (welldefined_dfa_p dfa) \<and>
                             (\<forall>M :: ('a,'a list,'o) moore_machine.
                                    accepted_language_eq_p M \<Sigma> L
                                  \<and> welldefined_dfa_p M 
                                  \<and> follows_prop_p M L_prop \<longrightarrow> dfa \<sqsubseteq> M) "

lemma minimal_acceptor_p_languageD[dest]:
  "minimal_acceptor_p dfa \<Sigma> L P \<Longrightarrow> accepted_language_eq_p dfa \<Sigma> L"
  by(simp add: minimal_acceptor_p_def)

lemma minimal_acceptor_p_wdefD[dest]:
  "minimal_acceptor_p dfa \<Sigma> L P \<Longrightarrow> (welldefined_dfa_p dfa) "
  by(simp add: minimal_acceptor_p_def)

lemma minimal_acceptor_p_minD[dest]:
  "minimal_acceptor_p dfa \<Sigma> L P \<Longrightarrow> (\<And>M :: ('a, 'a list, 'o) moore_machine.
   \<lbrakk>accepted_language_eq_p M \<Sigma> L; welldefined_dfa_p M; follows_prop_p M P\<rbrakk>
   \<Longrightarrow> dfa \<sqsubseteq> M)"
  by(simp add: minimal_acceptor_p_def)

lemma minimal_acceptor_pI[intro]:
  "\<lbrakk>accepted_language_eq_p dfa \<Sigma> L;
    welldefined_dfa_p dfa;
    \<And>M :: ('a, 'a list, 'o) moore_machine.
    accepted_language_eq_p M \<Sigma> L \<Longrightarrow> welldefined_dfa_p M \<Longrightarrow> follows_prop_p M P \<Longrightarrow> dfa \<sqsubseteq> M\<rbrakk>
    \<Longrightarrow> minimal_acceptor_p dfa \<Sigma> L P"
  by(simp add: minimal_acceptor_p_def)

text \<open>By transitivity of the less-states-than relation, we have the following useful
      property\<close>
lemma less_states_than_min_acceptor:
fixes M :: "('a,'s,'o) moore_machine"
shows 
  "\<lbrakk>minimal_acceptor_p M \<Sigma> L P; Q \<sqsubseteq> M; accepted_language_eq_p Q \<Sigma> L; welldefined_dfa_p Q\<rbrakk>
  \<Longrightarrow> minimal_acceptor_p Q \<Sigma> L P"
proof -
  assume assms: "minimal_acceptor_p M \<Sigma> L P" "Q \<sqsubseteq> M"
                "accepted_language_eq_p Q \<Sigma> L" "welldefined_dfa_p Q"
  then show "minimal_acceptor_p Q \<Sigma> L P" proof(intro minimal_acceptor_pI)
    show "\<And>Ma :: ('a, 'a list, 'o) moore_machine.
                \<lbrakk>minimal_acceptor_p M \<Sigma> L P; Q \<sqsubseteq> M; accepted_language_eq_p Q \<Sigma> L;
                welldefined_dfa_p Q; accepted_language_eq_p Ma \<Sigma> L;
                welldefined_dfa_p Ma; follows_prop_p Ma P\<rbrakk>
          \<Longrightarrow> Q \<sqsubseteq> Ma"
      using less_states_than_trans by auto
  qed(auto simp add: assms)
qed

section \<open>Product Automaton\<close>
text \<open>When instantiating the Moore-locale, we'll need to be able to justify that there exists
      counterexample of bounded length that can be returned by the conjecture oracle.
      To do so, we will need to construct a product automaton\<close>

definition prod_moore_trans :: "('a,'s1,'o1) moore_machine \<Rightarrow> ('a,'s2,'o2) moore_machine
    \<Rightarrow> ('a \<Rightarrow> ('s1 \<times> 's2) \<Rightarrow> ('s1 \<times> 's2))" where
  " prod_moore_trans m1 m2 a s = 
    (get_transition m1 a (fst s), get_transition m2 a (snd s))"

definition prod_moore_finalf :: "('a,'s1,'o1) moore_machine \<Rightarrow> ('a,'s2,'o2) moore_machine
    \<Rightarrow> (('s1 \<times> 's2) \<Rightarrow> ('o1 \<times> 'o2))" where
  " prod_moore_finalf m1 m2 s = 
    (get_finalf m1 (fst s), get_finalf m2 (snd s))"

text \<open>Builds the product automaton of m1 and m2. Both alphabets have to be equal\<close>
definition prod_moore_machine ::
  "('a,'s1,'o1) moore_machine \<Rightarrow> ('a,'s2,'o2) moore_machine
    \<Rightarrow> ('a,'s1 \<times> 's2, 'o1 \<times> 'o2) moore_machine" where
  " prod_moore_machine m1 m2 = \<lparr>
    get_alphabet = (get_alphabet m1),
    get_statespace = List.product (get_statespace m1) (get_statespace m2),
    get_initial = (get_initial m1, get_initial m2),
    get_transition = prod_moore_trans m1 m2,
    get_finalf = prod_moore_finalf m1 m2\<rparr>"

subsection \<open>Lemmas about the product automaton\<close>

lemma fst_get_initial_prod_moore_machine:
  "fst (get_initial (prod_moore_machine m1 m2)) = get_initial m1"
  by(auto simp add: prod_moore_machine_def)

lemma snd_get_initial_prod_moore_machine:
  "snd (get_initial (prod_moore_machine m1 m2)) = get_initial m2"
  by(auto simp add: prod_moore_machine_def)

lemma delta_prod_moore_machine:
  "\<delta>\<langle>prod_moore_machine m1 m2\<rangle> w s = (\<delta>\<langle>m1\<rangle> w (fst s), \<delta>\<langle>m2\<rangle> w (snd s))"
  unfolding delta_def apply(induction "w" arbitrary: s)
   apply(simp)[1]
  apply(auto simp add: prod_moore_machine_def prod_moore_trans_def)
  done

lemma delta_prod_moore_machine_cong_elim1:
  "\<delta>\<langle>prod_moore_machine m1 m2\<rangle> w1 s = \<delta>\<langle>prod_moore_machine m1 m2\<rangle> w2 s
   \<Longrightarrow> \<delta>\<langle>m1\<rangle> w1 (fst s) = \<delta>\<langle>m1\<rangle> w2 (fst s)"
  by (simp add: delta_prod_moore_machine)

lemma delta_prod_moore_machine_cong_elim2:
  "\<delta>\<langle>prod_moore_machine m1 m2\<rangle> w1 s = \<delta>\<langle>prod_moore_machine m1 m2\<rangle> w2 s
   \<Longrightarrow> \<delta>\<langle>m2\<rangle> w1 (snd s) = \<delta>\<langle>m2\<rangle> w2 (snd s)"
  by (simp add: delta_prod_moore_machine)

lemma welldefined_prod_moore_machine:
  "\<lbrakk>welldefined_dfa_p m1; welldefined_dfa_p m2;
    set (get_alphabet m1) = set (get_alphabet m2)\<rbrakk>
    \<Longrightarrow> welldefined_dfa_p (prod_moore_machine m1 m2)"                    
  by(auto simp add: prod_moore_machine_def prod_moore_trans_def welldefined_dfa_p_def)

lemma num_states_prod_moore_machine[simp]:
  "num_states (prod_moore_machine m1 m2) = num_states m1 * num_states m2"
  by(auto simp add: prod_moore_machine_def)

end      