section \<open> Implementation of Dana Angluin's LStar Learner algorithm \<close>

text \<open> Note: This file more or less follows the sequences of proofs in the original paper until the
       end of the proof of Theorem 1. \<close>

theory LStar_Prerequisite_Lemmas
  imports
    Basic_Defs      (* This file contains helpful definitions etc... *)
    Observation_Table (* Contains datatypes for observation tables *)
    Moore_Machine          (* Minimal implementation of MooreMachines *)
begin

text \<open> The main difference in Angluin's MAT model which allows us to break free from the NP-realm,
       is the fact that the oracle is able to answer "conjecture" queries.
       In this file, we define the necessary subroutines to convert observation tables (formed by
       usage of the "membership" oracle) to conjecture automata;
       We also show the corresponding lemmas. \<close>

section \<open> Converting Tables to Automata \<close>

text \<open> The statespace of the conjecture is the set of all equivalence classes of 
       the approximation of Nerode's congruence represented by the observation table;
       restricted to the set S. \<close>

subsection \<open> Preliminary steps \<close>

text \<open> In what follows we want to make the states of our minimal acceptor. The states are 
       equivalence classes of rows (according to @{const rows_eq_p}).
       We implement a mapping between rows and a set of n representatives of the eq-classes.
       In what follows, "n" will denote the number of equivalence classes of rows.
     \<close>

text \<open> We choose a set of representatives of the n equivalence classes \<close>

text \<open> Note: Here we use the following hack; For simplicity later, we want to ensure that
       [] is chosen as a representative for its equivalence class. To do so, we append it
       at the end of S (is thus appears twice). The nubby function will always keep the last
       element, thus [] will always be chosen as representative.
       This is justified since we'll always assume that the observation tables are wellformed
       which implies that [] is always in the set S \<close>         
definition state_reps_of :: " ('l,'o) observation_table \<Rightarrow> 'l list fset " where 
  " state_reps_of table = nubby (rows_eq_p table) ((get_S table)@[[]])"
                            
lemma state_reps_distinct[simp]: "distinct (state_reps_of table)"
  by (simp add: distinct_nubby state_reps_of_def)

lemma state_reps_rows_eq_p:
  "\<lbrakk>a \<in> set (state_reps_of table); b \<in> set (state_reps_of table);
   rows_eq_p table a b\<rbrakk> \<Longrightarrow> a = b"
  by (metis (full_types) equivp_def equivp_rows_eq_p find_nubby_Some option.sel state_reps_of_def)

lemma state_reps_partition_S:
  "x \<in> set (get_S table) \<Longrightarrow> \<exists>r \<in> set (state_reps_of table). table \<turnstile> r \<equiv> x"
  apply(simp only: state_reps_of_def)
  using equivp_rows_eq_p by (simp add: ex_repr_in_nubby)

text \<open> The previous lemma works for rows SA too if we assume that the table is closed \<close>
lemma state_reps_partition_rows:
  "closedp table \<Longrightarrow> x \<in> set (row_labels table)
   \<Longrightarrow> \<exists>r \<in> set (state_reps_of table). table \<turnstile> r \<equiv> x"
proof -
   assume "closedp table" 
   assume "x \<in> set (row_labels table)"
  (* Why do we need force for this ? *)
  then have "x \<in> set (get_S table) \<union> set (get_SA table)" using set_of_fset_union by force
  then show "?thesis" proof(rule UnE)
    assume "x \<in> set (get_S table)"
    then show ?thesis using state_reps_partition_S by blast
  next
   assume *: "x \<in> set (get_SA table)"
   from \<open>closedp table\<close> and * have **: "\<exists>y \<in> set (get_S table). table \<turnstile> x \<equiv> y"
     by(rule closedpE)
   then obtain y where ydef1: "y \<in> set (get_S table)" and ydef2: "table \<turnstile> x \<equiv> y"
     by(auto)
   from ydef1 have "\<exists>r \<in> set (state_reps_of table). rows_eq_p table r y"
     by(rule state_reps_partition_S)
   then obtain r where rdef1: "r \<in> set (state_reps_of table)"
                   and rdef2: "rows_eq_p table r y" by(auto)
   \<comment> \<open> We can conclude by using the fact that equivp is an equiv. rel\<close>
   have "equivp (rows_eq_p table)" by(rule equivp_rows_eq_p)
   then have "rows_eq_p table r x" using ydef2 and rdef2
     by (simp add: equivp_def)
   from this and rdef1 show "\<exists>r\<in>set (state_reps_of table). rows_eq_p table r x"
     by blast
 qed
qed

text \<open> Mapping of rows to representatives \<close>
definition row_to_state :: " ('a,'o) observation_table \<Rightarrow> ('a list, 'a list) map "
  where
  "row_to_state table r = find (rows_eq_p table r) (state_reps_of table) "

lemma row_to_state_range[simp]: "ran (row_to_state table) = set (state_reps_of table)"
proof(rule Set.equalityI)
  show "ran (row_to_state table) \<subseteq> set (state_reps_of table)" proof(rule subsetI)
    fix x
    assume "x \<in> ran (row_to_state table)"
    then have "\<exists>a. (row_to_state table a) = Some x" by (simp add: ran_def)
    then obtain a where "(row_to_state table a) = Some x" by(auto)
    then show "x \<in> set (state_reps_of table)"
      by (metis equivp_rows_eq_p find_None_iff find_nubby_Some option.discI option.sel row_to_state_def state_reps_of_def)
    qed
  show "set (state_reps_of table) \<subseteq> ran (row_to_state table)" proof(rule subsetI)
    fix x                   
    assume *: " x \<in> set (state_reps_of table)"
    show "x \<in> ran (row_to_state table)" proof(rule ranI)
      show "row_to_state table x = Some x"
        unfolding row_to_state_def
        using * by (auto simp add: find_nubby_Some rows_eq_p_def state_reps_of_def)
    qed
  qed
qed

lemma row_to_stateE[elim]: "(row_to_state table x) = Some s
  \<Longrightarrow> s \<in> set (state_reps_of table) \<and> (rows_eq_p table x s)"
proof(rule)
  assume asm: "row_to_state table x = Some s"
  then show "s \<in> set (state_reps_of table)"
    unfolding row_to_state_def
    by (metis (mono_tags, lifting) equivp_rows_eq_p find_None_iff
        find_nubby_Some option.discI option.sel state_reps_of_def)
  from asm show "rows_eq_p table x s"
    unfolding row_to_state_def
    by (metis find_Some_iff)
qed

lemma row_to_stateI: "\<lbrakk>x \<in> set (state_reps_of table); rows_eq_p table r x\<rbrakk>
  \<Longrightarrow> row_to_state table r = Some x"
  apply(simp only: row_to_state_def state_reps_of_def)
  using find_nubby_Some by (metis equivp_rows_eq_p)

lemma row_to_state_cong:
  "table \<turnstile> r \<equiv> q \<Longrightarrow> row_to_state table r = row_to_state table q"
  by (metis equivp_def equivp_rows_eq_p row_to_state_def)

lemma row_to_state_dom:
  "wellformedp table \<Longrightarrow> closedp table
   \<Longrightarrow> set (row_labels table) \<subseteq> dom (row_to_state table)"
proof(rule subsetI)
  fix x
  assume wf: "wellformedp table"
       and closed: "closedp table"
       and defx: "x \<in> set (row_labels table)"
  show "x \<in> dom (row_to_state table)" proof -
    from closed obtain y where "y \<in> (set (get_S table))"
                           and "rows_eq_p table x y"
      using closedp_def
      by (metis UnE defx equivp_reflp equivp_rows_eq_p set_of_fset_union)
    then show ?thesis
      by (meson closed defx domI equivp_rows_eq_p equivp_symp
          row_to_stateI state_reps_partition_rows)
  qed
qed

subsection \<open> Making the states of the acceptor \<close>

text \<open> The state space is a set of equivalence classes of rows according to the @{theory_text rows_eq_p}.
     We represent it by our set of row representatives (one by equivalence class) \<close>
definition make_statespace :: " ('a,'o) observation_table \<Rightarrow> 'a list fset" where
  " make_statespace table = state_reps_of table"

lemma make_statespace_in_S:
  "wellformedp table \<Longrightarrow> set (make_statespace table) \<subseteq> set (get_S table)"
  unfolding make_statespace_def state_reps_of_def
  using nubby_subseteq
  by (metis (no_types, lifting) lambda_in_S rotate1.simps(2)
      set_ConsD set_rotate1 subset_code(1))

(* TODO This lemma is not used, remove ? *)
text \<open> If the table is filled, then for any state and any extension we have a value of T (e@s) \<close>
lemma statespace_conc_E_in_dom_T: 
  assumes filled: " filledp table "
    and   wformed: " wellformedp table " \<comment> \<open> We need this to ensure $\lambda \in S$ \<close>
    and   defs: " s \<in> set (make_statespace table) "
    and   defe: " e \<in> set (get_E table) "
  shows   " (s@e) \<in> dom (get_T table)"
proof -
  from defs have " s \<in> set (nubby (rows_eq_p table) ((get_S table)@[[]]))"
    by(simp only: make_statespace_def state_reps_of_def)
  moreover from wformed have "[] \<in> set (get_S table)" by simp
  ultimately have "s \<in> set (get_S table)" using nubby_subseteq
    using assms(3) make_statespace_in_S wformed by fastforce
  from this and filled and defe show ?thesis using filledp_def set_of_fset_union by auto
qed 

text \<open> The following is directly lifted from @{theory_text state_reps_rows_eq_p}\<close>
lemma make_statespace_rows_eq_p:
  "\<lbrakk>a \<in> set (make_statespace table); b \<in> set (make_statespace table);
    table \<turnstile> a \<equiv> b\<rbrakk> \<Longrightarrow> a = b"
  unfolding make_statespace_def by(rule state_reps_rows_eq_p)

subsection \<open> Initial state. \<close>

text \<open> Conceptually the initial state is the equivalence class of the empty word.
       However, for simplicity later, we "hardcode" it to be the empty word.
       This is possible because under the assumption that the table is wellformed, [] is always
       in the set of all rows ! \<close>

definition make_initial :: " ('a,'o) observation_table \<Rightarrow> 'a list" where
  \<comment> \<open>Normally should be @{text "make_initial table = the (row_to_state table [])"}, this is
      justified by the hack in state-reps-of\<close> 
  " make_initial table = [] "

text \<open> If the table is non empty and S is prefix-closed $\lambda$ is in S,
       thus the initial state is well defined \<close>
lemma make_initial_in_statespace: "make_initial table \<in> set (make_statespace table) "
  by (simp add: make_initial_def nubby_last make_statespace_def state_reps_of_def)

subsection \<open> Accepting states \<close>

text \<open> The conjecture moore machines's final function simply takes the value that we have
       stored in the observation table for the extension $\lambda$\<close>

definition make_finalf :: " ('a,'o) observation_table \<Rightarrow> 'a list \<Rightarrow> 'o " where
  \<comment> \<open> We simply lookup the value of T for extension $\lambda$ \<close>
  " make_finalf table s = the (get_T table s) "

text \<open> All rows of the table can be evaluated by finalp (not only the statespace).
       We have the following way of avoiding the 'the' \<close>
lemma make_finalf_equiv:
  "\<lbrakk>filledp table; wellformedp table; s \<in> set (row_labels table)\<rbrakk>
    \<Longrightarrow> (make_finalf table s = out \<longleftrightarrow> (get_T table s) = Some out)"
  unfolding make_finalf_def proof
  assume "filledp table" "wellformedp table"
    "s \<in> set (row_labels table)" "the (get_T table s) = out"
  then have "the (get_T table s) = out" by(simp)
  then show "get_T table s = Some out"
    by (metis \<open>filledp table\<close> \<open>s \<in> set (row_labels table)\<close> \<open>wellformedp table\<close>
        append_Nil2 filledpE lambda_in_E option.collapse)
next
  assume "filledp table" "wellformedp table" "s \<in> set (row_labels table)" "get_T table s = Some out"
  then show "the (get_T table s) = out" by(simp)
qed

lemma make_finalf_cong:
  "\<lbrakk>r \<in> set (row_labels table); q \<in> set (row_labels table); filledp table;
    wellformedp table; table \<turnstile> r \<equiv> q\<rbrakk> \<Longrightarrow> 
     make_finalf table r = make_finalf table q"
  unfolding make_finalf_def proof -
  assume defr: "r \<in> set (row_labels table)" and defq: "q \<in> set (row_labels table)"
     and filled: "filledp table" and wf: "wellformedp table"
     and cong: "rows_eq_p table r q"
  have "(get_T table r) = (get_T table q)"
  proof(rule rows_eq_pE[of table r q "[]"])
    from cong show "rows_eq_p table r q" .
    show "[] \<in> set (get_E table)" using wf by(rule lambda_in_E)
  qed(auto)
  then show "the (get_T table r) = the (get_T table q)" using defr defq filled
    by simp
qed

subsection \<open> Definition of the transition function \<close>

text \<open> To deal with this -the- we'll need closedness \<close>
text \<open> To show that this is well-defined (with respect to the @{theory_text row_to_state} which projects to the
     representatives of eq-classes) we'll need consistency \<close>
definition make_transition :: " ('a,'o) observation_table \<Rightarrow> 'a \<Rightarrow> 'a list \<Rightarrow> 'a list"
  where
  " make_transition table a st = the (row_to_state table (st@[a]))"

text \<open> The transition function maps to values in the statespace; Note that we don't need the
       input state to already be in the statespace (we use @{theory_text row_to_state}) \<close>
lemma make_transition_in_statespace:
  "\<lbrakk>wellformedp table; closedp table; q \<in> set (get_S table); a \<in> set (get_A table)\<rbrakk>
   \<Longrightarrow> make_transition table a q \<in> set (make_statespace table)"
proof -
  assume wf: "wellformedp table" and
         closed: "closedp table" and
         defq: "q \<in> set (get_S table)" and defa: "a \<in> set (get_A table)"
  \<comment> \<open> We need to justify that we can remove the 'the' \<close>
  \<comment> \<open> q is in the statespace, thus, it's an element of S \<close>
  from defq make_statespace_in_S have "q \<in> set (get_S table)" by(auto)
  \<comment> \<open> Thus, qa is in SA (and thus row-labels) \<close>
  from this and wf have " q@[a] \<in> set (get_SA table)"
    by (simp add: \<open>q \<in> set (get_S table)\<close> defa wellformedpE2)
  then have " q@[a] \<in> set (row_labels table) " using set_of_fset_union by simp
  \<comment> \<open> Since the table is closed, this means that it's in the domain of row-to-state\<close>
  from this wf closed have *: "q@[a] \<in> dom (row_to_state table)" using row_to_state_dom
    by blast
  then show "make_transition table a q \<in> set (make_statespace table)" 
    unfolding make_transition_def make_statespace_def row_to_state_dom * dom_def
    using row_to_stateE by force
qed

text \<open> A trivial corollary is \<close>
lemma make_transition_in_S:
  "\<lbrakk>wellformedp table; closedp table; q \<in> set (get_S table); a \<in> set (get_A table)\<rbrakk> \<Longrightarrow> make_transition table a q \<in> set (get_S table)"
  using make_transition_in_statespace make_statespace_in_S
  by fastforce

text \<open> We show something done implicitely in the paper; The transition function is compatible
       with @{theory_text rows_eq_p} if the table is consistent. \<close>
lemma make_transition_cong_inner:
  assumes "consistentp table" and "r \<in> set (get_S table)"
      and "q \<in> set (get_S table)" and "a \<in> set (get_A table)"
      and "table \<turnstile> r \<equiv> q"
  shows "(make_transition table) a r = (make_transition table) a q"
proof -
  \<comment> \<open> Using consistency we get\<close>
  from assms have "table \<turnstile> (r@[a]) \<equiv> (q@[a])" using consistentpE by(auto)
  \<comment> \<open> Thus, we know r@[a] and q@[a] are mapped to the same state\<close>
  then have "row_to_state table (r@[a]) = row_to_state table (q@[a])"
    using row_to_state_cong by(auto)
  \<comment> \<open> Finally we use the definition of make-transition \<close>
  then show "(make_transition table) a r = (make_transition table) a q"
    by (simp add: make_transition_def)
qed

text \<open> We also have this second congruence \<close>
lemma make_transition_cong_outer:
  assumes wf: "wellformedp table" and closed: "closedp table"
    and defq: "q \<in> set (get_S table)" and defa: "a \<in> set (get_A table)"
  shows "table \<turnstile> ((make_transition table) a q) \<equiv> (q@[a])"
  proof -
  from wf closed row_to_state_dom have *: "set (row_labels table) \<subseteq> dom (row_to_state table)" by(simp)
  from wf have "q@[a] \<in> set (get_SA table)" by (simp add: defa defq wellformedpE2)
  then have **: "q@[a] \<in> set (row_labels table)" using fset_union_intro_r by(simp)
  from * ** and defq defa row_to_state_dom have "\<exists>x. row_to_state table (q@[a]) = Some x" by(auto)
  then obtain x where defx: "row_to_state table (q@[a]) = Some x" by(auto)
  then have "(make_transition table) a q = x" by(simp add: make_transition_def)
  \<comment> \<open> Main step, using lemma @{theory_text row_to_stateE} \<close>
  moreover have "rows_eq_p table (q@[a]) x" using defx row_to_stateE by auto
  \<comment> \<open> We just switch the result around using symmetry of rows-eq-p \<close>
  from this have "rows_eq_p table x (q@[a])" by (simp add: rows_eq_p_def)
  ultimately show ?thesis by (simp add: rows_eq_p_def)
qed

subsection \<open> Making a conjecture \<close>

definition make_conjecture :: " ('a,'o) observation_table \<Rightarrow> ('a, 'a list,'o) moore_machine" where
  " make_conjecture table = \<lparr>
      get_alphabet = (get_A table),
      get_statespace = make_statespace table,
      get_initial = make_initial table,
      get_transition = make_transition table,
      get_finalf = make_finalf table
   \<rparr>"

text \<open> We know the transition functions is well defined; We lift this to the delta function \<close>

text \<open> Similarly to the @{theory_text make_transition_in_statespace}, we don't actually require the input to already
       be in the statespace for the output to belong to it. However, this doesn't work if the word
       passed to the delta function is [] ! \<close>
lemma delta_in_statespace1:
  assumes wf: "wellformedp table" and 
          closed: "closedp table" and
          defq: "q0 \<in> set (get_S table)" and
          defw: "w \<in> lists (set (get_A table))" and
          notnil: "w \<noteq> []"
  shows "\<delta>\<langle>make_conjecture table\<rangle> w q0 \<in> set (make_statespace table)" proof -
  \<comment> \<open> First, we use the make-transition in statespace lemma we want to lift \<close>
  thm make_transition_in_statespace
  from wf and closed have 
    lem: "\<And>a q.  \<lbrakk>q \<in> set (get_S table); a \<in> set (get_A table)\<rbrakk> \<Longrightarrow> 
                make_transition table a q \<in> set (make_statespace table)"
    by (simp add: make_transition_in_statespace)
  \<comment> \<open> Now, we write down the definition of the $\delta$-function \<close>
  (* Note: We need to pipe the def of w in the induction *)
  from defw and notnil have "foldl2 (make_transition table) w q0 \<in> set (make_statespace table)"
    unfolding foldl2_def proof(induction w rule: rev_induct)
    case Nil \<comment> \<open> This case is impossible \<close>
    then show ?case by(simp add: notnil)
  next
    case (snoc x xs)
    then show ?case using lem make_statespace_in_S 
      by (metis (no_types, lifting) append_in_lists_conv defq foldl_Cons foldl_Nil foldl_append listsE local.wf subsetCE)
  qed
  then show "\<delta>\<langle>make_conjecture table\<rangle> w q0 \<in> set (make_statespace table)"
    by (simp add: delta_def make_conjecture_def)
qed

text \<open> We can extend to all words, but require the initial input to be in the statespace. \<close>
lemma delta_in_statespace:
  assumes wf: "wellformedp table" and 
          closed: "closedp table" and
          defq: "q0 \<in> set (make_statespace table)" and
          defw: "w \<in> lists (set (get_A table))"
  shows "\<delta>\<langle>make_conjecture table\<rangle> w q0 \<in> set (make_statespace table)" proof(cases w)
  \<comment> \<open> First, we use the make-transition in statespace lemma we want to lift \<close>
  case Nil
  then show ?thesis by(simp add: defq)
next
  case (Cons a list)
  then show ?thesis using delta_in_statespace1 assms
    by (metis list.discI make_statespace_in_S subsetCE)
qed

text \<open> An alternate form (with weaker assumption and conclusion) is \<close>
lemma delta_in_S:
  assumes wf: "wellformedp table" and 
          closed: "closedp table" and
          defq: "q0 \<in> set (get_S table)" and
          defw: "w \<in> lists (set (get_A table))"
  shows "\<delta>\<langle>make_conjecture table\<rangle> w q0 \<in> set (get_S table)" proof -
  from wf and closed have 
    lem: "\<And>a q.  \<lbrakk>q \<in> set (get_S table); a \<in> set (get_A table)\<rbrakk> \<Longrightarrow> 
                make_transition table a q \<in> set (get_S table)"
    by (simp add: make_transition_in_S)
  \<comment> \<open> Now, we write down the definition of the $\delta$-function \<close>
  from defw have "foldl2 (make_transition table) w q0 \<in> set (get_S table)"
    unfolding foldl2_def proof(induction w rule: rev_induct)
    case Nil
    then show ?case by(simp add: defq)
  next
    case (snoc x xs)
    then show ?case using lem by simp
  qed
  then show "\<delta>\<langle>make_conjecture table\<rangle> w q0 \<in> set (get_S table)"
    by (simp add: delta_def make_conjecture_def)
qed

text \<open> Corollaries of @{theory_text delta_in_statespace} are: \<close>
lemma delta_in_row_labels:
  assumes wf: "wellformedp table" and 
          closed: "closedp table" and
          defq: "q0 \<in> set (make_statespace table)" and
          defw: "w \<in> lists (set (get_A table))"
  shows "\<delta>\<langle>make_conjecture table\<rangle> w q0 \<in> set (row_labels table)"
  using assms delta_in_statespace make_statespace_in_S by(metis fset_union_intro_l subset_eq)

text \<open> Similar to @{theory_text make_transition_cong}, lifted to work with the delta function, but just for
       one letter words \<close>
lemma delta_cong_inner1:
  assumes consistent: "consistentp table" and 
          defr: "r \<in> set (get_S table)" and defq: "q \<in> set (get_S table)" and defa: "a \<in> set (get_A table)"
      and cong: "rows_eq_p table r q"
  shows "\<delta>\<langle>make_conjecture table\<rangle> [a] r = \<delta>\<langle>make_conjecture table\<rangle> [a] q"
proof -
  \<comment> \<open> We use make-transition-cong-inner first \<close>
  thm make_transition_cong_inner
  from assms have *: "make_transition table a r = make_transition table a q" by(auto intro: make_transition_cong_inner)
  show ?thesis unfolding delta_def by(rule foldl2_cong1, auto simp add: * make_conjecture_def)
qed

text \<open> Similar to @{theory_text delta_cong_inner1}, but lifted to n letter words\<close>
lemma delta_cong_inner:
  assumes consistent: "consistentp table" and 
          defr: "r \<in> set (get_S table)" and defq: "q \<in> set (get_S table)" and 
     \<comment> \<open> We ensure a word of length $\geq 1$ (Otherwise the thm is false) \<close>
          defw1: "w \<in> lists (set (get_A table))" and defw2: "w \<noteq> []"
      and cong: "table \<turnstile> r \<equiv> q"
  shows "\<delta>\<langle>make_conjecture table\<rangle> w r = \<delta>\<langle>make_conjecture table\<rangle> w q"
  using defw1 defw2
proof(induction w rule: rev_induct)
  case Nil
  then show ?case by(auto)
next
  case (snoc x xs)
  then show ?case
    by (metis append_in_lists_conv consistent defq defr
        delta_append delta_cong_inner1 listsE local.cong self_append_conv2)
qed

lemma delta_cong_outer:
  assumes wf: "wellformedp table" and
          closed: "closedp table" and
          \<comment> \<open> Note: This somewhat exploits the def of make-conjecture; Normally we should
               require q0 to be in the statespace, but it's also true as is.\<close>
          defq: "q \<in> set (get_S table)" and 
          defa: "a \<in> set (get_A table)"
  shows conc: "table \<turnstile> (\<delta>\<langle>make_conjecture table\<rangle> [a] q) \<equiv> (q@[a])" 
  proof -
  \<comment> \<open> We use make-transition-cong-outer \<close>
  from assms have "table \<turnstile> (make_transition table a q) \<equiv> (q @ [a])" 
    by (simp add: make_transition_cong_outer)
  then show ?thesis
    by (metis delta_Cons delta_Nil moore_machine.select_convs(4)
        make_conjecture_def self_append_conv2)
qed

subsection \<open> Welldefined conjecture \<close>
                          
text \<open> The conjecture automaton we make is well defined ! \<close>
lemma welldefined_dfa_p_make_conjecture:
  assumes wformed: "wellformedp table" and
    filled: "filledp table" and 
    closed: "closedp table" and 
    consistent: "consistentp table"
  shows "welldefined_dfa_p (make_conjecture table)" proof(rule welldefined_dfa_pI)
  let ?M = "make_conjecture table"
  let ?Q = "set (get_statespace ?M)"
  show "get_initial ?M \<in> ?Q" using assms by (simp add: make_conjecture_def make_initial_in_statespace)
  show "\<And>s a. (s \<in> ?Q \<Longrightarrow> a \<in> (set (get_alphabet ?M)) \<Longrightarrow> (get_transition ?M) a s \<in> ?Q)"
    using assms by (metis moore_machine.select_convs(1) moore_machine.select_convs(2) moore_machine.select_convs(4) 
        make_conjecture_def make_statespace_in_S make_transition_in_statespace subsetCE)
qed

section \<open> Main lemmas \<close>

subsection \<open> Proof of theorem 1 \<close>

subsubsection \<open> Lemma 2 \<close>

lemma angluin_lemma2:
  assumes
    filled: " filledp table " and 
    wformed: "wellformedp table" and
    closed: "closedp table" and
    consistent: "consistentp table"
  shows "\<forall>t \<in> set (row_labels table).
         table \<turnstile> (\<delta>\<langle>make_conjecture table\<rangle> t (make_initial table)) \<equiv> t "
proof(rule ballI)
  fix t
  assume deft: "t \<in> set (row_labels table)"
  \<comment> \<open> The proof proceeds by induction over t \<close>
  from this and deft show "rows_eq_p table (\<delta>\<langle>make_conjecture table\<rangle> t (make_initial table)) t"
  proof(induction t rule: rev_induct)
    case Nil
    then show ?case unfolding make_initial_def by (simp add: rows_eq_p_def)
  next
    case (snoc a s)
    have ih: "rows_eq_p table (\<delta>\<langle>make_conjecture table\<rangle> s (make_initial table)) s"
      by (meson fset_union_intro_l snoc.IH snoc.prems(1) wellformedpE(1) wformed)
    \<comment> \<open> We obtain a few useful facts. \<close>
    have "(s@[a] \<in> set (row_labels table))" using snoc.prems(1) by auto
    from this and wformed have *: "s \<in> set (get_S table)" and **: "a \<in> set (get_A table)" 
      by(auto simp add: wellformedpE)
    \<comment> \<open> The delta function stays in the statespace \<close>
    have ***: "(\<delta>\<langle>make_conjecture table\<rangle> s (make_initial table)) \<in> set (make_statespace table)"
      proof(rule delta_in_statespace)
        show "wellformedp table" using wformed .
        show "closedp table" using closed .
        show "make_initial table \<in> set (make_statespace table)"
          using closed filled make_initial_in_statespace wformed by blast
        show "s \<in> lists (set (get_A table))" using * wformed wellformedp_alphabetE
          by (metis fset_union_intro_l)
    qed
    show ?case proof -
      have "(\<delta>\<langle>make_conjecture table\<rangle> (s @ [a]) (make_initial table)) =
             \<delta>\<langle>make_conjecture table\<rangle> [a] (\<delta>\<langle>make_conjecture table\<rangle> s (make_initial table))"
        by(auto)
      also have "... = \<delta>\<langle>make_conjecture table\<rangle> [a] s"
        using * ** *** consistent make_statespace_in_S ih
        by (simp add: closed delta_cong_inner1 delta_in_S make_initial_def
            wellformedp_alphabetE wformed)
      also have "rows_eq_p table ... (s@[a])"
        by (simp add: delta_cong_outer "*" "**" closed wformed)
      finally show ?thesis .
    qed
  qed
qed

subsubsection \<open>Lemma 3\<close>

paragraph \<open> Notion of compatibility dfa - table \<close>

text \<open> Compatibility means that the moore machine has an output on words words (s@e)
       iif (T s@e) has the same output (s in $S \cup SA$, $e \in E$)
       This notion is called "consistency" in the paper; Here we rename it to avoid confusion with 
       consistency for observation tables.
       We also require both the automaton and the table to be defined on the same alphabet.\<close>
definition compat_with_p :: "('a,'s,'o) moore_machine \<Rightarrow> ('a,'o) observation_table \<Rightarrow> bool " where
  " compat_with_p machine table \<equiv> 
    (\<forall>s \<in> set (row_labels table). \<forall>e \<in> set (get_E table). \<forall>x.
      output_on machine (s@e) = x \<longleftrightarrow> (get_T table) (s@e) = Some x)
     \<and> (set (get_alphabet machine) = set (get_A table))"

lemma compat_with_pI[intro]:
  "\<lbrakk>\<And>s e x. \<lbrakk>s \<in> set (row_labels table); e \<in> set (get_E table)\<rbrakk> \<Longrightarrow>
      output_on machine (s@e) = x \<longleftrightarrow> (get_T table) (s@e) = Some x;
     (set (get_alphabet machine) = set (get_A table))\<rbrakk> \<Longrightarrow>
   compat_with_p machine table" by(simp add: compat_with_p_def)

lemma compat_with_p_same_funcE[elim]:
 "\<lbrakk>compat_with_p dfa table;
    s \<in> set (row_labels table); e \<in> set (get_E table)\<rbrakk> 
   \<Longrightarrow> output_on dfa (s@e) = x \<longleftrightarrow> (get_T table) (s@e) = Some x"
  unfolding compat_with_p_def by(auto)

lemma compat_with_p_same_alphE[elim]:
  "compat_with_p dfa table \<Longrightarrow> (set (get_alphabet dfa) = set (get_A table))"
  unfolding compat_with_p_def by(auto)

lemma compat_with_p_make_conjecture_sameA:
  "compat_with_p dfa table \<Longrightarrow>
   set (get_alphabet (make_conjecture table)) = set (get_alphabet dfa)"
proof -
  assume "compat_with_p dfa table"
  then have "(set (get_alphabet dfa) = set (get_A table))"
    by(simp add: compat_with_p_def)
  moreover have "set (get_alphabet (make_conjecture table)) = set (get_A table)"
    by(auto simp add: make_conjecture_def)
  ultimately show ?thesis by(simp)
qed

paragraph \<open> Proof of lemma 3 \<close>

text \<open> This lemma means that each conjecture we make from a closed and consistent obs table
       is compatible with the table's T function\<close>
lemma angluin_lemma3:
  assumes
    filled: " filledp table " and 
    wformed: "wellformedp table" and
    closed: "closedp table" and
    consistent: "consistentp table"
  shows " compat_with_p (make_conjecture table) table "
unfolding compat_with_p_def output_on_def proof(rule conjI, rule ballI, rule ballI, rule allI)
  fix s e x
  show "\<lbrakk>s \<in> set (row_labels table); e \<in> set (get_E table)\<rbrakk>
       \<Longrightarrow> (get_finalf (make_conjecture table) (\<delta>\<langle>make_conjecture table\<rangle> (s @ e) (get_initial (make_conjecture table))) = x)
            \<longleftrightarrow> (get_T table (s @ e) = Some x)"
  \<comment> \<open> We do induction on the extension e, note this time, we don't do rev induction \<close>
  proof(induction e arbitrary: s)
  case Nil
    \<comment> \<open> If e is $\lambda$, we use lemma2 and get: \<close>
    from filled wformed closed consistent Nil.prems(1)
    have *: "table \<turnstile> (\<delta>\<langle>make_conjecture table\<rangle> (s @ []) (make_initial table)) \<equiv> s" 
      by (simp add: angluin_lemma2)
    \<comment> \<open> We don't case-split like in the paper, since we use the congruence make-finalp-cong \<close>
    then show ?case proof -
      from assms have " (make_finalf table) s = x \<longleftrightarrow> (get_T table) s = Some x"
        using make_finalf_equiv Nil.prems(1) by simp
      moreover from * have "make_finalf table s =
         make_finalf table (\<delta>\<langle>make_conjecture table\<rangle> (s @ []) (make_initial table))"
        using make_finalf_cong delta_in_row_labels
        by (metis Nil.prems(1) append_Nil2 closed filled make_initial_in_statespace
            wellformedp_alphabetE wformed)
      ultimately show ?thesis using * by (simp add: make_conjecture_def)
  qed
  next
    case (Cons a e)
    \<comment> \<open> First, lets obtain that a is in A \<close>
    from \<open>a # e \<in> set (get_E table)\<close> and wformed have defa: "a \<in> set (get_A table)"
      by (meson Cons_in_lists_iff wellformedp_alphabetE2)
    \<comment> \<open> E is suffix closed, thus e is in E \<close>
    from wformed have "suffix_closed (set (get_E table))" using wellformedp_def by auto
    from this and Cons.prems have defe: "e \<in> set (get_E table)" using suffix_closedE by(auto)
    \<comment> \<open> Now, the idea is to "pass" the a to the s on the left and use the IH. However this won't work
         if s is in SA, since s @ [a] won't be in the table rows.
        Thus, we reduce to the case where s is in S by using closedness of the table \<close>
    from closed obtain s1 where s1def1: "s1 \<in> set (get_S table)" and s1def2: "rows_eq_p table s s1"
      using closedp_def by (metis Cons.prems(1) UnE equivp_reflp equivp_rows_eq_p set_of_fset_union)

    \<comment> \<open> Now we "pass" the a to the left ! \<close>
    have equality: "\<delta>\<langle>make_conjecture table\<rangle> (s @ a # e) (make_initial table) = 
                    \<delta>\<langle>make_conjecture table\<rangle> ((s1 @ [a]) @ e) (make_initial table) "
    proof -
      
      \<comment> \<open> We use the delta-append rule \<close>
      have "\<delta>\<langle>make_conjecture table\<rangle> (s @ a # e) (make_initial table) = 
           \<delta>\<langle>make_conjecture table\<rangle> (a # e) (\<delta>\<langle>make_conjecture table\<rangle> s (make_initial table))"
        by(simp)

      \<comment> \<open> Use lemma 2 and the def of s1 \<close>
      also have " ... =  \<delta>\<langle>make_conjecture table\<rangle> (a # e) s1"
      proof -
        have "table \<turnstile> (\<delta>\<langle>make_conjecture table\<rangle> s (make_initial table)) \<equiv> s"
          using angluin_lemma2 Cons.prems(1) closed consistent filled wformed
          by auto
        then have *: "table \<turnstile> (\<delta>\<langle>make_conjecture table\<rangle> s (make_initial table)) \<equiv> s1"
          using s1def2 by (simp add: rows_eq_p_def)
        show "\<delta>\<langle>make_conjecture table\<rangle> (a # e) (\<delta>\<langle>make_conjecture table\<rangle> s (make_initial table)) =
                \<delta>\<langle>make_conjecture table\<rangle> (a # e) s1" proof(intro delta_cong_inner)
          from assms show "consistentp table" by(simp)
          show "\<delta>\<langle>make_conjecture table\<rangle> s (make_initial table) \<in> set (get_S table)"
            using delta_in_S defa
            by (metis Cons.prems(1) closed lambda_in_S make_initial_def
                wellformedp_alphabetE wformed)
          from s1def1 show "s1 \<in> set (get_S table)" .
          from wformed wellformedp_alphabetE2 have "e \<in> lists (set (get_A table))"
            using \<open>e \<in> set (get_E table)\<close> by auto
          from this and defa show "a # e \<in> lists (set (get_A table))" by simp
          show "a # e \<noteq> []" by(simp)
          from * show "rows_eq_p table (\<delta>\<langle>make_conjecture table\<rangle> s (make_initial table)) s1" .
        qed
      qed

     \<comment> \<open> We split again with the delta append rule \<close>
    also have " ... = \<delta>\<langle>make_conjecture table\<rangle> e (\<delta>\<langle>make_conjecture table\<rangle> [a] s1)"
      by(rule delta_Cons_head)

    \<comment> \<open> We use lemma2 again (inversely) \<close>
    also have " ... = \<delta>\<langle>make_conjecture table\<rangle> e (\<delta>\<langle>make_conjecture table\<rangle> (s1@[a]) (make_initial table))"
      (is "\<delta>\<langle>make_conjecture table\<rangle> e ?w = \<delta>\<langle>make_conjecture table\<rangle> e ?q") proof -
      have "table \<turnstile> ?w \<equiv> (s1@[a])"
        using delta_cong_outer Cons.prems(2) closed s1def1 defa wformed by fastforce
      also have "table \<turnstile> ?q \<equiv> (s1@[a])" proof -
        have "s1@[a] \<in> set (row_labels table)" using wformed
          by (meson Cons.prems(2) fset_union_intro_r listsE s1def1 wellformedpE2 defa)
        from this and angluin_lemma2[OF filled wformed closed consistent]
        show ?thesis by blast
      qed
      \<comment> \<open> We use transitivity and symmetry of rows-eq-p \<close>
      ultimately have "table \<turnstile> ?w \<equiv> ?q" 
        by(auto simp add: rows_eq_p_def)
      thm delta_cong_inner
      \<comment> \<open> Here we can't directly use delta-cong-inner since the extension e might be [];
           Instead, we use the fact that both w and q are in the statespace, and related, which
           implies that they are equal. \<close>
      moreover have "?w \<in> set (make_statespace table)"
        by(intro delta_in_statespace1, auto simp add: wformed closed s1def1 defa)
      moreover have "?q \<in> set (make_statespace table)" using assms
        by (simp add: defa delta_in_statespace make_initial_in_statespace s1def1 wellformedp_alphabetE)
      ultimately have "?w = ?q" by(simp add: make_statespace_rows_eq_p)
      then show ?thesis by(simp)
    qed
    
    \<comment> \<open> Use delta-append again \<close>
    also have "... = \<delta>\<langle>make_conjecture table\<rangle> ((s1 @ [a]) @ e) (make_initial table)"
      by (rule delta_append[symmetric])

    finally show ?thesis .
  qed

  \<comment> \<open> Now, we can finally use the IH \<close>
  thm Cons.IH[of "s1 @ [a]"]
  have "(get_finalf (make_conjecture table)) ... = x \<longleftrightarrow> (get_T table ((s1 @ [a]) @ e) = Some x)"
    by (metis Cons.IH defa defe fset_union_intro_r make_conjecture_def moore_machine.select_convs(3) s1def1 wellformedpE2 wformed)
  from this and equality show ?case
    by (metis Cons.prems(2) append.assoc append_Cons make_conjecture_def moore_machine.select_convs(3) rows_eq_p_def s1def2 self_append_conv2)
  qed
qed(auto simp add: make_conjecture_def) (* End of angluin_lemma3 *)

subsubsection \<open> Lemma 4 \<close>

paragraph \<open> Converting states of the other acceptor to rows \<close>

text \<open> We define the notion of a 'row' corresponding to the ficticious row of an obs-table for
       any acceptor; This will be useful for the proof of lemma4 \<close>

text \<open> Given an obs table and a dfa, row(q') is defined as the finite function f from E (in the obs
       table) to bool such that $f(e) = 1$ iff $\delta'(q',e) \in F'$ \<close>
definition to_row_of :: " ('a,'o) observation_table \<Rightarrow> ('a,'s,'o) moore_machine \<Rightarrow> 's \<Rightarrow> ('a list, 'o) map" where
  " to_row_of table dfa q' e = (if (e \<in> set (get_E table))
                               then Some ((get_finalf dfa) (\<delta>\<langle>dfa\<rangle> e q'))
                               \<comment> \<open> The function f is only defined on E\<close>
                               else None)"

lemma to_row_of_iff: "e \<in> set (get_E table) \<Longrightarrow> to_row_of table dfa q' e = Some ((get_finalf dfa) (\<delta>\<langle>dfa\<rangle> e q'))"
  by(auto simp add: to_row_of_def)

paragraph \<open> Obtaining rows of the table as functions. \<close>

text \<open> We'll need to say that these obtained rows correspond to rows of the observation table;
       Let's define a function to obtain the rows of an obs-table as finite functions (like they
       are represented in the paper) \<close>

fun get_row_of :: " ('a,'o) observation_table \<Rightarrow> 'a list \<Rightarrow> ('a list, 'o) map" where
  \<comment> \<open> T is already a map, but we enforce that $e \in E$ for simplicity later \<close>
  " get_row_of table s e = (if (e \<in> set (get_E table))
                           then (get_T table) (s@e)
                           else None)"

text \<open> Defining the @{const get_row_of} function to be 'none' if e isn't in e has the following consequence,
     and avoids problems later !\<close>
lemma get_row_of_restrict_setE: " get_row_of table s = get_row_of table s |` set (get_E table)"
  by(auto)

lemma get_row_of_imp_rows_eq_p: " get_row_of table s = get_row_of table t \<Longrightarrow> table \<turnstile> s \<equiv> t" 
  unfolding get_row_of.simps by (meson rows_eq_p_def)

lemma rows_eq_p_imp_get_row_of: "table \<turnstile> s \<equiv> t \<Longrightarrow> get_row_of table s = get_row_of table t" 
  unfolding get_row_of.simps by (meson rows_eq_p_def)

lemma get_row_of_cong_app: "\<lbrakk>e1 \<in> set (get_E table1); e2 \<in> set (get_E table2); (get_T table1) (s1@e1) = (get_T table2) (s2@e2)\<rbrakk>
  \<Longrightarrow> get_row_of table1 s1 e1 = get_row_of table2 s2 e2"
  by(auto)

lemma get_row_of_cong: "\<lbrakk>set (get_E table1) = set (get_E table2); \<forall>e \<in> set (get_E table1). (get_T table1) (s1@e) = (get_T table2) (s2@e)\<rbrakk>
  \<Longrightarrow> get_row_of table1 s1 = get_row_of table2 s2"
  by(auto)

paragraph \<open> Proof of lemma 4 \<close>


text \<open> In the proof of lemma4 we'll need this helper lemma \<close>
lemma inj_on_card_leq_imp_bij_betw:
  "\<lbrakk>inj_on f A; f ` A \<subseteq> B; card B \<le> card A; finite A; finite B\<rbrakk> \<Longrightarrow> bij_betw f A B"
  by (simp add: bij_betw_def card_image card_inj_on_le card_subset_eq le_antisym)

lemma angluin_lemma4:  
    fixes M' :: "('a,'t,'o) moore_machine"
    assumes
    \<comment> \<open> Assume table is closed and consistent (and wellformed, etc...)\<close>
    filled: " filledp table " and 
    wformed: "wellformedp table" and
    closed: "closedp table" and
    consistent: "consistentp table" and
    \<comment> \<open> Assume the acceptor M from make-conjecture has n states. \<close>
    num_statesM: " num_states (make_conjecture table) = n" and
    \<comment> \<open> If M' is another acceptor consistent with T that has n or less states...\<close>
    defM': " compat_with_p M' table" and
    wdefM': " welldefined_dfa_p M' " and
    num_statesM': " num_states M' \<le> n "
    \<comment> \<open> then M' is isomorphic to M \<close>
    shows " \<exists>f. iso_betw_p f (make_conjecture table) M'"
proof -
  let ?M = "(make_conjecture table)"
  let ?E = "set (get_E table)"
  let ?Q = "set (get_statespace (make_conjecture table))"
  let ?Q' = "set (get_statespace M')"
  let ?q0 = "get_initial (make_conjecture table)"
  let ?q0' = "get_initial M'"

  text \<open> The main thing we'll need is an isomorphism ! Let's begin by defining the bijection between
         the statespaces of the conjecture-acceptor and another acceptor (called M') \<close>

  text \<open> This function $\Phi$ is a bijection from the statespace of M (conjecture-automaton) to the
         statespace of M' \<close>
  define phi :: " ('a,'t,'o) moore_machine \<Rightarrow> 'a list \<Rightarrow> 't" where
    \<comment> \<open> It might be a bit confusing that we're using the state of M as a 'word' to feed to the
         delta function of M', but keep in mind that the states of M are just the labels (words)
         associated with the representatives of the eq-classes of rows-eq-p. \<close>
    " phi M' s = \<delta>\<langle>M'\<rangle> s (get_initial M')" for M' s

  \<comment> \<open> Our first goal is to prove that phi is a bijection between the statespaces of M and M'.
       We begin by showing that it's an injection from Q (statespace of M) to Q' (statespace of M')
       Since we assume |Q'| <= |Q| this implies that it's actually a bijection ! \<close>

  \<comment> \<open> From our assumptions, M' is consistent with T. In particular this means: \<close>
  from defM' have sameA: "set (get_alphabet (make_conjecture table)) = set (get_alphabet M')"
    by(auto simp add: compat_with_p_make_conjecture_sameA)
 
  \<comment> \<open> We also use compatibility of M' to get a relation between the rows (from to-row-of) of M'
       and the 'actual' rows of the table (from get-row-of) \<close>
  \<comment> \<open> This implies (by using delta-append) \<close>
  from defM' have *: 
    "(\<forall>s \<in> set (row_labels table). \<forall>e \<in> set (get_E table). \<forall>x.
       (get_finalf M') (\<delta>\<langle>M'\<rangle> e (\<delta>\<langle>M'\<rangle> s ?q0')) = x \<longleftrightarrow> (get_T table) (s@e) = Some x)"
    unfolding compat_with_p_def output_on_def by simp

  \<comment> \<open> This means that we have the following helpful relation \<close>
  have "(\<forall>s \<in> set (row_labels table). to_row_of table M' (phi M' s) = get_row_of table s)"
    unfolding to_row_of_def get_row_of.simps phi_def proof(rule ballI)
    fix s
    assume defs: "s \<in> set (row_labels table)"
    then show "
       (\<lambda>e. if e \<in> ?E then Some (get_finalf M' (\<delta>\<langle>M'\<rangle> e (\<delta>\<langle>M'\<rangle> s (get_initial M'))))
                       else None) =
       (\<lambda>e. if e \<in> ?E then get_T table (s @ e) else None)"
    proof(intro ext)
      fix e
      \<comment> \<open> Let's start by insuring that e is in E \<close>
      show 
       "(if e \<in> ?E then Some (get_finalf M' (\<delta>\<langle>M'\<rangle> e (\<delta>\<langle>M'\<rangle> s (get_initial M'))))
                    else None) =
        (if e \<in> ?E then get_T table (s @ e) else None)"
        proof(cases "e \<in> ?E")
        \<comment> \<open> The false case is trivial (both sides are None) \<close>
        case False
          then show ?thesis by(auto)
        next
        \<comment> \<open> The true case uses the equality * derived earlier (from M' compatible with T) \<close>
        case True
          then show ?thesis (is "?lhs = ?rhs") 
          proof -
            \<comment> \<open> Let's prepare * for usage \<close>
            from * defs True have **:
               "\<And>x. (get_finalf M') (\<delta>\<langle>M'\<rangle> e (\<delta>\<langle>M'\<rangle> s ?q0')) = x
                     \<longleftrightarrow> (get_T table) (s@e) = Some x"
              by(simp)
            then have ***:
              "Some ((get_finalf M') (\<delta>\<langle>M'\<rangle> e (\<delta>\<langle>M'\<rangle> s ?q0'))) = (get_T table) (s@e)"
              by force
            \<comment> \<open> We'll also need this \<close>
            from filled True defs have notNone:"(get_T table) (s@e) \<noteq> None "
              using filledpE by(auto)
            \<comment> \<open> We can now use these facts and rewrite the lsh \<close>
            let ?x = "(get_finalf M' (\<delta>\<langle>M'\<rangle> e (\<delta>\<langle>M'\<rangle> s (get_initial M'))))"
            from True have "?lhs = Some (get_finalf M' (\<delta>\<langle>M'\<rangle> e (\<delta>\<langle>M'\<rangle> s (get_initial M'))))"
              by(simp)
            also have "... = get_T table (s @ e)" using *** by simp
            also have "... = ?rhs" using True by(simp)
            finally show ?thesis .
        qed
      qed
    qed
  qed (* End of the first "have" of this proof *)
  \<comment> \<open> We reformulate this nice fact as a rule \<close>
  then have to_row_of_get_row_of:
    "\<And>s. s \<in> set (row_labels table) \<Longrightarrow>
       to_row_of table M' (phi M' s) = get_row_of table s" by(simp)

  \<comment> \<open> Now, we show that phi is injective \<close>
  have "inj_on (phi M') ?Q" proof(intro inj_onI)
    fix x y
    assume defx: "x \<in> set (get_statespace (make_conjecture table))"
    and defy: "y \<in> set (get_statespace (make_conjecture table))"
    and phim: "phi M' x = phi M' y"
    \<comment> \<open>apply to-row-of on each side \<close>
    from phim have "to_row_of table M' (phi M' x) = to_row_of table M' (phi M' y)" by simp
    then have "get_row_of table x = get_row_of table y" using to_row_of_get_row_of defx defy
      by (metis make_conjecture_def make_statespace_in_S moore_machine.select_convs(2)
          row_labels_S_I subsetCE wformed)
    then have "table \<turnstile> x \<equiv> y" by (simp add: get_row_of_imp_rows_eq_p)
      \<comment> \<open> This implies x = y by using the fact that x and y are representatives of the eq
           classes of rows-eq-p ! \<close>
    then show "x = y" using defx defy
      by (simp add: make_conjecture_def make_statespace_rows_eq_p)
  qed

  \<comment> \<open> Now, we can show that phi is actually a bijection by using what we know of the cardinalities.
       This is where we need the well-definiteness of M'\<close>
  from num_statesM num_statesM' have "card ?Q' \<le> card ?Q" by auto
  from this and \<open>inj_on (phi M') ?Q\<close> have phi_bij: "bij_betw (phi M') ?Q ?Q'"
  proof(intro inj_on_card_leq_imp_bij_betw)
    have "\<And>x. x \<in> set (get_statespace (make_conjecture table))
          \<Longrightarrow> phi M' x \<in> set (get_statespace M')"
    unfolding phi_def by (metis (no_types, lifting) compat_with_p_same_alphE defM'
        make_conjecture_def make_statespace_in_S moore_machine.select_convs(2)
        row_labels_S_I subsetCE wdefM' welldefined_dfa_p_imp_delta_in_statespace
        welldefined_dfa_p_initialE wellformedp_alphabetE wformed)
    then show "\<lbrakk>num_states M' \<le> num_states (make_conjecture table);
                inj_on (phi M') (set (get_statespace (make_conjecture table)))\<rbrakk>
      \<Longrightarrow> phi M' ` set (get_statespace (make_conjecture table)) \<subseteq> set (get_statespace M')"
      by auto
  qed(auto)
  
  text \<open> We now need to prove the 3 properties required of phi;
         \<^enum> Phi takes q0 to q0'
         \<^enum> Phi respects the trans. functions 
         \<^enum> Phi respects the final functions\<close>

  text \<open> (1) - Phi takes q0 to q0'
         This is simple to prove because of the fact that we hacked make-initial to always return [] \<close>
  have prop1: "(phi M') ?q0 = ?q0'"
    by(auto simp add: phi_def make_conjecture_def make_initial_def)

  text \<open> (2) - Phi respects the trans. function
         Here, we want to prove that phi and the deltas commute \<close>
  have prop2': "(\<forall>s \<in> set (get_statespace ?M). \<forall>a \<in> set (get_alphabet ?M).
               (phi M') (\<delta>\<langle>?M\<rangle> [a] s) = \<delta>\<langle>M'\<rangle> [a] ((phi M') s))"
  proof(rule ballI, rule ballI)
    fix s a
    assume defs: "s \<in> set (get_statespace (make_conjecture table))" and
           defa: "a \<in> set (get_alphabet (make_conjecture table))"

    \<comment> \<open> We first obtain a few facts \<close>
    from defa have "a \<in> set (get_A table)" by (simp add: make_conjecture_def)
    moreover from defs have "s \<in> set (get_S table)" proof -
      from defs have "s \<in> set (make_statespace table)" by (simp add: make_conjecture_def)
      then show ?thesis using wformed make_statespace_in_S by(auto)
    qed
    ultimately have "(s@[a]) \<in> set (get_SA table)"
      using wformed by (simp add: wellformedpE2)

    text \<open> We have to adapt the proof presented in the paper, because of our little equivalence
           classes !
           We apply the inverse of phi to each side of @{theory_text to_row_of_get_row_of} \<close>
    \<comment> \<open> This is $\Phi^{-1}$ \<close>
    let ?phiinv = "Fun.the_inv_into ?Q (phi M')"
    have phiinv_img: "\<And>x. x \<in> ?Q' \<Longrightarrow> ?phiinv x \<in> ?Q"
      using bij_betwE bij_betw_the_inv_into phi_bij by blast
    have to_row_of_get_row_of_phiinv:
      "\<And>s'. s' \<in> ?Q' \<Longrightarrow> to_row_of table M' s' = get_row_of table (?phiinv s')"
    proof -
      fix s'
      assume defs': "s' \<in> ?Q'"
      then show "to_row_of table M' s' = get_row_of table (?phiinv s')" proof -
        from defs' and phi_bij obtain s where defs1: "s \<in> ?Q"
                                          and defs2: "(phi M') s = s'"
          by (metis bij_betw_def imageE)
        then have *: "s = ?phiinv s'"
          using \<open>inj_on (phi M') (set (get_statespace (make_conjecture table)))\<close>
                the_inv_into_f_f by fastforce
        have "to_row_of table M' ((phi M') s) = get_row_of table s"
          using to_row_of_get_row_of
          by (metis defs1 moore_machine.select_convs(2) fset_union_intro_l
              make_conjecture_def make_statespace_in_S subsetCE wformed)
        then show ?thesis using defs2 * by auto
      qed
    qed

    \<comment> \<open> This ressembles the two sided equality presented in the paper; It's essentially what we want
         to show, but wrapped in 'to-row-of'. \<close>
    have "to_row_of table M' ((phi M') (\<delta>\<langle>?M\<rangle> [a] s))
        = to_row_of table M' (\<delta>\<langle>M'\<rangle> [a] (phi M' s))"
      (is "?lhs = ?rhs")
    proof -
      text \<open>The proof proceeds by calculating from the lhs and rhs and reachine the common middle
            term @{text "get_row_of table (\<delta>\<langle>make_conjecture table\<rangle> [a] s)"}\<close>
      \<comment> \<open> From the lhs we can reach...\<close>
      have "\<delta>\<langle>make_conjecture table\<rangle> [a] s \<in> set (row_labels table)"
        by (simp add: \<open>a \<in> set (get_A table)\<close> \<open>s \<in> set (get_S table)\<close>
                      closed delta_in_S wformed)
      then have 1: "?lhs = get_row_of table (\<delta>\<langle>make_conjecture table\<rangle> [a] s)"
         using to_row_of_get_row_of by(auto)
      
      \<comment> \<open> From the rhs we get... (using delta-append) \<close>
      have 2: "?rhs = get_row_of table (s@[a])"
        using to_row_of_get_row_of \<open>s @ [a] \<in> set (get_SA table)\<close> 
        by (metis delta_append phi_def row_labels_SA_I)

      \<comment> \<open> The values obtained in 1 and 2 are equal ! Let's show it. \<close>
        
      have "table \<turnstile> (\<delta>\<langle>make_conjecture table\<rangle> [a] s) \<equiv> (s@[a])"
        by (simp add: \<open>a \<in> set (get_A table)\<close> \<open>s \<in> set (get_S table)\<close>
                      closed delta_cong_outer wformed)
      then have eq12:
        "get_row_of table (\<delta>\<langle>make_conjecture table\<rangle> [a] s) = get_row_of table (s@[a])"
        by (rule rows_eq_p_imp_get_row_of)
      
      \<comment> \<open> Thus, lhs and rhs are equal. \<close>
      from 1 2 and eq12 show ?thesis by(simp)
    qed

    text \<open> We can finally show that phi and delta commute, by removing the wrapping to-row-of
           in the previous equality with the to-row-of-get-row-of-phiinv lemma \<close>
    then have "get_row_of table (?phiinv ((phi M') (\<delta>\<langle>?M\<rangle> [a] s)))
             = get_row_of table (?phiinv (\<delta>\<langle>M'\<rangle> [a] (phi M' s)))"
      using to_row_of_get_row_of_phiinv
      by (metis (no_types, lifting) \<open>a \<in> set (get_A table)\<close> \<open>s \<in> set (get_S table)\<close>
          closed compat_with_p_same_alphE defM' delta_in_S fset_union_intro_l
          lists.Cons lists.Nil phi_def wdefM' welldefined_dfa_p_deltaE
          welldefined_dfa_p_initialE wellformedp_alphabetE wformed)
      then have 
        cong: "table \<turnstile> ?phiinv ((phi M') (\<delta>\<langle>?M\<rangle> [a] s)) \<equiv> ?phiinv (\<delta>\<langle>M'\<rangle> [a] (phi M' s))"
        by (simp add: get_row_of_imp_rows_eq_p)
    have "(?phiinv ((phi M') (\<delta>\<langle>?M\<rangle> [a] s))) = ?phiinv (\<delta>\<langle>M'\<rangle> [a] (phi M' s))"
    proof(intro make_statespace_rows_eq_p)
      from cong show "table \<turnstile> ?phiinv ((phi M') (\<delta>\<langle>?M\<rangle> [a] s)) \<equiv> ?phiinv (\<delta>\<langle>M'\<rangle> [a] (phi M' s))" .
      have "?phiinv (phi M' (\<delta>\<langle>make_conjecture table\<rangle> [a] s)) \<in> ?Q"
        by (metis (no_types, lifting) \<open>a \<in> set (get_A table)\<close> \<open>s \<in> set (get_S table)\<close>
            closed compat_with_p_same_alphE defM' delta_in_S lists.Cons lists.Nil phi_def
            phiinv_img row_labels_S_I wdefM' welldefined_dfa_p_deltaE welldefined_dfa_p_initialE
            wellformedp_alphabetE wformed)
      then show "?phiinv (phi M' (\<delta>\<langle>make_conjecture table\<rangle> [a] s)) \<in> set (make_statespace table)"
        by (simp add: make_conjecture_def)
      have "?phiinv (\<delta>\<langle>M'\<rangle> [a] (phi M' s)) \<in> ?Q" 
        by (metis (no_types, lifting) \<open>a \<in> set (get_A table)\<close> \<open>s \<in> set (get_S table)\<close>
            compat_with_p_same_alphE defM' lists.Cons lists.Nil phi_def phiinv_img
            row_labels_S_I wdefM' welldefined_dfa_p_imp_delta_in_statespace
            welldefined_dfa_p_initialE wellformedp_alphabetE wformed)
      then show "?phiinv (\<delta>\<langle>M'\<rangle> [a] (phi M' s)) \<in> set (make_statespace table)"
        by (simp add: make_conjecture_def)
    qed
   moreover have "inj_on ?phiinv ?Q'"
      by (metis bij_betw_def inj_on_the_inv_into phi_bij)
    moreover have "(phi M') (\<delta>\<langle>?M\<rangle> [a] s) \<in> ?Q'" 
      by (metis \<open>a \<in> set (get_A table)\<close> \<open>s \<in> set (get_S table)\<close> bij_betwE closed
          delta_in_statespace1 list.discI lists.simps make_conjecture_def make_initial_def
          moore_machine.select_convs(2) phi_bij wformed)
    moreover have "\<delta>\<langle>M'\<rangle> [a] (phi M' s) \<in> ?Q'"
      by (metis \<open>a \<in> set (get_A table)\<close> \<open>s \<in> set (get_S table)\<close> moore_machine.select_convs(1)
          fset_union_intro_l lists.Cons lists.Nil make_conjecture_def phi_def sameA wdefM'
          welldefined_dfa_p_imp_delta_in_statespace welldefined_dfa_p_initialE
          wellformedp_alphabetE wformed)
    ultimately show "(phi M') (\<delta>\<langle>?M\<rangle> [a] s) = \<delta>\<langle>M'\<rangle> [a] (phi M' s)"
      by (simp add: inj_on_def)
  qed

  \<comment> \<open> (3) - Phi respects accepting states \<close>
  have prop3: "(\<forall>s \<in> ?Q. (get_finalf ?M) s = (get_finalf M') ((phi M') s))"
  proof(rule ballI)
    fix s
    assume "s \<in> ?Q"
    show "(get_finalf ?M) s = (get_finalf M') ((phi M') s)" proof -
      let ?x = "(get_finalf ?M) s"
      have "(get_T table) s = Some ?x"
        by (metis (no_types, hide_lams) 
            \<open>s \<in> set (get_statespace (make_conjecture table))\<close> filled fset_union_intro_l
            make_conjecture_def make_finalf_equiv make_initial_def make_statespace_in_S 
            moore_machine.select_convs(2) moore_machine.select_convs(5) rev_subsetD wformed)
      then have "(get_T table) (s@[]) = Some ?x" by(simp)
      from this and defM' have "output_on M' (s@[]) = ?x"
        using compat_with_p_same_funcE filled filledpE3 by fastforce
      then show ?thesis unfolding output_on_def by (simp add: phi_def)
    qed
  qed
  
  \<comment> \<open> Before assembling, we have to convert the prop2' to our (equivalent) definition \<close>
  from wdefM' sameA phi_bij prop2' have prop2: 
    "(\<forall>s \<in> set (get_statespace ?M).
      \<forall>a \<in> set (get_alphabet ?M).
      \<forall>t \<in> set (get_statespace ?M).
       (get_transition ?M a) s = t \<longleftrightarrow> (get_transition M' a) ((phi M') s) = (phi M') t)"
    proof(intro delta_commute_imp_delta_respects)
      show "welldefined_dfa_p (make_conjecture table)"
        by(rule welldefined_dfa_p_make_conjecture[OF wformed filled closed consistent])
  qed(auto simp add: sameA)
  from sameA phi_bij prop1 prop2 prop3 show ?thesis unfolding iso_betw_p_def
    by auto
qed (* End of lemma4 *)

subsubsection \<open> Theorem 1 \<close>

lemma angluin_theorem1:
  assumes
    \<comment> \<open> Assume table is closed and consistent (and wellformed, etc...)\<close>
    filled: " filledp table " and 
    wformed: "wellformedp table" and
    closed: "closedp table" and
    consistent: "consistentp table" and
    \<comment> \<open> Assume the acceptor M from make-conjecture has n states. \<close>
    num_statesM: " num_states (make_conjecture table) = n" and
    \<comment> \<open> Let M' be another acceptor consistent with T...\<close>
    defM': " compat_with_p M' table" and
    wdefM': " welldefined_dfa_p M' "
  shows
    \<comment> \<open> Then the conjecture automaton is 'correct' (compatible with the T function) \<close>
    " compat_with_p (make_conjecture table) table" and
    \<comment> \<open> And 'minimal' ! \<close>
    " (num_states M' > n) \<or> (\<exists>f. iso_betw_p f (make_conjecture table) M')"
proof -
  show " compat_with_p (make_conjecture table) table" by(simp add: assms angluin_lemma3)
next
  show " (num_states M' > n) \<or> (\<exists>f. iso_betw_p f (make_conjecture table) M')"    
  using angluin_lemma4 assms not_less by blast
qed
  
end