section \<open> Instantiation of locales used \<close>

text \<open>We instantiate the modelling assumptions used. Since we still need to assume that the
      language is regular, we start from a minimal set of assumptions encoding this.
      Of course, we avoid the notions we defined ourselves (such as minimal-acceptor-p)
      in the SimpleLanguage locale that follows.\<close>

text \<open>We instantiate the locales abstractly, by using as minimal acceptor the acceptor
      constructed by Myhill-Nerode and define an abstract conjecture oracle based on it.
      Therefore, this theory also contains a proof of Myhill-Nerode using the concepts
      we defined (such as minimal-acceptor-p)\<close>

theory Locale_Instantiation
  imports LStar_Runtime
begin

locale SimpleLanguage =
  fixes    
    \<comment> \<open> The alphabet \<close>
        \<Sigma>     :: " 'letter fset "
    \<comment> \<open> The set of words over sigma \<close>
    and \<Sigma>star :: " 'letter list set "
    \<comment> \<open> L is a weighted language we want to learn\<close>
    and L     :: " 'letter list \<Rightarrow> 'mlo "
    \<comment> \<open> A wellformedness prop on the output of L. Can be always true if not needed.
         This is useful for the Mealy type embedding later.\<close>
    and L_prop :: " 'mlo \<Rightarrow> bool"
  assumes 
      def_sigmastar: " \<Sigma>star = List.lists (set \<Sigma>) "
    and L_has_prop: "\<And>w. L_prop (L w)"
    and L_regular: "\<exists>Q. (\<Sigma>star//nerode_rel = set Q \<and> distinct Q)"
begin

section \<open>Definition and Properties of Nerode's relation\<close>
definition nerode_relp :: "'letter list \<Rightarrow> 'letter list \<Rightarrow> bool" where
  "nerode_relp w1 w2 \<equiv> \<forall>e \<in> \<Sigma>star. L (w1@e) = L (w2@e)"

\<comment> \<open> We define the set version, since lemmas are expressed for that form.\<close>
definition nerode_rel :: "'letter list rel" where
  "nerode_rel \<equiv> {(x,y). nerode_relp x y} \<inter> (\<Sigma>star \<times> \<Sigma>star)"

lemma nerode_relI[intro]:
  "\<lbrakk>x \<in> \<Sigma>star; y \<in> \<Sigma>star; \<And>e. e \<in> \<Sigma>star \<Longrightarrow> L (x@e) = L (y@e)\<rbrakk> \<Longrightarrow> (x,y) \<in> nerode_rel"
  using nerode_rel_def
  by (metis Int_Collect SigmaI SimpleLanguage.nerode_relp_def SimpleLanguage_axioms
      case_prodI inf_commute)

lemma nerode_relE[elim?]:
  "\<lbrakk>(x,y) \<in> nerode_rel;
    (\<And>e. \<lbrakk>e \<in> \<Sigma>star; x \<in> \<Sigma>star; y \<in> \<Sigma>star\<rbrakk> \<Longrightarrow> L (x@e) = L (y@e)) \<Longrightarrow> P x y\<rbrakk>
   \<Longrightarrow> P x y"
  using nerode_rel_def SimpleLanguage.nerode_relp_def SimpleLanguage_axioms by fastforce

text \<open>Nerode's relation is an equivalence relation\<close>
lemma equivp_nerode_relp: "equivp nerode_relp"
  apply(rule equivpI)
    apply(rule reflpI, auto simp add: nerode_relp_def)
    apply(rule sympI, auto simp add: nerode_relp_def)
    apply(rule transpI, auto simp add: nerode_relp_def)
  done

lemma append_nerode_relp_cong:
  "a \<in> set \<Sigma> \<Longrightarrow> nerode_relp w1 w2 \<Longrightarrow> nerode_relp (w1@[a]) (w2@[a])"
  using nerode_relp_def def_sigmastar by simp

lemma append_word_nerode_relp_cong:
  "q \<in> \<Sigma>star \<Longrightarrow> nerode_relp w1 w2 \<Longrightarrow> nerode_relp (w1@q) (w2@q)"
proof(induction q rule: rev_induct)
  case Nil
  then show ?case by simp
next
  case (snoc a h)
  then have "a \<in> set \<Sigma>" and "h \<in> \<Sigma>star" using def_sigmastar by auto
  then show ?case
    using append_nerode_relp_cong snoc.IH snoc.prems(2) by fastforce
qed

(* TODO: Cleanup very ugly proof *)
lemma equiv_nerode_rel: "equiv \<Sigma>star nerode_rel"
  apply(rule equivI)
  unfolding nerode_rel_def apply (simp add: equivp_nerode_relp equivp_reflp refl_onI)
  unfolding nerode_rel_def using equivp_nerode_relp equivp_symp sym_def apply fastforce
  unfolding nerode_rel_def using equivp_nerode_relp equivp_transp trans_def
proof -
  have "\<forall>r. trans r \<or> (\<exists>ls lsa lsb. (ls::'letter list, lsb) \<notin> r \<and> (lsa, lsb) \<in> r \<and> (ls, lsa) \<in> r)"
    by (meson transI)
  then obtain lls :: "('letter list \<times> 'letter list) set \<Rightarrow> 'letter list"
         and llsa :: "('letter list \<times> 'letter list) set \<Rightarrow> 'letter list"
         and llsb :: "('letter list \<times> 'letter list) set \<Rightarrow> 'letter list" where
    f1: "\<forall>r. trans r \<or> (lls r, llsa r) \<notin> r \<and> (llsb r, llsa r) \<in> r \<and> (lls r, llsb r) \<in> r"
    by (metis (no_types))
  have "nerode_relp (lls {(x, y). nerode_relp x y})
       (llsb {(x, y). nerode_relp x y}) \<and> 
       nerode_relp (llsb {(x, y). nerode_relp x y}) (llsa {(x, y). nerode_relp x y})
   \<longrightarrow> nerode_relp (lls {(x, y). nerode_relp x y}) (llsa {(x, y). nerode_relp x y})"
    by (meson equivp_nerode_relp equivp_transp)
  then show "trans (Restr {(x, y). nerode_relp x y} \<Sigma>star)"
    using f1 by (metis (full_types) case_prodD case_prodI mem_Collect_eq trans_Restr)
qed

subsection \<open>Right congruence\<close>
lemma append_nerode_rel_cong:
  "a \<in> set \<Sigma> \<Longrightarrow> (w1,w2) \<in> nerode_rel \<Longrightarrow> ((w1@[a]),(w2@[a])) \<in> nerode_rel"
  unfolding nerode_rel_def using append_nerode_relp_cong def_sigmastar
  by auto

lemma append_word_nerode_rel_cong:
  "q \<in> \<Sigma>star \<Longrightarrow> (w1,w2) \<in> nerode_rel \<Longrightarrow> ((w1@q),(w2@q)) \<in> nerode_rel"
  unfolding nerode_rel_def using append_word_nerode_relp_cong def_sigmastar
  by auto

section \<open>Definition of the Myhill-Nerode's minimal acceptor\<close>

subsection \<open>Definition of the statespace\<close>

text \<open>The statespace is the quotient by Nerode's relation\<close>
definition min_statespace :: "'letter list set fset" where
  " min_statespace = (SOME Q. \<Sigma>star//nerode_rel = set Q \<and> distinct Q)"

lemma min_statespaceI[intro]: "x \<in> \<Sigma>star//nerode_rel \<Longrightarrow> x \<in> set min_statespace"
  unfolding min_statespace_def
proof -
  let ?Q = "(SOME Q. \<Sigma>star//nerode_rel = set Q \<and> distinct Q)"
  assume *: "x \<in> \<Sigma>star // nerode_rel"
  from L_regular have "set ?Q = \<Sigma>star//nerode_rel"
    by(metis (mono_tags, lifting) exE_some)
  from this and * show "x \<in> set ?Q" by auto
qed

lemma min_statespaceE[elim]: "x \<in> set min_statespace \<Longrightarrow> x \<in> \<Sigma>star//nerode_rel"
  unfolding min_statespace_def
  by (metis (mono_tags, lifting) L_regular someI_ex)

subsection \<open>Initial state\<close>

text \<open>The initial state is the equivalence class of the empty word\<close>
abbreviation min_initial :: "'letter list set" where
  " min_initial \<equiv> nerode_rel``{[]}"

lemma min_initial_in_statespace: "min_initial \<in> set min_statespace"
  unfolding min_statespace_def
  using def_sigmastar min_statespaceI min_statespace_def quotientI by fastforce

text \<open> Equivalent to the previous lemma\<close>
lemma min_initial_in_quotient: "min_initial \<in> \<Sigma>star//nerode_rel"
  using min_initial_in_statespace min_statespaceE by simp

subsection \<open>Definition of the transition function\<close>

text \<open>We begin by defining a few things and proving a few lemmas\<close>

definition append_in_class :: "'letter \<Rightarrow> 'letter list \<Rightarrow> 'letter list set" where
  " append_in_class a s = nerode_rel``{s@[a]}"

lemma append_cong_eq_class:
  "\<lbrakk>a \<in> set \<Sigma>; (y, z) \<in> nerode_rel\<rbrakk> 
  \<Longrightarrow> nerode_rel `` {y @ [a]} = nerode_rel `` {z @ [a]}" 
  using append_nerode_rel_cong by (meson equiv_class_eq_iff equiv_nerode_rel)

lemma append_word_cong_eq_class:
  "\<lbrakk>q \<in> \<Sigma>star; (y, z) \<in> nerode_rel\<rbrakk> 
  \<Longrightarrow> nerode_rel `` {y @ q} = nerode_rel `` {z @ q}" 
  using append_word_nerode_rel_cong by (meson equiv_class_eq_iff equiv_nerode_rel)

lemma append_in_class_cong:
  "a \<in> set \<Sigma> \<Longrightarrow> congruent nerode_rel (append_in_class a)"
  apply(rule congruentI)
    apply(simp add: append_in_class_def)
    apply(simp add: append_cong_eq_class)
  done

lemma language_cong: "congruent nerode_rel L"
proof(rule congruentI)
  have "[] \<in> \<Sigma>star" by(simp add: def_sigmastar)
  then show "\<And>y z. (y, z) \<in> nerode_rel \<Longrightarrow> L y = L z"
    unfolding nerode_rel_def nerode_relp_def by auto
qed

text \<open>The use of a union is justified, by the following lemmas\<close>
definition min_transition :: "'letter \<Rightarrow> 'letter list set \<Rightarrow> 'letter list set" where
  " min_transition a Q = (\<Union>x \<in> Q. append_in_class a x)"

lemma min_transition_cong[simp]:
  "a \<in> set \<Sigma> \<Longrightarrow> q \<in> \<Sigma>star \<Longrightarrow> min_transition a (nerode_rel``{q}) = nerode_rel``{q@[a]}"
proof -
  assume "a \<in> set \<Sigma>" "q \<in> \<Sigma>star"
  then have "(\<Union>x \<in> (nerode_rel``{q}). append_in_class a x) = append_in_class a q"
    by(rule UN_equiv_class[OF equiv_nerode_rel append_in_class_cong])
  then show ?thesis using append_in_class_def min_transition_def by auto
qed

subsection \<open>Definition of the final function\<close>
abbreviation lang_val :: "'letter list => 'mlo set" where
  "lang_val x \<equiv> {L x}"

text \<open> By congruence of L, on one eq-class there will be only
     one language value, so we can use the-elem. (next lemma)\<close>
definition min_finalf :: "'letter list set \<Rightarrow> 'mlo" where
  "min_finalf Q \<equiv> the_elem (\<Union>x \<in> Q. lang_val x)"

lemma min_finalf_singleton:
  "Q \<in> \<Sigma>star//nerode_rel \<Longrightarrow> x \<in> Q \<Longrightarrow> min_finalf Q = L x"
proof -
  assume defQ: "Q \<in> \<Sigma>star//nerode_rel"
     and defx: "x \<in> Q" 
  then have "L x \<in> (\<Union>x \<in> Q. lang_val x)" by auto
  then have "(lang_val x) \<subseteq> (\<Union>x \<in> Q. lang_val x)" by simp
  moreover have "(\<Union>x \<in> Q. lang_val x) \<subseteq> lang_val x" proof(rule subsetI)
    fix l
    assume "l \<in> (\<Union>x \<in> Q. lang_val x)"
    then obtain x0 where "x0 \<in> Q" and defl: "l \<in> lang_val x0" by blast
    then have "(x0,x) \<in> nerode_rel" using defQ
      by (meson \<open>x \<in> Q\<close> equiv_nerode_rel quotient_eq_iff)
    then have "lang_val x0 = lang_val x" proof(elim nerode_relE)
      show "(\<And>e. \<lbrakk>e \<in> \<Sigma>star; x0 \<in> \<Sigma>star; x \<in> \<Sigma>star\<rbrakk> \<Longrightarrow> L (x0 @ e) = L (x @ e))
        \<Longrightarrow> lang_val x0 = lang_val x" using def_sigmastar
        by (metis \<open>(x0, x) \<in> nerode_rel\<close> append_Nil2 equiv_class_eq_iff equiv_nerode_rel lists.Nil)
    qed
    from this defl show "l \<in> lang_val x" by simp
  qed
  ultimately have "(\<Union>x \<in> Q. lang_val x) = lang_val x" by auto
  then show ?thesis unfolding min_finalf_def by auto
qed

lemma min_finalf_wdef:
  "Q \<in> \<Sigma>star//nerode_rel \<Longrightarrow> \<exists>x \<in> \<Sigma>star. min_finalf Q = L x"
proof -
  assume defQ: "Q \<in> \<Sigma>star//nerode_rel"
  then have "Q \<noteq> {}"
    using equiv_nerode_rel in_quotient_imp_non_empty by blast
  then obtain x where "x \<in> Q" by auto
  then have "x \<in> \<Sigma>star" using defQ
    by (meson equiv_class_eq_iff equiv_nerode_rel quotient_eq_iff)
  from \<open>x \<in> Q\<close> have "min_finalf Q = L x"
    by(rule min_finalf_singleton[OF defQ])
  then show ?thesis using \<open>x \<in> \<Sigma>star\<close> by auto 
qed

section \<open>Myhill-Nerode\<close>

subsection \<open>Helper definitions and lemmas\<close>

lemma exists_repr: "Q \<in> \<Sigma>star//nerode_rel \<Longrightarrow> (\<exists>w \<in> \<Sigma>star. nerode_rel``{w} = Q)"
  using quotientE by blast

text \<open> Chooses a representative of a nerode eq class.\<close>
definition repr_of :: "'letter list set \<Rightarrow> 'letter list" where
  "repr_of Q = (SOME w. w \<in> \<Sigma>star \<and> nerode_rel``{w} = Q)"

lemma repr_ofI[intro]:
  "\<lbrakk>S \<in> \<Sigma>star//nerode_rel; \<And>w. \<lbrakk>w \<in> \<Sigma>star; nerode_rel``{w} = S\<rbrakk> \<Longrightarrow> P w\<rbrakk>
    \<Longrightarrow> P (repr_of S)"
  unfolding repr_of_def by(rule someI2_bex, auto simp add: exists_repr)

text \<open>Direct consequences of the previous lemma.\<close>
lemma class_repr_of[simp]: "Q \<in> \<Sigma>star//nerode_rel \<Longrightarrow> nerode_rel``{repr_of Q} = Q"
  by(rule repr_ofI,auto)

lemma repr_of_in_sigmastar: "Q \<in> \<Sigma>star//nerode_rel \<Longrightarrow> repr_of Q \<in> \<Sigma>star"
  by(rule repr_ofI)

text \<open>An alternative way to express the min-transition-cong lemma\<close>
lemma min_transition_repr_of_cong[simp]:
  "a \<in> set \<Sigma> \<Longrightarrow> Q \<in> \<Sigma>star//nerode_rel
   \<Longrightarrow> min_transition a Q = nerode_rel``{(repr_of Q)@[a]}"
proof -                                              
  assume assms: "a \<in> set \<Sigma>" "Q \<in> \<Sigma>star//nerode_rel"
  then have "Q = nerode_rel``{repr_of Q}" by simp
  then have "min_transition a Q = min_transition a (nerode_rel``{repr_of Q})" by simp
  also have "... = nerode_rel``{(repr_of Q)@[a]}" using assms min_transition_cong 
    using repr_ofI by blast
  finally show ?thesis .
qed

subsection \<open>Minimal acceptor and wellformedness\<close>
definition min_acceptor :: "('letter,'letter list set,'mlo) moore_machine" where
  " min_acceptor =
   \<lparr>get_alphabet = \<Sigma>,
    get_statespace = min_statespace,
    get_initial = min_initial,
    get_transition = min_transition,
    get_finalf = min_finalf\<rparr>"

abbreviation end_state :: "('a,'s,'o) moore_machine \<Rightarrow> 'a list \<Rightarrow> 's" where
  " end_state M w \<equiv> (\<delta>\<langle>M\<rangle> w (get_initial M))"

lemma end_state_imp_cong: 
  assumes langM: "accepted_language_eq_p M \<Sigma> L"
      and defp: "p \<in> \<Sigma>star"
      and defq: "q \<in> \<Sigma>star"
      and ends: "end_state M p = end_state M q"
    shows "(p,q) \<in> nerode_rel"
proof -
  from langM have "(set (get_alphabet M) = set \<Sigma>)"
              and "(\<And>w. w \<in> lists (set (get_alphabet M)) \<Longrightarrow> output_on M w = L w)"
    by(simp add: accepted_language_eq_p_def)+
  then have *: "(\<And>w. w \<in> \<Sigma>star \<Longrightarrow> output_on M w = L w)" using def_sigmastar by auto
  from defp defq show "(p,q) \<in> nerode_rel" proof(rule nerode_relI)
    fix e
    assume defe: "e \<in> \<Sigma>star"
    have "L (p @ e) = output_on M (p@e)" using * def_sigmastar defe defp by auto
    also have "... = (get_finalf M) (\<delta>\<langle>M\<rangle> (p@e) (get_initial M))"
      by(simp only: output_on_def)
    also have "... = (get_finalf M) (\<delta>\<langle>M\<rangle> e (\<delta>\<langle>M\<rangle> p (get_initial M)))" by simp
    also have "... = (get_finalf M) (\<delta>\<langle>M\<rangle> e (\<delta>\<langle>M\<rangle> q (get_initial M)))"
      using ends by simp
    also have "... = (get_finalf M) (\<delta>\<langle>M\<rangle> (q@e) (get_initial M))" by(simp)
    also have "... = output_on M (q@e)" by(simp only: output_on_def)
    also have "... = L (q@e)"  using * def_sigmastar defe defq by auto
    finally show "L (p@e) = L (q@e)" .
  qed
qed

text \<open>min-transition-cong lifted for the delta function\<close>
lemma delta_cong:
  "w \<in> \<Sigma>star \<Longrightarrow> Q \<in> set min_statespace
   \<Longrightarrow> \<delta>\<langle>min_acceptor\<rangle> w Q  = nerode_rel `` {repr_of Q @ w}"
proof -
  assume defw: "w \<in> \<Sigma>star" and "Q \<in> set min_statespace"
  then have defq: "Q \<in> \<Sigma>star//nerode_rel" by auto
  from defw have "\<delta>\<langle>min_acceptor\<rangle> w Q = nerode_rel `` {repr_of Q @ w}"
  proof(induction w rule: rev_induct)
    case Nil
    then show ?case unfolding min_acceptor_def delta_def
      by (simp add: defq)
    next
      case (snoc a wh)
      then have defwh: "wh \<in> \<Sigma>star" and defa: "a \<in> set \<Sigma>"
        using def_sigmastar by auto 
      have "\<delta>\<langle>min_acceptor\<rangle> (wh@[a]) Q = 
             (get_transition min_acceptor) a (\<delta>\<langle>min_acceptor\<rangle> wh Q)"
        by(rule delta_Cons)
      also have "... = min_transition a (\<delta>\<langle>min_acceptor\<rangle> wh Q)" 
         by(simp add: min_acceptor_def)
      also have "... = min_transition a (nerode_rel``{repr_of Q @ wh})" 
        using snoc.IH defwh by simp
      also have "... = nerode_rel``{(repr_of Q @wh)@[a]}" proof(rule min_transition_cong)
        from defa show "a \<in> set \<Sigma>" .
        from defq have "repr_of Q \<in> \<Sigma>star" by auto
        then show "repr_of Q @ wh \<in> \<Sigma>star" using defwh def_sigmastar by simp
      qed
      finally show ?case by simp
    qed
    then show ?thesis unfolding min_acceptor_def by simp
qed

lemma welldefined_min_acceptor: "welldefined_dfa_p min_acceptor"
proof(rule welldefined_dfa_pI)
  show "get_initial min_acceptor \<in> set (get_statespace min_acceptor)"
  unfolding min_acceptor_def using min_statespaceI
          by (simp add: def_sigmastar quotientI)
  have "\<And>s a. \<lbrakk>s \<in> set (get_statespace min_acceptor); a \<in> set (get_alphabet min_acceptor)\<rbrakk>
           \<Longrightarrow> min_transition a s \<in> set (get_statespace min_acceptor)"
  proof -
    fix s a
    assume "s \<in> set (get_statespace min_acceptor)" "a \<in> set (get_alphabet min_acceptor)"
    then have defs: "s \<in> \<Sigma>star//nerode_rel" and
              defa: "a \<in> set \<Sigma>" by(auto simp add: min_acceptor_def)
    from defs defa have *: "min_transition a s = nerode_rel``{repr_of s@[a]}"
      by(simp only: min_transition_repr_of_cong)
    have "nerode_rel``{repr_of s@[a]} \<in> \<Sigma>star//nerode_rel" proof(rule quotientI)
      show "repr_of s @ [a] \<in> \<Sigma>star"
      using repr_of_in_sigmastar[OF defs] defa def_sigmastar by auto
    qed
    then have **: "nerode_rel``{repr_of s@[a]} \<in> set (get_statespace min_acceptor)"
      unfolding min_acceptor_def by auto
    from * ** show "min_transition a s \<in> set (get_statespace min_acceptor)"
      by simp
  qed
  then show "\<And>s a. \<lbrakk>s \<in> set (get_statespace min_acceptor);
                    a \<in> set (get_alphabet min_acceptor)\<rbrakk>
           \<Longrightarrow> get_transition min_acceptor a s \<in> set (get_statespace min_acceptor)"
    unfolding min_acceptor_def by simp
qed

lemma min_finalf_eq_L: "S \<in> \<Sigma>star//nerode_rel \<Longrightarrow> min_finalf S = L (repr_of S)"
  apply(rule repr_ofI)
   apply(simp)
   using min_finalf_singleton by blast

lemma accepted_language_min_acceptor:
  "accepted_language_eq_p min_acceptor \<Sigma> L "
proof(rule accepted_language_eq_pI)
  show "\<And>w. w \<in> lists (set (get_alphabet min_acceptor))
        \<Longrightarrow> output_on min_acceptor w = L w" proof -
    fix w
    assume *: "w \<in> lists (set (get_alphabet min_acceptor))"
    \<comment> \<open>We use welldefiniteness of min-initial first (welldefined-min-acceptor lemma)\<close>
    from welldefined_min_acceptor
    have deltaw_wdef: "\<delta>\<langle>min_acceptor\<rangle> w min_initial \<in> set (get_statespace min_acceptor)"
      proof(rule welldefined_dfa_p_deltaE)
        from min_initial_in_statespace show
          "min_initial \<in> set (get_statespace min_acceptor)"
          by(simp add: min_acceptor_def)
        from * show "w \<in> lists (set (get_alphabet min_acceptor))" .
      qed
    then have deltaw_wdef2: "\<delta>\<langle>min_acceptor\<rangle> w min_initial \<in> \<Sigma>star // nerode_rel"
      using min_acceptor_def by auto

    from * have "w \<in> \<Sigma>star" using min_acceptor_def def_sigmastar
      by simp
    then have "output_on min_acceptor w
       = (get_finalf min_acceptor) (\<delta>\<langle>min_acceptor\<rangle> w (get_initial min_acceptor))"
      by(simp only: output_on_def)
    also have "... = min_finalf (\<delta>\<langle>min_acceptor\<rangle> w (min_initial))"
      by(simp add: min_acceptor_def)
    also have "... = L (repr_of (\<delta>\<langle>min_acceptor\<rangle> w (min_initial)))"
      using deltaw_wdef2 by(intro min_finalf_eq_L)
    also have "... = L w" proof -
      have "(repr_of (\<delta>\<langle>min_acceptor\<rangle> w (min_initial))) = (repr_of (nerode_rel``{w}))" proof -
        from \<open>w \<in> \<Sigma>star\<close> min_initial_in_statespace
        have "(\<delta>\<langle>min_acceptor\<rangle> w (min_initial)) = (nerode_rel``{repr_of min_initial @ w})"
          by(rule delta_cong)                                    
        also have "... = nerode_rel``{w}"
          by (metis Image_singleton_iff \<open>w \<in> \<Sigma>star\<close> append_word_cong_eq_class class_repr_of
              def_sigmastar lists.Nil min_initial_in_quotient nerode_relI self_append_conv2)
        finally show ?thesis by simp
      qed
      then have "L (repr_of (\<delta>\<langle>min_acceptor\<rangle> w (min_initial)))
                 = L (repr_of (nerode_rel``{w}))" by simp
      also have "... = L w" proof(rule repr_ofI)
        show "nerode_rel `` {w} \<in> \<Sigma>star // nerode_rel"
          by (simp add: \<open>w \<in> \<Sigma>star\<close> quotientI)
        show "\<And>wa. \<lbrakk>wa \<in> \<Sigma>star; nerode_rel `` {wa} = nerode_rel `` {w}\<rbrakk> \<Longrightarrow> L wa = L w"
          using nerode_relE
          by (metis \<open>w \<in> \<Sigma>star\<close> equiv_class_self equiv_nerode_rel min_finalf_singleton
              quotientI)
      qed
      finally show ?thesis .
    qed

    finally show "output_on min_acceptor w = L w" .
  qed
qed(simp add: min_acceptor_def)

subsection \<open>Minimality of the constructed acceptor.\<close>

text \<open>To prove the minimality in Myhill-Nerode, we use an isomorphism similar to the one
      from lemma4; however we need to convert the incoming equivalence class (state of the
      minimal acceptor to a representative)\<close>
definition phi :: "('letter,'s,'mlo) moore_machine \<Rightarrow> 'letter list set \<Rightarrow> 's" where
  " phi M q \<equiv> end_state M (repr_of q)"

text \<open>Returns a word ending in state s of M\<close>
definition word_ending_in :: "('letter,'s,'mlo) moore_machine \<Rightarrow> 's \<Rightarrow> 'letter list" where
  "word_ending_in M s = (SOME w. w \<in> \<Sigma>star \<and> end_state M w = s)"

lemma word_ending_inI[intro]:
  "\<lbrakk>s \<in> end_state M ` \<Sigma>star; \<And>w. \<lbrakk>w \<in> \<Sigma>star; end_state M w = s\<rbrakk> \<Longrightarrow> P w\<rbrakk>
    \<Longrightarrow> P (word_ending_in M s)"
  unfolding word_ending_in_def by(rule someI2_bex, auto)

lemma word_ending_in_end_state:
  "accepted_language_eq_p M \<Sigma> L \<Longrightarrow> w \<in> \<Sigma>star 
   \<Longrightarrow> nerode_rel``{word_ending_in M (end_state M w)} = nerode_rel``{w}"
  apply(rule word_ending_inI)
  apply blast
  using end_state_imp_cong by (metis eq_equiv_class_iff equiv_nerode_rel)

text \<open>Note the similarity with the to-row-of function from the proof of lemma4\<close>
definition to_eq_class:: "('letter,'s,'mlo) moore_machine \<Rightarrow> 's \<Rightarrow> 'letter list set" where
  "to_eq_class M s \<equiv> nerode_rel``{word_ending_in M s}"

text \<open> This lemma is used to prove the minimality from Myhill-Nerode,
       with our defined concepts of iso-betw-p, num-states, etc.\<close>
lemma min_acceptor_iso:
  assumes langM: "accepted_language_eq_p M \<Sigma> L"
      and wdefM: "welldefined_dfa_p M"
   and nstatesM: "num_states M \<le> card (set min_statespace)"
 shows "\<exists>f. iso_betw_p f (min_acceptor) M" proof -
  let ?Mm = "min_acceptor"
  let ?Q = "set (get_statespace M)"
  let ?Qm = "set (get_statespace ?Mm)"
  let ?q0 = "get_initial M"
  let ?q0m = "get_initial ?Mm"

  from langM have sameA: "set (get_alphabet ?Mm) = set (get_alphabet M)"
    unfolding min_acceptor_def using accepted_language_eq_p_alphE by auto
  then have alphM: "set (get_alphabet M) = set \<Sigma>" by(auto simp add: min_acceptor_def)

  have phi_inv: "\<And>s. s \<in> ?Qm \<Longrightarrow> to_eq_class M (phi M s) = s"
  proof -
    fix s
    assume "s \<in> ?Qm"
    \<comment> \<open>We obtain a representative of s.\<close>
    then have "s \<in> \<Sigma>star//nerode_rel" unfolding min_acceptor_def by auto
    let ?reps = "repr_of s"
    have "?reps \<in> \<Sigma>star"
      using \<open>s \<in> \<Sigma>star // nerode_rel\<close> by auto
    from \<open>s \<in> \<Sigma>star // nerode_rel\<close> have "\<And>sr. sr \<in> s \<Longrightarrow> (sr,?reps) \<in> nerode_rel"
    proof(rule repr_ofI)
      show "\<And>sr w. \<lbrakk>sr \<in> s; w \<in> \<Sigma>star; nerode_rel `` {w} = s\<rbrakk> \<Longrightarrow> (sr, w) \<in> nerode_rel"
        using equiv_class_eq_iff equiv_nerode_rel by fastforce
    qed
    let ?samestatew = "word_ending_in M (phi M s)"
    show "to_eq_class M (phi M s) = s" proof -
      have ssne: "\<Sigma>star \<noteq> {}" by(auto simp add: def_sigmastar)
      then have *: "?samestatew \<in> \<Sigma>star"
        using \<open>repr_of s \<in> \<Sigma>star\<close> by(auto simp add: phi_def)
      have **: "end_state M ?samestatew = end_state M ?reps"
        using \<open>repr_of s \<in> \<Sigma>star\<close> by(auto simp add: phi_def)
      from * ** \<open>?reps \<in> \<Sigma>star\<close> have "(?samestatew,?reps) \<in> nerode_rel"
        using end_state_imp_cong[OF langM]
        by auto
        (* This does not require ?reps in Sigmastar ?
           by (metis (no_types, lifting) \<open>\<And>sr. sr \<in> s \<Longrightarrow> (sr, SOME w. nerode_rel `` {w} = s) \<in> nerode_rel\<close> \<open>s \<in> \<Sigma>star // nerode_rel\<close> equiv_Eps_in equiv_class_eq_iff equiv_nerode_rel) *)
      then show ?thesis unfolding phi_def to_eq_class_def
        by (metis (no_types, lifting) \<open>\<And>sr. sr \<in> s \<Longrightarrow> (sr, repr_of s) \<in> nerode_rel\<close>
            \<open>s \<in> \<Sigma>star // nerode_rel\<close> equiv_class_eq equiv_class_self equiv_nerode_rel exists_repr)
    qed
  qed

  \<comment> \<open>Injectivity of phi directly follows\<close>
  have phi_inj: "inj_on (phi M) ?Qm" proof(rule inj_on_imageI2)
    show "inj_on ((to_eq_class M) \<circ> (phi M)) ?Qm" proof(rule inj_onI)
      fix x y
      assume asms: "x \<in> ?Qm" "y \<in> ?Qm"
        "((to_eq_class M) \<circ> (phi M)) x = ((to_eq_class M) \<circ> (phi M)) y"
      then show "x = y" using phi_inv by auto
    qed
  qed

  from wdefM have img_phi: "\<And>x. x \<in> ?Qm \<Longrightarrow> phi M x \<in> ?Q" proof -
    fix x
    assume defx: "x \<in> ?Qm"
    from wdefM show "phi M x \<in> set (get_statespace M)"
    unfolding phi_def proof(rule welldefined_dfa_p_deltaE)
      from wdefM show "get_initial M \<in> set (get_statespace M)" by auto
      from defx have "x \<in> \<Sigma>star // nerode_rel" by(auto simp add: min_acceptor_def) 
      then have "repr_of x \<in> \<Sigma>star"
        by auto
      then show "repr_of x \<in> lists (set (get_alphabet M))"
        using def_sigmastar alphM by auto
    qed
  qed
  then have img_phi2:
    "phi M ` set (get_statespace min_acceptor) \<subseteq> set (get_statespace M)"
    by auto

  \<comment> \<open> Now, we can show that phi is actually a bijection by using what we know of the cardinalities.\<close>
  from nstatesM have "num_states M \<le> card ?Qm" by(auto simp add: min_acceptor_def)
  from this and \<open>inj_on (phi M) ?Qm\<close> have phi_bij: "bij_betw (phi M) ?Qm ?Q"
    using img_phi2 by(auto intro: inj_on_card_leq_imp_bij_betw)

  text \<open> Using bijectivity of phi, we show helpful relations. \<close>
  text \<open>The inverse of phi (to-eq-class) is injective too. 
        Normally, We have to restrict to the image of phi, since there might be unreachable 
        states in M.
        However, in this case by the assumption that M has less states than Mm we could build
        the bijection phi, thus all states must be reachable.\<close>
  have to_eq_class_inj: "inj_on (to_eq_class M) ?Q" proof -
    have "inj_on (to_eq_class M) (phi M ` ?Qm)"
    proof(rule inj_on_imageI)
      have "\<And>x. x \<in> ?Qm \<Longrightarrow> ((to_eq_class M) \<circ> (phi M)) x = x" using phi_inv by simp
      then show "inj_on ((to_eq_class M) \<circ> (phi M)) ?Qm"
        by (simp add: inj_on_def)
    qed
    then show ?thesis using phi_bij by (simp add: bij_betw_imp_surj_on)
  qed

  have end_state_cong: "\<And>p q. (p,q) \<in> nerode_rel \<Longrightarrow> end_state M p = end_state M q"
  proof(rule ccontr)
    fix p q
    assume rel: "(p, q) \<in> nerode_rel" 
      and diff: "end_state M p \<noteq> end_state M q"
    from diff have "end_state M p \<in> ?Q" using wdefM
      using alphM def_sigmastar equiv_nerode_rel rel
      by (simp add: equiv_class_eq_iff welldefined_dfa_p_initialE welldefined_dfa_p_deltaE) 
    moreover from diff have "end_state M q \<in> ?Q" using wdefM
      using alphM def_sigmastar equiv_nerode_rel rel
      by (simp add: equiv_class_eq_iff welldefined_dfa_p_initialE welldefined_dfa_p_deltaE) 
    ultimately have "(to_eq_class M) (end_state M p) \<noteq> (to_eq_class M) (end_state M q)"
      using to_eq_class_inj diff by(auto dest: inj_on_contraD)
    moreover have "(to_eq_class M) (end_state M p) = (to_eq_class M) (end_state M q)"
    (is "(to_eq_class M) ?s1 = (to_eq_class M) ?s2") proof -
      have "?s1 \<in> end_state M ` \<Sigma>star" 
        by (meson equiv_class_eq_iff equiv_nerode_rel image_iff rel)
      have "?s2 \<in> end_state M ` \<Sigma>star"
        by (meson equiv_class_eq_iff equiv_nerode_rel image_iff rel)
      have "nerode_rel``{word_ending_in M ?s1}
          = nerode_rel``{p}"
      proof(rule word_ending_inI)
        from \<open>?s1 \<in> end_state M ` \<Sigma>star\<close> show "?s1 \<in> end_state M ` \<Sigma>star" .
        show "\<And>w. \<lbrakk>w \<in> \<Sigma>star; end_state M w = end_state M p\<rbrakk>
          \<Longrightarrow> nerode_rel``{w} = nerode_rel``{p}" proof -
          fix w
          assume "w \<in> \<Sigma>star" "end_state M w = end_state M p"
          then have "(w,p) \<in> nerode_rel"
            by (meson SimpleLanguage.end_state_imp_cong SimpleLanguage_axioms
                equiv_class_eq_iff equiv_nerode_rel langM rel)
          then show "nerode_rel``{w} = nerode_rel``{p}"
            by (meson equiv_class_eq_iff equiv_nerode_rel)
        qed
      qed
      also have "... = nerode_rel``{q}" using rel equiv_nerode_rel
        by (meson equiv_class_eq_iff)
      also have "... = nerode_rel``{word_ending_in M ?s2}"
      proof(rule word_ending_inI)
        from \<open>?s2 \<in> end_state M ` \<Sigma>star\<close> show "?s2 \<in> end_state M ` \<Sigma>star" .
        show "\<And>w. \<lbrakk>w \<in> \<Sigma>star; end_state M w = end_state M q\<rbrakk>
          \<Longrightarrow> nerode_rel``{q} = nerode_rel``{w}" proof -
          fix w
          assume "w \<in> \<Sigma>star" "end_state M w = end_state M q"
          then have "(w,q) \<in> nerode_rel"
            by (meson SimpleLanguage.end_state_imp_cong SimpleLanguage_axioms
                equiv_class_eq_iff equiv_nerode_rel langM rel)
          then have "(q,w) \<in> nerode_rel" using equiv_nerode_rel
            using equiv_class_eq_iff by fastforce
          then show "nerode_rel``{q} = nerode_rel``{w}"
            by (meson equiv_class_eq_iff equiv_nerode_rel)
        qed
      qed
     finally show ?thesis by(simp add: to_eq_class_def)
   qed
    ultimately show "False" by auto
  qed
    
  text \<open> We now need to prove the 3 properties required of phi;
         \<^enum> Phi takes q0 to q0'
         \<^enum> Phi respects the trans. functions 
         \<^enum> Phi respects the final functions\<close>

  text \<open> (1) - Phi takes q0 to q0' \<close>
  have prop1: "(phi M) ?q0m = ?q0" proof -
    let ?repw = "repr_of (get_initial min_acceptor)"
    have "end_state M ?repw = end_state M []"
    proof(rule repr_ofI)
      show "get_initial min_acceptor \<in> \<Sigma>star // nerode_rel"
          unfolding min_acceptor_def using min_initial_in_quotient by auto
      show "\<And>w. \<lbrakk>w \<in> \<Sigma>star; nerode_rel `` {w} = get_initial min_acceptor\<rbrakk>
            \<Longrightarrow> end_state M w = end_state M []"
        unfolding min_acceptor_def
        by (metis delta_Nil end_state_cong eq_equiv_class equiv_nerode_rel
            lists_empty moore_machine.select_convs(3))
      qed
    also have "... = ?q0" by simp
    finally show ?thesis by(simp add: phi_def)
  qed
   
  text \<open> (2) - Phi respects the trans. function
         Here, we want to prove that phi and the deltas commute \<close>
  have prop2': "(\<forall>s \<in> set (get_statespace ?Mm). \<forall>a \<in> set (get_alphabet ?Mm).
               (phi M) (\<delta>\<langle>?Mm\<rangle> [a] s) = \<delta>\<langle>M\<rangle> [a] ((phi M) s))" proof(rule ballI, rule ballI)
    fix s a                                  
    assume "s \<in> set (get_statespace ?Mm)"
       and "a \<in> set (get_alphabet ?Mm)"
    then have defs: "s \<in> \<Sigma>star//nerode_rel"
          and defa: "a \<in> set \<Sigma>" by(auto simp add: min_acceptor_def)
    \<comment> \<open> Desired equality wrapped in to-eq-class\<close>
    then have "(to_eq_class M) ((phi M) (\<delta>\<langle>?Mm\<rangle> [a] s))
             = (to_eq_class M) (\<delta>\<langle>M\<rangle> [a] ((phi M) s))" 
    (is "?lhs = ?rhs") proof -
      \<comment> \<open>We calculate from each side and show they meet in the middle.\<close>
      have "?lhs = \<delta>\<langle>?Mm\<rangle> [a] s" proof(intro phi_inv)
        show "\<delta>\<langle>min_acceptor\<rangle> [a] s \<in> set (get_statespace min_acceptor)" 
          using welldefined_dfa_p_deltaE[OF welldefined_min_acceptor]
                \<open>s \<in> set (get_statespace ?Mm)\<close> \<open>a \<in> set (get_alphabet ?Mm)\<close> by simp
      qed
      also have "... = nerode_rel``{repr_of s @ [a]}" proof(rule delta_cong)
        from defa show "[a] \<in> \<Sigma>star" using def_sigmastar
          using alphM sameA by auto
        from defs show "s \<in> set min_statespace" by auto
      qed 
      finally have one: "?lhs = nerode_rel``{repr_of s @ [a]}" .

      have "?rhs = (to_eq_class M) (\<delta>\<langle>M\<rangle> [a] (\<delta>\<langle>M\<rangle> (repr_of s) (get_initial M)))"
        by(simp only: phi_def)
      also have "... = (to_eq_class M) (\<delta>\<langle>M\<rangle> (repr_of s @ [a]) (get_initial M))"
        by simp
      also have "... = nerode_rel``{word_ending_in M (\<delta>\<langle>M\<rangle> (repr_of s @ [a]) (get_initial M))}"
        by(simp only: to_eq_class_def)
      also have "... = nerode_rel``{repr_of s @ [a]}"
      proof(rule word_ending_in_end_state[OF langM])
        have "repr_of s \<in> \<Sigma>star" using repr_of_in_sigmastar defs by simp
        then show "repr_of s @ [a] \<in> \<Sigma>star" using defa def_sigmastar by simp
      qed
      finally have two: "?rhs = nerode_rel``{repr_of s @ [a]}" .
      
      from one two show ?thesis by simp
    qed

    \<comment> \<open>Similarly to the proof of lemma4, we conclude by using injectiviy of the
        wrapping function; to-eq-class in our case.\<close>
    from to_eq_class_inj this show "((phi M) (\<delta>\<langle>?Mm\<rangle> [a] s)) = (\<delta>\<langle>M\<rangle> [a] ((phi M) s))"
    proof(rule inj_onD)
      show "phi M (\<delta>\<langle>min_acceptor\<rangle> [a] s) \<in> set (get_statespace M)" proof(rule img_phi)
        show "\<delta>\<langle>min_acceptor\<rangle> [a] s \<in> set (get_statespace min_acceptor)"
          using welldefined_dfa_p_deltaE[OF welldefined_min_acceptor]
                \<open>s \<in> set (get_statespace ?Mm)\<close> \<open>a \<in> set (get_alphabet ?Mm)\<close> by simp
      qed
      show "\<delta>\<langle>M\<rangle> [a] (phi M s) \<in> set (get_statespace M)" proof -
        from img_phi have "(phi M s) \<in> ?Q"
          using \<open>s \<in> set (get_statespace min_acceptor)\<close> by auto
        then show ?thesis using defa wdefM by (simp add: alphM welldefined_dfa_p_deltaE)
      qed
    qed
  qed

  text \<open>(3) - Phi respects the output function\<close>
  have prop3: "(\<forall>s \<in> ?Qm. (get_finalf ?Mm) s = (get_finalf M) ((phi M) s))"
  proof(rule ballI)
    fix s
    assume "s \<in> set (get_statespace min_acceptor)"
    then have "s \<in> \<Sigma>star//nerode_rel" by(auto simp add: min_acceptor_def)
    have "(get_finalf M) ((phi M) s) = (get_finalf M) (end_state M (repr_of s))"
      by(simp only: phi_def)
    also have "... = output_on M (repr_of s)" by(simp only: output_on_def)
    thm langM
    also from langM have "... = L (repr_of s)" proof(rule accepted_language_eq_pE)
      show "repr_of s \<in> lists (set (get_alphabet M))"
        using \<open>s \<in> \<Sigma>star // nerode_rel\<close> alphM def_sigmastar repr_of_in_sigmastar by blast
    qed
    also have "... = (get_finalf ?Mm) s"
      using min_finalf_eq_L[OF \<open>s \<in> \<Sigma>star//nerode_rel\<close>] by(simp add: min_acceptor_def)
    finally show "(get_finalf ?Mm) s = (get_finalf M) ((phi M) s)" by simp
  qed
  \<comment> \<open> Before assembling, we have to convert the prop2' to our (equivalent) definition \<close>
  from wdefM sameA phi_bij prop2' have prop2: 
    "(\<forall>s \<in> set (get_statespace ?Mm).
      \<forall>a \<in> set (get_alphabet ?Mm).
      \<forall>t \<in> set (get_statespace ?Mm).
       (get_transition ?Mm a) s = t \<longleftrightarrow> (get_transition M a) ((phi M) s) = (phi M) t)"
    proof(intro delta_commute_imp_delta_respects)
      from welldefined_min_acceptor show "welldefined_dfa_p min_acceptor" .
  qed(auto simp add: sameA)
  from sameA phi_bij prop1 prop2 prop3 show ?thesis unfolding iso_betw_p_def
    by auto
qed

subsection \<open>Assembling lemmas to get main theorem\<close>
text \<open>Myhill-Nerode minimality, with our definition\<close>
lemma min_acceptor_minimal:
  assumes langM: "accepted_language_eq_p M \<Sigma> L"
      and wdefM: "welldefined_dfa_p M"
 shows "(min_acceptor) \<sqsubseteq> M" proof(rule less_states_thanI)
  from langM wdefM have
    "num_states M \<le> card (set min_statespace) \<Longrightarrow> \<exists>f. iso_betw_p f min_acceptor M"
    by(auto simp add: min_acceptor_iso)
  then show
    "num_states M \<le>  num_states min_acceptor\<Longrightarrow> \<exists>f. iso_betw_p f min_acceptor M"
    using min_acceptor_def by auto
qed

text \<open>We have all the pieces of the Myhill-Nerode theorem\<close>
lemma myhill_nerode:
  shows "minimal_acceptor_p min_acceptor \<Sigma> L L_prop"
proof(rule minimal_acceptor_pI)
  from accepted_language_min_acceptor show "accepted_language_eq_p min_acceptor \<Sigma> L" .
  from welldefined_min_acceptor show "welldefined_dfa_p min_acceptor" . 
  show "\<And>M. \<lbrakk>accepted_language_eq_p M \<Sigma> L; welldefined_dfa_p M; follows_prop_p M L_prop\<rbrakk>
      \<Longrightarrow> min_acceptor \<sqsubseteq> M" using min_acceptor_minimal
    by auto
qed

section \<open>Defining Oracles based on the Myhill-Nerode acceptor\<close>
text \<open>Now that we have verified that a minimal acceptor (as per our own
      concepts) exists, we have to do the same for the MAT model.
      We show that it is possible to define valid membership and conjecture oracle.
      Note that we do not guarantee that the oracles are easily computable functions
      (or computable at all).
      The conjecture oracle is the piece of the MAT model solving NP-hardness, so it
      must be hard to have in reality.\<close>

text \<open>The membership oracle is simply sampling the language\<close>
definition mn_oraclem :: "('letter list \<Rightarrow> 'mlo) \<Rightarrow> ('letter, 'mlo) membership_oracle" where
  " mn_oraclem Lang x \<equiv> Lang x"

text \<open>We define a function to return a counterexample for a conjecture M that is not
      correct on the language Lang\<close>
definition cexamplep ::
  "'letter list \<Rightarrow> ('letter list \<Rightarrow> 'mlo) \<Rightarrow> ('letter, 's, 'mlo) moore_machine
    \<Rightarrow> 'letter list \<Rightarrow> bool" where
  " cexamplep A Lang M t \<equiv> t \<in> (lists (set A)) \<and> (output_on M t \<noteq> Lang t)"

lemma cexamplep_listsD[dest]: "cexamplep A Lang M t \<Longrightarrow> t \<in> lists (set A)"
  by(simp add: cexamplep_def)                                     

lemma cexamplep_output_onD[dest]: "cexamplep A Lang M t \<Longrightarrow> output_on M t \<noteq> Lang t"
  by(simp add: cexamplep_def)

lemma cexamplepI[intro]: "\<lbrakk>t \<in> lists (set A); output_on M t \<noteq> Lang t\<rbrakk> \<Longrightarrow> cexamplep A Lang M t"
  by(simp add: cexamplep_def)

lemma end_state_eq_output_on_eq:
  "end_state M t = end_state M u \<Longrightarrow> output_on M t = output_on M u"
  unfolding output_on_def by auto

lemma cexampleI2:
  "\<lbrakk>cexamplep A Lang M t; accepted_language_eq_p M A Lang;
   u \<in> lists (set A); end_state M u = end_state M t\<rbrakk>
  \<Longrightarrow> cexamplep A Lang M u" proof -
  assume assms: "cexamplep A Lang M t" "accepted_language_eq_p M A Lang"
                "u \<in> lists (set A)" "end_state M u = end_state M t"
  show "cexamplep A Lang M u" proof(intro cexamplepI)
    from assms show "u \<in> lists (set A)" by simp
    from assms have "output_on M u = output_on M t" using end_state_eq_output_on_eq by auto
    also have "... \<noteq> Lang t" using assms by auto
    finally have "output_on M u \<noteq> Lang t" by simp
    moreover from assms have "Lang t = Lang u"
      by (metis \<open>output_on M t \<noteq> Lang t\<close> accepted_language_eq_pE
          accepted_language_eq_p_alphE cexamplep_listsD)
    ultimately show "output_on M u \<noteq> Lang u" by simp
  qed
qed

lemma cexample_for_exists:                        
  "\<lbrakk>set (get_alphabet C) = set A; \<not>accepted_language_eq_p C A Lang\<rbrakk>
   \<Longrightarrow> \<exists>t. cexamplep A Lang C t"
  unfolding accepted_language_eq_p_def
  by auto

text \<open>Returns a bounded counterexample given a wrong conjecture for L\<close>
definition short_cexample_for :: 
  "('letter, 's, 'mlo) moore_machine \<Rightarrow> 'letter list" where
  " short_cexample_for M =
    (SOME w. cexamplep \<Sigma> L M w \<and> length w \<le> num_states min_acceptor * num_states M)"

text \<open>A conjecture oracle exists if we have the model at our disposition.
      Although this one is not computable, if we have the minimal acceptor
      we could run a polynomial dfa-equivalence testing algorithm which also
      returns a counterexample.\<close>
definition mn_oraclec ::
  "('letter, 's, 'mlo) conjecture_oracle" where
  "mn_oraclec C = (if (accepted_language_eq_p C \<Sigma> L)
                                 then None
                                 else Some (short_cexample_for C))"

(* TODO: Move these lemmas *)
lemma prefixpE[elim]: "\<lbrakk>prefixp x y; \<And>z. x@z = y \<Longrightarrow> P x y\<rbrakk> \<Longrightarrow> P x y"
  by(auto simp add: prefixp_def)

text \<open>The pigeonhold principle implies that when reading a long word (at least as long as
      the number of states) we must visit a state twice.\<close>
lemma pigeonhold_same_state_visited:
  assumes
       wdefC: "welldefined_dfa_p C"
   and deft: "t \<in> lists (set (get_alphabet C))"
   and lent: "length t \<ge> N"
   and statesC: "num_states C = N"             
  shows "\<exists>x1 x2. prefixp x1 t \<and> prefixp x2 t \<and> x1 \<noteq> x2 \<and> end_state C x1 = end_state C x2"
proof -
  let ?sigstar = "lists (set (get_alphabet C))"
  have "\<lbrakk>\<And>x1 x2. \<lbrakk>x1 \<in> ?sigstar; x2 \<in> ?sigstar; prefixp x1 t; prefixp x2 t; x1 \<noteq> x2\<rbrakk>
        \<Longrightarrow>end_state C x1 \<noteq> end_state C x2\<rbrakk> \<Longrightarrow> False" proof -
    assume *: "\<And>x1 x2. \<lbrakk>x1 \<in> ?sigstar; x2 \<in> ?sigstar; prefixp x1 t; prefixp x2 t; x1 \<noteq> x2\<rbrakk>
        \<Longrightarrow> end_state C x1 \<noteq> end_state C x2"
    let ?prefixest = "{y. prefixp y t}"
    have numprefixes: "card ?prefixest = length t + 1"
      by(simp add: all_prefixes_complete[symmetric] card_all_prefixes)
    have p1: "\<And>x. x \<in> ?prefixest \<Longrightarrow> prefixp x t" by simp
    have p2: "\<And>x. x \<in> ?prefixest \<Longrightarrow> x \<in> ?sigstar" using p1 deft def_sigmastar
      by (metis append_in_lists_conv prefixp_def)
    have **: "\<And>x1 x2. \<lbrakk>x1 \<in> ?prefixest; x2 \<in> ?prefixest; x1 \<noteq> x2\<rbrakk>
          \<Longrightarrow> end_state C x1 \<noteq> end_state C x2"
      using * p1 p2 by blast
    then have "inj_on (end_state C) ?prefixest" by(intro inj_onI, auto)
    then have "card ?prefixest \<le> card (set (get_statespace C))" proof(rule card_inj_on_le)
      show "end_state C ` ?prefixest \<subseteq> set (get_statespace C)" proof(rule subsetI)
        fix y
        assume "y \<in> end_state C ` ?prefixest"
        then obtain x where "x \<in> ?prefixest" and defy: "y = end_state C x" by auto
        then have "x \<in> ?sigstar" using p2 by simp
        then have "end_state C x \<in> set (get_statespace C)" using wdefC
          by (simp add: welldefined_dfa_p_deltaE welldefined_dfa_p_initialE)
        then show "y \<in> set (get_statespace C)" using defy by simp
      qed
    qed(simp)
    then have "card (set (get_statespace C)) > N" using numprefixes lent by linarith
    then show "False" using statesC by simp
  qed
  then show "\<exists>x1 x2. prefixp x1 t \<and> prefixp x2 t \<and> x1 \<noteq> x2 \<and> end_state C x1 = end_state C x2"
    by auto
qed

text \<open>As a consequence of the pigeonhold lemma, if we reach a state by a long word,
      we can reach the same state by a strictly shorter word\<close>
lemma shorten_word_with_end_state:
  assumes
       wdefC: "welldefined_dfa_p C"
   and deft: "t \<in> lists (set (get_alphabet C))"
   and lent: "length t \<ge> N"
   and statesC: "num_states C = N"             
  shows "\<exists>u \<in> lists (set (get_alphabet C)). end_state C u = end_state C t \<and> length u < length t"
proof -
  let ?sigstar = "lists (set (get_alphabet C))"
  \<comment> \<open>From the pigeonhold principle, two states traversed when reading t are the same\<close>
  from pigeonhold_same_state_visited have
    "\<exists>x1 x2. prefixp x1 t \<and> prefixp x2 t \<and> x1 \<noteq> x2 \<and> end_state C x1 = end_state C x2"
    using lent wdefC deft statesC by simp
  then obtain x1 x2 where "prefixp x1 t" and "prefixp x2 t" and "x1 \<noteq> x2"
     and "end_state C x1 = end_state C x2" by auto
  from \<open>prefixp x1 t\<close> obtain y1 where "x1@y1 = t" by auto
  then have "length t = length x1 + length y1" by auto
  from \<open>prefixp x2 t\<close> obtain y2 where "x2@y2 = t" by auto
  then have "length t = length x2 + length y2" by auto
  have "length x1 < length x2 \<or> length x2 < length x1"
    using \<open>\<And>thesis. (\<And>y1. x1 @ y1 = t \<Longrightarrow> thesis) \<Longrightarrow> thesis\<close>
      \<open>x1 \<noteq> x2\<close> \<open>x2 @ y2 = t\<close> by fastforce
  then show ?thesis proof(rule disjE)
    assume "length x1 < length x2"
    let ?u = "x1@y2"
    have "length ?u < length t"           
      by (simp add: \<open>length t = length x2 + length y2\<close> \<open>length x1 < length x2\<close>)
    moreover have "end_state C ?u = end_state C t"
      using \<open>\<delta>\<langle>C\<rangle> x1 (get_initial C) = \<delta>\<langle>C\<rangle> x2 (get_initial C)\<close> \<open>x2 @ y2 = t\<close> by auto
    ultimately show "\<exists>u\<in>lists (set (get_alphabet C)). 
      end_state C u = end_state C t \<and> length u < length t"
      by (metis \<open>x1 @ y1 = t\<close> \<open>x2 @ y2 = t\<close> append_in_lists_conv deft)
  next
    assume "length x2 < length x1"
    let ?u = "x2@y1"
    have "length ?u < length t"           
      by (simp add: \<open>length t = length x1 + length y1\<close> \<open>length x2 < length x1\<close>)
    moreover have "end_state C ?u = end_state C t"
      using \<open>\<delta>\<langle>C\<rangle> x1 (get_initial C) = \<delta>\<langle>C\<rangle> x2 (get_initial C)\<close> \<open>x1 @ y1 = t\<close> by auto
    ultimately show "\<exists>u\<in>lists (set (get_alphabet C)). 
      end_state C u = end_state C t \<and> length u < length t"
      by (metis \<open>x1 @ y1 = t\<close> \<open>x2 @ y2 = t\<close> append_in_lists_conv deft)
  qed
qed

text \<open>A consequence of the previous lemma is that we can reach any states by words shorter
      then the number of states of the automaton.\<close>
lemma short_word_with_end_state:
  assumes
       wdefC: "welldefined_dfa_p C"
   and deft: "t \<in> lists (set (get_alphabet C))"
   and lent: "length t \<ge> N"
   and statesC: "num_states C = N"             
  shows "\<exists>u \<in> lists (set (get_alphabet C)). end_state C u = end_state C t \<and> length u < N"
proof -
  from deft lent show ?thesis proof(induction "t" rule: length_induct)
    case (1 t)
    from shorten_word_with_end_state have
      "\<exists>st\<in>lists (set (get_alphabet C)). \<delta>\<langle>C\<rangle> st (get_initial C) = \<delta>\<langle>C\<rangle> t (get_initial C)
                                         \<and> length st < length t"
      using "1.prems" statesC wdefC by blast
    then obtain st where "st\<in>lists (set (get_alphabet C))"
                     and "\<delta>\<langle>C\<rangle> st (get_initial C) = \<delta>\<langle>C\<rangle> t (get_initial C)"
                     and "length st < length t" by auto
    then show ?case using 1 by (metis not_less)
  qed
qed

text \<open>We can provide a bound on the size of possible counterexamples by building
      the product automaton between C and the minimal acceptor for L and using the
      previous pigeonhold-lemmas.\<close>
lemma short_cexample_for_exists:
  assumes wdef: "welldefined_dfa_p C"
      and alphC: "set (get_alphabet C) = set \<Sigma>"
      and langC: "\<not>accepted_language_eq_p C \<Sigma> L" 
  shows "\<exists>t. cexamplep \<Sigma> L C t \<and> length t \<le> num_states min_acceptor * num_states C"
proof -
  from assms have "\<exists>t. cexamplep \<Sigma> L C t" by(simp add: cexample_for_exists)
  then obtain t where tcexp: "cexamplep \<Sigma> L C t" by auto
  then have deft: "t \<in> lists (set \<Sigma>)" by auto
  then have deft2: "t \<in> \<Sigma>star" by(simp add: def_sigmastar)
  show "\<exists>u. cexamplep \<Sigma> L C u \<and> length u \<le> num_states min_acceptor * num_states C"
  proof(cases "length t \<le> num_states min_acceptor * num_states C")
    case True
    then show ?thesis using tcexp by auto
  next
    case False
    \<comment> \<open>Here we need to build the product automaton to reason about 'both' the minimal
        and C automaton at the same time and shorten a word for both.\<close>
    let ?prodm = "prod_moore_machine min_acceptor C"
    have "\<exists>u \<in> lists (set (get_alphabet ?prodm)). end_state ?prodm u = end_state ?prodm t
                                   \<and> length u < num_states min_acceptor * num_states C"
    proof(rule short_word_with_end_state)
      \<comment> \<open>The product automaton is welldefined since both components are\<close>
      from wdef show "welldefined_dfa_p (prod_moore_machine min_acceptor C)"
        using welldefined_prod_moore_machine
        by (metis accepted_language_eq_p_def accepted_language_min_acceptor
            alphC welldefined_min_acceptor)
      from alphC deft show "t \<in> lists (set (get_alphabet (prod_moore_machine min_acceptor C)))"
        unfolding prod_moore_machine_def by (simp add: min_acceptor_def)
      from False show "num_states min_acceptor * num_states C \<le> length t"
        by simp
      show "num_states (prod_moore_machine min_acceptor C) = num_states min_acceptor * num_states C"
        by simp
    qed
    then obtain u where *: "u \<in> lists (set (get_alphabet ?prodm))" 
                    and **: "end_state ?prodm u = end_state ?prodm t"
                    and ***: "length u < num_states min_acceptor * num_states C"
          by auto
    from * have "u \<in> lists (set \<Sigma>)" unfolding prod_moore_machine_def
      by (simp add: min_acceptor_def)
    \<comment> \<open>Because we used the product automaton we get properties one and two; 
        They justify that we have successfully shortened the counterexample t
        and kept its properties both with respect to L and C\<close>
    from ** have "end_state min_acceptor u = end_state min_acceptor t"
      using delta_prod_moore_machine_cong_elim1
      by (metis fst_get_initial_prod_moore_machine)
    then have one: "L u = L t"
      by (metis (no_types, lifting) \<open>u \<in> lists (set \<Sigma>)\<close> accepted_language_eq_pE
          accepted_language_eq_p_alphE accepted_language_min_acceptor def_sigmastar
          deft2 end_state_eq_output_on_eq)
    from ** have "end_state C u = end_state C t"
      using delta_prod_moore_machine_cong_elim2
      by (metis snd_get_initial_prod_moore_machine)
    then have two: "output_on C u = output_on C t"
      using end_state_eq_output_on_eq by auto
    \<comment> \<open>Thus, the shortened word u is again a valid counterexample\<close>
    from tcexp one two have "output_on C u \<noteq> L u" by auto
    then have "cexamplep \<Sigma> L C u" using \<open>u \<in> lists (set \<Sigma>)\<close> by auto
    from this and *** show ?thesis by auto
  qed
qed

section \<open>Instantiation of the Moore locale\<close>

interpretation test: MAT_model_moore
  "\<Sigma>" (* Interpretation of Sigma *)
  "List.lists (set \<Sigma>)" (* Interpretation of SigmaStar *)
  "L"
  "L_prop"
  "min_acceptor" (* The minimal acceptor *)
  "mn_oraclem L"
  "mn_oraclec"
  \<comment> \<open>The number of states of the min-acceptor squared is a bound on the
      size of the counterexamples from the conjecture oracle.\<close>
  "(num_states min_acceptor) * (num_states min_acceptor)"
proof(unfold_locales)
  show "set (get_alphabet min_acceptor) = set \<Sigma>" by(simp add: min_acceptor_def)
  from myhill_nerode show "minimal_acceptor_p min_acceptor \<Sigma> L L_prop" .
  show "\<And>w. mn_oraclem L w = L w" by(simp add: mn_oraclem_def)
  show "\<And>C. \<lbrakk>welldefined_dfa_p C;
             set (get_alphabet C) = set \<Sigma>;
             follows_prop_p C L_prop\<rbrakk>
       \<Longrightarrow> (mn_oraclec C = None) \<longleftrightarrow> accepted_language_eq_p C \<Sigma> L"
  proof -
    fix C :: "('letter, 'sc, 'mlo) moore_machine"
    assume "welldefined_dfa_p C" "set (get_alphabet C) = set \<Sigma>" "follows_prop_p C L_prop"
    show "(mn_oraclec C = None) \<longleftrightarrow> accepted_language_eq_p C \<Sigma> L"
      by(auto simp add: mn_oraclec_def)
  qed
  show "\<And>conjecture x.
       \<lbrakk>welldefined_dfa_p conjecture; set (get_alphabet conjecture) = set \<Sigma>;
       mn_oraclec conjecture = Some x;
        follows_prop_p conjecture L_prop\<rbrakk>
       \<Longrightarrow> x \<in> lists (set \<Sigma>) \<and> L x \<noteq> output_on conjecture x"
  proof -
    fix C :: "('letter, 'sc, 'mlo) moore_machine"
    fix x
    assume wdef: "welldefined_dfa_p C"
       and alph: "set (get_alphabet C) = set \<Sigma> "
       and some: "mn_oraclec C = Some x"
       and fprop: "follows_prop_p C L_prop"
    from some have "\<not>accepted_language_eq_p C \<Sigma> L" by(auto simp add: mn_oraclec_def)
    then have "x = short_cexample_for C" using some by(auto simp add: mn_oraclec_def)
    moreover from this show "x \<in> lists (set \<Sigma>) \<and> L x \<noteq> output_on C x"
      unfolding short_cexample_for_def
      using short_cexample_for_exists[OF wdef alph \<open>\<not>accepted_language_eq_p C \<Sigma> L\<close>]
      by (metis (mono_tags, lifting) cexamplep_def cexamplep_output_onD def_sigmastar someI_ex)
  qed
  show "\<And>conjecture x. 
       \<lbrakk>welldefined_dfa_p conjecture; set (get_alphabet conjecture) = set \<Sigma>;
        follows_prop_p conjecture L_prop;
        num_states conjecture \<le> num_states min_acceptor;
       mn_oraclec conjecture = Some x\<rbrakk>
        \<Longrightarrow> length x \<le> num_states min_acceptor * num_states min_acceptor"
  proof -
    fix C :: "('letter, 'sc, 'mlo) moore_machine"
    fix x
    assume wdef: "welldefined_dfa_p C"
       and alph: "set (get_alphabet C) = set \<Sigma> "
       and fprop: "follows_prop_p C L_prop"
       and lstates: "num_states C \<le> num_states min_acceptor"
       and some: "mn_oraclec C = Some x"
    from some have "\<not>accepted_language_eq_p C \<Sigma> L" by(auto simp add: mn_oraclec_def)
    then have "x = short_cexample_for C" using some by(auto simp add: mn_oraclec_def)
    moreover from this have "length x \<le> num_states min_acceptor * num_states C"
      unfolding short_cexample_for_def
      using short_cexample_for_exists[OF wdef alph \<open>\<not>accepted_language_eq_p C \<Sigma> L\<close>]
      by (metis (no_types, lifting) someI_ex)
    then show "length x \<le> num_states min_acceptor * num_states min_acceptor" using lstates
      using order_subst1 by fastforce
  qed
  from L_has_prop show "\<And>w. L_prop (L w)" .
qed(simp)

end

end