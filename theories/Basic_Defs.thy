section \<open> Basic Definitions \<close>

text \<open>Basic definitions used in all theories. Most notably, we define the locale corresponding
      to weighted regular languages (languages accepted by Moore machines).\<close>

theory Basic_Defs
imports Main
    HOL.Relation
    Finite_Sets
    Moore_Machine
begin

text \<open> The following locale defines the usual notions of alphabets and regular languages. 
       @{text List.lists} acts as the Kleene star.\<close>

locale WeightedLanguagesMoore =
  fixes    
    \<comment> \<open> The alphabet \<close>
        \<Sigma>     :: " 'letter fset "
    \<comment> \<open> The set of words over sigma \<close>
    and \<Sigma>star :: " 'letter list set "
    \<comment> \<open> L is a weighted language we want to learn\<close>
    and L     :: " 'letter list \<Rightarrow> 'mlo "
    \<comment> \<open> A wellformedness prop on the output of L. Can be always true if not needed.
         This is useful for the Mealy type embedding later.\<close>
    and L_prop :: " 'mlo \<Rightarrow> bool"
    \<comment> \<open> A minimal acceptor for L (L regular)\<close>
    and M\<^sub>L    :: " ('letter, 'mls, 'mlo) moore_machine "
  assumes 
      def_sigmastar: " \<Sigma>star = List.lists (set \<Sigma>) "
    and L_has_prop: "\<And>w. L_prop (L w)"
    and L_regular: " minimal_acceptor_p M\<^sub>L \<Sigma> L L_prop"
    \<comment> \<open> We have to inform @{term M\<^sub>L} of what the alphabet is \<close>
  and acceptor_alphabet: "set (get_alphabet M\<^sub>L) = set \<Sigma>"


subsection \<open> Some facts about regular languages \<close>

subsubsection \<open> Prefixes and Suffixes of words \<close>

definition prefixp :: "'a list \<Rightarrow> 'a list \<Rightarrow> bool " where
  " prefixp xs ys \<longleftrightarrow> (\<exists>zs. ys = xs @ zs)"

definition suffixp :: "'a list \<Rightarrow> 'a list \<Rightarrow> bool " where
  " suffixp xs ys \<longleftrightarrow> (\<exists>zs. ys = zs @ xs)"

lemma prefixp_refl: "prefixp xs xs" unfolding prefixp_def by auto
lemma suffixp_refl: "suffixp xs xs" unfolding suffixp_def by auto

lemma prefixp_trans: "prefixp xs ys \<Longrightarrow> prefixp ys zs \<Longrightarrow> prefixp xs zs"
  using prefixp_def by (metis append.assoc)

lemma suffixp_trans: "suffixp xs ys \<Longrightarrow> suffixp ys zs \<Longrightarrow> suffixp xs zs"
  using suffixp_def by (metis append.assoc)

text \<open> Executable versions of the prefix and suffix predicates\<close>
lemma prefixp_code[code]:
  " prefixp [] ys = True "       (* The empty word is prefix of all words *)
  " prefixp (x#xs) [] = False "  (* ... and the only prefix of itself *)
  " prefixp (x#xs) (y#ys) = (if (length ys < length xs) then False
                      else (x=y) & prefixp xs ys) "
  by(auto simp add: prefixp_def)

lemma suffixp_code[code]:
  " suffixp [] [] = True "
  " suffixp (x#xs) [] = False "
  " suffixp xs (y#ys) = ((xs=(y#ys)) \<or> suffixp xs ys) "
  apply(auto simp add: suffixp_def)
  by (simp add: Cons_eq_append_conv)

lemma prefixp_length: "prefixp y t \<Longrightarrow> length y \<le> length t"
  by (metis le_add1 length_append prefixp_def)

subsubsection \<open> Prefix and Suffix closedness\<close>

text \<open> These definitions are not executable, but we'll only use them for proofs \<close>
definition prefix_closed :: " 'a list set \<Rightarrow> bool" where
  " prefix_closed S = (\<forall>x \<in> S. \<forall>q. ((prefixp q x) \<longrightarrow>  q \<in> S)) "

definition suffix_closed :: " 'a list set \<Rightarrow> bool" where
  " suffix_closed S = (\<forall>x \<in> S. \<forall>q. ((suffixp q x) \<longrightarrow>  q \<in> S)) "

text \<open> Let's prove a few useful lemmas to avoid unfolding defs \<close>

lemma prefix_closedE2: "\<lbrakk>prefix_closed S; a@b \<in> S\<rbrakk> \<Longrightarrow> a \<in> S"
  unfolding prefix_closed_def prefixp_def by(auto)

lemma suffix_closedE: "\<lbrakk>suffix_closed S; a#b \<in> S\<rbrakk> \<Longrightarrow> b \<in> S"
  unfolding suffix_closed_def suffixp_def by(auto)

lemma suffix_closedE2: "\<lbrakk>suffix_closed S; a@b \<in> S\<rbrakk> \<Longrightarrow> b \<in> S"
  unfolding suffix_closed_def suffixp_def by(auto)

text \<open> Some lemmas showing how to check for prefix/suffix closedness if we update the set S \<close>

lemma prefix_closed_updateI:
  "\<lbrakk>prefix_closed S; prefix_closed U\<rbrakk> \<Longrightarrow> prefix_closed (S \<union> U)"
  by(auto simp add: prefix_closed_def)

lemma suffix_closed_updateI:
  "\<lbrakk>suffix_closed S; suffix_closed U\<rbrakk> \<Longrightarrow> suffix_closed (S \<union> U)"
  by(auto simp add: suffix_closed_def)

lemma prefix_closed_update_step1I:
  "\<lbrakk>prefix_closed S; w \<in> S\<rbrakk> \<Longrightarrow> prefix_closed (S \<union> {w@[a]})"
  unfolding prefix_closed_def proof(rule ballI, rule allI, rule impI)
  fix x q
  assume pc: "(\<forall>x\<in>S. \<forall>q. prefixp q x \<longrightarrow> q \<in> S)" and "w \<in> S"
   and defx: "x \<in> S \<union> {w @ [a]}" and "prefixp q x"
  from defx show "q \<in> S \<union> {w @ [a]}" proof(rule UnE)
    assume "x \<in> S" have "q \<in> S" using pc using \<open>prefixp q x\<close> \<open>x \<in> S\<close> by auto
    then show ?thesis by(simp)
  next
    assume "x \<in> {w @ [a]}" then have "x = w@[a]" by(simp)
    then show ?thesis
      by (metis UnI1 \<open>prefixp q x\<close> \<open>w \<in> S\<close> butlast_append butlast_snoc
           defx pc prefixp_def)
  qed
qed

lemma prefix_closed_update_stepI:
  "\<lbrakk>prefix_closed (set S); {butlast x | x. x \<in> set U} \<subseteq> set S\<rbrakk> \<Longrightarrow>
    prefix_closed (set S \<union> set U)"
proof(induction U)
case Nil
  then show ?case by(simp)
next
  case (Cons a U)
  have "set S \<union> set (a # U) = (set S \<union> set U) \<union> {a}" by(simp)
  \<comment> \<open> We can apply the IH to set S union set U\<close>
  have "prefix_closed (set S \<union> set U)" proof(rule Cons.IH)
    from \<open>prefix_closed (set S)\<close> show "prefix_closed (set S)" by(simp)
  next
    from \<open>{butlast x |x. x \<in> set (a # U)} \<subseteq> set S\<close>
      show "{butlast x |x. x \<in> set U} \<subseteq> set S"
    proof -
      have "{butlast x |x. x \<in> set U} \<subseteq> {butlast x |x. x \<in> set (a # U)}"
        by(auto)
      then show ?thesis using \<open>{butlast x |x. x \<in> set (a # U)} \<subseteq> set S\<close>
        by(simp)
    qed
  qed
  \<comment> \<open> Then we can apply the @{thm prefix_closed_update_step1I} lemma\<close>
  from \<open>{butlast x |x. x \<in> set (a # U)} \<subseteq> set S\<close> have "butlast a \<in> set S"
    by(auto)
  then show ?case proof(induction a rule: rev_induct)
    case Nil
    then show ?case by(simp add: \<open>prefix_closed (set S \<union> set U)\<close> insert_absorb)
  next
    case (snoc x xs)
    then have "butlast (xs @ [x]) = xs" by(simp)
    then have "xs \<in> set S" using \<open>butlast (xs@[x]) \<in> set S\<close> by(simp)
    then have "prefix_closed ((set S \<union> set U) \<union> {xs @ [x]})"
      using prefix_closed_update_step1I \<open>prefix_closed (set S \<union> set U)\<close>
      by fastforce
    then show "prefix_closed (set S \<union> set ((xs @ [x]) # U))" by(simp) 
  qed
qed

lemma suffix_closed_step1I: "\<lbrakk>suffix_closed S; w \<in> S\<rbrakk> \<Longrightarrow> suffix_closed (S \<union> {a#w})"
  unfolding suffix_closed_def proof(rule ballI, rule allI, rule impI)
  fix x q
  assume pc: "(\<forall>x\<in>S. \<forall>q. suffixp q x \<longrightarrow> q \<in> S)" and "w \<in> S"
    and defx: "x \<in> S \<union> {a # w}" and "suffixp q x"
  from defx show "q \<in> S \<union> {a # w}" proof(rule UnE)
    assume "x \<in> S" have "q \<in> S" using pc using \<open>suffixp q x\<close> \<open>x \<in> S\<close> by auto
    then show ?thesis by(simp)
  next
    assume "x \<in> {a # w}" then have "x = a#w" by(simp)
    then show ?thesis using \<open>suffixp q x\<close> \<open>w \<in> S\<close> pc suffixp_code(3) by fastforce
  qed
qed

lemma suffix_closed_stepI:
  "\<lbrakk>suffix_closed (set S); \<forall>w \<in> set U. tl w \<in> set S\<rbrakk>
    \<Longrightarrow> suffix_closed (set S \<union> set U)"
proof(induction U)
case Nil
  then show ?case by(auto)
next
  case (Cons a U)
  have "suffix_closed (set S \<union> set U)"
    by (simp add: Cons.IH Cons.prems(1) Cons.prems(2))
  then show ?case proof(cases "a = []")
    case True
    then show ?thesis
      by (metis Cons.prems(2) UnI1 Un_insert_right \<open>suffix_closed (set S \<union> set U)\<close>
          insert_absorb list.sel(2) list.set_intros(1) list.simps(15))
    next
    case False
    then obtain x xs where "a = x#xs" by (meson remdups_adj.cases)
    from this and Cons.prems(2) have "xs \<in> set S" by(simp)
    then have "xs \<in> (set S \<union> set U)" by(simp)
    have "suffix_closed ((set S \<union> set U) \<union> {x#xs})"
      by(rule suffix_closed_step1I[OF \<open>suffix_closed (set S \<union> set U)\<close> \<open>xs \<in> (set S \<union> set U)\<close>])
    then show ?thesis by (simp add: \<open>a = x # xs\<close>)
  qed
qed


subsubsection \<open>Function to compute the set of all prefixes\<close>
fun all_prefixes :: " 'l fset \<Rightarrow> 'l list fset" where
  " all_prefixes [] = [[]] " |
  " all_prefixes t = [t]@(all_prefixes (butlast t)) "

text \<open>'learn-from-cexample' is simply an extendS call on all prefixes of the counterexample t.
       Here we show a few properties of 'all-prefixes'\<close>

lemma t_in_all_prefixes_t: "t \<in> set (all_prefixes t)" by(cases t, auto)

lemma all_prefixes_snoc: "all_prefixes (xs@[x]) = [xs@[x]]@(all_prefixes xs)"
  by (metis all_prefixes.elims snoc_eq_iff_butlast)

lemma length_all_prefixes: "length (all_prefixes t) = length t + 1" proof(induction t rule: rev_induct)
  case Nil
  then show ?case by auto
next
  case (snoc x xs)
  have "length (all_prefixes (xs@[x])) = length ([xs@[x]]@(all_prefixes xs))" by(simp add: all_prefixes_snoc)
  moreover from snoc.IH have "length (all_prefixes xs) = length xs + 1" by simp
  ultimately show ?case by auto
qed

lemma all_prefixes_complete: "set (all_prefixes x) = {y. prefixp y x}" proof(induction x rule: rev_induct)
  case Nil
  then show ?case using prefixp_def by auto
next
  case (snoc x xs)
  have one: "set (all_prefixes (xs @ [x])) = {xs@[x]} \<union> set (all_prefixes xs)" by(simp add: all_prefixes_snoc)
  from snoc.IH have "set (all_prefixes xs) = {y. prefixp y xs}" .
  also have "... = {y. prefixp y (xs @ [x])} - {xs@[x]}" proof
    show "{y. prefixp y xs} \<subseteq> {y. prefixp y (xs @ [x])} - {xs @ [x]}" proof
      fix k
      assume "k \<in> {y. prefixp y xs}"
      then have *: "prefixp k xs" by auto
      then have "prefixp k (xs @ [x])" by(rule prefixp_trans, simp add: prefixp_def)
      moreover have "k \<noteq> xs @ [x]" proof(rule ccontr)
        show "\<not> k \<noteq> xs @ [x] \<Longrightarrow> False" using * by (simp add: prefixp_def)
      qed
      ultimately show "k \<in> {y. prefixp y (xs @ [x])} - {xs @ [x]}" by auto
    qed
    show "{y. prefixp y (xs @ [x])} - {xs @ [x]} \<subseteq> {y. prefixp y xs}" proof
      fix k
      assume "k \<in> {y. prefixp y (xs @ [x])} - {xs @ [x]}"
      then have *: "prefixp k (xs @ [x])" and **: "k \<noteq> xs @ [x]" by auto
      then have "prefixp k xs"
        by (metis butlast_append butlast_snoc prefixp_def self_append_conv)
      then show "k \<in> {y. prefixp y xs}" by auto
    qed
  qed
  finally have two: "set (all_prefixes xs) = {y. prefixp y (xs @ [x])} - {xs @ [x]}" .
  show ?case proof -
    from one have "set (all_prefixes (xs @ [x])) = {xs@[x]} \<union> set (all_prefixes xs)" .
    also from two have "... = {xs@[x]} \<union> ({y. prefixp y (xs @ [x])} - {xs @ [x]})" by simp
    also have "... = {xs@[x]} \<union> {y. prefixp y (xs @ [x])}" by simp
    also have "... = {y. prefixp y (xs @ [x])}" proof -
      have "prefixp (xs@[x]) (xs@[x])" by(rule prefixp_refl)
      then have "xs@[x] \<in> {y. prefixp y (xs @ [x])}" by simp
      then show ?thesis by auto
    qed
    finally show ?thesis .
  qed
qed

lemma prefix_closed_all_prefixes: "prefix_closed (set (all_prefixes t))" proof(induction t rule: rev_induct)
case Nil
  have "prefix_closed (set [[]])" by(auto simp add: prefix_closed_def prefixp_def)
  then show ?case by(simp)
next
  case (snoc x xs)
  have "(all_prefixes (xs @ [x])) = [xs@[x]]@(all_prefixes xs)" by(simp add: all_prefixes_snoc)
  then have "set (all_prefixes (xs @ [x])) = (set (all_prefixes xs)) \<union> {xs@[x]}" by(simp)
  moreover have "prefix_closed ((set (all_prefixes xs)) \<union> {xs@[x]})" using prefix_closed_update_step1I
    by (metis Un_iff all_prefixes.elims list.set_intros(1) set_append snoc.IH)
  ultimately show ?case by(simp)
qed

lemma distinct_all_prefixes: "distinct (all_prefixes t)" proof(induction t rule: rev_induct)
  case Nil
  then show ?case by simp
next
  case (snoc x xs)
  have *: "(all_prefixes (xs @ [x])) = [xs@[x]]@(all_prefixes xs)" by(simp add: all_prefixes_snoc)
  moreover have "distinct ([xs@[x]]@(all_prefixes xs))" proof -
    have "distinct [xs@[x]]" by simp
    moreover from snoc.IH have "distinct (all_prefixes xs)" .
    moreover have "set [xs@[x]] \<inter> set (all_prefixes xs) = {}" proof(rule ccontr)
      assume "set [xs @ [x]] \<inter> set (all_prefixes xs) \<noteq> {}"
      then obtain e where "e \<in> set [xs @ [x]] \<inter> set (all_prefixes xs)" by auto
      then have *: "e = (xs @ [x])" and **: "e \<in> set (all_prefixes xs)" by auto
      from ** all_prefixes_complete have "prefixp e xs" by auto
      from this and * show False by (simp add: prefixp_def)
    qed
   ultimately show ?thesis using distinct_append by simp
  qed
  then show ?case using * by auto
qed

lemma card_all_prefixes: "card (set (all_prefixes t)) = length t + 1"
  by (simp add: distinct_all_prefixes length_all_prefixes distinct_card)

subsection \<open> Misc Theorems and Defs. \<close>

text \<open> The following lemma will help us prove strict inequalities between variants to get
       termination. \<close>
lemma non_bij_surj_card_less:
  "\<lbrakk>finite A; B \<subseteq> f ` A; x \<in> A; y \<in> A; x \<noteq> y; f x = f y\<rbrakk> \<Longrightarrow> card B < card A"
proof -
  assume assms: "finite A" "B \<subseteq> f ` A" "x \<in> A" "y \<in> A" "x \<noteq> y" "f x = f y"
  let ?A' = "A - {x}"
  have "B \<subseteq> f ` ?A'" proof(rule subsetI)
    fix b
    assume "b \<in> B"
    then show "b \<in> f ` ?A'" proof(cases "b = f x")
      case True
      then show ?thesis using \<open>y \<in> A\<close> \<open>f x = f y\<close> \<open>x \<noteq> y\<close> by blast
      next
      case False
      then show ?thesis using \<open>B \<subseteq> f ` A\<close> \<open>b \<in> B\<close> by auto
    qed
  qed
  then have "card B \<le> card ?A'" using \<open>finite A\<close> finite_Diff surj_card_le by blast
  also have "... < card A" using \<open>finite A\<close> by (meson \<open>x \<in> A\<close> card_Diff1_less)
  finally show ?thesis .
qed

lemma map_upds_updated: "a \<in> set queries \<Longrightarrow> (map_upds f queries (map g queries)) a = Some (g a)"
proof(induction queries rule: rev_induct)
  case Nil
  then show ?case by(simp)
next
  case (snoc x xs)
  then show ?case proof(cases "x=a")
    case True
    then show ?thesis unfolding map_upds_def by simp
  next
    case False
    assume ih: "a \<in> set xs \<Longrightarrow> (f(xs [\<mapsto>] map g xs)) a = Some (g a)" and defa: "a \<in> set (xs @ [x])" and 
           defx: "x \<noteq> a"
    from defa and defx have "a \<in> set xs" by(simp)
    then have "(f(xs [\<mapsto>] map g xs)) a = Some (g a)" using ih by(simp)
    then show ?thesis unfolding map_upds_def by(auto)
  qed
qed

end (* end of theory *)