theory Automata_Common
imports Main
begin

text \<open> Helper function to define the transition function\<close>
definition foldl2 :: "('a \<Rightarrow> 'b \<Rightarrow> 'b) \<Rightarrow> 'a list \<Rightarrow> 'b \<Rightarrow> 'b" where
 " foldl2 f xs a = foldl (\<lambda>a b. f b a) a xs "

text \<open> Congruence rule for foldl2, for one element \<close>
lemma foldl2_cong1:
  "(a = b) \<Longrightarrow> (f a x0 = g b y0) \<Longrightarrow> (foldl2 f [a] x0) = (foldl2 g [b] y0)"
  by(simp add: foldl2_def)

text \<open> General congruence rule for foldl2\<close>
lemma foldl2_cong[fundef_cong]:
  "\<lbrakk>a = b; xs = ys; \<And>x. x \<in> set xs \<Longrightarrow> f x = g x\<rbrakk>
    \<Longrightarrow> foldl2 f xs a = foldl2 g ys b"
  apply(induction xs arbitrary: a b)
  apply(simp add: foldl2_def)
  apply(simp add: foldl2_def)
  by (metis (no_types, lifting) foldl_cong)

lemma foldl2_simps[simp]:
  "foldl2 f [] a = a"
  "foldl2 f (x#xs) a = foldl2 f xs (f x a)"
  by(simp_all add:foldl2_def)
  
end