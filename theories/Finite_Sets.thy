section \<open> Finite Sets and Lemmas \<close>

text \<open> Angluin's algorithm is based on a sampling set technique. To ensure our code
       stays easily executable, we define a datatype of finite sets and related operations. \<close>

theory Finite_Sets
  imports Main
          HOL.Relation
begin

text \<open> A finite set is modeled by a list. In the operations provided, we ensure
       that no duplicates are inserted. \<close>
type_synonym 'x fset = "'x list"

section \<open> Functions on finite sets \<close>

subsection \<open> Union of finite sets \<close>

text \<open> This definition means that we'll always add new elements at the beginning of the list. \<close>
fun fset_union :: " 'a fset \<Rightarrow> 'a fset \<Rightarrow> 'a fset " (infixl "\<union>\<^sub>f" 65) where
  " fset_union [] ys = ys " |
  " fset_union (x#xs) ys = (let result = fset_union xs ys in
      (if (x \<in> set result) then result else (x#result))) "

lemma set_of_fset_union[simp]: "set (fset_union xs ys) = set xs \<union> set ys"
  by(induction xs; auto simp add: Let_def)

lemma fset_union_intro_l[intro]: "x \<in> set xs \<Longrightarrow> x \<in> set (fset_union xs ys)"
  using set_of_fset_union by(fastforce)

lemma fset_union_intro_r[intro]: "x \<in> set ys \<Longrightarrow> x \<in> set (fset_union xs ys)"
  using set_of_fset_union by(fastforce)

lemma distinct_fset_union:
  "distinct xs \<Longrightarrow> distinct ys \<Longrightarrow> (distinct (fset_union xs ys))"
  by(induction xs; auto simp add: Let_def)

text \<open> This lemma eases case splits over finite set unions \<close>
lemma fset_union_cases[consumes 1, case_names inLeft inRight]: 
  "\<lbrakk>t \<in> set (A \<union>\<^sub>f B); t \<in> set A \<Longrightarrow> P; t \<in> set B \<Longrightarrow> P\<rbrakk> \<Longrightarrow> P"
  using set_of_fset_union by auto

subsection \<open>NubBy function on finite sets \<close>

text \<open> Helper function for the nubby function; Tests if some x in xs makes p true \<close>
fun list_some :: "('a \<Rightarrow> bool) \<Rightarrow> 'a list \<Rightarrow> bool" where
  " list_some p [] = False " |
  " list_some p (x#xs) = (p x \<or> list_some p xs) "

lemma list_someE: "list_some (p a) S \<Longrightarrow> \<exists>k \<in> set S. p a k"
  by(induction S, auto)

lemma list_some_iff: "list_some (p a) S \<longleftrightarrow> (\<exists>b \<in> set S. p a b)"
  by(induction S, auto)

text \<open> Removes duplicates in a list according to a supplied equivalence relation \<close>
fun nubby :: "('a \<Rightarrow> 'a \<Rightarrow> bool) \<Rightarrow> 'a fset \<Rightarrow> 'a fset " where
  " nubby p [] = [] " |
  " nubby p (x#xs) = (if (list_some (p x) xs) then (nubby p xs)
                                             else (x#(nubby p xs))) "

lemma nubby_subseteq: " set (nubby p S) \<subseteq> set S"
  by(induction S, auto)

lemma distinct_nubby: "equivp p \<Longrightarrow> distinct (nubby p S)"
  apply(induction S)
  apply(simp)
  using list_some_iff distinct.simps(2) distinct1_rotate equivp_reflp nubby_subseteq
  by fastforce

lemma nubby_last: "x \<in> set (nubby p (S@[x]))" by(induction S, auto)

lemma nubby_sound:
  "\<lbrakk>equivp p; s1 \<in> set (nubby p S); s2 \<in> set (nubby p S); p s1 s2\<rbrakk>
    \<Longrightarrow> s1 = s2"
proof(induction S)
  case Nil
  then show ?case by(auto)
next
  case (Cons a S)
  show ?case proof(cases "list_some (p a) S")
    case True
    then show ?thesis
      using Cons.IH Cons.prems by auto
  next
    case False
    then have *: "nubby p (a#S) = a#(nubby p S)" and **: "\<forall>x \<in> set S. \<not> p a x"
      using False by (auto simp add: list_some_iff)
    show ?thesis proof(cases "a = s1")
      case True
      from \<open>p s1 s2\<close> have "s2 \<notin> set S" using "**" \<open>a = s1\<close> by blast
      then show ?thesis
        by (metis Cons.prems(3) True nubby_subseteq set_ConsD subset_eq)
    next
      case False
      \<comment> \<open> Then, we set things up for recursion \<close>
      then have "s1 \<in> set (nubby p S)" using "*" Cons.prems(2) by auto
      moreover have "s2 \<in> set (nubby p S)"
        by (metis "*" "**" Cons.prems(1) Cons.prems(3) Cons.prems(4)
            calculation equivp_symp nubby_subseteq set_ConsD subsetCE)
      ultimately show ?thesis using Cons.IH
        using Cons.prems(1) Cons.prems(4) by blast
    qed
  qed
qed

lemma nubby_complete: "equivp p \<Longrightarrow> x \<in> set Q \<Longrightarrow> \<exists>q \<in> set (nubby p Q). p x q"
proof(induction Q arbitrary: x)
  case Nil
  then show ?case by(simp)
next
  case (Cons a Q)
  then show ?case proof(cases "list_some (p a) Q")
    case True
    then have *: "nubby p (a#Q) = nubby p Q" by(simp)
    then show ?thesis proof(cases "x = a")
      case True
      from \<open>list_some (p a) Q\<close> have "\<exists>y \<in> set Q. p a y" by(rule list_someE)
      then obtain y where "y \<in> set Q" and "p a y" by(auto)
      then have "p x y" using True by simp
      \<comment> \<open> Now, we can use the IH for y\<close>
      from Cons.IH have "\<exists>z \<in> set (nubby p Q). p y z"
        by (simp add: Cons.prems(1) \<open>y \<in> set Q\<close>)
      then obtain z where "z \<in> set (nubby p Q)" and "p y z" by(blast)
      from \<open>p x y\<close> \<open>p y z\<close> \<open>equivp p\<close> have "p x z"
        by(auto simp add: equivp_def)
      then show ?thesis using \<open>z \<in> set (nubby p Q)\<close> by auto
    next
      case False
      then have "x \<in> set Q" using Cons.prems(2) by auto
      then show ?thesis by (simp add: Cons.IH Cons.prems(1))
    qed
  next
    case False
    then show ?thesis
      using Cons.IH Cons.prems(1) Cons.prems(2) equivp_reflp by fastforce
  qed
qed

lemma nubby_subseteq_complete:
  "\<lbrakk>equivp p; set S \<subseteq> set Q\<rbrakk>
    \<Longrightarrow> \<forall>s \<in> set (nubby p S). \<exists>q \<in> set (nubby p Q). p s q"
proof -
  assume "equivp p" and sub: "set S \<subseteq> set Q"
  then show "\<forall>s \<in> set (nubby p S). \<exists>q \<in> set (nubby p Q). p s q" proof(safe)
    fix s
    assume "s \<in> set (nubby p S)"
    from this and sub show "\<exists>q\<in>set (nubby p Q). p s q" proof(induction S)
    case Nil
      then show ?case by(auto)
    next
      case (Cons a S)
      then show ?case proof(cases "s = a")
        case True
        \<comment> \<open> In this case, we make another case distinction \<close>
        then show ?thesis proof(cases "list_some (p a) S")
          case True
          then have "nubby p (a#S) = nubby p S" by(simp)
          from this and \<open>s \<in> set (nubby p (a # S))\<close> have "s \<in> set (nubby p S)"
            by(simp)
          then show ?thesis using Cons.IH using Cons.prems(2) by auto
        next
          case False
          then have "nubby p (a#S) = a#(nubby p S)" by(simp)
          then have "a \<in> set (nubby p (a#S))" by(simp)
          then have "a \<in> set (a#S)" by(simp)
          then have "a \<in> set Q" using Cons.prems(2) by blast
          then show ?thesis using nubby_complete
            by (simp add: nubby_complete True \<open>equivp p\<close>)
        qed
      next
        \<comment> \<open> If s is not a, then s is in S and we use the IH directly \<close>
        case False
        then have "s \<in> set S"
          by(meson Cons.prems nubby_subseteq set_ConsD subsetCE)
        then show ?thesis
          by (metis Cons.prems(2) \<open>equivp p\<close> nubby_complete set_mp set_subset_Cons)
      qed
    qed
  qed
qed

lemma card_nubby_leq:
  "\<lbrakk>equivp p; set S \<subseteq> set Q\<rbrakk>
   \<Longrightarrow> card (set (nubby p S)) \<le> card (set (nubby p Q))"
proof -
  assume "equivp p" and "set S \<subseteq> set Q"
  then show "card (set (nubby p S)) \<le> card (set (nubby p Q))"
  proof -
    \<comment> \<open> The idea is to define an injection \<close>
    define f :: "'a \<Rightarrow> 'a" where
      " f s = (SOME q. q \<in> set (nubby p Q) \<and> p s q) " for s
    have fwdef0: "\<forall>s \<in> set (nubby p S). \<exists>q \<in> set (nubby p Q). p s q"
      by (metis \<open>equivp p\<close> \<open>set S \<subseteq> set Q\<close> nubby_complete nubby_subseteq subsetD)
    then have fwdef: "\<forall>s \<in> set (nubby p S). \<exists>q \<in> set (nubby p Q). f s = q"
      unfolding f_def by (metis (no_types, lifting) exE_some)
    have "inj_on f (set (nubby p S))" proof
      fix x y
      assume "x \<in> set (nubby p S)" and "y \<in> set (nubby p S)" and "f x = f y"
      then have "\<exists>q \<in> set (nubby p Q). p x q \<and> p y q"
        by (metis (mono_tags, lifting) fwdef0 f_def fwdef someI_ex)
      then obtain q where "q \<in> set (nubby p Q)" and "p x q" and "p y q" by(blast)
      then have "p x y" using \<open>equivp p\<close> by (simp add: equivp_def)
      then show "x = y" using nubby_sound
        using \<open>equivp p\<close> \<open>x \<in> set (nubby p S)\<close> \<open>y \<in> set (nubby p S)\<close> by fastforce
    qed
    then show ?thesis proof(rule card_inj_on_le)
      show "f ` set (nubby p S) \<subseteq> set (nubby p Q)" using fwdef by auto
      show "finite (set (nubby p Q))" by(simp)
    qed
  qed
qed
  
text \<open> From @{thm card_nubby_leq} we can get the following.
       We don't have equality in general on the rhs. \<close>                                
lemma card_nubby_equal: 
  "\<lbrakk>equivp p; set S = set Q\<rbrakk> \<Longrightarrow> card (set (nubby p S)) = card (set (nubby p Q))"
proof -
  assume *: "equivp p" and **: "set S = set Q"
  then have " card (set (nubby p S)) \<le> card (set (nubby p Q))"
    by(intro card_nubby_leq, auto)
  moreover from * ** have " card (set (nubby p Q)) \<le> card (set (nubby p S))"
    by(intro card_nubby_leq, auto)
  ultimately show ?thesis by(simp)
qed

text \<open> NubBy leaves a representant for each class; 
       For simplicity, we require that p is an equivalence relation \<close>
lemma ex_repr_in_nubby:
  "equivp p \<Longrightarrow> x \<in> set S \<Longrightarrow> (\<exists>y \<in> set (nubby p S). p y x)"
proof(induction S arbitrary: x)
  case Nil
  then show ?case by(simp)
next
  case (Cons a S)
  \<comment> \<open> If x is the head of the list, we have to do the work... \<close>
  then show ?case proof(cases "x = a")
    case True
    \<comment> \<open> In this case, we have to use transitivity of p\<close>
    then show ?thesis proof(cases "list_some (p a) S")
      case True
      then have "\<exists>k \<in> set S. p a k" by(rule list_someE)
      then obtain k where kdef1: "k \<in> set S" and kdef2: "p a k" by(auto)
      from kdef1 and \<open>equivp p\<close> have "\<exists>y\<in>set (nubby p (a # S)). p y k"
        using Cons.IH by(auto)
      then obtain y where ydef1: "y \<in> set (nubby p (a # S))" and ydef2: "p y k"
        by (auto)
      from ydef1 ydef2 kdef2 and \<open>equivp p\<close> have "p y a"
        by (simp add: equivp_def)
      then show ?thesis using Cons.IH Cons.prems(1) Cons.prems(2) ydef1
        by(auto)
    next
      \<comment> \<open> In that case, x is directly included in the output, and we use reflexivity of p \<close>
      case False
      from this and \<open>x = a\<close> have "x \<in> set (nubby p (a # S))" by(simp)
      then show ?thesis by (meson Cons.prems(1) equivp_reflp)
    qed
    next
    \<comment> \<open> Otherwise, we use the IH \<close>
    case False
      from this and \<open>x \<in> set (a#S)\<close> have "x \<in> set S" by(auto)
      then have "\<exists>y\<in>set (nubby p S). p y x"
        using Cons.IH \<open>equivp p\<close> by(auto)
      then show ?thesis by(simp)
  qed
qed

text \<open> We show how find and nubby interact \<close>
lemma find_nubby_Some:
  "\<lbrakk>equivp f; x \<in> set (nubby f xs); f r x\<rbrakk> \<Longrightarrow> find (f r) (nubby f xs) = Some x"
  apply(induction f xs rule: nubby.induct)
  apply(simp)[1]
  apply(auto)[1]
  apply (metis equivp_def list_some_iff nubby_subseteq subset_iff)
  by (metis (full_types) set_ConsD)

subsection \<open> Definition of a safe-head function\<close>

fun safe_head :: "'l fset \<Rightarrow> 'l option" where
  " safe_head [] = None " |
  " safe_head (x#xs) = Some x "

lemma safe_head_None[simp]: "safe_head x = None \<longleftrightarrow> x = []"
  using safe_head.elims by auto
lemma safe_head_Some[simp]: "safe_head xs = Some x \<longleftrightarrow> xs \<noteq> [] \<and> hd xs = x"
  by (metis (full_types) list.sel(1) option.sel
       option.simps(3) safe_head.elims safe_head.simps(1))

end