section \<open> Properties (Correctness and Termination) of the LStar algorithm.
          (Modified for Moore machines)\<close>

theory LStar_Properties
  imports
    LStar_Implementation
begin

text \<open> The following locale represents the assumptions that the oracleM and oracleM are 'correct';
       Together they form Angluin's Minimal Adequate Teacher model. \<close>

text \<open> Note: The alphabet is a *finite* set, otherwise we can't build a DFA. \<close>
locale MAT_model_moore = Basic_Defs.WeightedLanguagesMoore \<Sigma> \<Sigma>star L L_prop M\<^sub>L for
  \<Sigma> :: " 'letter fset " and \<Sigma>star and L and L_prop
                        and M\<^sub>L :: " ('letter, 'mls, 'mlo) moore_machine" +
  fixes oracleM :: " ('letter, 'mlo) membership_oracle "
    and oracleC :: " ('letter, 'letter list, 'mlo) conjecture_oracle "
    and m_bound :: "nat"
  assumes 
          \<comment> \<open> The membership oracle samples L\<close>
         oracleM_correct: " oracleM w = L w " and

          \<comment> \<open> The conjecture oracle returns None iif the conjecture is correct \<close>
          \<comment> \<open> If the conjecture oracle returns none, the given conjecture machine accepts the correct
               language. Importantly, the oracle doesn't have to check that the conjecture is minimal
               (eg. has less states than any other acceptor, or is isomorphic). Minimality can be
               guaranteed by the L* algorithm itself, without requiring the oracle to check for it.\<close>
          oracleC_correct_None:
           "\<lbrakk>welldefined_dfa_p conjecture; set (get_alphabet conjecture) = set \<Sigma>; follows_prop_p conjecture L_prop\<rbrakk>
             \<Longrightarrow> oracleC conjecture = None \<longleftrightarrow> accepted_language_eq_p conjecture \<Sigma> L" and
         
         \<comment> \<open> If the conjecture oracle returns something, it's indeed a counterexample (in sym-diff).
               Note: We need to add the assumptions that the conjecture given to the oracle is wdef
               and has the same alphabet as the language under learning (which will be insured by
               the l* algorithm for the conjectures it presents to the oracle). \<close>
          oracleC_correct_Some_correct:
            "\<lbrakk>welldefined_dfa_p conjecture; set (get_alphabet conjecture) = set \<Sigma>; follows_prop_p conjecture L_prop;
               oracleC conjecture = Some x\<rbrakk>
              \<Longrightarrow> x \<in> \<Sigma>star \<and> (L x \<noteq> output_on conjecture x) " and
         
          \<comment> \<open> If a counterexample exists, the conjecture oracle will return something,
               can be deduced from the other properties.\<close>
          (* oracleC_correct_Some_sound: "x \<in> \<Sigma>star \<and> (x \<in> L \<longleftrightarrow> x \<notin> (acceptedLanguage conjecture)) \<Longrightarrow> \<exists>y. oracleC conjecture = Some y" *)

          \<comment> \<open> The oracle returns counterexamples of finite length (bounded by m)\<close>
               oracleC_bounded:
           "\<lbrakk>welldefined_dfa_p conjecture; set (get_alphabet conjecture) = set \<Sigma>; follows_prop_p conjecture L_prop;
             num_states conjecture \<le> num_states M\<^sub>L;
            oracleC conjecture = Some x\<rbrakk> \<Longrightarrow> length x \<le> m_bound"

context MAT_model_moore begin


text \<open> Before diving in, let's show oracleC-correct-Some-sound from the other facts in the locale.\<close>
lemma oracleC_correct_Some_sound:
  "\<lbrakk>x \<in> \<Sigma>star; (L x \<noteq> output_on conjecture x); 
    welldefined_dfa_p conjecture; set (get_alphabet conjecture) = set \<Sigma>;
    follows_prop_p conjecture L_prop\<rbrakk>
     \<Longrightarrow> \<exists>y. oracleC conjecture = Some y" proof -
  assume *: "x \<in> \<Sigma>star" "(L x \<noteq> output_on conjecture x)" 
            "welldefined_dfa_p conjecture" "set (get_alphabet conjecture) = set \<Sigma>"
            "follows_prop_p conjecture L_prop"
  have "\<not> (accepted_language_eq_p conjecture \<Sigma> L)" proof(rule ccontr)
    assume "\<not> \<not> accepted_language_eq_p conjecture \<Sigma> L"
    then have "accepted_language_eq_p conjecture \<Sigma> L" by simp
    then have "output_on conjecture x = L x" proof(rule accepted_language_eq_pE)
      from * have "x \<in> \<Sigma>star" by auto
      then have "x \<in> lists (set \<Sigma>)" by (simp add: def_sigmastar)
      then show "x \<in> lists (set (get_alphabet conjecture))" using * by simp
    qed
    then show "False" using * by simp
  qed
  then have "oracleC conjecture \<noteq> None" using * by(auto simp add: oracleC_correct_None) 
  then show ?thesis
    by blast
qed

(* To inspect the locales *)
print_locale! MAT_model_moore

section \<open> Needed Notions \<close>
text \<open> We begin by defining concepts needed for correctness and termination of the L* alg. \<close>

subsection \<open> Number of different rows (Termination measure) \<close>
text \<open> The following function will be useful to speak about termination of various functions. It
       counts the number of different rows (as expressed by rows-eq-p !). \<close>
text \<open> We reuse the function 'getRowsOf' which gets a row as a finite function (on which equality
       is the same as rows-eq-p); This function was used in the proof of Lemma4 originally. \<close>

definition num_diff_rows :: " ('a,'o) observation_table \<Rightarrow> nat " where
  " num_diff_rows table = card {get_row_of table s | s. s \<in> set (get_S table)} "

text \<open> Our statespace is exactly defined as the set of 'different' rows eq-classes. We have to add
       the assumption that the table is wellformed to deal with our little earlier hack forcing []
       to be chosen as one representative. \<close>
lemma card_make_statespace_eq_num_diff_rows: "wellformedp table \<Longrightarrow> card (set (make_statespace table)) = num_diff_rows table" proof -
  assume wformed: "wellformedp table"
  \<comment> \<open> We begin by proving the statement without our little hack. We need to generalize first...\<close>
  have "\<And>S. card (set (nubby (rows_eq_p table) S)) = card {get_row_of table s | s. s \<in> set S}" proof -
    fix S
    show "card (set (nubby (rows_eq_p table) S)) = card {get_row_of table s | s. s \<in> set S}" proof(induction S)
      case Nil
      then show ?case by(auto)
    next
      case (Cons a S)
      then show ?case proof(cases "list_some (rows_eq_p table a) S")
        case True
        from True have "\<exists>b \<in> set S. table \<turnstile> a \<equiv> b" by(auto elim: list_someE)
        then obtain b where bdef0: "b \<in> set S" and bdef1: "table \<turnstile> a \<equiv> b" by(auto)
        then have "card {get_row_of table s | s. s \<in> set (a#S)} = card {get_row_of table s | s. s \<in> set S}"
          by (metis \<open>\<And>thesis. (\<And>b. \<lbrakk>b \<in> set S; table \<turnstile> a \<equiv> b\<rbrakk> \<Longrightarrow> thesis) \<Longrightarrow> thesis\<close> list.set_intros(2) rows_eq_p_imp_get_row_of set_ConsD)
        then show ?thesis
          by (metis Cons.IH True nubby.simps(2))
      next
        case False
        then have "\<forall>b \<in> set S. \<not> table \<turnstile> a \<equiv> b" by (simp add: list_some_iff)
        then have "get_row_of table a \<notin> {get_row_of table s | s. s \<in> set S}" using get_row_of_imp_rows_eq_p by blast
        then have *: "card ({get_row_of table a} \<union> {get_row_of table s | s. s \<in> set (S)}) = card {get_row_of table a} + card {get_row_of table s | s. s \<in> set S}"
          by(intro card_Un_disjoint, auto)
        have "card {get_row_of table s | s. s \<in> set (a#S)} = 1 + card {get_row_of table s | s. s \<in> set S}" proof -
          have "{get_row_of table s | s. s \<in> set (a#S)} = {get_row_of table a} \<union> {get_row_of table s | s. s \<in> set (S)}" by(auto)
          then have "card ({get_row_of table a} \<union> {get_row_of table s | s. s \<in> set (S)}) = card {get_row_of table s | s. s \<in> set (a#S)}" by(simp)
          then show ?thesis using * by auto
        qed
        moreover have "card (set (nubby (rows_eq_p table) (a # S))) = 1 + card (set (nubby (rows_eq_p table) S))" proof -
          from False have "(set (nubby (rows_eq_p table) (a # S))) =  {a} \<union> (set (nubby (rows_eq_p table) S))" by auto
          moreover have "a \<notin> (set (nubby (rows_eq_p table) S))" using False
            using \<open>get_row_of table a \<notin> {get_row_of table s |s. s \<in> set S}\<close> nubby_subseteq by fastforce
          ultimately show ?thesis
            by (metis (no_types, lifting) "*" Cons.IH List.finite_set One_nat_def \<open>get_row_of table a \<notin> {get_row_of table s |s. s \<in> set S}\<close> add.right_neutral card.infinite card.insert card_empty empty_iff finite.emptyI insert_is_Un)
        qed
        ultimately show ?thesis using Cons.IH by linarith
      qed
    qed
  qed (* End of generalized subproof *)
  \<comment> \<open> We now get the statement without the hack\<close>
  then have stm_no_hack: "card (set (nubby (rows_eq_p table) (get_S table))) = card {get_row_of table s | s. s \<in> set (get_S table)}" .
  then show ?thesis proof -
    \<comment> \<open> From wellformedness, our hack is harmless. \<close>
    from wformed have "[] \<in> set (get_S table)" by (rule lambda_in_S)
    then have "set (get_S table) = set (get_S table @ [[]])" by(auto)
    then have "card (set (nubby (rows_eq_p table) (get_S table))) = card (set (nubby (rows_eq_p table) (get_S table @ [[]])))" using card_nubby_equal
      using equivp_rows_eq_p by blast
    from this and stm_no_hack show ?thesis unfolding make_statespace_def state_reps_of_def by(auto simp add: num_diff_rows_def)
  qed
qed

text \<open> We'll need to juggle with these notions later on, so here is a reformulation of the previous
       lemma. \<close>
lemma num_states_eq_num_diff_rows: 
  "wellformedp table \<Longrightarrow> (num_states \<circ> make_conjecture) table = num_diff_rows table"
  by (simp add: card_make_statespace_eq_num_diff_rows make_conjecture_def)

subsection \<open> Lemma 5 \<close>

text \<open> At first sight, it might seem like we get this lemma for 'free' from Theorem 1.
       However, this lemma *does not* require the assumptions that the table is closed and consistent !
       This will be required to prove termination of the functions making the table closed and consistent. \<close>

text \<open> The proof uses the following function from words to states; We'll show that it's injective. \<close>
definition lemma5f :: " ('a,'s,'o) moore_machine \<Rightarrow> 'a list \<Rightarrow> 's" where
  " lemma5f M s = \<delta>\<langle>M\<rangle> s (get_initial M)"

lemma angluin_lemma5:
  assumes
    \<comment> \<open> Assume only basic wellformedness of the table. \<close>
    filled: " filledp table " and 
    wformed: "wellformedp table" and
    \<comment> \<open> Let M be any acceptor compatible with the T function \<close>
    defM: "compat_with_p M table" and
    wdefM: " welldefined_dfa_p M "
   \<comment> \<open> It must have more states than the number of equivalence classes of rows-eq-p ! \<close>
  shows " num_states M \<ge> num_diff_rows table "
proof -
  let ?S = "set (get_S table)"
  let ?q0 = "get_initial M"
  let ?f = " lemma5f M"
  let ?statespace = "set (make_statespace table)"
  \<comment> \<open> We begin by showing injectivity of f \<close>
  have f_inj_contr: "\<And>s1 s2. \<lbrakk>s1 \<in> ?S; s2 \<in> ?S; get_row_of table s1 \<noteq> get_row_of table s2\<rbrakk> \<Longrightarrow> ?f s1 \<noteq> ?f s2" proof -
    fix s1 s2
    assume s1def: "s1 \<in> ?S" and s2def: "s2 \<in> ?S" and *: "get_row_of table s1 \<noteq> get_row_of table s2"
    then have "\<exists>e \<in> set (get_E table). (get_T table) (s1@e) \<noteq> (get_T table) (s2@e)" by(auto)
    then obtain e where edef0: "e \<in> set (get_E table)" and edef1: "(get_T table) (s1@e) \<noteq> (get_T table) (s2@e)"
      by(auto)
    from filled have "\<exists>xe1. (get_T table) (s1@e) = Some xe1" and "\<exists>xe2. (get_T table) (s2@e) = Some xe2"
      by(simp add: edef0 filledpE2 s1def,simp add: edef0 filledpE2 s2def)
    then obtain xe1 xe2 where xe1def: "(get_T table) (s1@e) = Some xe1" and 
                              xe2def: "(get_T table) (s2@e) = Some xe2" by(auto)
    then have **: "xe1 \<noteq> xe2" using edef1 by auto
    from this and defM have " output_on M (s1@e) \<noteq> output_on M (s2@e) "
      (* TODO: This is a big metis call :/*)
      by (metis (no_types, hide_lams) \<open>\<And>thesis. (\<And>xe1 xe2. \<lbrakk>get_T table (s1 @ e) = Some xe1; get_T table (s2 @ e) = Some xe2\<rbrakk> \<Longrightarrow> thesis) \<Longrightarrow> thesis\<close> \<open>\<exists>xe1. get_T table (s1 @ e) = Some xe1\<close> \<open>\<exists>xe2. get_T table (s2 @ e) = Some xe2\<close> compat_with_p_same_funcE edef0 edef1 fset_union_intro_l s1def s2def)
    then have "\<delta>\<langle>M\<rangle> s1 ?q0 \<noteq> \<delta>\<langle>M\<rangle> s2 ?q0" by (metis output_on_def delta_append)
    then show "lemma5f M s1 \<noteq> lemma5f M s2" unfolding lemma5f_def .
  qed
  \<comment> \<open> From this we get that f is injective from the set of different rows (which is also how we
       defined make-statespace) ! \<close>
  have "inj_on ?f ?statespace" proof
    fix s1 s2
    assume "s1 \<in> ?statespace" and "s2 \<in> ?statespace" and "lemma5f M s1 = lemma5f M s2"
    then have "get_row_of table s1 = get_row_of table s2" using f_inj_contr[of s1 s2] using make_statespace_in_S wformed by auto
    then have "table \<turnstile> s1 \<equiv> s2" by (simp add: get_row_of_imp_rows_eq_p)
    then show "s1 = s2" using \<open>s1 \<in> set (make_statespace table)\<close> \<open>s2 \<in> set (make_statespace table)\<close> make_statespace_rows_eq_p by auto
  qed
  \<comment> \<open> From this, we know that M has at least as many states as the size of the statespace (which
       is also proven in Thm1, but with more assumptions). \<close>
  then have "card (?statespace) \<le> num_states M" proof(rule card_inj_on_le)
    from defM have same_alphs: "set (get_alphabet M) = set (get_A table)" by (simp add: compat_with_p_same_alphE) 
    show "lemma5f M ` set (make_statespace table) \<subseteq> set (get_statespace M)" proof(rule subsetI)
      fix x 
      assume "x \<in> lemma5f M ` set (make_statespace table)"
      then obtain y where "y \<in> set (make_statespace table)" and "x = lemma5f M y" by auto
      then have "y \<in> lists (set (get_A table))" using make_statespace_in_S wellformedp_alphabetE wformed by fastforce
      from this and same_alphs have "y \<in> lists (set (get_alphabet M))" by(simp)
      then show "x \<in> set (get_statespace M)" using wdefM
        by (simp add: \<open>x = lemma5f M y\<close> lemma5f_def welldefined_dfa_p_deltaE welldefined_dfa_p_initialE)
    qed
  next
    show "finite (set (get_statespace M))" by(simp)
  qed
  \<comment> \<open> Finally, we can use the previous lemma (more or less showing nubby and our hack are correct)
       to conclude. \<close>
  then show ?thesis by (simp add: card_make_statespace_eq_num_diff_rows wformed)
qed

subsection \<open> Using lemma 5 on minimum acceptor \<close>

text \<open> The next step is to use Lemma 5 on the minimum acceptor for L (exists since L is regular).
       We'll need to be able to talk about tables in which the T function has been built through
       the answers of the membership oracle. Let's define this; \<close>
text \<open> Note that we force the observation table to share the same output alphabet as the one in our
       locale.\<close>
definition follows_orcm_p :: " ('letter,'mlo) observation_table \<Rightarrow> bool" where
  " follows_orcm_p table \<equiv> (\<forall>s \<in> set (row_labels table). \<forall>e \<in> set (get_E table).
                                       \<forall>x. get_T table (s@e) = Some x \<longrightarrow> x = oracleM (s@e))"

lemma follows_orcm_pE[elim]:
  "\<And>s e x. \<lbrakk>follows_orcm_p table; s \<in> set (row_labels table); e \<in> set (get_E table); get_T table (s@e) = Some x\<rbrakk> \<Longrightarrow> 
  x = oracleM (s@e)" by(auto simp add: follows_orcm_p_def)

lemma follows_orcm_pI[intro]:
  "\<lbrakk>\<And>s e x. \<lbrakk>s \<in> set (row_labels table); e \<in> set (get_E table); get_T table (s@e) = Some x\<rbrakk> \<Longrightarrow> x = oracleM (s@e)\<rbrakk>
            \<Longrightarrow> follows_orcm_p table"
  by(auto simp add: follows_orcm_p_def)

lemma follows_orcm_follows_prop_make_conjecture:
  assumes sameAlphs: "set (get_A table) = set \<Sigma>" and
          filled: "filledp table" and
          wformed: "wellformedp table" and 
          fconj: "follows_orcm_p table"
    shows "follows_prop_p (make_conjecture table) L_prop"
unfolding follows_prop_p_def proof
  fix s
  assume *:"s \<in> set (get_statespace (make_conjecture table))"
  have defs: "s \<in> set (row_labels table)" proof -
    from * have "s \<in> set (get_S table)" using make_statespace_in_S[OF wformed]
      unfolding make_conjecture_def by auto
    then show ?thesis by auto
  qed
  have "(get_finalf (make_conjecture table) s) = the (get_T table s)"
    by (simp add: make_conjecture_def make_finalf_def)
  also have "... = oracleM s" proof -
    from filled defs have "\<exists>x. (get_T table (s@[])) = Some x" proof(rule filledpE2)
      from wformed show "[] \<in> set (get_E table)" by simp
    qed
    then obtain x where "get_T table s = Some x" by auto
    then show ?thesis using fconj filled filledpE3 by fastforce
  qed
  also have "... = L s" using oracleM_correct by simp
  finally show "L_prop (get_finalf (make_conjecture table) s)" using L_has_prop by simp
qed

lemma follows_orcm_imp_compatibleWith_min_acceptor: 
  assumes sameAlphs: "set (get_A table) = set \<Sigma>" and
          filled: "filledp table" and
          wformed: "wellformedp table" and 
          "follows_orcm_p table"
  shows "compat_with_p M\<^sub>L table" proof -
  from assms have *: "(\<forall>s \<in> set (row_labels table). \<forall>e \<in> set (get_E table).
               \<forall>x. get_T table (s@e) = Some x \<longrightarrow> x = L (s@e))"
    by(simp add: follows_orcm_p_def oracleM_correct)
  \<comment> \<open> We show the three properties requires for compat-with-p \<close>
  show ?thesis proof(rule compat_with_pI)
    \<comment> \<open> First, the alphabets have to match; This follows from our assumptions. \<close>
    from sameAlphs and acceptor_alphabet show "(set (get_alphabet M\<^sub>L) = set (get_A table))" by(simp)

  \<comment> \<open> Then, we prove the compatibility with the T function \<close>
    fix s e x
    assume defs: "s \<in> set (row_labels table)" and defe: "e \<in> set (get_E table)"
    show "(output_on M\<^sub>L (s @ e) = x) = (get_T table (s @ e) = Some x)" proof -
      \<comment> \<open> Let's gather a few facts \<close>
      from defs defe * have **: "\<forall>x. get_T table (s@e) = Some x \<longrightarrow> x = L (s@e)" by(simp)
      have "e \<in> lists (set \<Sigma>)" using sameAlphs wformed defe wellformedp_alphabetE2 by auto
      moreover have "s \<in> lists (set \<Sigma>)" proof -
        have "s \<in> lists (set (get_A table))" by(rule wellformedp_alphabetE, rule wformed, rule defs)
        then show ?thesis by(simp add: sameAlphs)
      qed
      ultimately have "s@e \<in> lists (set (get_alphabet M\<^sub>L))" by (simp add: acceptor_alphabet)
      \<comment> \<open> Now, we can show compatibility \<close>
      from L_regular have "accepted_language_eq_p M\<^sub>L \<Sigma> L" by(simp add: minimal_acceptor_p_def)
      then have ***: "output_on M\<^sub>L (s@e) = L (s@e)" proof(rule accepted_language_eq_pE)
      qed(rule \<open>s@e \<in> lists (set (get_alphabet M\<^sub>L))\<close>)
      show  "(output_on M\<^sub>L (s @ e) = x) = (get_T table (s @ e) = Some x)" proof -
        have "(get_T table) (s@e) = Some (output_on M\<^sub>L (s@e))" proof -
          from filled have "\<exists>x. (get_T table) (s@e) = Some x"
              by(rule filledpE2, auto simp only: defs defe)
          then obtain x where defx: "(get_T table) (s@e) = Some x" by(auto)
          from this and ** have "x = L (s@e)" by(simp)
          then show ?thesis by (simp add: "***" defx)
        qed
        then show ?thesis by simp
      qed
    qed
  qed
qed

text \<open> We can now apply this lemma on the minimal acceptor $M_L$ and get that the number of different
       rows is always bounded by N (number of states of the minimal acceptor) \<close>
abbreviation N :: "nat" where " N \<equiv> num_states M\<^sub>L"
lemma num_diff_rows_bounded: 
assumes sameAlphs: "set (get_A table) = set \<Sigma>" and
        filled: "filledp table" and
        wformed: "wellformedp table" and 
        "follows_orcm_p table"
shows "num_diff_rows table \<le> N" proof -
  from assms have "compat_with_p M\<^sub>L table" by(rule follows_orcm_imp_compatibleWith_min_acceptor)
  then show ?thesis using angluin_lemma5 using L_regular filled minimal_acceptor_p_def wformed by auto
qed

text \<open> An alternative view of the previous bound is the following (we'll need it for the second
       termination proof; of lstar-learner-loop)\<close>
lemma num_states_bounded: 
assumes sameAlphs: "set (get_A table) = set \<Sigma>" and
        filled: "filledp table" and
        wformed: "wellformedp table" and follows_orc: "follows_orcm_p table" and
        closed: "closedp table" and
        consistent: "consistentp table"
shows "(num_states \<circ> make_conjecture) table \<le> N" using angluin_lemma5
  using assms follows_orc num_diff_rows_bounded num_states_eq_num_diff_rows by force

subsection \<open> Improvement of the @{text follows_orcm} lemma\<close>

text \<open> We'll also need this lemma, which is an improved variant of "follows conjecture oracle"
       (equality in the conclusion instead of implication). \<close>
lemma filledp_follows_orcm_improved: 
  assumes sameAlphs: "set (get_A table) = set \<Sigma>" and
          filled: "filledp table" and
          wformed: "wellformedp table" and 
          followsOrc: "follows_orcm_p table"
  shows " (\<forall>s \<in> set (row_labels table). \<forall>e \<in> set (get_E table).
          \<forall>x. get_T table (s@e) = Some x \<longleftrightarrow> x = oracleM (s@e))"
proof(rule ballI, rule ballI, rule allI)
  fix s e x
  assume defs: "s \<in> set (row_labels table)" and defe: "e \<in> set (get_E table)"
  show "get_T table (s@e) = Some x \<longleftrightarrow> x = oracleM (s@e)" proof
    \<comment> \<open> This direction is easy (it's the one we already know) \<close>
    assume "get_T table (s @ e) = Some x" then show "x = oracleM (s @ e)" using followsOrc defs defe by(intro follows_orcm_pE, auto)
  next
    \<comment> \<open> For this direction we use filledp \<close>
    assume "x = oracleM (s @ e)" then show "get_T table (s @ e) = Some x" proof -
      from defs defe filled have "\<exists>y. get_T table (s@e) = Some y" using filledpE2 by(auto)
      then obtain y where "get_T table (s@e) = Some y" by(auto)
      from this and followsOrc have "y = oracleM (s@e)" using defs defe by(intro follows_orcm_pE, auto)
      then have "x = y" using \<open>x = oracleM (s @ e)\<close> by(simp)
      then show ?thesis using \<open>get_T table (s@e) = Some y\<close> by(simp)
    qed
  qed
qed

section \<open> Correctness of table access functions \<close>
text \<open> Before we can show correctness of the L* alg, we need termination (correctness is proven via
       termination). Before we can show termination, we need correctness of the helper functions.
       We need to show, for instance that they preserve various invariants needed to use the lemmas
       before \<close>

paragraph \<open> Main invariant \<close>
text \<open> The main invariant that needs to be preserved during execution of the algorithm are the
       preconditions of lemma num-diff-rows-bounded which will allow us to prove termination.
       We also have the 'distinct' conditions to make sure our representation of sets as lists does
       not explode. \<close>
definition lstar_table_inv :: " ('letter,'mlo) observation_table \<Rightarrow> bool" where
  " lstar_table_inv table \<equiv>
          (set (get_A table) = set \<Sigma>) \<and>
          filledp table \<and>
          wellformedp table \<and>
          follows_orcm_p table \<and>
          distinct (get_A table) \<and>
          distinct (get_E table) \<and>
          distinct (get_S table) \<and>
          distinct (get_SA table)"

lemma lstar_table_invI[intro]: 
  "\<lbrakk>(set (get_A table) = set \<Sigma>);
    filledp table; wellformedp table; follows_orcm_p table;
    distinct (get_A table);
    distinct (get_E table);
    distinct (get_S table);
    distinct (get_SA table)\<rbrakk> \<Longrightarrow> lstar_table_inv table"
  unfolding lstar_table_inv_def by(auto)

lemma lstar_table_invE[elim]:
  assumes "lstar_table_inv table"
  shows "(set (get_A table) = set \<Sigma>)" and "filledp table"
     and "wellformedp table" and "follows_orcm_p table"
     and "distinct (get_A table)" and "distinct (get_E table)"
     and "distinct (get_S table)" and "distinct (get_SA table)"
  unfolding lstar_table_inv_def using assms
  by (simp add: lstar_table_inv_def)+

paragraph \<open>Some helpful lemmas using the invariant\<close>

lemma num_states_make_conjecture:
  assumes
    inv: "lstar_table_inv table" and 
    closed: "closedp table" and
    consistent: "consistentp table"
  shows "num_states (make_conjecture table) \<le> num_states M\<^sub>L"
  using inv closed consistent num_states_bounded by(simp add: lstar_table_invE)

lemma make_conjecture_less_states_than:
  assumes
    inv: "lstar_table_inv table" and 
    closed: "closedp table" and
    consistent: "consistentp table"
  shows "make_conjecture table \<sqsubseteq> M\<^sub>L"
proof -
  let ?conj = "make_conjecture table"
  from inv closed consistent have "num_states ?conj \<le> num_states M\<^sub>L"
    by (rule num_states_make_conjecture)
  then have "(num_states M\<^sub>L > num_states ?conj) \<or> (\<exists>f. iso_betw_p f ?conj M\<^sub>L)"
    using angluin_theorem1(2) inv closed consistent
    by (metis L_regular follows_orcm_imp_compatibleWith_min_acceptor lstar_table_invE(1)
        lstar_table_invE(2) lstar_table_invE(3) lstar_table_invE(4) minimal_acceptor_p_wdefD)
  then show ?thesis by(auto simp add: less_states_than_def)
qed

text \<open> A convenient lemma arount oracleC-correct-Some-correct and oracleC-bounded\<close>
lemma oracleC_make_conjecture_properties: 
  assumes inv: "(lstar_table_inv \<circ> fst) s"
    and some: "(oracleC (make_conjecture (fst s))) = Some x"
    and closed: "(closedp \<circ> fst) s" 
    and consistent: "(consistentp \<circ> fst) s"
  shows "x \<in> \<Sigma>star" and "(L x \<noteq> (output_on (make_conjecture (fst s))) x)"
                     and "length x \<le> m_bound"
proof -
  let ?conj = "(make_conjecture (fst s))"
  have 1: "welldefined_dfa_p (make_conjecture (fst s))"
    using \<open>(lstar_table_inv \<circ> fst) s\<close> welldefined_dfa_p_make_conjecture
      lstar_table_inv_def closed consistent by auto
  have 2: "set (get_alphabet (make_conjecture (fst s))) = set \<Sigma>" using \<open>(lstar_table_inv \<circ> fst) s\<close> 
    by (metis lstar_table_invE(3) lstar_table_inv_def angluin_lemma3 closed compat_with_p_same_alphE consistent o_apply)
  have 3: "follows_prop_p (make_conjecture (fst s)) L_prop"
    apply(rule follows_orcm_follows_prop_make_conjecture)
    using inv lstar_table_invE by auto
  have *: "x \<in> \<Sigma>star \<and> (L x \<noteq> (output_on ?conj) x)" using 1 2 3 some
    by(cases s, simp add: oracleC_correct_Some_correct)
  then show "x \<in> \<Sigma>star" by simp
  from * show "L x \<noteq> output_on (make_conjecture (fst s)) x" by simp
  from 1 2 3 show "length x \<le> m_bound" proof(rule oracleC_bounded)
    from inv closed consistent num_states_make_conjecture
    show "num_states (make_conjecture (fst s)) \<le> N"
      by(simp)
  qed(simp add: some)
qed

subsection \<open> The make-initial-table function respects the invariant \<close>

text \<open> We'll need a few lemmas about the range of map updates. \<close>
(* There is also the stronger theorem Map.ran_map_add (but needs more assms) *)
lemma ran_map_add_subseteq: "ran (m1 ++ m2) \<subseteq> ran m1 \<union> ran m2"
  unfolding ran_def by auto

lemma ran_map_of: "ran (map_of ps) \<subseteq> set (map snd ps)" proof(induction ps)
  case Nil
  then show ?case by simp
next
  case (Cons a ps)
  then show ?case unfolding ran_def by auto
qed

lemma ran_map_upds: "ran (map_upds f xs ys) \<subseteq> ran f \<union> set ys" proof -
  have "ran (map_upds f xs ys) \<subseteq> ran f \<union> ran (map_of (rev (zip xs ys)))"
    by (simp add: ran_map_add_subseteq map_upds_def)
  moreover have "ran (map_of (rev (zip xs ys))) \<subseteq> set ys" proof -
    have "set (map snd (rev (zip xs ys))) \<subseteq> set ys" proof(induction ys)
      case Nil
      then show ?case by simp
    next
      case (Cons a ys)
      then show ?case using set_zip_rightD by fastforce
    qed
    then show ?thesis using ran_map_of[of "(rev (zip xs ys))"] by simp
  qed
  ultimately show ?thesis by auto
qed

(* We could technically pass a non-distinct sigma, which could cause havoc *)
lemma make_initial_table_invariant:
  "distinct \<Sigma> \<Longrightarrow> lstar_table_inv (make_initial_table \<Sigma> oracleM)" proof -
  let ?inittbl = "make_initial_table \<Sigma> oracleM"
  let ?queries = "[[]]@[[a]. a <- \<Sigma>]"
  let ?answers = "map oracleM ?queries"
  assume "distinct \<Sigma>"
  show "lstar_table_inv ?inittbl" proof(rule lstar_table_invI)
    show setA: "set (get_A ?inittbl) = set \<Sigma>" by(auto)
  next
    show "filledp ?inittbl" proof(rule filledpI)
      fix s e
      assume defs: "s \<in> set (row_labels ?inittbl)" and
             defe: "e \<in> set (get_E (?inittbl))"
      have setS: "set (get_S ?inittbl) = {[]}" by(auto)
      have setSA: "set (get_SA ?inittbl) = {[a] | a. a \<in> set \<Sigma>}" by(auto)
      have setE: "set (get_E ?inittbl) = {[]}" by(auto)
      from setS setSA have setRL: "set (row_labels ?inittbl) = {[]} \<union>  {[a] | a. a \<in> set \<Sigma>}" by(auto)
      from defs have "(s@e) \<in> set ?queries" using defe by(cases rule: row_labels_cases, auto)
      then have "(s@e) \<in> dom (map_upds Map.empty ?queries ?answers)" by simp
      then show "s @ e \<in> dom (get_T ?inittbl)" by(auto)
    next
      show "\<And>x. x \<in> dom (get_T ?inittbl) \<Longrightarrow>
        \<exists>s\<in>set (row_labels ?inittbl). \<exists>e\<in>set (get_E ?inittbl). x = s @ e" proof -
        fix x
        assume "x \<in> dom (get_T ?inittbl)"
        then have "x \<in> dom (map_upds Map.empty ?queries ?answers)" by simp
        then have "x \<in> set (take (length ?answers) ?queries)" by simp (* Uses Map.dom_map_upds *)
        then have "x \<in> set ?queries" by simp
        then have "x \<in> set (row_labels ?inittbl)" proof(cases "x = []")
          case True
          then show ?thesis
            by (metis fset_union_intro_l list.set_intros(1) make_initial_table.simps observation_table.select_convs(2))
        next
          case False
          then have "x \<in> set (get_SA ?inittbl)" using \<open>x \<in> set ([[]] @ map (\<lambda>a. [a]) \<Sigma>)\<close> by auto
          then show ?thesis by(rule row_labels_SA_I)
        qed
        moreover have "[] \<in> set (get_E ?inittbl)" by simp
        ultimately show "\<exists>s\<in>set (row_labels ?inittbl). \<exists>e\<in>set (get_E ?inittbl). x = s @ e" by auto
      qed
    qed
    show "wellformedp (?inittbl)" proof(rule wellformedpI)
      show "prefix_closed (set (get_S (?inittbl)))" unfolding prefix_closed_def prefixp_def by(simp)
      show "suffix_closed (set (get_E (?inittbl)))" unfolding suffix_closed_def suffixp_def by(simp)
      show "\<forall>s\<in>set (row_labels (?inittbl)). s \<in> lists (set (get_A (?inittbl)))" proof(rule ballI)
        fix s
        assume "s \<in> set (row_labels (?inittbl))"
        then show "s \<in> lists (set (get_A (?inittbl)))" by(cases rule: row_labels_cases, auto)
      qed
    qed(auto)
  next
    show "follows_orcm_p (?inittbl)" proof(rule follows_orcm_pI)
      let ?queries = "[[]]@[[a]. a <- \<Sigma>]"
      let ?answers = "map oracleM ?queries"
      let ?newT = "(map_upds Map.empty ?queries ?answers)"
      fix s e x
      assume defs: "s \<in> set (row_labels (?inittbl))" and
             defe: "e \<in> set (get_E (?inittbl))" and
             "get_T (?inittbl) (s @ e) = Some x"
      \<comment> \<open> We make the def of x more convenient. \<close>
      then have defx: "?newT (s@e) = Some x" by simp
      from defs have "(s@e) \<in> set ?queries" using defe by(cases rule: row_labels_cases, auto simp add: defe)
      then have "?newT (s@e) = Some (oracleM (s@e))" using map_upds_updated by metis
      then show "x = oracleM (s @ e)" using defx by(simp)
    qed
  qed(simp_all add: \<open> distinct \<Sigma> \<close>)
qed

subsection \<open> The 'extend' functions respect the invariant \<close>
text \<open> The main routines's accesses to the obs table are via the extendS and extendE functions.
       Here, we show that these respect the lstar-table-inv above; There is a small caveat
       about wellformedness. Wellformedness cannot be guaranteed by extendS/E, but only by their 
       callers. Thus, we add necessary preconditions to the corresponding lemmas. \<close>

subsection \<open> Properties of safe-lookup \<close>
text \<open> To optimize the queries performed by extendE and extendS we defined the 'safe-lookup'
       function. We show some of its properties. \<close>

lemma safe_lookup_mapf_some: "mf q = Some x \<Longrightarrow> safe_lookup mf sf q = x"
  by(simp add: safe_lookup_def)

lemma safe_lookup_mapf_none: "mf q = None \<Longrightarrow> safe_lookup mf sf q = sf q"
  by(simp add: safe_lookup_def)

lemma safe_lookup_eq_safef: 
  "(\<And>x. mf q = Some x \<Longrightarrow> sf q = x) \<Longrightarrow> safe_lookup mf sf q = sf q"
  by(auto simp add: safe_lookup_def)

(* Just a lifted version of the previous lemma *)
lemma map_safe_lookup_eq_safef: "(\<And>q x. q \<in> set queries \<Longrightarrow> mf q = Some x \<Longrightarrow> sf q = x) \<Longrightarrow> 
  map (safe_lookup mf sf) queries = map sf queries"
  by(auto simp add: safe_lookup_def)

(*
text \<open> We begin by showing a few lemmas about how the extend functions modify the sets S and E.
       We begin by showing them for the underlying lists first since we'll need this properties
       when bounding the size of the observation table later; The same statements on the lists
       converted to sets follow directly. \<close>

text \<open> We also show that the list representation of the finite sets doesn't "explode". Its length
       is the same as the cardinality of the sets. This will be used to bound the size of the tbl.\<close>
*)
subsubsection \<open> ExtendS \<close>

paragraph \<open> Effect on A \<close>

lemma extendS_listA: "(get_A (extendS oracleM table updS)) = (get_A table)"
  unfolding extendS_def by (meson observation_table.select_convs(1))

lemma extendS_distinct_get_A:
  "distinct (get_A table) \<Longrightarrow> distinct (get_A (extendS oracleM table updS))"
  using extendS_listA by simp

paragraph \<open> Effect on E\<close>

text \<open> The extendS function doesn't touch the set E \<close>

lemma extendS_listE: "(get_E (extendS oracleM table updS)) = (get_E table)"
  unfolding extendS_def
  by (meson observation_table.select_convs(4))

lemma extendS_setE: "set (get_E (extendS oracleM table updS)) = set (get_E table)"
  using extendS_listE by simp

text \<open> The list E doesn't contain unnecessary elements (If it didn't before) \<close>
lemma extendS_distinct_get_E:
  "distinct (get_E table) \<Longrightarrow> distinct (get_E (extendS oracleM table updS))"
  by (simp add: extendS_listE) 

paragraph \<open> Effect on S\<close>

text \<open> extendS updates the set S and SA as follows\<close>
lemma extendS_listS: "(get_S (extendS oracleM table updS)) = (get_S table) \<union>\<^sub>f updS"
  unfolding extendS_def by (metis observation_table.select_convs(2))

lemma extendS_setS: "set (get_S (extendS oracleM table updS)) = set (get_S table) \<union> set updS"
  using extendS_listS set_of_fset_union by simp

text \<open> The list S doesn't contain unnecessary elements\<close>
lemma extendS_distinct_get_S:
  "distinct (get_S table) \<Longrightarrow> distinct updS \<Longrightarrow> distinct (get_S (extendS oracleM table updS))"
  by (simp add: distinct_fset_union extendS_listS)

paragraph \<open> Effect on SA\<close>

lemma extendS_setSA: "wellformedp table \<Longrightarrow> x \<in> set (get_SA (extendS oracleM table updS)) \<longleftrightarrow>
  (\<exists>a \<in> set (get_A table). \<exists>s \<in> set (get_S table). x = s@[a]) \<or> (\<exists>a \<in> set (get_A table). \<exists>s \<in> set updS. x = s@[a])"
proof -
  assume wformed: "wellformedp table"
  show "x \<in> set (get_SA (extendS oracleM table updS)) \<longleftrightarrow>
  (\<exists>a \<in> set (get_A table). \<exists>s \<in> set (get_S table). x = s@[a]) \<or> (\<exists>a \<in> set (get_A table). \<exists>s \<in> set updS. x =s@[a])"
  (is "x \<in> ?lhs = (?rhs1 \<or> ?rhs2)")
  proof
    let ?newS = "(get_S table) \<union>\<^sub>f updS"
    assume "x \<in> ?lhs"
    then have "x \<in> set (remdups [s@[a]. s <- ?newS, a <- (get_A table)])" unfolding extendS_def
      by (metis observation_table.select_convs(3))
    \<comment> \<open> Let's split s in a head sh and the tail-letter a\<close>
    then have "\<exists>sh \<in> set ?newS. \<exists>a \<in> set (get_A table). x = sh@[a]" by(auto)
    then obtain s a where defs: "s \<in> set ?newS" and defa: "a \<in> set (get_A table)" and *: "x = s@[a]"
      by(auto)
    \<comment> \<open> Now, we case-split on which set (old S or updS) s belongs to\<close>
    from defs show "(?rhs1 \<or> ?rhs2)" proof(cases rule: fset_union_cases)
      case inLeft
      then show ?thesis using "*" defa by blast
    next
      case inRight
      then show ?thesis using "*" defa by blast
    qed
  next
    let ?newS = "(get_S table) \<union>\<^sub>f updS"
    show "?rhs1 \<or> ?rhs2 \<Longrightarrow> x \<in> ?lhs" proof(erule disjE)
      assume "?rhs1" then have "x \<in> set (remdups [s@[a]. s <- ?newS, a <- (get_A table)])" by(auto)
      then show "x \<in> ?lhs" unfolding extendS_def by (metis observation_table.select_convs(3))
    next
      assume "?rhs2" then have "x \<in> set (remdups [s@[a]. s <- ?newS, a <- (get_A table)])" by(auto)
      then show "x \<in> ?lhs" unfolding extendS_def by (metis observation_table.select_convs(3))
    qed
  qed
qed

lemma setSA_subseteq_extendS_setSA: "wellformedp table \<Longrightarrow> set (get_SA table) \<subseteq> set (get_SA (extendS oracleM table updS))"
proof -
  assume wf: "wellformedp table"
  show "set (get_SA table) \<subseteq> set (get_SA (extendS oracleM table updS))" proof(rule)
    fix x
    assume defx: "x \<in> set (get_SA table)"
    then have  "x \<in> set (row_labels table)" by(simp)
    then have "\<exists>a\<in>set (get_A table). \<exists>s\<in>set (get_S table). x = s @ [a]" using wf unfolding wellformedp_def
      using defx by auto
    then show "x \<in> set (get_SA (extendS oracleM table updS))" using extendS_setSA[OF wf] by(simp)
  qed
qed

lemma extendS_distinct_get_SA:
  "distinct (get_SA (extendS oracleM table updS))" proof -
  have "(get_SA (extendS oracleM table updS)) =
         remdups [s@[a]. s <- (get_S table) \<union>\<^sub>f updS, a <- (get_A table)]"
    unfolding extendS_def by (meson observation_table.select_convs(3))
  then show ?thesis by simp
qed

paragraph \<open> The type of queries extendS will perform\<close>

lemma extendS_queries_iff: 
  "x \<in> set (extendS_queries table updS) \<longleftrightarrow> 
   (\<exists>s \<in> set updS. \<exists>e \<in> set (get_E table). x = s@e) \<or> (\<exists>s \<in> set updS. \<exists>a \<in> set (get_A table). \<exists>e \<in> set (get_E table). x=s@[a]@e)"
proof
  assume "x \<in> set (extendS_queries table updS)"
  then have "x \<in> set ([s@e. s <- updS, e <- (get_E table)] \<union>\<^sub>f [(s@[a]@e). s <- updS, a <- (get_A table), e <- (get_E table)])"
    by(simp only: extendS_queries_def)
  then show "(\<exists>s \<in> set updS. \<exists>e \<in> set (get_E table). x = s@e) \<or> (\<exists>s \<in> set updS. \<exists>a \<in> set (get_A table). \<exists>e \<in> set (get_E table). x=s@[a]@e)"
  proof(cases rule: fset_union_cases)
    case inLeft
    then have "(\<exists>s \<in> set updS. \<exists>e \<in> set (get_E table). x = s@e)" by(auto)
    then show ?thesis by(simp)
  next
    case inRight
    then have "(\<exists>s \<in> set updS. \<exists>a \<in> set (get_A table). \<exists>e \<in> set (get_E table). x=s@[a]@e)" by(auto)
    then show ?thesis by(simp)
  qed
next
  assume "(\<exists>s\<in>set updS. \<exists>e\<in>set (get_E table). x = s @ e) \<or>
    (\<exists>s\<in>set updS. \<exists>a\<in>set (get_A table). \<exists>e\<in>set (get_E table). x = s @ [a] @ e)"
  then show "x \<in> set (extendS_queries table updS)" proof(rule disjE)
    show "\<exists>s\<in>set updS. \<exists>e\<in>set (get_E table). x = s @ e \<Longrightarrow> x \<in> set (extendS_queries table updS)" by(auto simp add: extendS_queries_def)
  next
    assume "\<exists>s\<in>set updS. \<exists>a\<in>set (get_A table). \<exists>e\<in>set (get_E table). x = s @ [a] @ e"
    then have "x \<in> set [(s@[a]@e). s <- updS, a <- (get_A table), e <- (get_E table)]" by(auto)
    then show ?thesis by(auto simp add: extendS_queries_def)
  qed
qed

lemma extendS_answers_eq: "extendS_answers tbl orc updS = map orc (extendS_queries tbl updS)"
  by(simp add: extendS_answers_def)

text \<open> This lemma justifies the optimization we made in extendS-answers-optimized. \<close>
lemma extendS_answers_optimized_eq: "lstar_table_inv tbl \<Longrightarrow>         
  extendS_answers_optimized tbl oracleM updS = map oracleM (extendS_queries tbl updS)" proof -
  assume inv: "lstar_table_inv tbl"
  have "extendS_answers_optimized tbl oracleM updS =
          map (safe_lookup (get_T tbl) oracleM) (extendS_queries tbl updS)"
    by(simp add: extendS_answers_optimized_def)
  also have "... = map oracleM (extendS_queries tbl updS)" proof(rule map_safe_lookup_eq_safef)
    from inv have filled: "filledp tbl" and forc: "follows_orcm_p tbl " by (auto dest: lstar_table_invE)
    show "\<And>q x. \<lbrakk>q \<in> set (extendS_queries tbl updS); get_T tbl q = Some x\<rbrakk>
           \<Longrightarrow> oracleM q = x" proof -
      fix q x
      assume "q \<in> set (extendS_queries tbl updS)" and *: "get_T tbl q = Some x"
      from inv have filled: "filledp tbl" and followsOrc: "follows_orcm_p tbl " by (auto dest: lstar_table_invE)
      from filled and \<open>get_T tbl q = Some x\<close> have
        "\<exists>s \<in> set (row_labels tbl). \<exists>e \<in> set (get_E tbl). q = s@e" by(rule filledpE3)
      then obtain s e where "s \<in> set (row_labels tbl)" and "e \<in> set (get_E tbl)" and "q = s@e"
        by auto
      then have "x = oracleM (s@e)" using follows_orcm_pE * forc by auto
      then show "oracleM q = x" using \<open>q = s@e\<close> by auto
    qed
  qed
  finally show ?thesis .
qed

paragraph \<open> Parts of the invariant \<close>

lemma extendS_alphabet_invariant: "lstar_table_inv table \<Longrightarrow> set (get_A (extendS oracleM table updS)) = set \<Sigma>" proof -
  assume "lstar_table_inv table"
  then have "set (get_A table) = set \<Sigma>" by (elim lstar_table_invE)
  then show ?thesis using extendS_def
    by (metis observation_table.select_convs(1))
qed

lemma extendS_filledp_invariant:
  assumes inv_bef: "lstar_table_inv table"
  shows "filledp (extendS oracleM table updS)" proof -
  \<comment> \<open> These bindings mimic the let bindings in the def of extendS \<close>
  let ?newS = "(get_S table) \<union>\<^sub>f updS"
  let ?queries = "extendS_queries table updS"
  let ?answers = "extendS_answers_optimized table oracleM updS"
  \<comment> \<open> outtable is the return value of extendS \<close>
  let ?outtable = "\<lparr>get_A = (get_A table),
                    get_S = ?newS,
                    get_SA = remdups [s@[a]. s <- ?newS, a <- (get_A table)],
                    get_E = (get_E table),
                    get_T = (map_upds (get_T table) ?queries ?answers)
                    \<rparr>"
  \<comment> \<open> The updated T function \<close>
  let ?newT = "(map_upds (get_T table) ?queries ?answers)"
  \<comment> \<open> Let's also expand the definition of the invariant \<close>
  from \<open>lstar_table_inv table\<close> have
    filled: "filledp table" and
    wformed: "wellformedp table" and
    follows_orc: "follows_orcm_p table" by(auto dest: lstar_table_invE)
  \<comment> \<open> Since this is the extend*S* function, we don't touch E \<close>
  have Eequal: "set (get_E ?outtable) = set (get_E table)" by(simp)

  have "filledp ?outtable" proof(rule filledpI)
    fix s e
    assume defs: "s \<in> set (row_labels ?outtable)" and defe: "e \<in> set (get_E ?outtable)"
    from Eequal and defe have defe2: "e \<in> set (get_E table)" by(simp)
    \<comment> \<open> The main thing is here: We do a case distinction; There are 4 cases:
         s can be in S or SA, s (or the first part without a) can be in the old S or in the update
         If s is in the old S then we can use the inv-bef to conclude.
         If s is in the updS we prove using the map-upds lemmas \<close>
    from defs show "(s@e) \<in> dom (get_T ?outtable)" proof(cases rule: row_labels_cases)
      \<comment> \<open> If s is in S \<close>
      case (S)
      then have "s \<in> set ?newS" by(simp)
      from this show ?thesis proof(cases rule: fset_union_cases)
        \<comment> \<open> If s is in the old set S \<close>
        case (inLeft)
          then have "(s@e) \<in> dom (get_T table)" using filled defe2
            by (simp add: domIff filledpE)
          then have "(s@e) \<in> dom ?newT" by simp
          then show "(s@e) \<in> dom (get_T ?outtable)" by(simp)
      next
        \<comment> \<open> If s is in updS \<close>
        case (inRight)
          then have "(s@e) \<in> set [s@e. s <- updS, e <- (get_E table)]" using inRight defe2 by(auto)
          then have "(s@e) \<in> set ?queries" by(simp add: extendS_queries_def)
          then have "(s@e) \<in> dom ?newT" by (simp add: extendS_answers_optimized_eq[OF inv_bef])
          then show ?thesis by(simp)
      qed
    next
      \<comment> \<open> If s is in SA \<close>
      case (SA)
      \<comment> \<open> Let's split s in a head sh and the tail-letter a\<close>
      then have "\<exists>sh \<in> set ?newS. \<exists>a \<in> set (get_A table). s = sh@[a]" by(auto)
      then obtain sh a where defsh: "sh \<in> set ?newS" and defa: "a \<in> set (get_A table)" and *: "s = sh@[a]"
        by(auto)
      \<comment> \<open> Now, we case-split on which set (old S or updS) sh belongs to\<close>
      from defsh show ?thesis proof(cases rule: fset_union_cases)
        \<comment> \<open> If sh is in the old set S\<close>
        case inLeft
          then have "sh@[a] \<in> set (row_labels table)"
            by (simp add: defa wellformedpE2 wformed)
          then have "(sh@[a]@e) \<in> dom (get_T table)" using filled defe2
            using filledpE2 by fastforce
          then show "(s@e) \<in> dom (get_T ?outtable)" by(simp add: *)
      next
        \<comment> \<open> If sh is in the updS \<close>
        case inRight
          then have "(sh@[a]@e) \<in> set [(s@[a]@e). s <- updS, a <- (get_A table), e <- (get_E table)]"
            using inRight defa defe2 by(auto)
          then have "(sh@[a]@e) \<in> set ?queries" by(simp add: extendS_queries_def)
          then have "(s@e) \<in> dom ?newT" by(simp add: * extendS_answers_optimized_eq[OF inv_bef])
          then show ?thesis by(simp)
      qed
    qed
  next
    \<comment> \<open> The converse; The only elements in the domain of T are the slots of the table.\<close>
    show "\<And>x. x \<in> dom (get_T ?outtable) \<Longrightarrow>
          (\<exists>s \<in> set (row_labels ?outtable). \<exists>e \<in> set (get_E ?outtable). x = s@e)" proof -
      fix x
      assume "x \<in> dom (get_T ?outtable)"
      then have "x \<in> dom (map_upds (get_T table) ?queries ?answers)" by simp
      then have "x \<in> set (take (length ?answers) ?queries) \<union> dom (get_T table)" by simp
      then have "x \<in> (set ?queries \<union> dom (get_T table))" using set_take_subset by fastforce
      \<comment> \<open> We show that both possibilites lead to our thesis (disjE)\<close>
      moreover have "x \<in> set ?queries \<Longrightarrow> 
        (\<exists>s \<in> set (row_labels ?outtable). \<exists>e \<in> set (get_E ?outtable). x = s@e)" proof -
        assume "x \<in> set ?queries"
        then have "x \<in> set ([s@e. s <- updS, e <- (get_E table)] \<union>\<^sub>f [(s@[a]@e). s <- updS, a <- (get_A table), e <- (get_E table)])"
          by(simp add: extendS_queries_def)
        then have "(\<exists>s \<in> set updS. \<exists>e \<in> set (get_E table). x = s@e) \<or> (\<exists>s \<in> set updS. \<exists>e \<in> set (get_E table). \<exists>a \<in> set (get_A table). x = s@[a]@e)"
          by fastforce
        then show "(\<exists>s \<in> set (row_labels ?outtable). \<exists>e \<in> set (get_E ?outtable). x = s@e)"
        proof(rule disjE)
          show "(\<exists>s \<in> set updS. \<exists>e \<in> set (get_E table). x = s@e) \<Longrightarrow> ?thesis" by auto
          show "(\<exists>s \<in> set updS. \<exists>e \<in> set (get_E table). \<exists>a \<in> set (get_A table). x = s@[a]@e) \<Longrightarrow> ?thesis"
          proof -
            assume "(\<exists>s \<in> set updS. \<exists>e \<in> set (get_E table). \<exists>a \<in> set (get_A table). x = s@[a]@e)"
            then obtain s e a where "s \<in> set updS" and "e \<in> set (get_E table)" and "a \<in> set (get_A table)"
              and "x = s@[a]@e" by auto
            then have "s@[a] \<in> set (get_SA ?outtable)" by auto
            moreover have "e \<in> set (get_E ?outtable)" using Eequal \<open>e \<in> set (get_E table)\<close> by auto
            ultimately show ?thesis using \<open>x = s@[a]@e\<close>
              by (metis append.assoc row_labels_SA_I)
          qed
        qed
      qed
      moreover have "x \<in> dom (get_T table) \<Longrightarrow> (\<exists>s \<in> set (row_labels ?outtable). \<exists>e \<in> set (get_E ?outtable). x = s@e)"
      proof -
        assume "x \<in> dom (get_T table)"
        then have "(\<exists>s \<in> set (row_labels table). \<exists>e \<in> set (get_E table). x = s@e)"
          using filled filledpE3 by auto
        then obtain s e where "s \<in> set (row_labels table)" and "e \<in> set (get_E table)" and "x = s@e"
          by auto
        then have "s \<in> set (row_labels ?outtable)" proof(cases rule: row_labels_cases)
          case S
          then show ?thesis by simp
        next
          case SA
          then have "\<exists>s1 \<in> set (get_S table). \<exists>a \<in> set (get_A table). s = s1@[a]"
            using wformed wellformedp_get_SA_E by auto
          then obtain s1 a where "s1 \<in> set (get_S table)" "a \<in> set (get_A table)" "s = s1@[a]"
            by auto
          then have "s1 \<in> set (get_S ?outtable)" by auto
          then have "s1 \<in> set ?newS" by simp
          moreover have "a \<in> set (get_A ?outtable)" by (simp add: \<open>a \<in> set (get_A table)\<close>)
          ultimately have "s1@[a] \<in> set (get_SA ?outtable)" by auto
          then show ?thesis using \<open>s = s1 @ [a]\<close> using row_labels_SA_I by simp
        qed
        moreover have "e \<in> set (get_E ?outtable)" using \<open>e \<in> set (get_E table)\<close> by auto
        ultimately show ?thesis using \<open>x = s@e\<close> by auto
      qed
      ultimately show "(\<exists>s \<in> set (row_labels ?outtable). \<exists>e \<in> set (get_E ?outtable). x = s@e)"
        by auto
    qed
  qed (* End of 'have filled' sub-proof *)
  then show "filledp (extendS oracleM table updS)" by(simp add: Let_def extendS_def)
qed

lemma extendS_follows_orcm_p_invariant:
  assumes inv_bef: "lstar_table_inv table"
  shows "follows_orcm_p (extendS oracleM table updS)" proof -
  \<comment> \<open> These bindings mimic the let bindings in the def of extendS \<close>
  let ?newS = "(get_S table) \<union>\<^sub>f updS"
  let ?queries = "extendS_queries table updS"
  let ?answers = "extendS_answers_optimized table oracleM updS"
  \<comment> \<open> outtable is the return value of extendS \<close>
  let ?outtable = "\<lparr> get_A = (get_A table),
                     get_S = ?newS,
                     get_SA = remdups [s@[a]. s <- ?newS, a <- (get_A table)],
                     get_E = (get_E table),
                     get_T = (map_upds (get_T table) ?queries ?answers)
                     \<rparr>"
  \<comment> \<open> The updated T function \<close>
  let ?newT = "(map_upds (get_T table) ?queries ?answers)"
  \<comment> \<open> Let's also expand the definition of the invariant \<close>
  from \<open>lstar_table_inv table\<close> have
    filled: "filledp table" and
    wformed: "wellformedp table" and
    follows_orc: "follows_orcm_p table" by (auto dest: lstar_table_invE)
  
  have "follows_orcm_p ?outtable" proof(rule follows_orcm_pI)
    fix s e x
    assume defs: "s \<in> set (row_labels ?outtable)" and defe: "e \<in> set (get_E ?outtable)" and
           "get_T ?outtable (s@e) = Some x"
    \<comment> \<open> We make the def of x more convenient. \<close>
    then have defx: "?newT (s@e) = Some x" by simp
    from defs show "x = oracleM (s@e)" proof(cases rule: row_labels_cases)
      case S
      then show ?thesis proof(cases "s@e \<in> set (extendS_queries table updS)")
        \<comment> \<open> If s@e was queried, then we can conclude, since we just used the oracle \<close>
        case True
        then have "?newT (s@e) = Some (oracleM (s@e))"
          by (simp add: map_upds_updated extendS_answers_optimized_eq[OF inv_bef])
        then show ?thesis using defx by auto
      next
        case False
        then have "?newT (s@e) = (get_T table) (s@e)" by simp
        then show ?thesis using follows_orc False S Un_iff defe defx extendS_queries_iff follows_orcm_pE 
          by (metis extendS_def extendS_listE extendS_listS set_of_fset_union)
      qed
    next
      case SA
      then show ?thesis proof(cases "s@e \<in> set (extendS_queries table updS)")
        \<comment> \<open> This is exactly like before \<close>
        case True
        then have "?newT (s@e) = Some (oracleM (s@e))"
          by (simp add: map_upds_updated extendS_answers_optimized_eq[OF inv_bef])
        then show ?thesis using defx by auto
      next
        case False
        then have "s \<notin> set updS" using extendS_queries_iff using defe by fastforce
        then have "s \<in> set (get_SA table)" proof -
          from defs have "s \<in> set (get_SA ?outtable)" using SA by auto
          then have "s \<in> set (get_SA (extendS oracleM table updS))" unfolding extendS_def by meson
          then have "(\<exists>a \<in> set (get_A table). \<exists>so \<in> set (get_S table). s = so@[a]) \<or> (\<exists>a \<in> set (get_A table). \<exists>so \<in> set updS. s = so@[a])"
            using extendS_setSA[OF wformed] by auto
          \<comment> \<open> This second possibility is impossible, otherwise we would have queried about
               @{text "s@e"} \<close>
          moreover have "(\<exists>a \<in> set (get_A table). \<exists>so \<in> set updS. s = so@[a]) \<Longrightarrow> False" proof -
            assume "\<exists>a \<in> set (get_A table). \<exists>so \<in> set updS. s = so@[a]"
            then obtain a so where "a \<in> set (get_A table)" and "so \<in> set updS" and "s = so@[a]" by(auto)
            then have "so@[a]@e \<in> set ?queries" using extendS_queries_iff using defe by fastforce
            then have "(s@e) \<in> set ?queries" using \<open>s = so@[a]\<close> by(simp)
            from this and False show False by(simp)
          qed
          \<comment> \<open> Thus, we have the one on the left \<close>
          ultimately have "(\<exists>a \<in> set (get_A table). \<exists>so \<in> set (get_S table). s = so@[a])" by(auto)
          then obtain a so where "a \<in> set (get_A table)" and "so \<in> set (get_S table)" and "s = so@[a]" by(auto)
          \<comment> \<open> Which implies that s is in the set SA of the original table \<close>
          then show ?thesis using wformed by (simp add: wellformedpE2)
        qed
        then show ?thesis using follows_orc
          using False defe defx follows_orcm_p_def by auto
      qed
    qed
  qed
  then show "follows_orcm_p (extendS oracleM table updS)"
    by (metis extendS_def)
qed

text \<open> For this lemma we need the precondition that the update requested won't cause non
       prefix closedness. We also have a precondition that the update requested is 'sane'. \<close>
lemma extendS_wellformedp_invariant:
  assumes inv_bef: "lstar_table_inv table" and
          updS_wdef: "\<forall>s \<in> set updS. s \<in> lists (set (get_A table))" and
          updS_maintains_closedness: "prefix_closed (set updS) \<or> set updS \<subseteq> set (get_SA table)"
  shows "wellformedp (extendS oracleM table updS)" proof -
  \<comment> \<open> These bindings mimic the let bindings in the def of extendS \<close>
  let ?newS = "(get_S table) \<union>\<^sub>f updS"
  let ?queries = "extendS_queries table updS"
  let ?answers = "extendS_answers_optimized table oracleM updS"
  \<comment> \<open> outtable is the return value of extendS \<close>
  let ?outtable = "\<lparr> get_A = (get_A table),
                     get_S = ?newS,
                     get_SA = remdups [s@[a]. s <- ?newS, a <- (get_A table)],
                     get_E = (get_E table),
                     get_T = (map_upds (get_T table) ?queries ?answers)
                     \<rparr>"
  \<comment> \<open> The updated T function \<close>
  let ?newT = "(map_upds (get_T table) ?queries ?answers)"
  \<comment> \<open> Let's also expand the definition of the invariant \<close>
  from \<open>lstar_table_inv table\<close> have
    filled: "filledp table" and
    wformed: "wellformedp table" and
    follows_orc: "follows_orcm_p table" by (auto dest: lstar_table_invE)

  have "wellformedp ?outtable" proof(rule wellformedpI)
    \<comment> \<open> S is non-empty \<close>
    from wformed show "set (get_S ?outtable) \<noteq> {}"  by (simp add: wellformedp_def)
  next
    \<comment> \<open> S is prefix-closed \<close>
    show "prefix_closed (set (get_S ?outtable))" proof -
      from wformed have *: "prefix_closed (set (get_S table))" by(simp add: wellformedp_def)
      from updS_maintains_closedness have "prefix_closed (set ?newS)" proof(rule disjE)
        assume "prefix_closed (set updS)" then have "prefix_closed ((set (get_S table)) \<union> set updS)"
          using prefix_closed_updateI * by auto
        then show ?thesis by simp
      next
        assume "set updS \<subseteq> set (get_SA table)" then have "prefix_closed ((set (get_S table)) \<union> set updS)"
        proof(intro prefix_closed_update_stepI)
          show "prefix_closed (set (get_S table))" using wformed by(simp add: wellformedp_def)
        next
          show "set updS \<subseteq> set (get_SA table) \<Longrightarrow> {butlast x |x. x \<in> set updS} \<subseteq> set (get_S table)" proof -
            from wformed have *: "(set (get_SA table) = {(s@[a]) | a s. a \<in> set (get_A table) \<and> s \<in> set (get_S table)})"
              by(simp add: wellformedp_def)
            show "{butlast x |x. x \<in> set updS} \<subseteq> set (get_S table)" proof(rule)
              fix x
              assume "x \<in> {butlast x |x. x \<in> set updS}"
              then obtain y where "y \<in> set updS" and "x = butlast y" by(blast)
              then have "y \<in> set (get_SA table)" using \<open>set updS \<subseteq> set (get_SA table)\<close> by(auto)
              from this and * obtain ya ys where "y = ys@[ya]" and "ya \<in> set (get_A table)" and "ys \<in> set (get_S table)" by(blast)
              moreover from this and \<open>x = butlast y\<close> have "x = ys" by(auto)
              ultimately show "x \<in> set (get_S table)" by simp
            qed
          qed
        qed
        then show ?thesis by(simp)
      qed
      then show ?thesis by(simp)
    qed
  next
    \<comment> \<open> E is not empty and suffix-closed \<close>
    show "set (get_E ?outtable) \<noteq> {}" and "suffix_closed (set (get_E ?outtable))" proof -
      have "set (get_E (extendS oracleM table updS)) \<noteq> {}" using extendS_setE wformed by(simp add: wellformedp_def)
      then show "set (get_E ?outtable) \<noteq> {}" by (simp add: extendS_setE)
    next
      have "suffix_closed (set (get_E (extendS oracleM table updS)))" using extendS_setE wformed by(simp add: wellformedp_def)
      then show "suffix_closed (set (get_E ?outtable))" by (simp add: extendS_setE)
    qed
  next
    \<comment> \<open> The set SA has the form required (simple by definition) \<close>
    show "(set (get_SA ?outtable) = {(s@[a]) | a s. a \<in> set (get_A ?outtable) \<and> s \<in> set (get_S ?outtable)})" proof -
      have "set (get_A ?outtable) = set (get_A table)" by(simp)
      then show ?thesis by(auto)
    qed
  next
    \<comment> \<open> A is indeed the alphabet I\<close>
    show "\<forall>s \<in> set (row_labels ?outtable). s \<in> lists (set (get_A ?outtable))" proof(rule ballI)
      fix s
      assume "s \<in> set (row_labels ?outtable)"
      then show "s \<in> lists (set (get_A ?outtable))" proof(cases s rule: row_labels_cases)
        case S
        then show ?thesis
          using updS_wdef wellformedp_alphabetE wformed by fastforce
      next
        case SA
        then have "s \<in> set [s@[a]. s <- ?newS, a <- (get_A table)]" by(auto)
        then obtain so ao where defso: "so \<in> set ?newS" and defao: "ao \<in> set (get_A table)" and *: "s = so@[ao]" by(auto)
        from defso show ?thesis proof(cases so rule: fset_union_cases)
          \<comment> \<open> If so is in the old set S\<close>
          case inLeft
          then show ?thesis using * defao wformed wellformedp_alphabetE by(auto)
        next
          case inRight
          then show ?thesis using defao updS_wdef by (simp add: "*")
        qed
      qed
    qed
  next
    \<comment> \<open> A is indeed the alphabet II \<close>
    show "(\<forall>s \<in> set (get_E ?outtable). s \<in> lists (set (get_A ?outtable)))" using wformed
      by (simp add: wellformedp_alphabetE2)
  qed
  then show ?thesis by (metis extendS_def)
qed

subsubsection \<open> ExtendE \<close>

paragraph \<open> Effect on A \<close>

lemma extendE_listA: "(get_A (extendE oracleM table updE)) = (get_A table)"
  unfolding extendE_def by (meson observation_table.select_convs(1))

lemma extendE_distinct_get_A:
  "distinct (get_A table) \<Longrightarrow> distinct (get_A (extendE oracleM table updS))"
  using extendE_listA by simp

paragraph \<open> Effect on S and SA2 \<close>

text \<open> ExtendE does not change the sets S and SA\<close>
lemma extendE_listS: "(get_S (extendE oracleM table updE)) = (get_S table)"
  unfolding extendE_def by (metis observation_table.select_convs(2))

lemma extendE_setS: "set (get_S (extendE oracleM table updE)) = set (get_S table)"
  using extendE_listS by simp

lemma extendE_listSA: "(get_SA (extendE oracleM table updE)) = (get_SA table)"
  unfolding extendE_def by (metis observation_table.select_convs(3))

lemma extendE_setSA: "set (get_SA (extendE oracleM table updE)) = set (get_SA table)"
  using extendE_listSA by simp

lemma extendE_row_labels: "set (row_labels (extendE oracleM table updE)) = set (row_labels table)"
  by(auto simp add: extendE_setS extendE_setSA)

text \<open> We don't add duplicates \<close>
lemma extendE_distinct_get_S:
  "distinct (get_S table) \<Longrightarrow> distinct (get_S (extendE oracleM table updE))"
  by (simp add: extendE_listS)

lemma extendE_distinct_get_SA:
  "distinct (get_SA table) \<Longrightarrow> distinct (get_SA (extendE oracleM table updE))"
  by (simp add: extendE_listSA)

paragraph \<open> Effect on E \<close>

text \<open> ExtendE changes the set E as follows\<close>
lemma extendE_listE: "(get_E (extendE oracleM table updE)) = (get_E table) \<union>\<^sub>f updE"
  unfolding extendE_def by (metis observation_table.select_convs(4))

lemma extendE_setE: "set (get_E (extendE oracleM table updE)) = set (get_E table) \<union> set updE"
  using extendE_listE by simp

lemma extendE_distinct_get_E:
  "distinct (get_E table) \<Longrightarrow> distinct updE \<Longrightarrow> distinct (get_E (extendE oracleM table updE))"
  by (simp add: distinct_fset_union extendE_listE)

paragraph \<open> The types of queries extendE will perform \<close>

lemma extendE_queries_iff:
  "x \<in> set (extendE_queries table updE) \<longleftrightarrow> (\<exists>s \<in> set (row_labels table). \<exists>e \<in> set updE. x = s@e)"
  unfolding extendE_queries_def by(auto)

lemma extendE_answers_eq:
  "extendE_answers tbl orc updE = map orc (extendE_queries tbl updE)"
  by(simp add: extendE_answers_def)

text \<open> This lemma justifies the optimization we made in extendS-answers-optimized. \<close>
lemma extendE_answers_optimized_eq: "lstar_table_inv tbl \<Longrightarrow>
  extendE_answers_optimized tbl oracleM updE = map oracleM (extendE_queries tbl updE)" proof -
  assume inv: "lstar_table_inv tbl"
  have "extendE_answers_optimized tbl oracleM updE =
          map (safe_lookup (get_T tbl) oracleM) (extendE_queries tbl updE)"
    by(simp add: extendE_answers_optimized_def)
  also have "... = map oracleM (extendE_queries tbl updE)" proof(rule map_safe_lookup_eq_safef)
    from inv have filled: "filledp tbl" and forc: "follows_orcm_p tbl "
      by (auto dest: lstar_table_invE)
    show "\<And>q x. \<lbrakk>q \<in> set (extendE_queries tbl updE); get_T tbl q = Some x\<rbrakk>
           \<Longrightarrow> oracleM q = x" proof -
      fix q x
      assume "q \<in> set (extendE_queries tbl updE)" and *: "get_T tbl q = Some x"
      from inv have filled: "filledp tbl" and followsOrc: "follows_orcm_p tbl "
        by (auto dest: lstar_table_invE)
      from filled and \<open>get_T tbl q = Some x\<close> have
        "\<exists>s \<in> set (row_labels tbl). \<exists>e \<in> set (get_E tbl). q = s@e" by(rule filledpE3)
      then obtain s e where "s \<in> set (row_labels tbl)" and "e \<in> set (get_E tbl)" and "q = s@e"
        by auto
      then have "x = oracleM (s@e)" using follows_orcm_pE * forc by auto
      then show "oracleM q = x" using \<open>q = s@e\<close> by auto
    qed
  qed
  finally show ?thesis .
qed

paragraph \<open> Parts of the invariant \<close>

lemma extendE_alphabet_invariant: "lstar_table_inv table \<Longrightarrow> set (get_A (extendE oracleM table updE)) = set \<Sigma>" proof -
  assume "lstar_table_inv table"
  then have "set (get_A table) = set \<Sigma>" by(elim lstar_table_invE)
  then show ?thesis using extendE_def
    by (metis observation_table.select_convs(1))
qed

lemma extendE_filledp_invariant:
  assumes inv_bef: "lstar_table_inv table"
  shows "filledp (extendE oracleM table updE)" proof -
  let ?newE = "(get_E table) \<union>\<^sub>f updE"
  let ?queries = "extendE_queries table updE"
  let ?answers = "extendE_answers_optimized table oracleM updE"
  \<comment> \<open> outtable is the return value of extendS \<close>
  let ?outtable = "\<lparr> get_A = (get_A table),
                     get_S = (get_S table),
                     get_SA = (get_SA table),
                     get_E = ?newE,
                     get_T = (map_upds (get_T table) ?queries ?answers)
                     \<rparr>"
  \<comment> \<open> The updated T function \<close>
  let ?newT = "(map_upds (get_T table) ?queries ?answers)"
  \<comment> \<open> Let's also expand the definition of the invariant \<close>
  from \<open>lstar_table_inv table\<close> have
    filled: "filledp table" and
    wformed: "wellformedp table" and
    follows_orc: "follows_orcm_p table" by (auto dest: lstar_table_invE)
  \<comment> \<open> Since this is the extend*E* function, we don't touch S or SA \<close>
  have Sequal: "set (get_S ?outtable) = set (get_S table)" by(simp)
  have SAequal: "set (get_SA ?outtable) = set (get_SA table)" by(simp)
  
  have "filledp ?outtable" proof(rule filledpI)
    fix s e
    assume defs: "s \<in> set (row_labels ?outtable)" and defe: "e \<in> set (get_E ?outtable)"
    from Sequal and SAequal and defs have defs2: "s \<in> set (row_labels table)" by(auto)
    from defe have "e \<in> set ((get_E table) \<union>\<^sub>f updE)" by(auto)
    then show "(s@e) \<in> dom (get_T ?outtable)" proof(cases rule: fset_union_cases)
      \<comment> \<open> If the extension e is in the old set E, then we conclude using filled \<close>
      case inLeft
      then show ?thesis using filled defs2 by auto
    next
      \<comment> \<open> If the extension is in the updE, then we have queried it. \<close>
      case inRight
      then have "(s@e) \<in> set ?queries" using defs2 extendE_queries_iff by blast
      then have "(s@e) \<in> dom ?newT" by(auto simp add: extendE_answers_optimized_eq[OF inv_bef])
      then show ?thesis by(simp)
    qed
  next
 \<comment> \<open> The converse; The only elements in the domain of T are the slots of the table\<close>
 \<comment> \<open> Let's begin by showing a helpful fact\<close>
    have extendE_rl: "set (row_labels table) \<subseteq> set (row_labels ?outtable)" proof
      fix s
      assume "s \<in> set (row_labels table)"
      then show "s \<in> set (row_labels ?outtable)" proof(cases rule: row_labels_cases)
        case S
        then show ?thesis by simp
      next
        case SA
        then have "s \<in> set (get_SA ?outtable)" by auto
        then show ?thesis using row_labels_SA_I by simp
      qed
    qed
  \<comment> \<open> We can now show our thesis. \<close>
    show "\<And>x. x \<in> dom (get_T ?outtable) \<Longrightarrow>
          (\<exists>s \<in> set (row_labels ?outtable). \<exists>e \<in> set (get_E ?outtable). x = s@e)" proof -
      fix x
      assume "x \<in> dom (get_T ?outtable)"
      then have "x \<in> dom (map_upds (get_T table) ?queries ?answers)" by simp
      then have "x \<in> set (take (length ?answers) ?queries) \<union> dom (get_T table)" by simp
      then have "x \<in> (set ?queries \<union> dom (get_T table))"
        by (simp add: dom_def extendE_answers_optimized_eq[OF inv_bef])
      \<comment> \<open> We show that both possibilites lead to our thesis (disjE)\<close>
      moreover have "x \<in> set ?queries \<Longrightarrow> 
        (\<exists>s \<in> set (row_labels ?outtable). \<exists>e \<in> set (get_E ?outtable). x = s@e)" proof -
        assume "x \<in> set ?queries"
        then have "x \<in> {(s@e) | s e. s \<in> set (row_labels table) \<and> e \<in> set updE}"
          unfolding extendE_queries_def by auto
        then have "\<exists>s \<in> set (row_labels table). \<exists>e \<in> set updE. x = s@e" by auto
        then obtain s e where "s \<in> set (row_labels table)" "e \<in> set updE" "x = s@e" by auto
        then have "s \<in> set (row_labels ?outtable)" using extendE_rl by simp
        moreover from \<open>e \<in> set updE\<close> have "e \<in> set (get_E ?outtable)" by simp
        ultimately show ?thesis using \<open>x = s@e\<close> by auto
      qed
      moreover have "x \<in> dom (get_T table) \<Longrightarrow> (\<exists>s \<in> set (row_labels ?outtable). \<exists>e \<in> set (get_E ?outtable). x = s@e)"
      proof -
      assume "x \<in> dom (get_T table)"
        then have "(\<exists>s \<in> set (row_labels table). \<exists>e \<in> set (get_E table). x = s@e)"
          using filled filledpE3 by auto
        then obtain s e where "s \<in> set (row_labels table)" and "e \<in> set (get_E table)" and "x = s@e"
          by auto
        then have "s \<in> set (row_labels ?outtable)" using extendE_rl by simp
        moreover have "e \<in> set (get_E ?outtable)" using \<open>e \<in> set (get_E table)\<close> by auto
        ultimately show ?thesis using \<open>x = s@e\<close> by auto
      qed
      ultimately show "(\<exists>s \<in> set (row_labels ?outtable). \<exists>e \<in> set (get_E ?outtable). x = s@e)"
        by auto
    qed
  qed
  then show ?thesis using extendE_def by(simp add: Let_def extendE_def)
qed


lemma extendE_follows_orcm_p_invariant:
  assumes inv_bef: "lstar_table_inv table"
  shows "follows_orcm_p (extendE oracleM table updE)" proof -
  let ?newE = "(get_E table) \<union>\<^sub>f updE"
  let ?queries = "extendE_queries table updE"
  let ?answers = "extendE_answers_optimized table oracleM updE"
  \<comment> \<open> outtable is the return value of extendS \<close>
  let ?outtable = "\<lparr> get_A = (get_A table),
                     get_S = (get_S table),
                     get_SA = (get_SA table),
                     get_E = ?newE,
                     get_T = (map_upds (get_T table) ?queries ?answers)
                     \<rparr>"
  \<comment> \<open> The updated T function \<close>
  let ?newT = "(map_upds (get_T table) ?queries ?answers)"
  \<comment> \<open> Let's also expand the definition of the invariant \<close>
  from \<open>lstar_table_inv table\<close> have
    filled: "filledp table" and
    wformed: "wellformedp table" and
    follows_orc: "follows_orcm_p table" by (auto dest: lstar_table_invE)
  \<comment> \<open> Since this is the extend*E* function, we don't touch S or SA \<close>
  have Sequal: "set (get_S ?outtable) = set (get_S table)" by(simp)
  have SAequal: "set (get_SA ?outtable) = set (get_SA table)" by(simp)

  have "follows_orcm_p ?outtable" proof(rule follows_orcm_pI)
    fix s e x
    assume defs: "s \<in> set (row_labels ?outtable)" and defe: "e \<in> set (get_E ?outtable)" and
           "get_T ?outtable (s@e) = Some x"
    \<comment> \<open> We make the def of x more convenient. \<close>
    then have defx: "?newT (s@e) = Some x" by simp
    from defe have "e \<in> set ((get_E table) \<union>\<^sub>f updE)" by(auto)
    show "x = oracleM (s@e)" proof(cases "s@e \<in> set (extendE_queries table updE)")
    \<comment> \<open> If s@e was queried, then we can conclude, since we just used the oracle \<close>
      case True
      then have "?newT (s@e) = Some (oracleM (s@e))"
        by (simp add: map_upds_updated extendE_answers_optimized_eq[OF inv_bef])
      then show ?thesis using defx by auto
    next
      case False
      then have "?newT (s@e) = (get_T table) (s@e)" by simp
      moreover from \<open>s \<in> set (row_labels ?outtable)\<close> have "s \<in> set (row_labels table)" by(simp)
      ultimately show ?thesis using follows_orc extendE_queries_iff follows_orcm_pE
        by (metis False \<open>e \<in> set (get_E table \<union>\<^sub>f updE)\<close> defx fset_union_cases)
    qed
  qed
  then show ?thesis unfolding extendE_def by meson
qed

text \<open> For this lemma we need the precondition that the update requested won't cause non
       suffix closedness. We also have a precondition that the update requested is 'sane'. \<close>
lemma extendE_wellformedp_invariant:
  assumes inv_bef: "lstar_table_inv table" and
          updE_wdef: "\<forall>e \<in> set updE. e \<in> lists (set (get_A table))" and
          updE_maintains_closedness: "\<forall>e \<in> set updE. tl e \<in> set (get_E table)"
  shows "wellformedp (extendE oracleM table updE)" proof -
  let ?newE = "(get_E table) \<union>\<^sub>f updE"
  let ?queries = "extendE_queries table updE"
  let ?answers = "extendE_answers_optimized table oracleM updE"
  \<comment> \<open> outtable is the return value of extendS \<close>
  let ?outtable = "\<lparr> get_A = (get_A table),
                     get_S = (get_S table),
                     get_SA = (get_SA table),
                     get_E = ?newE,
                     get_T = (map_upds (get_T table) ?queries ?answers)
                     \<rparr>"
  \<comment> \<open> The updated T function \<close>
  let ?newT = "(map_upds (get_T table) ?queries ?answers)"
  \<comment> \<open> Let's also expand the definition of the invariant \<close>
  from \<open>lstar_table_inv table\<close> have
    filled: "filledp table" and
    wformed: "wellformedp table" and
    follows_orc: "follows_orcm_p table" by (auto dest: lstar_table_invE)
  \<comment> \<open> Since this is the extend*E* function, we don't touch S or SA \<close>
  have Aequal: "set (get_A ?outtable) = set (get_A table)" by(simp)
  have Sequal: "set (get_S ?outtable) = set (get_S table)" by(simp)
  have SAequal: "set (get_SA ?outtable) = set (get_SA table)" by(simp)
  have "wellformedp ?outtable" proof(rule wellformedpI)

    \<comment> \<open> We show all subgoals about S and SA trivially since, we dind't touch these sets \<close>
    show "set (get_S ?outtable) \<noteq> {}" using Sequal wformed wellformedp_def by(auto)
    show "prefix_closed (set (get_S ?outtable))" using Sequal wformed wellformedp_def by(auto)
    show "(set (get_SA ?outtable) = {(s@[a]) | a s. a \<in> set (get_A ?outtable) \<and> s \<in> set (get_S ?outtable)})" proof -
      from wformed have "set (get_SA table) = {(s@[a]) | a s. a \<in> set (get_A table) \<and> s \<in> set (get_S table)}"
        by (auto simp add: wellformedp_def)
      then show ?thesis using Aequal Sequal SAequal by(auto)
    qed
    show "(\<forall>s \<in> set (row_labels ?outtable). s \<in> lists (set (get_A ?outtable)))" using Aequal Sequal SAequal wformed
      using wellformedp_alphabetE by auto

    \<comment> \<open> Then, let's show the properties about the updated set E \<close>
    from wformed show "set (get_E ?outtable) \<noteq> {}" by(auto simp add: wellformedp_def) 

    \<comment> \<open> The new E is suffix closed \<close>
    show "suffix_closed (set (get_E ?outtable))" proof -
      have "suffix_closed (set (get_E table) \<union> set updE)" proof(intro suffix_closed_stepI)
        show "suffix_closed (set (get_E table))" using wformed wellformedp_def by auto
        show "\<forall>w\<in>set updE. tl w \<in> set (get_E table) " using updE_maintains_closedness .
      qed
      then show ?thesis by simp
    qed
    show "\<forall>s \<in> set (get_E ?outtable). s \<in> lists (set (get_A ?outtable))"
      using updE_wdef wellformedp_alphabetE2 wformed by fastforce
  qed
  then show ?thesis unfolding extendE_def by meson
qed

subsection \<open> The 'fix' functions respect the invariant \<close>
text \<open> Using the previous lemmas, we can show that the 'fix' functions also respect the invariants.
       (Given the necessary preconditions) \<close>

lemma fix_closedness_prob_invariant:
  assumes inv_bef: "lstar_table_inv table" and
          update_wdef: "s@[a] \<in> set (get_SA table)"
  shows "lstar_table_inv (fix_closedness_prob oracleM (s,a) table)" proof -
  from \<open>lstar_table_inv table\<close> have
    filled: "filledp table" and
    wformed: "wellformedp table" and
    follows_orc: "follows_orcm_p table" by (auto dest: lstar_table_invE)
  from update_wdef and wformed have defs: "s \<in> set (get_S table)"  
    using wellformedpE by fastforce
  from update_wdef and wformed have defa: "a \<in> set (get_A table)"
    using update_wdef wellformedpE(2) wformed by fastforce
  \<comment> \<open> We show the preconditions to the lemmas about extendS above \<close>
  have updS_wdef: "\<forall>s \<in> set [s@[a]]. s \<in> lists (set (get_A table))" using defs defa wformed
    by (simp add: wellformedp_alphabetE)
  from update_wdef have updS_maintains_closedness: "prefix_closed (set [s@[a]]) \<or> set [s@[a]] \<subseteq> set (get_SA table)" by(simp)
  show ?thesis proof(rule lstar_table_invI)
    show "set (get_A (fix_closedness_prob oracleM (s, a) table)) = set \<Sigma>" using extendS_alphabet_invariant[OF inv_bef] by(simp)
  next
    show "filledp (fix_closedness_prob oracleM (s, a) table)" using extendS_filledp_invariant[OF inv_bef] by(simp)
  next
    show "wellformedp (fix_closedness_prob oracleM (s, a) table)"
      using extendS_wellformedp_invariant[OF inv_bef updS_wdef updS_maintains_closedness] by(simp)
  next                                                                                                               
    show "follows_orcm_p (fix_closedness_prob oracleM (s, a) table)"
      using extendS_follows_orcm_p_invariant[OF inv_bef] by(simp)
  next
    show "distinct (get_A (fix_closedness_prob oracleM (s, a) table))"
      using inv_bef extendS_listA by (auto dest: lstar_table_invE)
    show "distinct (get_E (fix_closedness_prob oracleM (s, a) table))"
      using inv_bef extendS_listE by (auto dest: lstar_table_invE)
    show "distinct (get_S (fix_closedness_prob oracleM (s, a) table))"
      using inv_bef extendS_distinct_get_S 
      by (simp add: lstar_table_invE(7))
    show "distinct (get_SA (fix_closedness_prob oracleM (s, a) table))"
      by (simp add: extendS_distinct_get_SA)
  qed
qed

lemma fix_consistency_prob_invariant:
  assumes inv_bef: "lstar_table_inv table" and
          update_wdef_a: "a \<in> set (get_A table)" and
          update_wdef_e: "e \<in> set (get_E table)"
  shows "lstar_table_inv (fix_consistency_prob oracleM (s1,s2,a,e) table)" proof -
  let ?updE = "[a#e]"
  \<comment> \<open> We show the preconditions to the lemmas about extendE \<close>
  have updE_wdef: "\<forall>e \<in> set ?updE. e \<in> lists (set (get_A table))"
    by (metis Cons_in_lists_iff MAT_model_moore.lstar_table_inv_def MAT_model_moore_axioms empty_iff inv_bef list.set(1) set_ConsD update_wdef_a update_wdef_e wellformedp_alphabetE2)
  have updE_maintains_closedness: "\<forall>e \<in> set ?updE. tl e \<in> set (get_E table)" by (simp add: update_wdef_e)
  show ?thesis proof(rule lstar_table_invI)
    show "set (get_A (fix_consistency_prob oracleM (s1,s2,a,e) table)) = set \<Sigma>" using extendE_alphabet_invariant[OF inv_bef] by(simp)
  next
    show "filledp (fix_consistency_prob oracleM (s1,s2,a,e) table)" using extendE_filledp_invariant[OF inv_bef] by(simp)
  next
    show "wellformedp (fix_consistency_prob oracleM (s1,s2,a,e) table)"
      using extendE_wellformedp_invariant[OF inv_bef updE_wdef updE_maintains_closedness] by(simp)
  next                                                                                                               
    show "follows_orcm_p (fix_consistency_prob oracleM (s1,s2,a,e) table)"
      using extendE_follows_orcm_p_invariant[OF inv_bef] by(simp)
  next
    show "distinct (get_A (fix_consistency_prob oracleM (s1, s2, a, e) table))"
       using inv_bef extendE_listA by (auto dest: lstar_table_invE)
    show "distinct (get_E (fix_consistency_prob oracleM (s1, s2, a, e) table))"
      using inv_bef extendE_distinct_get_E by (auto dest: lstar_table_invE)
    show "distinct (get_S (fix_consistency_prob oracleM (s1, s2, a, e) table))"
      using inv_bef extendE_distinct_get_S by (auto dest: lstar_table_invE)
    show "distinct (get_SA (fix_consistency_prob oracleM (s1, s2, a, e) table))"
      using inv_bef extendE_distinct_get_SA by (auto dest: lstar_table_invE)
    qed
qed

subsection \<open> The make-closed/make-consistent functions respect the invariant. \<close>
text \<open> This time we have reached the 'real' caller of the extendS functions (via the fix-functions).
       They get the updates required from 'getConsistency/ClosednessProblem' for which we have already
       proven correctness in the observation table theory. \<close>

lemma make_closed_invariant:
  assumes inv_bef: "lstar_table_inv table" and
          not_closed: "\<not> (closedp table)"
          \<comment> \<open> This function should only be called on a non-closed table (otherwise crashes due to use of 'the')\<close>
  shows "lstar_table_inv (make_closed oracleM table)" proof -
  from \<open>lstar_table_inv table\<close> have
    filled: "filledp table" and
    wformed: "wellformedp table" and
    follows_orc: "follows_orcm_p table" by (auto dest: lstar_table_invE)
  let ?problem = "get_closedness_prob table"
  from not_closed have "\<exists>s a. ?problem = Some (s,a)" using get_closedness_prob_complete by auto
  then obtain s a where defp: "?problem = Some (s,a)" by(auto)
  then have "s \<in> set (get_S table)" and "a \<in> set (get_A table)"
    using get_closedness_prob_correct[OF wformed] by(auto)
  then have "s@[a] \<in> set (get_SA table)" using wformed by (simp add: wellformedpE2)
  then have "lstar_table_inv (fix_closedness_prob oracleM (s,a) table)" by(rule fix_closedness_prob_invariant[OF inv_bef])
  then show ?thesis unfolding make_closed_def using defp by(simp)
qed

lemma make_consistent_invariant:
  assumes inv_bef: "lstar_table_inv table" and
          not_consistent: "\<not> (consistentp table)"
          \<comment> \<open> This function should only be called on a non-consistent table (otherwise crashes due to use of 'the')\<close>
  shows "lstar_table_inv (make_consistent oracleM table)" proof -
  from \<open>lstar_table_inv table\<close> have
    filled: "filledp table" and
    wformed: "wellformedp table" and
    follows_orc: "follows_orcm_p table" by (auto dest: lstar_table_invE)
  let ?problem = "get_consistency_prob table"
  from not_consistent have "\<exists>s1 s2 a e. ?problem = Some (s1,s2,a,e)" using get_consistency_prob_complete by auto
  then obtain s1 s2 a e where defp: "?problem = Some (s1,s2,a,e)" by(auto)
  then have "a \<in> set (get_A table)" and "e \<in> set (get_E table)"
    using get_consistency_prob_correct[OF wformed] by(auto)
  then have "lstar_table_inv (fix_consistency_prob oracleM (s1,s2,a,e) table)" by(rule fix_consistency_prob_invariant[OF inv_bef])
  then show ?thesis unfolding make_consistent_def using defp by(simp)
qed

subsection \<open> The 'learn from counter-examples function' respects the invariant \<close>
text \<open> The last function we'll need to show preserves the invariant is 'learn-from-cexample'.
       This one causes the looping in the main l* loop to stop. \<close>

paragraph \<open> learn-from-cexample preserves the invariant \<close>

lemma learn_from_cexample_invariant:
  assumes inv_bef: "lstar_table_inv table"
          and cexample_wformed: "t \<in> \<Sigma>star"
  shows "lstar_table_inv (learn_from_cexample t oracleM table)" proof -
  from \<open>lstar_table_inv table\<close> have
    alph_sigma: "(set (get_A table) = set \<Sigma>)" and
    filled: "filledp table" and
    wformed: "wellformedp table" and
    follows_orc: "follows_orcm_p table" by(simp_all add: lstar_table_invE)
  \<comment> \<open> Let's prove the preconditions of the extendS lemmas \<close>
  let ?updS = "all_prefixes t"
  from alph_sigma and cexample_wformed have "t \<in> lists (set (get_A table))" by (simp add: def_sigmastar)
  then have updS_wdef: "\<forall>s \<in> set ?updS. s \<in> lists (set (get_A table))" proof(induction t rule: rev_induct)
  case Nil
    then show ?case by(auto)
  next
    case (snoc x xs)
    then show ?case by (simp add: all_prefixes_snoc)
  qed
  have updS_maintains_closedness: "prefix_closed (set ?updS) \<or> set ?updS \<subseteq> set (get_SA table)" proof -
    from prefix_closed_all_prefixes have "prefix_closed (set ?updS)" .
    then show ?thesis by(simp)
  qed
  show ?thesis proof(rule lstar_table_invI)
    show "set (get_A (learn_from_cexample t oracleM table)) = set \<Sigma>" by (simp add: extendS_alphabet_invariant inv_bef)
    show "filledp (learn_from_cexample t oracleM table)" by (simp add: extendS_filledp_invariant inv_bef)
    from updS_wdef updS_maintains_closedness show "wellformedp (learn_from_cexample t oracleM table)" by (simp add: extendS_wellformedp_invariant inv_bef)
    show "follows_orcm_p (learn_from_cexample t oracleM table)" by (simp add: extendS_follows_orcm_p_invariant inv_bef)
  next
    show "distinct (get_A (learn_from_cexample t oracleM table))"
      using inv_bef extendS_listA by (auto dest: lstar_table_invE)
    show "distinct (get_E (learn_from_cexample t oracleM table))"
      using inv_bef extendS_listE by (auto dest: lstar_table_invE)
    show "distinct (get_S (learn_from_cexample t oracleM table))"
      using inv_bef distinct_all_prefixes extendS_distinct_get_S by (auto dest: lstar_table_invE)
    show "distinct (get_SA (learn_from_cexample t oracleM table))"
      by (simp add: extendS_distinct_get_SA)
  qed
qed

subsection \<open> Further properties. \<close>

paragraph \<open> make-consistent\<close>

text \<open> We add at most one element to the set E after make-consistent. \<close>
lemma make_consistent_setE:
  assumes not_consistent: "get_consistency_prob tbl = Some (s1,s2,a,e)"
  shows "set (get_E (make_consistent oracleM tbl)) = set (get_E tbl) \<union> {a#e}"
proof -
  from make_consistent_def have "(make_consistent oracleM tbl) = 
         fix_consistency_prob oracleM (the (get_consistency_prob tbl)) tbl" by simp
  moreover have "... = extendE oracleM tbl [a#e]" by(auto simp add: not_consistent)
  ultimately show ?thesis using extendE_setE by simp
qed

text \<open> make-consistent doesn't change the set S\<close>
lemma make_consistent_setS:
  shows "set (get_S (make_consistent oracleM tbl)) = set (get_S tbl)"
proof -
  from make_consistent_def have "(make_consistent oracleM tbl) = 
         fix_consistency_prob oracleM (the (get_consistency_prob tbl)) tbl" by simp
  moreover have "\<exists>a e. ... = extendE oracleM tbl [a#e]"
    by(cases "the (get_consistency_prob tbl)", auto)
  ultimately obtain a e where "(make_consistent oracleM tbl) = extendE oracleM tbl [a#e]" by auto
  then show ?thesis using extendE_listS by auto
qed

paragraph \<open> make-closed\<close>

text \<open> We add at most one element to S after make-closed.\<close>
lemma make_closed_setS: 
  assumes not_closed: "get_closedness_prob tbl = Some (s,a)"
  shows "set (get_S (make_closed oracleM tbl)) = set (get_S tbl) \<union> {s@[a]}"
proof -
  from make_closed_def have "(make_closed oracleM tbl) = 
         fix_closedness_prob oracleM (the (get_closedness_prob tbl)) tbl" by simp
  moreover have "... = extendS oracleM tbl [s@[a]]" by(auto simp add: not_closed)
  ultimately show ?thesis using extendS_setS by auto
qed

text \<open> make-closed doesn't touch the set E.\<close>
lemma make_closed_setE: "set (get_E (make_closed oracleM tbl)) = set (get_E tbl)"
proof -
  from make_closed_def have "(make_closed oracleM tbl) = 
         fix_closedness_prob oracleM (the (get_closedness_prob tbl)) tbl" by simp
  moreover have "\<exists>s a. ... = extendS oracleM tbl [s@[a]]"
    by(cases "the (get_closedness_prob tbl)", auto)
  ultimately show ?thesis using extendS_setE by auto
qed

paragraph \<open>Learn from counter-example\<close>

text \<open> learn-from-cexample adds all prefixes of t to the set S.\<close>
lemma learn_from_cexample_setS_abstract:                              
  "set (get_S (learn_from_cexample t oracleM tbl)) = set (get_S tbl) \<union> set (all_prefixes t)"
  by (simp add: extendS_setS)

text \<open> Same as the previous lemma, but expands 'all-prefixes' and bakes in the following remark.
       If the table is wellformed (which comes from the main invariant), we know that lambda is already
       in S!\<close>
lemma learn_from_cexample_setS:                              
  assumes inv: "lstar_table_inv tbl" \<comment> \<open> We only need wellformedp in fact. \<close>
  shows
  "set (get_S (learn_from_cexample t oracleM tbl)) = set (get_S tbl) \<union> (set (all_prefixes t) - {[]})"
proof -
  from inv have "wellformedp tbl" by(elim lstar_table_invE)
  then have "[] \<in> set (get_S tbl)" using lambda_in_S by simp
  moreover have "set (get_S (learn_from_cexample t oracleM tbl)) = 
                 set (get_S tbl) \<union> set (all_prefixes t)"
    using learn_from_cexample_setS_abstract by auto
  ultimately show ?thesis by auto
qed
                         
text \<open> learn-from-cexample doesn't touch the set E.\<close>
lemma learn_from_cexample_setE:
  "set (get_E (learn_from_cexample t oracleM tbl)) = set (get_E tbl)"
  using extendS_setE by auto

section \<open> Termination of the L* Algorithm \<close>
text \<open> Using the correctness of the helper functions derived above; we can show that the functions
       being called repeatedly increase the number of different rows of the obs table.
       Since we have shown that the number of distinct rows stayed bounded by some N, this will show
       termination ! \<close>

subsection \<open> Properties of make-cc \<close>

subsubsection \<open> Variant for make-closed \<close>

text \<open> The following helper lemma is required; It says that we don't update values of the T function
       in the set S of the old table. \<close>
lemma get_row_of_extendS_cong:
  assumes
    inv: "lstar_table_inv table" and
    defs: "s \<in> set (row_labels table)" \<comment> \<open> s is in the *old* set of all rows \<close>
  shows "get_row_of table s = get_row_of (extendS oracleM table updS) s"
proof(rule get_row_of_cong)
  from inv have wf: "wellformedp table" by(simp add: lstar_table_invE)
  show "set (get_E table) = set (get_E (extendS oracleM table updS))" using inv
    by (simp add: extendS_setE)
  \<comment> \<open> The idea is to show this lazily via the invariant (by using filled + follows oracle) \<close>
  show "\<forall>e\<in>set (get_E table). get_T table (s @ e) = get_T (extendS oracleM table updS) (s @ e)" proof(rule ballI)
    fix e
    assume defe: "e \<in> set (get_E table)"
    \<comment> \<open> We'll need a few facts...\<close>
    from defs have defs2: "s \<in> set (row_labels (extendS oracleM table updS))" proof(cases rule: row_labels_cases)
      case S
      then show ?thesis by (simp add: extendS_setS)
    next
      case SA
      then show ?thesis using setSA_subseteq_extendS_setSA wf by(auto)
    qed
    from defe have defe2: "e \<in> set (get_E (extendS oracleM table updS))" using extendS_setE by simp
    
    \<comment> \<open> Now we show filledp and followsOrcP for both old and new tables. \<close>
    have "filledp table" using inv by(simp add: lstar_table_invE)
    then obtain x1 where defx1: "get_T table (s @ e) = Some x1"
      using defs defe filledpE2 inv by blast
    have "filledp (extendS oracleM table updS)" using inv
      by (simp add: extendS_filledp_invariant)
    then obtain x2 where defx2: "get_T (extendS oracleM table updS) (s @ e) = Some x2"
     using defs2 defe2 filledpE2 inv by blast
    have "follows_orcm_p table" using inv by(simp add: lstar_table_invE)
    moreover have "follows_orcm_p (extendS oracleM table updS)" using inv
      by (simp add: extendS_follows_orcm_p_invariant)
    
    ultimately show "get_T table (s @ e) = get_T (extendS oracleM table updS) (s @ e)"
      unfolding follows_orcm_p_def using defx1 defx2 defs defe defe2 defs2 by fastforce
  qed
qed

text \<open> Now we generalize the previous lemma up to make-closed \<close>
lemma get_row_of_make_closed_cong:
  assumes
    inv: "lstar_table_inv table" and
    not_closed: "\<not> closedp table" and  \<comment> \<open> We need this to remove the 'the'\<close>
    defs: "s \<in> set (row_labels table)"
  shows "get_row_of table s = get_row_of (make_closed oracleM table) s"
proof -
  from \<open>lstar_table_inv table\<close> have
    wformed: "wellformedp table" by (auto dest: lstar_table_invE)
  let ?problem = "get_closedness_prob table"
  from not_closed have "\<exists>s a. ?problem = Some (s,a)" using get_closedness_prob_complete by auto
  then obtain ps pa where defp: "?problem = Some (ps,pa)" by(auto)
  then have "ps \<in> set (get_S table)" and "pa \<in> set (get_A table)"
    using get_closedness_prob_correct[OF wformed] by(auto)
  then have "get_row_of table s = get_row_of (fix_closedness_prob oracleM (ps,pa) table) s"
    unfolding fix_closedness_prob.simps using defs get_row_of_extendS_cong inv by simp
  then show ?thesis unfolding make_closed_def by (simp add: defp)
qed

lemma make_closed_variant:
  assumes inv_bef: "lstar_table_inv table" and
          not_closed: "\<not> (closedp table)"
          \<comment> \<open> This function should only be called on a non-consistent table (otherwise crashes due to use of 'the')\<close>
  shows "num_diff_rows (make_closed oracleM table) > num_diff_rows table" proof -
  from \<open>lstar_table_inv table\<close> have
    filled: "filledp table" and
    wformed: "wellformedp table" and
    follows_orc: "follows_orcm_p table" by (auto dest: lstar_table_invE)
  let ?problem = "get_closedness_prob table"
  from not_closed have "\<exists>s a. ?problem = Some (s,a)" using get_closedness_prob_complete by auto
  then obtain s a where defp: "?problem = Some (s,a)" by(auto)
  then have *: "(\<forall>x \<in> set (get_S table). \<not> (table \<turnstile> s@[a] \<equiv> x))" and
          defs: "s \<in> set (get_S table)" and
          defa: "a \<in> set (get_A table)" 
    using get_closedness_prob_correct[OF wformed] by(auto)
  from this and wformed have sainSA: "s@[a] \<in> set (get_SA table)" by(auto elim: wellformedpE2)
  from extendS_setS have defS: "set (get_S (make_closed oracleM table)) = set (get_S table) \<union> {s@[a]}"
    unfolding make_closed_def using defp by auto
  from * have **: "get_row_of table (s@[a]) \<notin> {get_row_of table s | s. s \<in> set (get_S table)}"
    using get_row_of_imp_rows_eq_p by auto
  show ?thesis unfolding num_diff_rows_def proof(intro psubset_card_mono)
    show "finite {get_row_of (make_closed oracleM table) s |s. s \<in> set (get_S (make_closed oracleM table))}" by(auto)
    show "{get_row_of table s |s. s \<in> set (get_S table)}
        \<subset> {get_row_of (make_closed oracleM table) s |s. s \<in> set (get_S (make_closed oracleM table))}" 
      (is "?lhs \<subset> ?rhs") proof(rule)
      show "?lhs \<subseteq> ?rhs" using get_row_of_make_closed_cong defS not_closed inv_bef by auto
      show "?lhs \<noteq> ?rhs" proof -
        have "get_row_of (make_closed oracleM table) (s@[a]) \<in> ?rhs" using defS by(auto)
        then have "get_row_of table (s@[a]) \<in> ?rhs" using sainSA inv_bef not_closed get_row_of_make_closed_cong by(auto)
        moreover have "get_row_of table (s@[a]) \<notin> ?lhs" using ** .
        ultimately show ?thesis by(auto)
      qed
    qed
  qed
qed

subsubsection \<open> Variant for make-consistent \<close>

text \<open> Similarly to the get-row-of-extendS-cong, we show this lemma for extendE. It says that we don't
       update the values of the T function which are not in the columns we have just added.\<close>
lemma get_row_of_extendE_cong:
  assumes
    inv: "lstar_table_inv table" and
    defe: "e \<in> set (get_E table)" and \<comment> \<open> e is in the *old* set E \<close>
    defs: "s \<in> set (row_labels table)" \<comment> \<open>s is any row (we don't update rows in extendE)\<close>
  shows "get_row_of table s e = get_row_of (extendE oracleM table updE) s e"
proof(rule get_row_of_cong_app)
  from defe show "e \<in> set (get_E table)" .
  from defe show defe2: "e \<in> set (get_E (extendE oracleM table updE))" using extendE_setE by simp
  \<comment> \<open> Again, we show the next goal lazily via the invariant (by using filled + follows oracle) \<close>
  show "get_T table (s @ e) = get_T (extendE oracleM table updE) (s @ e)" proof -
    \<comment> \<open> We'll need a few facts...\<close>
    from defs have defs2: "s \<in> set (row_labels (extendE oracleM table updE))" using extendE_row_labels by simp
    
    \<comment> \<open> Now we show filledp and followsOrcP for both old and new tables. \<close>
    have "filledp table" using inv by(simp add: lstar_table_invE)
    then obtain x1 where defx1: "get_T table (s @ e) = Some x1"
      using defs defe filledpE2 inv by blast
    have "filledp (extendE oracleM table updE)" using inv
      by (simp add: extendE_filledp_invariant)
    then obtain x2 where defx2: "get_T (extendE oracleM table updE) (s @ e) = Some x2"
     using defs2 defe2 filledpE2 inv by blast
    have "follows_orcm_p table" using inv by(simp add: lstar_table_invE)
    moreover have "follows_orcm_p (extendE oracleM table updE)" using inv
      by (simp add: extendE_follows_orcm_p_invariant)
    ultimately show "get_T table (s @ e) = get_T (extendE oracleM table updE) (s @ e)"
      unfolding follows_orcm_p_def using defx1 defx2 defs defe defe2 defs2 by fastforce
  qed
qed

text \<open> Now we generalize the previous lemma up to make-consistent \<close>
lemma get_row_of_make_consistent_cong_app:
  assumes
    inv: "lstar_table_inv table" and
    not_consistent: "\<not> consistentp table" and
    defe: "e \<in> set (get_E table)" and \<comment> \<open> e is in the *old* set E \<close>
    defs: "s \<in> set (row_labels table)" \<comment> \<open>s is any row (we don't update rows in extendE)\<close>
  shows "get_row_of table s e = get_row_of (make_consistent oracleM table) s e"
proof -
  from \<open>lstar_table_inv table\<close> have wformed: "wellformedp table" by (auto dest: lstar_table_invE)
  let ?problem = "get_consistency_prob table"
  from not_consistent have "\<exists>s a. ?problem = Some (s,a)" using get_consistency_prob_complete by auto
  then obtain ps1 ps2 pa pe where defp: "?problem = Some (ps1,ps2,pa,pe)" by(auto)
  then have "pa \<in> set (get_A table)" and "pe \<in> set (get_E table)"
    using get_consistency_prob_correct[OF wformed] by(auto)
  then have "get_row_of table s e = get_row_of (fix_consistency_prob oracleM (ps1,ps2,pa,pe) table) s e"
    unfolding fix_consistency_prob.simps using defs defe get_row_of_extendE_cong inv by auto
  then show ?thesis unfolding make_consistent_def by (simp add: defe defp)
qed

lemma get_row_of_make_consistent_cong:
  assumes
    inv: "lstar_table_inv table" and
    not_consistent: "\<not> consistentp table" and  \<comment> \<open> We need this to remove the 'the'\<close>
    defs: "s \<in> set (row_labels table)"
    \<comment> \<open> Both row functions are the same when restricted to the setE of the original table (without added columns)\<close>
  shows "get_row_of table s = get_row_of (make_consistent oracleM table) s |` set (get_E table)"
proof(rule ext)
  fix x
  have "(get_row_of table s |` set (get_E table)) x =
         (get_row_of (make_consistent oracleM table) s |` set (get_E table)) x" proof(cases "x \<in> set (get_E table)")
    case True
    then show ?thesis using get_row_of_make_consistent_cong_app
      using assms(3) inv not_consistent by auto
  next
    case False
  then show ?thesis by simp
  qed
  \<comment> \<open> Finally, we can remove the restriction to the set E on the lhs, because the get-row-of function
       ensures the row as a finite func is exactly defined on the set E. \<close>
  then show "(get_row_of table s) x =
             (get_row_of (make_consistent oracleM table) s |` set (get_E table)) x"
    using get_row_of_restrict_setE by auto
qed

lemma make_consistent_variant:
  assumes inv_bef: "lstar_table_inv table" and
          not_consistent: "\<not> (consistentp table)"
          \<comment> \<open> This function should only be called on a non-consistent table (otherwise crashes due to use of 'the')\<close>
  shows "num_diff_rows (make_consistent oracleM table) > num_diff_rows table" proof -
  from \<open>lstar_table_inv table\<close> have
    filled: "filledp table" and
    wformed: "wellformedp table" and
    follows_orc: "follows_orcm_p table" by(auto)
  let ?problem = "get_consistency_prob table"
  from not_consistent have "\<exists>s1 s2 a e. ?problem = Some (s1,s2,a,e)" using get_consistency_prob_complete by auto
  then obtain s1 s2 a e where defp: "?problem = Some (s1,s2,a,e)" by(auto)
  then have "table \<turnstile> s1 \<equiv> s2" and "(get_T) table (s1@[a]@e) \<noteq> get_T table (s2@[a]@e)"
    and defs1: "s1 \<in> set (get_S table)" and defs2: "s2 \<in> set (get_S table)"
    and defa: "a \<in> set (get_A table)" and defe: "e \<in> set (get_E table)"
    using get_consistency_prob_correct[OF wformed] by(auto)
  \<comment> \<open> We obtain a few facts...\<close>
  from extendE_setS have sameS: "set (get_S (make_consistent oracleM table)) = set (get_S table)"
    unfolding make_consistent_def using defp by auto
  from extendE_setE have newE: "set (get_E (make_consistent oracleM table)) = set (get_E table) \<union> {[a]@e}"
    unfolding make_consistent_def using defp by auto
  \<comment> \<open> This proof is a bit tricky; The idea is to show the inequality between cards by building a
       *surjection*. We don't use an injection because we couldn't infer the value of the T function
       on the new column just from the rowLabel function (we would need the row label too).
       To show the inequality is strict, we will prove that the surjection is not a bijection
       (two elements map to the same value) and use the finiteness of both sets to conclude.
       This idea is summarized in the non-bij-surj-card-less lemma. \<close>
  \<comment> \<open> This is the surjection we define; It simply maps a row func of the table with an extended set E
       to a row func of the table without E extended. \<close>
  let ?rows_old = "{get_row_of table s | s. s \<in> set (get_S table)}"
  let ?rows_new = "{get_row_of (make_consistent oracleM table) s | s. s \<in> set (get_S (make_consistent oracleM table))}" 
  have finite_row_old: "finite ?rows_old" by(auto)
  have finite_row_new: "finite ?rows_new" by(auto)
  define f :: " ('letter list, 'mlo) map \<Rightarrow> ('letter list, 'mlo) map " where
    " f row = row |` set (get_E table) " for row
  have "card ?rows_old < card ?rows_new" proof(rule non_bij_surj_card_less)
    show "finite ?rows_new" by(auto)
    \<comment> \<open> We show that f is a surjection \<close>
    show " ?rows_old \<subseteq> f ` ?rows_new " proof
      fix x
      assume "x \<in> {get_row_of table s |s. s \<in> set (get_S table)}"
      then obtain s where "s \<in> set (get_S table)" and "x = get_row_of table s" by(blast)
      then have defs2: "s \<in> set (get_S (make_consistent oracleM table))" using sameS by(simp)
      let ?y = "get_row_of (make_consistent oracleM table) s"
      have defy: "?y \<in> {get_row_of (make_consistent oracleM table) s |s. s \<in> set (get_S (make_consistent oracleM table))}"
        by(auto simp add: defs2)
      then have "f ?y = x" \<comment> \<open> This follows from the get-row-of-make-consistent-cong lemma. \<close>
        by (simp add: \<open>s \<in> set (get_S table)\<close> \<open>x = get_row_of table s\<close> f_def get_row_of_make_consistent_cong inv_bef not_consistent)
      then show "x \<in> f ` ?rows_new" using defy by auto
    qed
    show "get_row_of (make_consistent oracleM table) s1 \<in> ?rows_new" using defs1 sameS by auto
    show "get_row_of (make_consistent oracleM table) s2 \<in> ?rows_new" using defs2 sameS by auto
    show "get_row_of (make_consistent oracleM table) s1 \<noteq> get_row_of (make_consistent oracleM table) s2" proof -
      from \<open>(get_T) table (s1@[a]@e) \<noteq> get_T table (s2@[a]@e)\<close>
      have "(get_T) (make_consistent oracleM table) (s1@[a]@e) \<noteq> get_T (make_consistent oracleM table) (s2@[a]@e)" proof -
        have s1a: "s1@[a] \<in> set (row_labels table)" and s2a: "s2@[a] \<in> set (row_labels table)" using wformed defs1 defs2 defa
          by (simp add: wellformedpE2,simp add: wellformedpE2)
        assume \<open>(get_T) table (s1@[a]@e) \<noteq> get_T table (s2@[a]@e)\<close>
        then have "get_row_of table (s1@[a]) e \<noteq> get_row_of table (s2@[a]) e" by (simp add: defe)
        then have "get_row_of (make_consistent oracleM table) (s1@[a]) e \<noteq> 
                   get_row_of (make_consistent oracleM table) (s2@[a]) e"
          using inv_bef not_consistent s1a s2a defe
                get_row_of_make_consistent_cong_app by (metis)
        then show ?thesis by (metis append.assoc get_row_of.simps)
      qed
      then have "\<not> (make_consistent oracleM table) \<turnstile> s1 \<equiv> s2" using newE by (simp add: rows_eq_p_def)
      then show ?thesis using get_row_of_imp_rows_eq_p by auto
      qed
    show "f (get_row_of (make_consistent oracleM table) s1) = f (get_row_of (make_consistent oracleM table) s2)"
      unfolding f_def proof
      fix x
      show "(get_row_of (make_consistent oracleM table) s1 |` set (get_E table)) x =
            (get_row_of (make_consistent oracleM table) s2 |` set (get_E table)) x " proof(cases "x \<in> set (get_E table)")
        case True
        then have "(get_row_of (make_consistent oracleM table) s1 |` set (get_E table)) x = get_row_of (make_consistent oracleM table) s1 x" by(simp)
        also have "... = get_row_of (make_consistent oracleM table) s2 x" using get_row_of_make_consistent_cong_app
          by (metis True \<open>table \<turnstile> s1 \<equiv> s2\<close> defs1 defs2 fset_union_intro_l inv_bef not_consistent rows_eq_p_imp_get_row_of)
        also have "... = (get_row_of (make_consistent oracleM table) s2 |` set (get_E table)) x" using True by(simp)
        finally show ?thesis .
      next
        case False
        then show ?thesis by(auto)
      qed
    qed
  qed
  then show "num_diff_rows table < num_diff_rows (make_consistent oracleM table)" by(simp add: num_diff_rows_def)
qed

subsubsection \<open> Generalizing to the make-cc-step function \<close>

lemma make_cc_step_invariant:
  assumes inv_bef: "lstar_table_inv table"
  shows "(lstar_table_inv \<circ> fst) (make_cc_step oracleM (table,log))"
  by(auto simp add: inv_bef make_closed_invariant make_consistent_invariant)

lemma make_cc_step_variant:
  assumes inv_bef: "lstar_table_inv table" and
          not_cc: "(\<not> closedp table) \<or> (\<not> consistentp table)"
  shows "(num_diff_rows \<circ> fst) (make_cc_step oracleM (table, log)) > (num_diff_rows \<circ> fst) (table, log)"
  using not_cc inv_bef make_closed_variant make_consistent_variant by simp

subsection \<open> Termination Proof of make-cc \<close>

text \<open> Helper lemma for the termination proof.\<close>
lemma make_cc_termination_step: 
  "\<lbrakk>(lstar_table_inv \<circ> fst) s; case s of (tbl, log) \<Rightarrow> \<not> closedp tbl \<or> \<not> consistentp tbl\<rbrakk>
         \<Longrightarrow> (lstar_table_inv \<circ> fst) (make_cc_step oracleM s) \<and>
             N - (num_diff_rows \<circ> fst) (make_cc_step oracleM s) < N - (num_diff_rows \<circ> fst) s"
proof(cases s)
  case (Pair tbl log)
  assume "(lstar_table_inv \<circ> fst) s" and "case s of (tbl, log) \<Rightarrow> \<not> closedp tbl \<or> \<not> consistentp tbl"
  then have inv: "lstar_table_inv tbl" and not_cc: "\<not> closedp tbl \<or> \<not> consistentp tbl" using Pair by(auto)
  \<comment> \<open> We preserve the invariant in the loop \<close>
  have inv_after: "(lstar_table_inv \<circ> fst) (make_cc_step oracleM (tbl, log))"
    using inv make_cc_step_invariant by(auto)
  \<comment> \<open> We change the variant \<close>
  moreover have "N - (num_diff_rows \<circ> fst) (make_cc_step oracleM (tbl, log)) < N - (num_diff_rows \<circ> fst) (tbl, log)" proof -
    from not_cc have "(num_diff_rows \<circ> fst) (make_cc_step oracleM (tbl, log)) > (num_diff_rows \<circ> fst) (tbl, log)"
      using inv make_cc_step_variant by(simp)
    \<comment> \<open> We have to use te num-diff-rows-bounded thm twice, using the old and new invariants. \<close>
    moreover have "num_diff_rows tbl \<le> N" using num_diff_rows_bounded inv by(simp add: lstar_table_invE)
    moreover have "(num_diff_rows \<circ> fst) (make_cc_step oracleM (tbl, log)) \<le> N" using num_diff_rows_bounded inv_after by(simp add: lstar_table_invE)
    ultimately show ?thesis by(simp)
  qed
  ultimately show "(lstar_table_inv \<circ> fst) (make_cc_step oracleM s) \<and>
           N - (num_diff_rows \<circ> fst)(make_cc_step oracleM s) < N - (num_diff_rows \<circ> fst) s" using Pair by(simp)
qed

lemma make_cc_termination:
  assumes inv_bef: "lstar_table_inv tbl"
  shows "\<exists>newtbl newlog. make_cc oracleM (tbl,log) = Some (newtbl,newlog)"
proof -
  have "\<exists>t. make_cc oracleM (tbl,log) = Some t" unfolding make_cc.simps
    apply(rule measure_while_option_Some[where P = "lstar_table_inv \<circ> fst" and f = "\<lambda>x. N - (num_diff_rows \<circ> fst) x"])
    using inv_bef make_cc_termination_step by auto
  then show ?thesis 
    by simp
qed


subsection \<open> Helper proof rules (makeCC) \<close>
text \<open> Some helpful rules to prove properties of make-cc. \<close>

lemma make_cc_step_cases:
  "\<lbrakk>(\<not> closedp tbl) \<or> (\<not> consistentp tbl);
   (\<not> closedp tbl) \<Longrightarrow> P ((make_closed orc tbl), log_mkcl_call tbl log);
   (closedp tbl \<and> \<not>consistentp tbl) \<Longrightarrow> P ((make_consistent orc tbl), log_mkco_call tbl log)\<rbrakk> \<Longrightarrow>
     P (make_cc_step orc (tbl, log))"
  by auto

text \<open> We'll often need to prove properties about the makeCC loop for which the induction step lemma
       requires the invariant as a precondition. We have shown the loop preserves the invariant,
       so the following rule has this baked inside, and allows the step lemma to use the inv. \<close>
lemma make_cc_rule_with_invariant:                                                      
  assumes 
    initP: "P iter" and
    init_invar: "(lstar_table_inv \<circ> fst) iter" and
    step: "\<And>s. \<lbrakk>P s; case s of (tbl, log) \<Rightarrow> \<not> closedp tbl \<or> \<not> consistentp tbl;
           \<comment> \<open> We can add the invariant as an assumption of the step lemma \<close>     
           (lstar_table_inv \<circ> fst) s\<rbrakk>
         \<Longrightarrow> P (make_cc_step oracleM s)" and
    new: "(make_cc oracleM iter) = Some newIter"
  shows "P newIter"
proof -
  have "P newIter \<and> (lstar_table_inv \<circ> fst) newIter"
  proof(rule while_option_rule[where P = "\<lambda>i. P i \<and> (lstar_table_inv \<circ> fst) i"])
    show "while_option (\<lambda>(tbl,log). \<not>closedp tbl \<or> \<not>consistentp tbl) 
             (make_cc_step oracleM)
             iter = Some newIter" using new by(simp)
    from initP and init_invar show "P iter \<and> (lstar_table_inv \<circ> fst) iter" by simp
    show "\<And>s. \<lbrakk>P s \<and> (lstar_table_inv \<circ> fst) s; case s of (tbl, log) \<Rightarrow> \<not> closedp tbl \<or> \<not> consistentp tbl\<rbrakk>
         \<Longrightarrow> P (make_cc_step oracleM s) \<and>
             (lstar_table_inv \<circ> fst) (make_cc_step oracleM s)" proof -
      fix s
      assume "P s \<and> (lstar_table_inv \<circ> fst) s" and
             not_cc: "case s of (tbl, log) \<Rightarrow> \<not> closedp tbl \<or> \<not> consistentp tbl"
      then have *: "P s" and **: "(lstar_table_inv \<circ> fst) s" by(auto)
      have "P (make_cc_step oracleM s)" using step * ** not_cc by auto
      moreover have "(lstar_table_inv \<circ> fst) (make_cc_step oracleM s)"
      proof(cases s)
        case (Pair table log)
        then show ?thesis using make_cc_step_invariant using "**" by auto
      qed
      ultimately show "P (make_cc_step oracleM s) \<and>
             (lstar_table_inv \<circ> fst) (make_cc_step oracleM s)" by(simp)
    qed
  qed
  then show ?thesis by(simp)
qed

subsection \<open> Further properties of make-cc. \<close>

text \<open> We'll need to prove a few properties that are true *after* the whole makeCC routine has been
       called since makeCC is used as a subroutine later. \<close>

text \<open> The invariant still holds after the whole loop. For this we can use the previously shown
       enhanced proof rule for makeCC to avoid using while-option and the invariant lemma for the
       makeCC step.\<close>
lemma make_cc_invariant:
  assumes inv_bef: " (lstar_table_inv \<circ> fst) it" and
                *: " make_cc oracleM it = Some it2"
  shows "(lstar_table_inv \<circ> fst) it2"
proof(rule make_cc_rule_with_invariant)
  from * show "make_cc oracleM it = Some it2" .
  show "\<And>s. \<lbrakk>(lstar_table_inv \<circ> fst) s;
          case s of (tbl, log) \<Rightarrow> \<not> closedp tbl \<or> \<not> consistentp tbl;
          (lstar_table_inv \<circ> fst) s\<rbrakk>
         \<Longrightarrow> (lstar_table_inv \<circ> fst) (make_cc_step oracleM s)"
    using make_cc_termination_step by blast
  \<comment> \<open> This appears twice, since it's also the property we want to prove (which normally
       differs from the invariant). \<close>
  from inv_bef show "(lstar_table_inv \<circ> fst) it" .
  from inv_bef show "(lstar_table_inv \<circ> fst) it" .
qed

text \<open> The correctness of the loop is trivial once termination is proven since it keeps looping 
       until the table is closed and consistent. \<close>
lemma make_cc_correct:
  assumes inv_bef: " (lstar_table_inv \<circ> fst) it " and
                *: " make_cc oracleM it = Some it2"
  shows "(closedp \<circ> fst) it2" and "(consistentp \<circ> fst) it2" proof -
  from * have "\<not> (\<not>(closedp (fst it2)) \<or> \<not>(consistentp (fst it2)))" using while_option_stop
    by fastforce
  then show "(closedp \<circ> fst) it2" and "(consistentp \<circ> fst) it2" by(auto)
qed

thm while_option_rule

text \<open> It will also be useful to say that the number of different rows can only increase after
       the makeCC loop (the equality is only if the loop does nothing). We show a strict version
       of the lemma first, where we also assume that the table is not cc originally to perform
       at least one loop iteration. \<close>
lemma make_cc_variant_strict:
  assumes inv_bef: "(lstar_table_inv \<circ> fst) iter" and
           not_cc: "\<not> (closedp \<circ> fst) iter \<or> \<not> (consistentp \<circ> fst) iter" and
              new: "make_cc oracleM iter = Some newiter" 
  shows "(num_diff_rows \<circ> fst) iter < (num_diff_rows \<circ> fst) newiter"
proof -
  let ?c = "(make_cc_step oracleM)"
  let ?b = "(\<lambda>(tbl,log). \<not>closedp tbl \<or> \<not>consistentp tbl)"
  from new have "(while_option ?b ?c iter) = Some newiter" by simp
  \<comment> \<open> We unfold one iteration \<close>
  then have *: "(if ?b iter then while_option ?b ?c (?c iter) else Some iter) = Some newiter"
    by (metis (no_types, lifting) while_option_unfold)
  \<comment> \<open> We'll always loop at least once, since the tbl is not cc\<close>
  have "while_option ?b ?c (?c iter) = Some newiter" proof -
    from not_cc have "?b iter" by auto
    then show ?thesis using * by auto
  qed
  \<comment> \<open> We rewrite the while-option as the function itself\<close>
  then have unfolded: "make_cc oracleM (?c iter) = Some newiter" by simp
  \<comment> \<open> Now we can apply the while-option-rule (via our enhanced proof rule)\<close>
  show ?thesis proof(rule make_cc_rule_with_invariant)
    from unfolded show "make_cc oracleM (?c iter) = Some newiter" .
    show "(num_diff_rows \<circ> fst) iter < (num_diff_rows \<circ> fst) (make_cc_step oracleM iter)"
      using inv_bef not_cc make_cc_step_variant by(cases iter, auto)
    show "(lstar_table_inv \<circ> fst) (make_cc_step oracleM iter)"
      using inv_bef make_cc_step_invariant by(cases iter, auto)
    show "\<And>s. \<lbrakk>(num_diff_rows \<circ> fst) iter < (num_diff_rows \<circ> fst) s;
               case s of (tbl, log) \<Rightarrow> \<not> closedp tbl \<or> \<not> consistentp tbl;
               (lstar_table_inv \<circ> fst) s\<rbrakk> \<Longrightarrow>
         (num_diff_rows \<circ> fst) iter < (num_diff_rows \<circ> fst) (make_cc_step oracleM s)"
    proof -
      fix s :: "('letter,'mlo) lstar_iteration"
      assume init_bef: "(num_diff_rows \<circ> fst) iter < (num_diff_rows \<circ> fst) s" and
             loop: "case s of (tbl, log) \<Rightarrow> \<not> closedp tbl \<or> \<not> consistentp tbl" and
             invar_bef: "(lstar_table_inv \<circ> fst) s"
      show "(num_diff_rows \<circ> fst) iter < (num_diff_rows \<circ> fst) (make_cc_step oracleM s)"
      proof(cases s)
        case (Pair tbl log)
        from this and loop have "\<not> closedp tbl \<or> \<not> consistentp tbl" by simp
        then have "(num_diff_rows \<circ> fst) (tbl, log)
                    < (num_diff_rows \<circ> fst) (make_cc_step oracleM (tbl, log))"
          using Pair make_cc_step_variant invar_bef by simp
        moreover have "(num_diff_rows \<circ> fst) iter \<le> (num_diff_rows \<circ> fst) (tbl,log)"
          using Pair init_bef by simp
        ultimately show ?thesis by (simp add: Pair)
      qed
    qed
  qed
qed

lemma make_cc_variant:
  assumes inv_bef: "(lstar_table_inv \<circ> fst) iter" and
              new: "make_cc oracleM iter = Some newiter" 
  shows "(num_diff_rows \<circ> fst) iter \<le> (num_diff_rows \<circ> fst) newiter"
proof(cases "\<not> (closedp \<circ> fst) iter \<or> \<not> (consistentp \<circ> fst) iter")
  \<comment> \<open> If we loop at least once we directly use the previous lemma\<close>
  case True
  then have "(num_diff_rows \<circ> fst) iter < (num_diff_rows \<circ> fst) newiter"
    using assms make_cc_variant_strict by auto
  then show ?thesis by simp
next
  \<comment> \<open> If we don't loop, we have equality\<close>
  case False
  have "make_cc oracleM iter = 
    (if ((\<lambda>(tbl,log). \<not>closedp tbl \<or> \<not>consistentp tbl) iter) then
      (while_option (\<lambda>(tbl,log). \<not>closedp tbl \<or> \<not>consistentp tbl) 
                                                  (make_cc_step oracleM)
                                                  iter)
      else Some iter)" by (simp add: while_option_unfold)
  moreover from False have "\<not>((\<lambda>(tbl,log). \<not>closedp tbl \<or> \<not>consistentp tbl) iter)"
    by(cases iter, simp)
  ultimately have "make_cc oracleM iter = Some iter" by simp
  then have "(num_diff_rows \<circ> fst) iter = (num_diff_rows \<circ> fst) newiter" using new by simp 
  then show ?thesis by simp
qed

subsection \<open> Properties for Termination of lStarLearner \<close>

text \<open> The lStarLearner is a doWhile loop which repeatedly calls make-cc (for which
       we now know properties and termination) and learns from counterexamples given by the conjecture
       oracle.\<close>


text \<open> We summarize "learning from counterexample and making the table CC" as the 'lStarLearnerStep'
       and need to prove its properties and a variant for it. 
       We use the same variant as in the paper; namely the number of states of the conjecture. This is
       similar to the variant above, but the subtelty is that we'll only prove that it increases for a
       "whole" step: Learning from a counterexample AND ALSO restoring closedness and consistency.
       We can't show in general that learning from a counterexample will increase the number of 
       distinct rows in the table; This is only true *after* have have restored closedness and consistency.\<close>

subsubsection \<open> Lemmas about extending the table \<close>

text \<open> One (obvious) fact that we'll need to show is that each new updated table we get is an'
       'extension' of the old tables. We only show that the sets row-labels and E increase and get
        the statement that the T function is extended too by the invariants. \<close>

paragraph \<open> Extending the row-labels (S union SA) \<close>

lemma extendsS_extends_row_labels: "wellformedp tbl \<Longrightarrow> set (row_labels tbl) \<subseteq> set (row_labels (extendS oracleM tbl upd))"
  using extendS_setS setSA_subseteq_extendS_setSA by fastforce

text \<open> This lemma is trivial \<close>
lemma extendsE_extends_row_labels: "set (row_labels tbl) \<subseteq> set (row_labels (extendE oracleM tbl upd))"
  using extendE_row_labels by simp

text \<open> The full invariant is stronger than what we need, but we'll have it in any case. \<close>
lemma make_closed_extends_row_labels:
  assumes inv_bef: "lstar_table_inv tbl" and
          not_closed: "\<not> (closedp tbl)"
  shows "set (row_labels tbl) \<subseteq> set (row_labels (make_closed oracleM tbl))" proof -
  from \<open>lstar_table_inv tbl\<close> have
    wformed: "wellformedp tbl" by(auto)
  let ?problem = "get_closedness_prob tbl"
  from not_closed have "\<exists>s a. ?problem = Some (s,a)" using get_closedness_prob_complete by auto
  then obtain s a where defp: "?problem = Some (s,a)" by(auto)
  have "set (row_labels tbl) \<subseteq> set (row_labels (fix_closedness_prob oracleM (s,a) tbl))"
    using extendsS_extends_row_labels[OF wformed] by(simp)
  moreover have "(fix_closedness_prob oracleM (s,a) tbl) = (make_closed oracleM tbl)" 
    by(auto simp add: Let_def make_closed_def defp)
  ultimately show ?thesis by(simp)
qed

lemma make_consistent_extends_row_labels:
  assumes not_consistent: "\<not> (consistentp tbl)"
  shows "set (row_labels tbl) \<subseteq> set (row_labels (make_consistent oracleM tbl))" proof -
  let ?problem = "get_consistency_prob tbl"
  from not_consistent have "\<exists>s1 s2 a e. ?problem = Some (s1,s2,a,e)" using get_consistency_prob_complete by auto
  then obtain s1 s2 a e where defp: "?problem = Some (s1,s2,a,e)" by(auto)
  have "set (row_labels tbl) \<subseteq> set (row_labels (fix_consistency_prob oracleM (s1,s2,a,e) tbl))"
    using extendsE_extends_row_labels by(simp)
  moreover have "(fix_consistency_prob oracleM (s1,s2,a,e) tbl) = (make_consistent oracleM tbl)" 
    by(auto simp add: Let_def make_consistent_def defp)
  ultimately show ?thesis by(simp)
qed

lemma make_cc_step_extends_row_labels:
  assumes inv_bef: "(lstar_table_inv \<circ> fst) it"
  shows "set ((row_labels \<circ> fst) it) \<subseteq> set ((row_labels \<circ> fst) (make_cc_step oracleM it))"
proof(cases it)
  case (Pair tbl log)
  from this and inv_bef have inv: "lstar_table_inv tbl" by(simp)
  \<comment> \<open> We know that both make-closed and make-consistent extend the row-labels; Let's make a case distinction. \<close>
  then have "set (row_labels tbl) \<subseteq> set ((row_labels \<circ> fst) (make_cc_step oracleM (tbl,log)))" proof(cases "closedp tbl")
    case False
    from make_closed_extends_row_labels show ?thesis using False inv by simp
  next
    case True
    from make_consistent_extends_row_labels show ?thesis using True by simp
  qed
  then show ?thesis using Pair by simp
qed

text \<open> This is just applying induction to the above. \<close>
lemma make_cc_extends_row_labels:
  "(lstar_table_inv \<circ> fst) (tbl,log) \<Longrightarrow> make_cc oracleM (tbl,log) = Some (newtbl,newlog) \<Longrightarrow> set (row_labels tbl) \<subseteq> set (row_labels newtbl)" proof -
  assume inv_bef: "(lstar_table_inv \<circ> fst) (tbl,log)"
  assume "make_cc oracleM (tbl,log) = Some (newtbl,newlog)"
  then have while: "while_option (\<lambda>(tbl,log). \<not>closedp tbl \<or> \<not>consistentp tbl) 
                                                (make_cc_step oracleM)
                                                (tbl,log)
             = Some (newtbl,newlog)" by(simp)
  \<comment> \<open> We prove the induction step. We need to add the piggybacking invariant too, to get preconditions
       for one of the extends lemmas above \<close>
  let ?P = "\<lambda>it. set (row_labels tbl) \<subseteq> set ((row_labels \<circ> fst) it) \<and> (lstar_table_inv \<circ> fst) it"
  let ?b = "\<lambda>(tbl,log). \<not>closedp tbl \<or> \<not>consistentp tbl"
  have step: "\<And>s. \<lbrakk>?P s; ?b s\<rbrakk> \<Longrightarrow> ?P ((make_cc_step oracleM) s)" proof -
    fix it :: "('letter,'mlo) lstar_iteration"
    assume "?b it"
    assume "?P it"
        then have one_step: "set (row_labels tbl) \<subseteq> set ((row_labels \<circ> fst) it)" and inv: "(lstar_table_inv \<circ> fst) it"
      by(auto)
    \<comment> \<open> We use our step lemma above \<close>
    from inv have "set ((row_labels \<circ> fst) it) \<subseteq> set ((row_labels \<circ> fst) ((make_cc_step oracleM) it))"
      by(rule make_cc_step_extends_row_labels)
    from one_step this have "set (row_labels tbl) \<subseteq> set ((row_labels \<circ> fst) ((make_cc_step oracleM) it))" by(auto)
    moreover have "(lstar_table_inv \<circ> fst) ((make_cc_step oracleM) it)"
      using inv \<open>?b it\<close> make_cc_termination_step by(auto) 
    ultimately show "set (row_labels tbl) \<subseteq> set ((row_labels \<circ> fst) (make_cc_step oracleM it)) \<and>
             (lstar_table_inv \<circ> fst) (make_cc_step oracleM it)" by(auto)
  qed
  have "set (row_labels tbl) \<subseteq> set ((row_labels \<circ> fst) (newtbl,newlog))
        \<and> (lstar_table_inv \<circ> fst) (newtbl,newlog)" proof(rule while_option_rule[where ?P = "?P"])
    show " while_option (\<lambda>(tbl, log). \<not> closedp tbl \<or> \<not> consistentp tbl) (make_cc_step oracleM)
           (tbl, log) =  Some (newtbl, newlog)" using while .
    show "\<And>s. \<lbrakk>set (row_labels tbl) \<subseteq> set ((row_labels \<circ> fst) s) \<and> (lstar_table_inv \<circ> fst) s;
          case s of (tbl, log) \<Rightarrow> \<not> closedp tbl \<or> \<not> consistentp tbl\<rbrakk>
         \<Longrightarrow> set (row_labels tbl) \<subseteq> set ((row_labels \<circ> fst) (make_cc_step oracleM s)) \<and>
             (lstar_table_inv \<circ> fst) (make_cc_step oracleM s)" using step .
    show "set (row_labels tbl) \<subseteq> set ((row_labels \<circ> fst) (tbl, log)) \<and> (lstar_table_inv \<circ> fst) (tbl, log)"
      using inv_bef by(auto)
  qed
  then show ?thesis by(auto)
qed

text \<open> Again, here the invariant is stronger than what we need, but avoid opening it later. \<close>
lemma learn_from_cexample_extends_row_labels:
  "lstar_table_inv tbl \<Longrightarrow> set (row_labels tbl) \<subseteq> set (row_labels (learn_from_cexample t oracleM tbl))" proof -
  assume "lstar_table_inv tbl"
  then have "wellformedp tbl" by(simp add: lstar_table_invE)
  then show ?thesis using extendsS_extends_row_labels by simp
qed

paragraph \<open> Extending the set E\<close>

lemma extendsS_extends_E: "set (get_E tbl) \<subseteq> set (get_E (extendS oracleM tbl upd))"
  by (simp add: extendS_setE)

lemma extendsE_extends_E: "set (get_E tbl) \<subseteq> set (get_E (extendE oracleM tbl upd))"
  using extendE_setE by simp

lemma make_closed_extends_E: 
  assumes not_closed: "\<not> (closedp tbl)"
  shows "set (get_E tbl) \<subseteq> set (get_E (make_closed oracleM tbl))" proof -
  let ?problem = "get_closedness_prob tbl"
  from not_closed have "\<exists>s a. ?problem = Some (s,a)" using get_closedness_prob_complete by auto
  then obtain s a where defp: "?problem = Some (s,a)" by(auto)
  have "set (get_E tbl) \<subseteq> set (get_E (fix_closedness_prob oracleM (s,a) tbl))"
    using extendsS_extends_E by(simp)
  moreover have "(fix_closedness_prob oracleM (s,a) tbl) = (make_closed oracleM tbl)"
    by(auto simp add: Let_def make_closed_def defp)
  ultimately show ?thesis by(simp)
qed

lemma make_consistent_extends_E:
  assumes not_consistent: "\<not> (consistentp tbl)"
  shows "set (get_E tbl) \<subseteq> set (get_E (make_consistent oracleM tbl))" proof -
  let ?problem = "get_consistency_prob tbl"
  from not_consistent have "\<exists>s1 s2 a e. ?problem = Some (s1,s2,a,e)" using get_consistency_prob_complete by auto
  then obtain s1 s2 a e where defp: "?problem = Some (s1,s2,a,e)" by(auto)
  have "set (get_E tbl) \<subseteq> set (get_E (fix_consistency_prob oracleM (s1,s2,a,e) tbl))"
    using extendsE_extends_E by(simp)
  moreover have "(fix_consistency_prob oracleM (s1,s2,a,e) tbl) = (make_consistent oracleM tbl)" 
    by(auto simp add: Let_def make_consistent_def defp)
  ultimately show ?thesis by(simp)
qed

lemma make_cc_step_extends_E:
  "set ((get_E \<circ> fst) it) \<subseteq> set ((get_E \<circ> fst) (make_cc_step oracleM it))"
proof(cases it)
  case (Pair tbl log)
  \<comment> \<open> We know that both make-closed and make-consistent extend the row-labels; Let's make a case distinction. \<close>
  then have "set (get_E tbl) \<subseteq> set ((get_E \<circ> fst) (make_cc_step oracleM (tbl,log)))" proof(cases "closedp tbl")
    case False
    from make_closed_extends_E show ?thesis using False by simp
  next
    case True
    from make_consistent_extends_E show ?thesis using True by simp
  qed
  then show ?thesis using Pair by simp
qed

text \<open> This is simpler that the same lemma for row-labels, since we won't need to propagate the invariant. \<close>
lemma make_cc_extends_E:
  "make_cc oracleM (tbl,log) = Some (newtbl,newlog) \<Longrightarrow> set (get_E tbl) \<subseteq> set (get_E newtbl)" proof -
  assume "make_cc oracleM (tbl,log) = Some (newtbl,newlog)"
  then have while: "while_option (\<lambda>(tbl,log). \<not>closedp tbl \<or> \<not>consistentp tbl) 
                                                (make_cc_step oracleM)
                                                (tbl,log)
             = Some (newtbl,newlog)" by(simp)
  \<comment> \<open> We prove the induction step. We need to add the piggybacking invariant too, to get preconditions
       for one of the extends lemmas above \<close>
  let ?P = "\<lambda>it. set (get_E tbl) \<subseteq> set ((get_E \<circ> fst) it)"
  let ?b = "\<lambda>(tbl,log). \<not>closedp tbl \<or> \<not>consistentp tbl"
  have step: "\<And>s. \<lbrakk>?P s; ?b s\<rbrakk> \<Longrightarrow> ?P ((make_cc_step oracleM) s)" proof -
    fix it :: "('letter,'mlo) lstar_iteration"
    assume "?P it"
    then have one_step: "set (get_E tbl) \<subseteq> set ((get_E \<circ> fst) it)" by(auto)
    \<comment> \<open> We use our step lemma above \<close>
     have "set ((get_E \<circ> fst) it) \<subseteq> set ((get_E \<circ> fst) ((make_cc_step oracleM) it))"
      by(rule make_cc_step_extends_E)
    moreover from one_step this have "set (get_E tbl) \<subseteq> set ((get_E \<circ> fst) ((make_cc_step oracleM) it))" by(auto)
    ultimately show "set (get_E tbl) \<subseteq> set ((get_E \<circ> fst) (make_cc_step oracleM it))" by(auto)
  qed
  have "set (get_E tbl) \<subseteq> set ((get_E \<circ> fst) (newtbl,newlog))" proof(rule while_option_rule[where ?P = "?P"])
    from while show "while_option (\<lambda>(tbl,log). \<not>closedp tbl \<or> \<not>consistentp tbl) 
                                                (make_cc_step oracleM)
                                                (tbl,log) = Some (newtbl,newlog)" .
    from step show "\<And>s. \<lbrakk>set (get_E tbl) \<subseteq> set ((get_E \<circ> fst) s); case s of (tbl, log) \<Rightarrow> \<not> closedp tbl \<or> \<not> consistentp tbl\<rbrakk>
         \<Longrightarrow> set (get_E tbl) \<subseteq> set ((get_E \<circ> fst) (make_cc_step oracleM s))" .
    qed(simp)
  then show ?thesis by(simp)
qed

lemma learn_from_cexample_extends_E:
  "set (get_E tbl) \<subseteq> set (get_E (learn_from_cexample t oracleM tbl))"
  by (simp add: extendsS_extends_E)

subsubsection \<open> Properties of extensions of tables. \<close>

text \<open> These helper lemmas will be needed for the following; If a table is an 'extension' of a
       previous one; then a dfa compatible with the extension is also compatible with the old table. \<close>
lemma table_extends_imp_compat_with_p:
  assumes inv: " lstar_table_inv tbl" and
          invnew: " lstar_table_inv newtbl " and
          compnew: " compat_with_p newConj newtbl" and
          extendsRL: " set (row_labels tbl) \<subseteq> set (row_labels newtbl) " and  
          extendsE: " set (get_E tbl) \<subseteq> set (get_E newtbl) "
  shows "compat_with_p newConj tbl"
proof(rule compat_with_pI)
  fix s e x  
  assume "s \<in> set (row_labels tbl)" and "e \<in> set (get_E tbl)"
    then have sInNew: "s \<in> set (row_labels newtbl)" and eInNew: "e \<in> set (get_E newtbl)"
      using extendsRL extendsE lstar_table_invE(2) inv invnew by auto
    then have "output_on newConj (s@e) = x \<longleftrightarrow> (get_T newtbl) (s@e) = Some x" using compnew by(intro compat_with_p_same_funcE)
    also have "... = (oracleM (s@e) = x)" using invnew using eInNew filledp_follows_orcm_improved sInNew
      by (metis lstar_table_invE(1) lstar_table_invE(3) lstar_table_invE(4) MAT_model_moore.lstar_table_invE(2) MAT_model_moore_axioms)
    also have "... = (get_T tbl (s @ e) = Some x)" using inv filledp_follows_orcm_improved defs
      by (metis lstar_table_invE(1) lstar_table_invE(3) lstar_table_invE(4) MAT_model_moore.lstar_table_invE(2) MAT_model_moore_axioms \<open>e \<in> set (get_E tbl)\<close> \<open>s \<in> set (row_labels tbl)\<close>)
    finally show "(output_on newConj (s @ e) = x) = (get_T tbl (s @ e) = Some x)" .
  next
    show "set (get_alphabet newConj) = set (get_A tbl)"
      using lstar_table_invE(1) compat_with_p_same_alphE compnew inv invnew by blast
qed

text \<open> This helper lemma justifies that 'learn-from-cexample' does not update the
       already existing rows in the table. \<close>
lemma get_row_of_learn_from_cexample_cong:
  assumes
    inv: "lstar_table_inv table" and
    defs: "s \<in> set (row_labels table)"
  shows "get_row_of table s = get_row_of (learn_from_cexample t oracleM table) s"
proof -
  from \<open>lstar_table_inv table\<close> have
    wformed: "wellformedp table" by(auto)
  then show "get_row_of table s = get_row_of (learn_from_cexample t oracleM table) s"
    using defs get_row_of_extendS_cong inv by simp
qed

text \<open> We'll need this fact later on;
       Note that the non-strict inequality is exactly why we couldn't directly use the number of 
       different rows as a variant ! \<close>
lemma learn_from_cexample_num_diff_rows:
  assumes inv_bef: "lstar_table_inv tbl"
         and t_wf: "t \<in> \<Sigma>star"
   shows "num_diff_rows (learn_from_cexample t oracleM tbl) \<ge> num_diff_rows tbl"
proof -
  let ?newtbl = "(learn_from_cexample t oracleM tbl)"
  let ?tblrows = "{get_row_of tbl s | s. s \<in> set (get_S tbl)}"
  let ?newtblrows = "{get_row_of ?newtbl s | s. s \<in> set (get_S ?newtbl)}"
  have *: "set (get_S tbl) \<subseteq> set (get_S ?newtbl)" by (simp add: extendS_setS)
  have inclusion: "?tblrows \<subseteq> ?newtblrows" proof
    fix x
    assume "x \<in> ?tblrows"
    then obtain s where defs1: "s \<in> set (get_S tbl)" and defs2: "x = get_row_of tbl s" by auto
    \<comment> \<open> We show that x is in the ?newtblrows \<close>
    \<comment> \<open> First, the s on which x is based is also in the S of ?newtbl\<close>
    from * have one: "s \<in> set (get_S ?newtbl)" using defs1 by auto
    \<comment> \<open> Then, the row of x is also a row of ?newtbl\<close>
    from defs1 have "s \<in> set (row_labels tbl)" by auto
    then have two: "get_row_of tbl s = get_row_of ?newtbl s"
      by (rule get_row_of_learn_from_cexample_cong[OF inv_bef])
    \<comment> \<open> One and two allow us to conclude that x is in the rhs \<close>
    from one and two show "x \<in> ?newtblrows" using defs2 by auto
  qed
  have "card ?tblrows \<le> card ?newtblrows" proof(rule card_mono)
    show "finite ?newtblrows" by simp
    from inclusion show "?tblrows \<subseteq> ?newtblrows" .
  qed
  then show ?thesis unfolding num_diff_rows_def .
qed

subsection \<open> Termination of the lstar-learner-loop \<close>

lemma lstar_learner_step_invariant:
  assumes inv_bef: "lstar_table_inv tbl" and
             t_wf: "t \<in> \<Sigma>star"
  shows "(lstar_table_inv \<circ> fst) (lstar_learner_step oracleM t (tbl,log))"
proof -
  from inv_bef t_wf have *: "lstar_table_inv (learn_from_cexample t oracleM tbl)" by(rule learn_from_cexample_invariant)
  then have "\<exists>newtbl newlog. make_cc oracleM (learn_from_cexample t oracleM tbl, log_lece_call tbl log) = Some (newtbl, newlog)"
    by(rule make_cc_termination)
  then obtain newtbl newlog where **: "make_cc oracleM (learn_from_cexample t oracleM tbl, log_lece_call tbl log) = Some (newtbl, newlog)" by auto
  from * have "(lstar_table_inv \<circ> fst) (learn_from_cexample t oracleM tbl, log_lece_call tbl log)" by simp
  from this ** make_cc_invariant have "(lstar_table_inv \<circ> fst) (newtbl, newlog)" by blast
  then show ?thesis using ** by simp
qed

text \<open> lstar-learner-step always leaves the table closed and consistent (ready for a new conjecture)\<close>
lemma lstar_learner_step_closed_consistent:
  assumes inv_bef: "lstar_table_inv tbl" and
             t_wf: "t \<in> \<Sigma>star"
  shows "(closedp \<circ> fst) (lstar_learner_step oracleM t (tbl,log))" and "(consistentp \<circ> fst) (lstar_learner_step oracleM t (tbl,log))"
proof -
  let ?midtbl = "(learn_from_cexample t oracleM tbl)"
  from inv_bef t_wf learn_from_cexample_invariant have inv_mid: " lstar_table_inv ?midtbl "
    by simp
  then have "\<exists>newtbl newlog. make_cc oracleM (?midtbl, log_lece_call tbl log) = Some (newtbl,newlog)"
    using make_cc_termination by auto
  then obtain newtbl newlog where *: "make_cc oracleM (?midtbl, log_lece_call tbl log) = Some (newtbl,newlog)"
    by(auto)
  moreover from inv_mid have inv_midl: "(lstar_table_inv \<circ> fst) (?midtbl, log_lece_call tbl log)" by(simp)
  from inv_midl * have "(closedp \<circ> fst) (newtbl,newlog)" by(rule make_cc_correct)
  moreover from inv_midl * have "(consistentp \<circ> fst) (newtbl,newlog)" by(rule make_cc_correct)
  ultimately show "(closedp \<circ> fst) (lstar_learner_step oracleM t (tbl,log))" and "(consistentp \<circ> fst) (lstar_learner_step oracleM t (tbl,log))"
    using * by auto
qed

lemma lstar_learner_step_variant:
  assumes inv_bef: "lstar_table_inv tbl" and
       closed_bef: "closedp tbl" and
   consistent_bef: "consistentp tbl" and
             t_wf: "t \<in> \<Sigma>star" and
             t_ce: "L t \<noteq> (output_on (make_conjecture tbl)) t" \<comment> \<open> t is indeed a counter-example\<close>
  shows "(num_states \<circ> make_conjecture \<circ> fst) (lstar_learner_step oracleM t (tbl,log)) > (num_states \<circ> make_conjecture \<circ> fst) (tbl,log)"
proof -
  from inv_bef t_wf have *: "lstar_table_inv (learn_from_cexample t oracleM tbl)" by(rule learn_from_cexample_invariant)
  then have inv_midl: "(lstar_table_inv \<circ> fst) (learn_from_cexample t oracleM tbl, log_lece_call tbl log)" by(simp)
  from * have "\<exists>newtbl newlog. make_cc oracleM (learn_from_cexample t oracleM tbl, log_lece_call tbl log) = Some (newtbl, newlog)"
    by(rule make_cc_termination)
  then obtain newtbl newlog where **: "make_cc oracleM (learn_from_cexample t oracleM tbl, log_lece_call tbl log) = Some (newtbl, newlog)"
    by(auto)
  from inv_midl and this have inv_aftl: "(lstar_table_inv \<circ> fst) (newtbl,newlog)" by(rule make_cc_invariant)
  then have inv_aft: "lstar_table_inv newtbl" by(auto)
  from inv_midl have inv_mid: "lstar_table_inv (learn_from_cexample t oracleM tbl)" by(simp)
  let ?oldConj = "make_conjecture tbl"
  let ?newConj = "make_conjecture newtbl"
  \<comment> \<open> The newtbl is closed and consistent \<close>
  have closed_aft: "closedp newtbl" and consistent_aft: "consistentp newtbl" using make_cc_correct[OF inv_midl **] by(auto)
  \<comment> \<open> Now, we show that t is not classified correctly by table (since it was a counter example) but *will*
       be classified correctly in newtbl (since t is in tbl's S set and newtbl follows the invariant).
       Then, we'll show that the number of states between the conjecture of table and newtbl increases \<close>
  \<comment> \<open> First, let's use Lemma3 to show that both conjecture automata are compatible with their respective
       tables T functions. \<close>
  from angluin_lemma3 have comp_old: "compat_with_p ?oldConj tbl" using inv_bef closed_bef consistent_bef
    using lstar_table_invE(3) lstar_table_invE(4) by (auto dest: lstar_table_invE)
  from angluin_lemma3 have comp_new: "compat_with_p ?newConj newtbl" using inv_aftl closed_aft consistent_aft
    using lstar_table_invE(3) lstar_table_invE(4) by (auto dest: lstar_table_invE)
  \<comment> \<open> We'll need to argue that the newtbl 'extends' the old table; Our lemmas about this only require us to
       show the row-labels and columns are contained in the update. \<close>
  have extendsRL: "set (row_labels tbl) \<subseteq> set (row_labels newtbl)" 
    using learn_from_cexample_extends_row_labels make_cc_extends_row_labels **
          inv_bef inv_midl by blast
  have extendsE: "set (get_E tbl) \<subseteq> set (get_E newtbl)" 
    using learn_from_cexample_extends_E make_cc_extends_E **
    by (metis extendS_setE learn_from_cexample.simps)
  \<comment> \<open> Since newtbl extends tbl we can use compatibility with newTable to get compatiblity with tbl \<close>
  have comp_new_old: "compat_with_p ?newConj tbl" proof(rule table_extends_imp_compat_with_p)
    show "lstar_table_inv tbl" and "lstar_table_inv newtbl" by(auto simp add: inv_bef inv_aft)
    from comp_new show "compat_with_p (make_conjecture newtbl) newtbl" .
    from extendsRL show "set (row_labels tbl) \<subseteq> set (row_labels newtbl)" .
    from extendsE show "set (get_E tbl) \<subseteq> set (get_E newtbl)" .
  qed
  from welldefined_dfa_p_make_conjecture have wdef_old: "welldefined_dfa_p ?oldConj" using inv_bef closed_bef consistent_bef
    by (auto simp add: welldefined_dfa_p_make_conjecture dest: lstar_table_invE)
  from welldefined_dfa_p_make_conjecture have wdef_new: "welldefined_dfa_p ?newConj" using inv_aft closed_aft consistent_aft
    by (auto simp add: welldefined_dfa_p_make_conjecture dest: lstar_table_invE)

  \<comment> \<open> Now, our goal is to use Theorem 1\<close>
  thm angluin_theorem1
  from inv_bef closed_bef consistent_bef comp_new_old wdef_new
  have "num_states ?oldConj < num_states ?newConj \<or> (\<exists>f. iso_betw_p f ?oldConj ?newConj)" by(intro angluin_theorem1,auto)
  
  \<comment> \<open> Furthermore, an isomorphism is impossible (since the new conjecture is correct on t unlike the old one \<close>
  moreover have "\<exists>f. iso_betw_p f ?oldConj ?newConj \<Longrightarrow> False" proof -
    assume "\<exists>f. iso_betw_p f ?oldConj ?newConj"
    then obtain f where deff: "iso_betw_p f ?oldConj ?newConj" by(auto)
    then have iso: "output_on ?oldConj t = output_on ?newConj t" proof(intro iso_betw_p_imp_eq_acceptsP)
      show "t \<in> lists (set (get_alphabet (make_conjecture tbl)))" using t_wf 
        by (simp add: def_sigmastar inv_bef make_conjecture_def lstar_table_invE)
      from deff show "iso_betw_p f ?oldConj ?newConj" .
    qed(simp add: wdef_old)
    have t_in_newtbl: "t \<in> set (row_labels newtbl)" proof -
      have "set (all_prefixes t) \<subseteq> set (get_S (learn_from_cexample t oracleM tbl))" using extendS_setS
        by simp
      moreover from t_in_all_prefixes_t have "t \<in> set (all_prefixes t)" .
      ultimately have "t \<in> set (row_labels (learn_from_cexample t oracleM tbl))" by(auto)
      then show ?thesis using **
        make_cc_extends_row_labels inv_bef inv_midl by blast
    qed
    have accepts_eq_inL: "output_on ?newConj t = L t" proof -
      let ?x = "output_on ?newConj t"
      have "(get_T newtbl (t @ []) = Some (output_on (make_conjecture newtbl) t))"
        using compat_with_p_same_funcE[of "?newConj" "newtbl" t "[]" "?x"]
        lstar_table_invE(4) comp_new inv_aft t_in_newtbl by(auto dest: lstar_table_invE)
      then show ?thesis by (metis lstar_table_invE(3) lstar_table_invE(4)
            append_self_conv follows_orcm_pE inv_aft lambda_in_E oracleM_correct t_in_newtbl)
    qed

    \<comment> \<open> Now we can show our contradiction !\<close>
    from t_ce have "(output_on ?oldConj) t \<noteq> L t" by simp
    moreover have "(output_on ?oldConj) t = L t"
      using accepts_eq_inL iso t_ce by auto
    ultimately show False by simp
  qed
  \<comment> \<open> Thus, we must have increased the number of states (our variant !)\<close>
  ultimately have "num_states ?newConj > num_states ?oldConj" by(auto)
  then show ?thesis using ** by(simp)
qed

lemma lstar_learner_loop_termination:
  assumes inv_bef: "lstar_table_inv tbl"
  shows "\<exists>newtbl newlog. lstar_learner_loop oracleM oracleC (tbl,log) = Some (newtbl,newlog)" proof -
  \<comment> \<open> The P is mainly the invariant, but we'll also need closedness and consistency maintained 
       (which the step function of course does !)\<close>
  let ?P = "\<lambda>it. ((lstar_table_inv \<circ> fst) it \<and> (closedp \<circ> fst) it \<and> (consistentp \<circ> fst) it)"
  \<comment> \<open> The variant is the number of states of the conjecture automaton \<close>
  let ?f = "\<lambda>x. N - (num_states \<circ> make_conjecture \<circ> fst) x"
  have "\<exists>t. lstar_learner_loop oracleM oracleC (tbl,log) = Some t"
    unfolding lstar_learner_loop.simps
    valued_predicate_while_option_def
  proof(rule measure_while_option_Some[where P = ?P and f = ?f])
    fix s :: "('letter, 'mlo) lstar_iteration"
    assume invP: "(lstar_table_inv \<circ> fst) s \<and> (closedp \<circ> fst) s \<and> (consistentp \<circ> fst) s"
       and loop: "(oracleC \<circ> make_conjecture \<circ> fst) s \<noteq> None"
    
    let ?stepbody = "lstar_learner_step oracleM (the ((oracleC \<circ> make_conjecture \<circ> fst) s))"
    let ?body = "?stepbody s"
    from invP have inv: "(lstar_table_inv \<circ> fst) s"
              and closed: "(closedp \<circ> fst) s"
              and consistent: "(consistentp \<circ> fst) s" by(auto)
    show "?P ?body \<and> ?f ?body < ?f s" proof(cases s)
      case (Pair tbl log)
      from this and loop obtain t where deft: "(oracleC \<circ> make_conjecture \<circ> fst) s = Some t" by(blast)
      let ?t = "the ((oracleC \<circ> make_conjecture \<circ> fst) s)"                                                      
      have "?t \<in> \<Sigma>star \<and> (L ?t \<noteq> (output_on (make_conjecture tbl)) ?t)" proof(intro oracleC_correct_Some_correct)
        show "oracleC (make_conjecture tbl) = Some (the ((oracleC \<circ> make_conjecture \<circ> fst) s))" 
          using deft by (simp add: Pair deft)
        show "welldefined_dfa_p (make_conjecture tbl)" using Pair invP welldefined_dfa_p_make_conjecture
          by (auto simp add: welldefined_dfa_p_make_conjecture dest: lstar_table_invE)
        show "set (get_alphabet (make_conjecture tbl)) = set \<Sigma>"
          using Pair angluin_lemma3 compat_with_p_same_alphE invP lstar_table_invE
          by (metis o_apply prod.sel(1))
        show "follows_prop_p (make_conjecture tbl) L_prop" 
          using follows_orcm_follows_prop_make_conjecture
          by (metis MAT_model_moore.lstar_table_invE(2) MAT_model_moore.lstar_table_invE(3)
              MAT_model_moore.lstar_table_invE(4) MAT_model_moore_axioms Pair fst_conv invP
              lstar_table_invE(1) o_apply)
      qed
      then have t_wf: "?t \<in> \<Sigma>star" and
                t_ce: "L ?t \<noteq> (output_on (make_conjecture tbl)) ?t" by(auto)
      from Pair and inv have inv_tbl: "lstar_table_inv tbl" by(auto)

      \<comment> \<open> We preserve the invariant \<close>
      from this and t_wf have "(lstar_table_inv \<circ> fst) (lstar_learner_step oracleM ?t (tbl,log))"
        using lstar_learner_step_invariant by(auto)
      from this and Pair have inv_after: "(lstar_table_inv \<circ> fst) ?body" by(auto)

      \<comment> \<open> And update the variant... \<close>
      have "(num_states \<circ> make_conjecture \<circ> fst) (lstar_learner_step oracleM ?t (tbl,log)) > 
            (num_states \<circ> make_conjecture \<circ> fst) (tbl,log)"
        proof(rule lstar_learner_step_variant)
        show "closedp tbl" and "consistentp tbl" using closed consistent Pair by(auto)
        from inv Pair show "lstar_table_inv tbl" by(simp)
        show "the ((oracleC \<circ> make_conjecture \<circ> fst) s) \<in> \<Sigma>star"
          using t_wf by auto
        show "L (the ((oracleC \<circ> make_conjecture \<circ> fst) s)) \<noteq> output_on (make_conjecture tbl) (the ((oracleC \<circ> make_conjecture \<circ> fst) s))"
          using t_ce by auto
      qed

      \<comment> \<open> We also need to show that it remains bounded by N\<close>
      moreover have "(num_states \<circ> make_conjecture) tbl \<le> N" apply(rule num_states_bounded)
        using closed consistent inv_tbl Pair lstar_table_invE by(auto)
      moreover have "(num_states \<circ> make_conjecture \<circ> fst) (?stepbody s) \<le> N"
        proof(cases "?stepbody s")
        case (Pair midtbl midlog)
        have closed_m: "closedp midtbl" using lstar_learner_step_closed_consistent
          by (metis lstar_learner_step_closed_consistent(1) Pair comp_apply fstI invP prod.collapse t_wf) 
        have consistent_m: "consistentp midtbl"
          by (metis lstar_learner_step_closed_consistent(2) Pair comp_apply fstI invP prod.collapse t_wf) 
       have inv_m: "lstar_table_inv midtbl"
         by (metis lstar_learner_step_invariant Pair fstI invP o_apply prod.collapse t_wf)
        have "(num_states \<circ> make_conjecture) midtbl \<le> N" apply(rule num_states_bounded)
            using closed_m consistent_m inv_m lstar_table_invE by(auto)
          then show ?thesis using Pair by(auto)
        qed
      ultimately have var_after: "?f ?body < ?f s" using Pair by auto
      from var_after inv_after Pair show ?thesis
       using lstar_learner_step_closed_consistent(1) lstar_learner_step_closed_consistent(2)
             inv_tbl t_wf
       by auto
    qed
  next

    \<comment> \<open> Finally, we show that P is true afterwards too. \<close>
    from inv_bef show "?P (the (make_cc oracleM (tbl,log)))"
    proof -
      from inv_bef make_cc_termination have "\<exists>newtbl newlog. make_cc oracleM (tbl,log) = Some (newtbl,newlog)"
        by(simp)
      then obtain newtbl newlog where ob: "(make_cc oracleM (tbl,log)) = Some (newtbl,newlog)" by(auto)
      then have "(lstar_table_inv \<circ> fst) (newtbl,newlog)" using inv_bef make_cc_invariant
        by (metis (mono_tags, lifting) comp_apply fst_conv)
      moreover have "(closedp \<circ> fst) (the (make_cc oracleM (tbl, log)))"
        using inv_bef make_cc_correct(1) ob by auto
      moreover have "(consistentp \<circ> fst) (the (make_cc oracleM (tbl, log)))"
        using inv_bef make_cc_correct(2) ob by auto
      ultimately show ?thesis using ob by(simp)
    qed
  qed
  then show ?thesis by(auto)
qed

subsection \<open> Helper proof rule (lstar-learner-loop) \<close>
              
(* TODO: Do we only use this one or the new one only ? *)
lemma lstar_learner_step_rule:
  assumes inv_bef: "(lstar_table_inv \<circ> fst) iter" and
          t_wf: "t \<in> \<Sigma>star" and
          p_bef: "P iter" and
          p_mid_step: "\<And>tbl log. \<lbrakk>(lstar_table_inv \<circ> fst) (tbl,log); P (tbl,log)\<rbrakk> \<Longrightarrow> P ((learn_from_cexample t oracleM tbl), log_lece_call tbl log)" and
          p_aft_step: "\<And>iter newiter. \<lbrakk>(lstar_table_inv \<circ> fst) iter; P iter; make_cc oracleM iter = Some newiter\<rbrakk> \<Longrightarrow> P newiter"
  shows "P (lstar_learner_step oracleM t iter)" proof(cases iter)
  case (Pair tbl log)
  from p_mid_step p_bef inv_bef have "P ((learn_from_cexample t oracleM tbl), log_lece_call tbl log)" by(simp add: Pair)
  then have p_mid: "P (learn_from_cexample t oracleM tbl, log_lece_call tbl log)" by simp
  have inv_mid: "lstar_table_inv (learn_from_cexample t oracleM tbl)"
    using learn_from_cexample_invariant Pair inv_bef t_wf by auto
  from inv_mid have
    "\<exists>newiter.(make_cc oracleM (learn_from_cexample t oracleM tbl, log_lece_call tbl log)) = Some newiter"
    using make_cc_termination by auto
  then obtain newiter where new: "(make_cc oracleM (learn_from_cexample t oracleM tbl, log_lece_call tbl log)) = Some newiter"
    by auto
  from p_aft_step p_mid new have "P newiter" by (metis comp_apply fst_conv inv_mid)
  then show ?thesis using new Pair by auto
qed

lemma lstar_learner_loop_rule_with_invariant:
  assumes
    \<comment> \<open> The 'initial' property takes this form since the initial thing we loop on
         is (the (make-cc oracleM iter)). \<close>
    initP: "\<And>newiter. make_cc oracleM iter = Some newiter \<Longrightarrow> P newiter" and
    \<comment> \<open> This invariant will allow us to prove termination of the first makeCC. \<close>
    init_invar: "(lstar_table_inv \<circ> fst) iter" and
    step: "\<And>s. \<lbrakk>P s; (oracleC \<circ> make_conjecture \<circ> fst) s \<noteq> None;
         \<comment> \<open> Here we add the invariant as assumption of the step lemma\<close>
         (lstar_table_inv \<circ> fst) s;
         \<comment> \<open> And also closedness + consistency \<close>
         (closedp \<circ> fst) s;
         (consistentp \<circ> fst) s\<rbrakk>
         \<comment> \<open> Here we only need to show the property for a counterexample which is actually
              the one given by the oracle (with correctness properties)\<close>
         \<Longrightarrow> P (lstar_learner_step oracleM (the (oracleC (make_conjecture (fst s)))) s)" and
    new: "(lstar_learner_loop oracleM oracleC iter) = Some newIter"
  shows "P newIter"
proof -
  have "P newIter \<and> (lstar_table_inv \<circ> fst) newIter
                  \<and> (closedp \<circ> fst) newIter
                  \<and> (consistentp \<circ> fst) newIter"
  proof(rule while_option_rule
      [where P = "\<lambda>i. P i \<and> (lstar_table_inv \<circ> fst) i
                          \<and> (closedp \<circ> fst) i
                          \<and> (consistentp \<circ> fst) i"])
    let ?b = "oracleC \<circ> make_conjecture \<circ> fst"
    let ?c = "\<lambda>val s. (lstar_learner_step oracleM (the val)) s"
    let ?s = "(the (make_cc oracleM iter))"
    
  \<comment> \<open> We unify the main while option call \<close>
    from new show "while_option (\<lambda>s. ?b s \<noteq> None) (\<lambda>s. ?c (?b s) s) ?s = Some newIter"
      by(simp add: valued_predicate_while_option_def)
    
  \<comment> \<open> Showing the initial value has all properties follows from the lemmas about makeCC.\<close>
  show "P ?s \<and>
    (lstar_table_inv \<circ> fst) ?s \<and>
    (closedp \<circ> fst) ?s \<and>
    (consistentp \<circ> fst) ?s" proof -
    \<comment> \<open> We'll need termination of the first makeCC multiple times. \<close>
    from init_invar make_cc_termination have 
    "\<exists>inititer. (make_cc oracleM iter) = Some inititer" by(cases iter, auto)
    then obtain inititer where *: "(make_cc oracleM iter) = Some inititer" by auto
    then have "P ?s" using initP by simp
    moreover have "(lstar_table_inv \<circ> fst) ?s"
    proof -
      have "(lstar_table_inv \<circ> fst) inititer"
        by(rule make_cc_invariant[OF init_invar *])
      then show ?thesis using * by simp
    qed
    moreover have "(closedp \<circ> fst) ?s" and 
                  "(consistentp \<circ> fst) ?s"
      using init_invar *  make_cc_correct by auto
    ultimately show ?thesis by simp
  qed
next
  \<comment> \<open> The body of the lstar-learner-loop \<close>
  let ?loopbody = "\<lambda>s. 
    lstar_learner_step oracleM (the ((oracleC \<circ> make_conjecture \<circ> fst) s)) s"
  \<comment> \<open> Finally, we have to show that the stronger step lemma needed follows from our weaker step
       lemma in the assms. \<close>
  show "\<And>s. \<lbrakk>P s \<and> (lstar_table_inv \<circ> fst) s \<and> (closedp \<circ> fst) s \<and> (consistentp \<circ> fst) s;
        (oracleC \<circ> make_conjecture \<circ> fst) s \<noteq> None\<rbrakk>
       \<Longrightarrow> P (?loopbody s) \<and>
           (lstar_table_inv \<circ> fst) (?loopbody s) \<and>
           (closedp \<circ> fst) (?loopbody s) \<and>
           (consistentp \<circ> fst) (?loopbody s)" proof -
    fix s
    assume "P s \<and> (lstar_table_inv \<circ> fst) s \<and> (closedp \<circ> fst) s \<and> (consistentp \<circ> fst) s" 
           and loop: "(oracleC \<circ> make_conjecture \<circ> fst) s \<noteq> None"
    then have P_bef_iter: "P s" and
              inv_bef_iter: "(lstar_table_inv \<circ> fst) s" and
              closed_bef_iter: "(closedp \<circ> fst) s" and
              consistent_bef_iter: "(consistentp \<circ> fst) s" by auto
    have P_aft_iter: "P (?loopbody s)" proof -
      have "P (lstar_learner_step oracleM (the (oracleC (make_conjecture (fst s)))) s)"
        using step P_bef_iter inv_bef_iter closed_bef_iter consistent_bef_iter loop by auto
      then show ?thesis by(cases s, simp)
    qed
    \<comment> \<open> t is the counterexample returned by the oracleC. We show that it has all the
         properties we want from it. \<close>
    let ?t = "(oracleC (make_conjecture (fst s)))"
    from loop have tdef: "\<exists>x. ?t = Some x" by(cases s, simp)
    then obtain x where xdef0: "?t = Some x" by auto
    then have xdef1: "x \<in> \<Sigma>star" and
              xdef2: "L x \<noteq> (output_on (make_conjecture (fst s))) x"
    proof -
      have "x \<in> \<Sigma>star \<and> L x \<noteq> (output_on (make_conjecture (fst s))) x"
      proof(rule oracleC_correct_Some_correct)
        show "welldefined_dfa_p (make_conjecture (fst s))"
          using P_bef_iter inv_bef_iter closed_bef_iter consistent_bef_iter
          welldefined_dfa_p_make_conjecture by(auto dest: lstar_table_invE)
        (* There are simpler ways to show this, but it works :-0 *)
        show "set (get_alphabet (make_conjecture (fst s))) = set \<Sigma>" using angluin_lemma3 
            closed_bef_iter compat_with_p_same_alphE consistent_bef_iter inv_bef_iter lstar_table_invE
            by (metis comp_apply)
        from xdef0 show "oracleC (make_conjecture (fst s)) = Some x" .
        show "follows_prop_p (make_conjecture (fst s)) L_prop"
          using follows_orcm_follows_prop_make_conjecture
          by (metis MAT_model_moore.lstar_table_invE(2) MAT_model_moore.lstar_table_invE(3)
              MAT_model_moore.lstar_table_invE(4) MAT_model_moore_axioms comp_apply inv_bef_iter
              lstar_table_invE(1))
      qed
      then show "x \<in> \<Sigma>star" and "(L x \<noteq> (output_on (make_conjecture (fst s))) x)"
        by auto
    qed
    have inv_aft_iter: "(lstar_table_inv \<circ> fst) (?loopbody s)" proof -
      have "(lstar_table_inv \<circ> fst)
              (lstar_learner_step oracleM (the (oracleC (make_conjecture (fst s)))) s)"
        using lstar_learner_step_invariant
        by (metis (mono_tags, lifting) inv_bef_iter o_apply option.sel prod.collapse xdef0 xdef1)
      then show ?thesis by(cases s, simp)
    qed
    have closed_aft_iter: "(closedp \<circ> fst) (?loopbody s)" and 
         consistent_aft_iter: "(consistentp \<circ> fst) (?loopbody s)" proof -
      have "(closedp \<circ> fst) 
              (lstar_learner_step oracleM (the (oracleC (make_conjecture (fst s)))) s)"
        using lstar_learner_step_closed_consistent
        by (metis (mono_tags, lifting) inv_bef_iter comp_apply option.sel prod.collapse xdef0 xdef1)
      then show "(closedp \<circ> fst) (?loopbody s)"
        by(cases s, auto)
      have "(consistentp \<circ> fst) 
              (lstar_learner_step oracleM (the (oracleC (make_conjecture (fst s)))) s)"
        using lstar_learner_step_closed_consistent
        by (metis (mono_tags, lifting) inv_bef_iter comp_apply option.sel prod.collapse xdef0 xdef1)
      then show "(consistentp \<circ> fst) (?loopbody s)"
        by(cases s, auto)
    qed
    
    from P_aft_iter inv_aft_iter closed_aft_iter consistent_aft_iter show 
           "P (?loopbody s) \<and>
           (lstar_table_inv \<circ> fst) (?loopbody s) \<and>
           (closedp \<circ> fst) (?loopbody s) \<and>
           (consistentp \<circ> fst) (?loopbody s)" by auto
    qed
  qed
  then show ?thesis by simp
qed
     
section \<open> Correctness of the L* Algorithm \<close>

text \<open> A corollary of the previous rule is that the invariant is preserved
       after the whole lstar-learner-loop\<close>
lemma lstar_learner_loop_invariant:
  assumes
    inv: "(lstar_table_inv \<circ> fst) iter" and
    new: "(lstar_learner_loop oracleM oracleC iter) = Some newiter"
  shows "(lstar_table_inv \<circ> fst) newiter"
proof -
  have "\<exists>miditer. make_cc oracleM iter = Some miditer"
    by (metis (no_types, hide_lams) inv make_cc_termination not_Some_eq o_apply prod.collapse)
  then obtain miditer where *: "make_cc oracleM iter = Some miditer" by auto
  show ?thesis proof(rule lstar_learner_loop_rule_with_invariant[where iter=iter])
    show "\<And>newiter. make_cc oracleM iter = Some newiter \<Longrightarrow> (lstar_table_inv \<circ> fst) newiter"
      using inv make_cc_invariant by simp      
    show "\<And>s. \<lbrakk>(lstar_table_inv \<circ> fst) s; (oracleC \<circ> make_conjecture \<circ> fst) s \<noteq> None;
          (lstar_table_inv \<circ> fst) s; (closedp \<circ> fst) s; (consistentp \<circ> fst) s\<rbrakk>
         \<Longrightarrow> (lstar_table_inv \<circ> fst) (lstar_learner_step oracleM (the (oracleC (make_conjecture (fst s)))) s)"
    proof -
      fix s :: "('letter,'mlo) lstar_iteration"
      assume "(lstar_table_inv \<circ> fst) s" "(oracleC \<circ> make_conjecture \<circ> fst) s \<noteq> None"
             "(lstar_table_inv \<circ> fst) s" "(closedp \<circ> fst) s" "(consistentp \<circ> fst) s"
      show "(lstar_table_inv \<circ> fst) (lstar_learner_step oracleM (the (oracleC (make_conjecture (fst s)))) s)"
      proof(cases s)
        case (Pair tbl log)
        have "(lstar_table_inv \<circ> fst) (lstar_learner_step oracleM (the (oracleC (make_conjecture tbl))) (tbl,log))"
        proof(rule lstar_learner_step_invariant)
          show "lstar_table_inv tbl" using Pair \<open>(lstar_table_inv \<circ> fst) s\<close> by auto
          show "the (oracleC (make_conjecture tbl)) \<in> \<Sigma>star" 
            using Pair \<open>(oracleC \<circ> make_conjecture \<circ> fst) s \<noteq> None\<close> oracleC_make_conjecture_properties
              \<open>(closedp \<circ> fst) s\<close> \<open>(consistentp \<circ> fst) s\<close> \<open>(lstar_table_inv \<circ> fst) s\<close> by auto
        qed
        then show ?thesis using Pair by simp
      qed
    qed
  qed(simp only: inv, simp only: new)
qed

text \<open> Another corollary is that lstar-learner-loop terminates with a closed and consistent
       observation table.\<close>
lemma lstar_learner_loop_closed_consistent:
  assumes
    inv: "(lstar_table_inv \<circ> fst) iter" and
    new: "(lstar_learner_loop oracleM oracleC iter) = Some newiter"
  shows "(closedp \<circ> fst) newiter" and "(consistentp \<circ> fst) newiter"
proof -
  have "\<exists>miditer. make_cc oracleM iter = Some miditer"
    by (metis (no_types, hide_lams) inv make_cc_termination not_Some_eq o_apply prod.collapse)
  then obtain miditer where *: "make_cc oracleM iter = Some miditer" by auto
  have "(closedp \<circ> fst) newiter \<and> (consistentp \<circ> fst) newiter"
  proof(rule lstar_learner_loop_rule_with_invariant[where iter=iter])
    show "\<And>newiter. make_cc oracleM iter = Some newiter
          \<Longrightarrow> (closedp \<circ> fst) newiter \<and> (consistentp \<circ> fst) newiter"
      using inv make_cc_correct by auto
    show "\<And>s. \<lbrakk>(closedp \<circ> fst) s \<and> (consistentp \<circ> fst) s;
               (oracleC \<circ> make_conjecture \<circ> fst) s \<noteq> None;
               (lstar_table_inv \<circ> fst) s;
          (closedp \<circ> fst) s; (consistentp \<circ> fst) s\<rbrakk>
         \<Longrightarrow> (closedp \<circ> fst) (lstar_learner_step oracleM (the (oracleC (make_conjecture (fst s)))) s) \<and>
             (consistentp \<circ> fst) (lstar_learner_step oracleM (the (oracleC (make_conjecture (fst s)))) s)"
    proof -
      fix s :: "('letter,'mlo) lstar_iteration"
      assume "(lstar_table_inv \<circ> fst) s" "(oracleC \<circ> make_conjecture \<circ> fst) s \<noteq> None"
             "(lstar_table_inv \<circ> fst) s" "(closedp \<circ> fst) s" "(consistentp \<circ> fst) s"
      show "(closedp \<circ> fst) (lstar_learner_step oracleM (the (oracleC (make_conjecture (fst s)))) s) \<and>
             (consistentp \<circ> fst) (lstar_learner_step oracleM (the (oracleC (make_conjecture (fst s)))) s)"
      proof(cases s)
        case (Pair tbl log)
        have "(closedp \<circ> fst) (lstar_learner_step oracleM (the (oracleC (make_conjecture tbl))) (tbl,log))"
        proof(rule lstar_learner_step_closed_consistent)
          show "lstar_table_inv tbl" using Pair \<open>(lstar_table_inv \<circ> fst) s\<close> by auto
          show "the (oracleC (make_conjecture tbl)) \<in> \<Sigma>star" 
            using Pair \<open>(oracleC \<circ> make_conjecture \<circ> fst) s \<noteq> None\<close> oracleC_make_conjecture_properties
              \<open>(closedp \<circ> fst) s\<close> \<open>(consistentp \<circ> fst) s\<close> \<open>(lstar_table_inv \<circ> fst) s\<close> by auto
        qed
        moreover have 
          "(consistentp \<circ> fst) (lstar_learner_step oracleM (the (oracleC (make_conjecture tbl))) (tbl,log))"
        proof(rule lstar_learner_step_closed_consistent)
          show "lstar_table_inv tbl" using Pair \<open>(lstar_table_inv \<circ> fst) s\<close> by auto
          show "the (oracleC (make_conjecture tbl)) \<in> \<Sigma>star" 
            using Pair \<open>(oracleC \<circ> make_conjecture \<circ> fst) s \<noteq> None\<close> oracleC_make_conjecture_properties
              \<open>(closedp \<circ> fst) s\<close> \<open>(consistentp \<circ> fst) s\<close> \<open>(lstar_table_inv \<circ> fst) s\<close> by auto
        qed
        ultimately show ?thesis using Pair by simp
      qed
    qed
  qed(simp only: inv, simp only: new)
  then show "(closedp \<circ> fst) newiter" and "(consistentp \<circ> fst) newiter" by auto
qed

text \<open> From termination of lstar-learner-loop, since we never stop looping until
       the conjecture oracle returns None, we have: \<close>
lemma lstar_learner_loop_correct:
  "lstar_learner_loop om oc (tbl,log) = Some (newtbl,newlog)
    \<Longrightarrow> oc (make_conjecture newtbl) = None"
  unfolding lstar_learner_loop.simps valued_predicate_while_option_def
  using while_option_stop by fastforce

text \<open>From the previous lemma and the properties of the conjecture oracle,
      the final conjecture is a minimal acceptor for L.\<close>
lemma lstar_correct: 
  assumes no_dups_sigma: "distinct \<Sigma>"
  shows "minimal_acceptor_p (lstar_learner_conjecture \<Sigma> oracleM oracleC) \<Sigma> L L_prop"
    \<comment> \<open>The output of the L* algorithm is isomorphic to the minimal acceptor\<close>
    and "\<exists>f. iso_betw_p f (lstar_learner_conjecture \<Sigma> oracleM oracleC) M\<^sub>L"
proof -
  let ?lstar_output = "lstar_learner_loop oracleM oracleC
                         ((make_initial_table \<Sigma> oracleM), initial_log)"
  let ?conj = "(make_conjecture \<circ> fst \<circ> the) ?lstar_output"
  have initinv: "(lstar_table_inv \<circ> fst) ((make_initial_table \<Sigma> oracleM), initial_log)"
    using make_initial_table_invariant by(simp add: no_dups_sigma)
  then have "\<exists>it. ?lstar_output = Some it"
    using lstar_learner_loop_termination by auto
  then obtain it where defit: "?lstar_output = Some it" by auto
  then have "oracleC (make_conjecture (fst it)) = None" using lstar_learner_loop_correct
    by(cases it, simp)
  from this and defit have orcC_none: "oracleC ?conj = None" by(auto)
  let ?out_tbl = "(fst \<circ> the) ?lstar_output"
  have endinv: "lstar_table_inv ?out_tbl" 
    using lstar_learner_loop_invariant initinv by (metis comp_apply defit option.sel)
  have c1: "welldefined_dfa_p ?conj" proof -
    have "welldefined_dfa_p (make_conjecture ?out_tbl)"
    proof(rule welldefined_dfa_p_make_conjecture)
      from endinv show "wellformedp ?out_tbl" by auto
      from endinv show "filledp ?out_tbl" by auto
      from lstar_learner_loop_closed_consistent(1)[OF initinv defit]
        have "(closedp \<circ> fst) it" .
      then show "closedp ?out_tbl" using defit by simp
      from lstar_learner_loop_closed_consistent(2)[OF initinv defit]
        have "(consistentp \<circ> fst) it" .
      then show "consistentp ?out_tbl" using defit by simp
    qed
    then show ?thesis by simp
  qed
  from endinv have alph_out_tbl: "set (get_A ?out_tbl) = set \<Sigma>" using lstar_table_invE by auto
  then have c2: "set (get_alphabet ?conj) = set \<Sigma>" by(simp add: make_conjecture_def)
  have c3: "follows_prop_p ?conj L_prop"
    using follows_orcm_follows_prop_make_conjecture
    by (metis MAT_model_moore.lstar_table_inv_def MAT_model_moore_axioms comp_apply
        defit endinv fst_conv option.sel prod.exhaust)
  from orcC_none have conj_lang: 
    "accepted_language_eq_p (lstar_learner_conjecture \<Sigma> oracleM oracleC) \<Sigma> L"
    using oracleC_correct_None[OF c1 c2 c3] by simp
  
  text \<open>To show the goal, we use the fact that the output M of the L* algorithm:
         \<^item> is welldefined
         \<^item> accepts the correct language
         \<^item> has less-states-than (less or iso) to a minimal acceptor $M_L$
        The rule less-states-than-min-acceptor shows that M itself is a minimal acceptor.\<close>

  \<comment> \<open>First, we get this useful property\<close>
  have "make_conjecture ?out_tbl \<sqsubseteq> M\<^sub>L" using make_conjecture_less_states_than
    by (metis comp_apply defit endinv initinv lstar_learner_loop_closed_consistent(1)
        lstar_learner_loop_closed_consistent(2) option.sel)
  then have *: "lstar_learner_conjecture \<Sigma> oracleM oracleC \<sqsubseteq> M\<^sub>L" by simp

  show min_acc:
    "minimal_acceptor_p (lstar_learner_conjecture \<Sigma> oracleM oracleC) \<Sigma> L L_prop"
  proof(rule less_states_than_min_acceptor)
    from L_regular show "minimal_acceptor_p M\<^sub>L \<Sigma> L L_prop" .
    from conj_lang
      show "accepted_language_eq_p (lstar_learner_conjecture \<Sigma> oracleM oracleC) \<Sigma> L" .
    from c1 show "welldefined_dfa_p (lstar_learner_conjecture \<Sigma> oracleM oracleC)"
      by simp
    from * show "lstar_learner_conjecture \<Sigma> oracleM oracleC \<sqsubseteq> M\<^sub>L" .
  qed
  show 
    "\<exists>f. iso_betw_p f (lstar_learner_conjecture \<Sigma> oracleM oracleC) M\<^sub>L"
  proof -
    from * have "lstar_learner_conjecture \<Sigma> oracleM oracleC \<sqsubseteq> M\<^sub>L" .
    moreover from min_acc have "M\<^sub>L \<sqsubseteq> lstar_learner_conjecture \<Sigma> oracleM oracleC"
      using L_regular c1 c3 defit lstar_learner.simps lstar_learner_loop.simps
            minimal_acceptor_p_minD by auto
    ultimately show ?thesis using less_states_than_antisym by auto
  qed
qed

end (* End of MAT_model context *)

end