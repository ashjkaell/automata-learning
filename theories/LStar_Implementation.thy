section \<open> Implementation of Dana Angluin's LStar algorithm for learning DFAs
          (Modified for moore-machines) \<close>

text \<open> The implementation of the $L^*$ algorithm is in this file; The properties of the algorithm
       (such as termination, correctness, etc..) are in the LStarAlgorithmProperties file where we
       wrap them in a Locale expressing correctness of the MAT-oracles. \<close>

theory LStar_Implementation
  imports
    LStar_Prerequisite_Lemmas       (* This file also contains subroutines needed for the L* alg. *)
    "HOL-Library.While_Combinator"
begin

section \<open> Datatypes for MAT - oracles \<close>

text \<open> We model the MAT as a pair of oracles each answering membership and conjecture queries
       respectively\<close>

text \<open> The membership oracle receives a word and returns which char of the output alphabet it 
       corresponds to. (We're now working with weighted languages and moore machines)\<close>
type_synonym ('l,'o) membership_oracle = " 'l list \<Rightarrow> 'o "

text \<open> The conjecture oracle receives a 'conjecture' dfa. If it returns 'None' then the conjecture is
     correct. Otherwise, it returns 'Some x' where x is a counter-example word. \<close>
type_synonym ('l,'s,'o) conjecture_oracle = " ('l,'s,'o) moore_machine \<Rightarrow> ('l list) option "

section \<open> The L* algorithm \<close>

subsection \<open> Necessary subroutines \<close>

text \<open> The main idea behind the algorithm is simply to maintain an observation table (initially empty),
       keep fixing closedness and consistency problems and generate conjectures that we can ask the
       oracle about. \<close>

text \<open> When fixing closedness and consistency, we'll need to add rows/extensions to our observation
       table. We begin by defining functions to extend the set S or E of an observation table; by
       asking membership queries. \<close>

paragraph \<open> Looking up with alternatives. \<close>

text \<open> We define the following @{theory_text safe_lookup} function which allows us to look for something in the
       table's T function with an alternative way of getting the answer (the oracle) if the result
       isn't found. \<close>
definition safe_lookup :: "('a \<Rightarrow> 'b option) \<Rightarrow> ('a \<Rightarrow> 'b) \<Rightarrow> 'a \<Rightarrow> 'b" where
  " safe_lookup map_f safe_f q = (if (map_f q) \<noteq> None then (the (map_f q)) else safe_f q)"

subsubsection \<open> Extending tables via membership queries \<close>

definition extendS_queries :: " ('l,'o) observation_table \<Rightarrow> 'l list fset \<Rightarrow> 'l list fset " where
  " extendS_queries tbl updS = [s@e. s <- updS, e <- (get_E tbl)] \<union>\<^sub>f [(s@[a]@e). s <- updS, a <- (get_A tbl), e <- (get_E tbl)]"

definition extendS_answers :: "('l,'o) observation_table \<Rightarrow> ('l,'o) membership_oracle \<Rightarrow> 'l list fset \<Rightarrow> 'o list" where
  " extendS_answers tbl orc updS  = map orc (extendS_queries tbl updS)"

text \<open> This new function is like the previous one, but doesn't query if already known. We will show
       a lemma for equivalence (given conditions on the table)\<close>
definition extendS_answers_optimized :: "('l,'o) observation_table \<Rightarrow> ('l,'o) membership_oracle \<Rightarrow> 'l list fset \<Rightarrow> 'o list" where
  " extendS_answers_optimized tbl orc updS =
      map (safe_lookup (get_T tbl) orc) (extendS_queries tbl updS)"

(* TODO: We're not trimming our queries here with (not . known) ! *)
\<comment> \<open> Given a membership oracle, an observation table and an update of the set S (new extensions we
     want to add), return a new observation table for which the table has been extended and the
     table function has been filled correspondingly using the oracle.
    Note: This function also updates the set SA for it to stay consistent \<close>
definition extendS :: " ('l,'o) membership_oracle \<Rightarrow> ('l,'o) observation_table \<Rightarrow> 'l list fset \<Rightarrow> ('l,'o) observation_table " where
\<comment> \<open> It's very important to use unionf here ! \<close>
  " extendS orc tbl updS = (let newS = (get_S tbl) \<union>\<^sub>f updS
                        in (let queries = extendS_queries tbl updS
                        in (let answers = extendS_answers_optimized tbl orc updS
                        in \<lparr> get_A = (get_A tbl),
                             get_S = newS,
                             \<comment> \<open> Here, there could be duplicates to remove \<close>
                             get_SA = remdups [s@[a]. s <- newS, a <- (get_A tbl)],
                             get_E = (get_E tbl),
                             get_T = (map_upds (get_T tbl) queries answers)
                           \<rparr>
                         )
                         )
                         )"

definition extendE_queries :: " ('l,'o) observation_table \<Rightarrow> 'l list fset \<Rightarrow> 'l list fset " where
  " extendE_queries tbl updE = [(s@e). s <- (get_S tbl) \<union>\<^sub>f (get_SA tbl), e <- updE]"

definition extendE_answers :: "('l,'o) observation_table \<Rightarrow> ('l,'o) membership_oracle \<Rightarrow> 'l list fset \<Rightarrow> 'o list" where
  " extendE_answers tbl orc updS  = map orc (extendE_queries tbl updS)"

text \<open> This new function is like the previous one, but doesn't query if already known. We will show
       a lemma for equivalence (given conditions on the table)\<close>
definition extendE_answers_optimized :: "('l,'o) observation_table \<Rightarrow> ('l,'o) membership_oracle \<Rightarrow> 'l list fset \<Rightarrow> 'o list" where
  " extendE_answers_optimized tbl orc updE =
      map (safe_lookup (get_T tbl) orc) (extendE_queries tbl updE)"

\<comment> \<open> Given a membership oracle, an observation table and an update of the set E (new extensions we
     want to add), return a new observation table for which the table function has been filled
    correspondingly using the oracle. \<close>
(* TODO: Is it actually needed to use union_f here ? (It's definitely needed in extendS) *)
definition extendE :: " ('l,'o) membership_oracle \<Rightarrow> ('l,'o) observation_table \<Rightarrow> 'l list fset \<Rightarrow> ('l,'o) observation_table " where
  " extendE orc tbl updE = (let newE = (get_E tbl) \<union>\<^sub>f updE
                        \<comment> \<open> Note that we need to ask question about all rows of the table (but some
                             might be shared between S and SA) \<close>
                        in (let queries = extendE_queries tbl updE
                        in (let answers = extendE_answers_optimized tbl orc updE
                        in \<lparr> get_A = (get_A tbl),
                             get_S = (get_S tbl),
                             get_SA = (get_SA tbl),
                             get_E = newE,
                             get_T = (map_upds (get_T tbl) queries answers)
                           \<rparr>
                         )
                         )
                         ) "

subsubsection \<open> Fixing Closedness and Consistency problems \<close>

fun fix_closedness_prob :: " ('l,'o) membership_oracle \<Rightarrow> 'l closedness_prob \<Rightarrow> ('l,'o) observation_table \<Rightarrow> ('l,'o) observation_table " where
  " fix_closedness_prob orc (s,a) tbl = extendS orc tbl [s@[a]] "

fun fix_consistency_prob :: " ('l,'o) membership_oracle \<Rightarrow> 'l consistency_prob \<Rightarrow> ('l,'o) observation_table \<Rightarrow> ('l,'o) observation_table " where
  " fix_consistency_prob orc (s1,s2,a,e) tbl = extendE orc tbl [a#e] "

text \<open> Using the previous functions, we can make the table closed and consistent. \<close>

text \<open> The following two functions get a closedness/consistency problem and fix it if it exists;
        They don't guarantee that the table is actually closed/consistent after being used !
        (They only fix 'one' problem, others might remain !)\<close>
definition make_closed :: " ('l,'o) membership_oracle \<Rightarrow> ('l,'o) observation_table \<Rightarrow> ('l,'o) observation_table" where
  " make_closed orc tbl = (let problem = get_closedness_prob tbl in
                               fix_closedness_prob orc (the problem) tbl
                          )"

definition make_consistent :: " ('l,'o) membership_oracle \<Rightarrow> ('l,'o) observation_table \<Rightarrow> ('l,'o) observation_table" where
  " make_consistent orc tbl = (let problem = get_consistency_prob tbl in
                                    fix_consistency_prob orc (the problem) tbl
                              )"

subsubsection \<open> Wrapping observation tables with logs \<close>

text \<open> We wrap the table we are iterating on in lStarIteration to add logging information
       to each iteration. This will allow us to reason about the complexity of the algorithm. \<close>

text \<open> The log type stores the maximum sizes of the sets S and E seen during execution of the alg.
       We compute the length of the lists they are represented by, to check that the list
       representation does not explode. We'll show lemmas proving that these lengths are equal
       to the cardinalities of the sets.\<close>

record log_type =
  get_mkcl :: "nat" \<comment> \<open> num. calls of make-closed \<close>
  get_mkco :: "nat" \<comment> \<open> num. calls of make-consistent \<close>
  get_lece :: "nat" \<comment> \<open> num. calls of learn-from-cexample (related to make-conjecture)\<close>
  get_max_len_S :: "nat" \<comment> \<open> Maximum length of S (as a list) seen during execution \<close>
  get_max_len_E :: "nat" \<comment> \<open> Maximum length of E (as a list) seen during execution \<close>
  get_max_str_len_S :: "nat" \<comment> \<open> Max. string length in S seen during execution \<close>
  get_max_str_len_E :: "nat" \<comment> \<open> Max. string length in E seen during execution \<close>

type_synonym ('l,'o) lstar_iteration = "('l,'o) observation_table \<times> log_type"

paragraph \<open> Definition of maximum-str-length\<close>

text \<open> Returns the maximum length of an 'l string in xs\<close>
definition max_str_len :: "'l list set \<Rightarrow> nat" where
  " max_str_len xs = Max (length ` xs)"

paragraph \<open> Incrementing the function-call counts. \<close>
abbreviation incr_mkcl :: " log_type \<Rightarrow> log_type " where
  " incr_mkcl l \<equiv> l\<lparr>get_mkcl := Suc (get_mkcl l)\<rparr>"

abbreviation incr_mkco :: " log_type \<Rightarrow> log_type " where
  " incr_mkco l \<equiv> l\<lparr>get_mkco := Suc (get_mkco l)\<rparr>"

abbreviation incr_lece :: " log_type \<Rightarrow> log_type " where
  " incr_lece l \<equiv> l\<lparr>get_lece := Suc (get_lece l)\<rparr>"

paragraph \<open> Updating the max seen quantities. \<close>
abbreviation upd_max_card_S :: " nat \<Rightarrow> log_type \<Rightarrow> log_type " where
  " upd_max_card_S c l \<equiv> l\<lparr>get_max_len_S := max c (get_max_len_S l)\<rparr>"

abbreviation upd_max_card_E :: " nat \<Rightarrow> log_type \<Rightarrow> log_type " where
  " upd_max_card_E c l \<equiv> l\<lparr>get_max_len_E := max c (get_max_len_E l)\<rparr>"

abbreviation upd_max_str_len_S :: " nat \<Rightarrow> log_type \<Rightarrow> log_type " where
  " upd_max_str_len_S c l \<equiv> l\<lparr>get_max_str_len_S := max c (get_max_str_len_S l)\<rparr>"

abbreviation upd_max_str_len_E :: " nat \<Rightarrow> log_type \<Rightarrow> log_type " where
  " upd_max_str_len_E c l \<equiv> l\<lparr>get_max_str_len_E := max c (get_max_str_len_E l)\<rparr>"

definition upd_max_quantities :: "('l,'o) observation_table \<Rightarrow> log_type \<Rightarrow> log_type" where
  " upd_max_quantities tbl log = 
     log\<lparr>get_max_len_S := max ((length \<circ> get_S) tbl) (get_max_len_S log),
         get_max_len_E := max ((length \<circ> get_E) tbl) (get_max_len_E log),
         get_max_str_len_S := max ((max_str_len \<circ> set \<circ> get_S) tbl) (get_max_str_len_S log),
         get_max_str_len_E := max ((max_str_len \<circ> set \<circ> get_E) tbl) (get_max_str_len_E log)\<rparr>"

definition log_mkcl_call :: "('l,'o) observation_table \<Rightarrow> log_type \<Rightarrow> log_type" where
  " log_mkcl_call tbl log = upd_max_quantities tbl (incr_mkcl log)"

definition log_mkco_call :: "('l,'o) observation_table \<Rightarrow> log_type \<Rightarrow> log_type" where
  " log_mkco_call tbl log = upd_max_quantities tbl (incr_mkco log)"

definition log_lece_call :: "('l,'o) observation_table \<Rightarrow> log_type \<Rightarrow> log_type" where
  " log_lece_call tbl log = upd_max_quantities tbl (incr_lece log)"

subsubsection \<open> Looping until we can make a conjecture. \<close>

text \<open> We'll call the previous two functions repeatedly until the table is both closed and consistent
       which will allows us to make a conjecture.
       We use the @{theory_text "while_combinator (while_option)"} to represent this while loop. \<close>


fun make_cc_step :: " ('l,'o) membership_oracle \<Rightarrow>
  ('l,'o) lstar_iteration \<Rightarrow> ('l,'o) lstar_iteration"
  where
  " make_cc_step orc (tbl,log) = 
    (if (\<not> closedp tbl) then
    (make_closed orc tbl, log_mkcl_call tbl log) else
    (if (\<not> consistentp tbl) then
      (make_consistent orc tbl, log_mkco_call tbl log) else
      (tbl,log)))"

text \<open> Given an observation table, keeps using the oracle and fixing consistency and closedness problems
     until the table is closed and consistent
     Note, we can't separately make the table closed and consistent, since repairing one problem
     might cause the other \<close>
fun make_cc :: " ('l,'o) membership_oracle \<Rightarrow>
  ('l,'o) lstar_iteration \<Rightarrow> ('l,'o) lstar_iteration option"
  where
  " make_cc orc inititer =
      while_option (\<lambda>(tbl,log). \<not>closedp tbl \<or> \<not>consistentp tbl) 
                   (make_cc_step orc)
                   inititer"

subsubsection \<open> Making the initial table \<close>

text \<open> The initial observation contains only $\lambda$ in S and E, but this means that SA is all one
       letter words. We have to fill the initial table function. \<close>

fun make_initial_table :: " 'l fset \<Rightarrow> ('l,'o) membership_oracle \<Rightarrow> ('l,'o) observation_table " where
  " make_initial_table alph orc = 
   \<comment> \<open> We need to ask about lambda and each one letter word \<close>
           (let queries = [[]]@[[a]. a <- alph]
        in (let answers = map orc queries
        in  \<lparr>
              get_A = alph,
              get_S = [[]], \<comment> \<open> S contains only lambda \<close>
              \<comment> \<open> Here removing dups is not useful, but avoids a proof !\<close>
              get_SA = remdups [[a]. a <- alph],
              get_E = [[]], \<comment> \<open> E contains only lambda \<close>
              get_T = (map_upds Map.empty queries answers)
             \<rparr> 
         )) "

subsubsection \<open> Learning from counter-examples \<close>

text \<open> What allows us to break free from the NP realm are "conjecture queries" posed to the oracle.
       The oracle helps us by giving us a word in the symmetric difference of our conjecture's language
       and the real language if the conjecture is wrong.
       We define a function to learn from the counterexamples given. \<close>

text \<open> The idea is very simple: We just add the counter-example to the set S of the observation table.
       We'll have to keep the invariants in check and ask membership queries for the new entried of
       table T. \<close>

text \<open> We also need to add all prefixes of the counterexample word t to the set S as well, to ensure
       there are no "holes" in the path of the transition function. (Maintain S prefix closed)\<close>

\<comment> \<open> Returns a finite set (list) containing all prefixes of a word t (including t)\<close>
term all_prefixes

\<comment> \<open> Note: We need a membership oracle to fill the newly created entried of the table. \<close>

fun learn_from_cexample :: " 'l fset \<Rightarrow> ('l,'o) membership_oracle \<Rightarrow>
  ('l,'o) observation_table \<Rightarrow> ('l,'o) observation_table "
  where
  " learn_from_cexample t orc tbl = extendS orc tbl (all_prefixes t) "

subsection \<open> Main algorithm loop \<close>

text \<open> The idea of the algorithm is simply to keep fixing problems, make conjectures and loop.
       Termination is implied by the fact that our approximations always have less states than
       the "real" minimal dfa the oracle has in mind. \<close>

text \<open> This is a 'do-while' loop, so we model it as a while after an initial iteration. \<close>

text \<open> This function is to be called after a table has been found not correct by the conjecture oracle.
       It learns from the counter example, and makes the table closed and consistent. \<close>
fun lstar_learner_step
  :: " ('l,'o) membership_oracle \<Rightarrow> 'l list \<Rightarrow>
       ('l,'o) lstar_iteration \<Rightarrow> ('l,'o) lstar_iteration "
  where
  " lstar_learner_step orcM t (tbl,log) = 
       the (make_cc orcM (learn_from_cexample t orcM tbl, log_lece_call tbl log))"

definition "valued_predicate_while_option b c s
  = while_option (\<lambda>s. b s \<noteq> None) (\<lambda>s. c (b s) s) s"

lemma valued_predicate_while_option_unfold[code]: 
  "valued_predicate_while_option b c s
   = (let bs = b s in
       if bs \<noteq> None then valued_predicate_while_option b c (c bs s)
                     else Some s)"
  unfolding valued_predicate_while_option_def
  by (subst while_option_unfold) auto


fun lstar_learner_loop 
  :: "('l,'o) membership_oracle \<Rightarrow> ('l,'l list,'o) conjecture_oracle \<Rightarrow>
        ('l,'o) lstar_iteration \<Rightarrow> ('l,'o) lstar_iteration option"
  where
  " lstar_learner_loop orcM orcC init_iter
    = valued_predicate_while_option
         (orcC \<circ> make_conjecture \<circ> fst)
         (\<lambda>val iter. (lstar_learner_step orcM (the val)) iter)
         (the (make_cc orcM init_iter))"

abbreviation initial_log :: "log_type" where
  "initial_log \<equiv> 
    \<lparr>
      get_mkcl = 0, \<comment> \<open> num. calls of @{theory_text make_closed} \<close>
      get_mkco = 0, \<comment> \<open> num. calls of @{theory_text make_consistent} \<close>
      get_lece = 0, \<comment> \<open> num. calls of @{theory_text learn_from_cexample} (related to make-conjecture)\<close>
      get_max_len_S = 1, \<comment> \<open> Maximum cardinality of the S set seen during execution \<close>
      get_max_len_E = 1, \<comment> \<open> Maximum cardinality of the E set seen during execution \<close>
      get_max_str_len_S = 0, \<comment> \<open> Max. string length in S seen during execution \<close>
      get_max_str_len_E = 0 \<comment> \<open> Max. string length in E seen during execution \<close>
    \<rparr> "

fun lstar_learner
  ::"'l fset \<Rightarrow> ('l,'o) membership_oracle 
             \<Rightarrow> ('l, 'l list, 'o) conjecture_oracle \<Rightarrow> ('l, 'o) lstar_iteration"
  where
  " lstar_learner alph orcM orcC =
      the (lstar_learner_loop orcM orcC (make_initial_table alph orcM, initial_log)) "

abbreviation lstar_learner_conjecture :: "'l fset \<Rightarrow> ('l,'o) membership_oracle \<Rightarrow> ('l, 'l list, 'o) conjecture_oracle \<Rightarrow> ('l, 'l list, 'o) moore_machine" where
  "lstar_learner_conjecture alph orcM orcC \<equiv> (make_conjecture \<circ> fst) (lstar_learner alph orcM orcC)"

export_code lstar_learner in Haskell module_name ExportedMoore

end
