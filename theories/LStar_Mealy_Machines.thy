theory LStar_Mealy_Machines
  imports
    LStar_Properties
    Mealy_Machine
begin

locale WeightedLanguagesMealy =
  fixes    
    \<comment> \<open> The alphabet \<close>
        \<Sigma>     :: " 'letter fset "
    \<comment> \<open> The set of words over sigma \<close>
    and \<Sigma>star :: " 'letter list set "
    \<comment> \<open> L is a weighted language we want to learn\<close>
    and L     :: " 'letter list \<Rightarrow> 'mlo "
    \<comment> \<open> A minimal acceptor for L (L regular)\<close>
    and M\<^sub>L    :: " ('letter, 'mls, 'mlo) mealy_machine "
  assumes
      sigma_ne: "\<Sigma> \<noteq> []"
    and def_sigmastar: " \<Sigma>star = List.lists (set \<Sigma>) "
    and L_regular: "minimal_acceptor_p M\<^sub>L \<Sigma> L "     
    \<comment> \<open> We have to inform @{term M\<^sub>L} of what the alphabet is \<close>
  and acceptor_alphabet: "set (get_alphabet M\<^sub>L) = set \<Sigma>"

type_synonym ('l,'s,'o) conjecture_oracle_mealy = " ('l,'s,'o) mealy_machine \<Rightarrow> ('l list) option "

locale MAT_model_mealy = WeightedLanguagesMealy \<Sigma> \<Sigma>star L M\<^sub>L for
  \<Sigma> :: " 'letter fset " and \<Sigma>star and L and M\<^sub>L :: " ('letter, 'mealymls, 'mealyo) mealy_machine" +
  fixes oracleM :: " ('letter, 'mealyo) membership_oracle "                  (* Oracle for membership queries *)
    and oracleC :: " ('letter, 'letter list, 'mealyo) conjecture_oracle_mealy "  (* Oracle for conjecture queries *)
    and m :: "nat"                  (* The maximal size of counterexamples returned by the oracleC*)
  assumes oracleM_correct: " oracleM w = L w " and
          \<comment> \<open> The conjecture oracle returns None iif the conjecture is correct \<close>
          oracleC_correct_None: " oracleC conjecture = None \<longleftrightarrow> accepted_language_eq_p conjecture \<Sigma> L" and
          \<comment> \<open> If the conjecture oracle returns something, it's indeed a counterexample (in sym-diff).
               For mealy machine oracles, oracles must return non-empty words.
               Note: We need to add the assumptions that the conjecture given to the oracle is wdef
               and has the same alphabet as the language under learning (which will be insured by
               the l* algorithm for the conjectures it presents to the oracle). \<close>
          oracleC_correct_Some_correct:
            "\<lbrakk>welldefined_mealy_p conjecture; set (get_alphabet conjecture) = set \<Sigma>; 
               oracleC conjecture = Some x\<rbrakk>
              \<Longrightarrow> x \<in> \<Sigma>star \<and> x \<noteq> [] \<and> (L x \<noteq> (output_on conjecture) x) " and
          \<comment> \<open> If a counterexample exists, the conjecture oracle will return something.\<close>
          (* oracleC_correct_Some_sound: "x \<in> \<Sigma>star \<and> (x \<in> L \<longleftrightarrow> x \<notin> (acceptedLanguage conjecture)) \<Longrightarrow> \<exists>y. oracleC conjecture = Some y" *)
          \<comment> \<open> The oracle returns counterexamples of finite length (bounded by m)\<close>
          oracleC_bounded:
            "\<lbrakk>welldefined_mealy_p conjecture; set (get_alphabet conjecture) = set \<Sigma>;
             num_states conjecture \<le> num_states M\<^sub>L;
            oracleC conjecture = Some x\<rbrakk> \<Longrightarrow> length x \<le> m_bound"

type_synonym ('x,'l) mealy_vector = "('x,'l) map"
abbreviation mealy_vector_wellformedp :: "'a set \<Rightarrow> ('a,'l) mealy_vector \<Rightarrow> bool" where
  " mealy_vector_wellformedp A mv \<equiv> dom mv = A"

definition mealy_vector_of :: "'x list \<Rightarrow> ('x \<Rightarrow> 'l) \<Rightarrow> ('x,'l) mealy_vector" where
  " mealy_vector_of A f = [A [\<mapsto>] (map f A)]"

lemma mealy_vec_wf_mealy_vec_of: "mealy_vector_wellformedp (set A) (mealy_vector_of A f)"
  by(simp add: mealy_vector_of_def)
                                           
lemma mealy_vector_of_Some: "x \<in> set A \<Longrightarrow> mealy_vector_of A f x = Some (f x)"
  \<comment> \<open>We have already needed the similar lemma before\<close>
  unfolding mealy_vector_of_def by (simp add: map_upds_updated)

lemma mealy_vector_of_None: "x \<notin> set A \<Longrightarrow> mealy_vector_of A f x = None"
  unfolding mealy_vector_of_def by simp

lemma mealy_vec_cong:
  "\<lbrakk>\<And>x. x \<in> set A \<Longrightarrow> f x = g x\<rbrakk> \<Longrightarrow> mealy_vector_of A f = mealy_vector_of A g" proof -
  assume *: "(\<And>x. x \<in> set A \<Longrightarrow> f x = g x)"
  show "mealy_vector_of A f = mealy_vector_of A g" proof(rule ext)
    fix x
    show "mealy_vector_of A f x = mealy_vector_of A g x" proof(cases "x \<in> set A")
      case True
      then have "f x = g x" using * by simp
      then show ?thesis unfolding mealy_vector_of_def
        by (simp add: True map_upds_updated)
    next
      case False
      then show ?thesis unfolding mealy_vector_of_def by auto
    qed
  qed
qed

text \<open> Representation function\<close>
definition moore_of :: "('a,'s,'o) mealy_machine \<Rightarrow> ('a,'s,('a,'o) mealy_vector) moore_machine" where
  " moore_of m = \<lparr>
                  moore_machine.get_alphabet = get_alphabet m,
                  get_statespace = get_statespace m,
                  get_initial = get_initial m,
                  get_transition = get_transition m,
                  get_finalf = get_finalf m
                 \<rparr>"

text \<open> Abstraction function\<close>
definition mealy_of :: "('a,'s,('a,'o) mealy_vector) moore_machine \<Rightarrow> ('a,'s,'o) mealy_machine" where
  " mealy_of m = \<lparr>
                  mealy_machine.get_alphabet = Moore_Machine.get_alphabet m,
                  get_statespace = Moore_Machine.get_statespace m,
                  get_initial = Moore_Machine.get_initial m,
                  get_transition = Moore_Machine.get_transition m,
                  get_finalf = Moore_Machine.get_finalf m
                 \<rparr>"

definition mealy_inv :: "('a,'s,('a,'o) mealy_vector) moore_machine \<Rightarrow> bool" where
  " mealy_inv m \<equiv> follows_prop_p m (mealy_vector_wellformedp (set (moore_machine.get_alphabet m)))"

lemma mealy_invI[intro]:
  "\<lbrakk>\<And>s. s \<in> set (moore_machine.get_statespace m) \<Longrightarrow> 
      mealy_vector_wellformedp (set (moore_machine.get_alphabet m)) (Moore_Machine.get_finalf m s)\<rbrakk>
     \<Longrightarrow> mealy_inv m"
  by(simp add: mealy_inv_def follows_prop_p_def)

lemma mealy_invE[elim]:
  "\<lbrakk>mealy_inv m; s \<in> set (moore_machine.get_statespace m)\<rbrakk>
  \<Longrightarrow> mealy_vector_wellformedp (set (moore_machine.get_alphabet m)) (Moore_Machine.get_finalf m s)"
  by(simp add: mealy_inv_def follows_prop_p_def)

paragraph \<open> Properties of the embedding\<close>

lemma mealy_of_moore_of: "mealy_of (moore_of m) = m" 
  by(simp add: mealy_of_def moore_of_def)

lemma moore_of_mealy_of: "moore_of (mealy_of m) = m" 
  by(simp add: mealy_of_def moore_of_def)

lemma mealy_inv_moore_of: "welldefined_mealy_p m \<Longrightarrow> mealy_inv (moore_of m)"
  by (metis mealy_invI mealy_machine.select_convs(1) mealy_machine.select_convs(2)
      mealy_machine.select_convs(5) mealy_of_def mealy_of_moore_of welldefined_mealy_p_finalfE)

lemma welldefined_mealy_of: "\<lbrakk>welldefined_dfa_p m; mealy_inv m\<rbrakk> \<Longrightarrow> welldefined_mealy_p (mealy_of m)"
  by (metis mealy_invE mealy_machine.select_convs(1) mealy_machine.select_convs(2)
      mealy_machine.select_convs(3) mealy_machine.select_convs(4) mealy_machine.select_convs(5)
      mealy_of_def welldefined_dfa_p_def welldefined_mealy_pI)

lemma welldefined_moore_of:
  "welldefined_mealy_p A \<Longrightarrow> welldefined_dfa_p (moore_of A)"
proof -
  assume wdef: "welldefined_mealy_p A"
  show "welldefined_dfa_p (moore_of A)" proof(rule welldefined_dfa_pI)
    show "moore_machine.get_initial (moore_of A) \<in> set (moore_machine.get_statespace (moore_of A))"
      using wdef by (simp add: moore_of_def welldefined_mealy_p_def)
    show "\<And>s a. \<lbrakk>s \<in> set (moore_machine.get_statespace (moore_of A)); a \<in> set (moore_machine.get_alphabet (moore_of A))\<rbrakk>
           \<Longrightarrow> moore_machine.get_transition (moore_of A) a s \<in> set (moore_machine.get_statespace (moore_of A))"
      using wdef by (simp add: moore_of_def welldefined_mealy_p_def)
  qed
qed

paragraph \<open> Defining oracles\<close>

definition moore_oraclem_of ::
   "'letter list \<Rightarrow> ('letter, 'mealyo) membership_oracle
      \<Rightarrow> ('letter, ('letter,'mealyo) mealy_vector) membership_oracle" where
  " moore_oraclem_of A mealy_orc s = mealy_vector_of A (\<lambda>a. mealy_orc (s@[a]))"

definition moore_oraclec_of ::
   "('letter, 'letter list, 'mealyo) conjecture_oracle_mealy
      \<Rightarrow> ('letter, 'letter list, ('letter,'mealyo) mealy_vector) conjecture_oracle" where
  \<comment> \<open>Such a definition would be wrong:
      {@text "moore_oraclec_of mealy_orc \<equiv> mealy_orc \<circ> mealy_of"
      Instead we have to disregard the last letter in the counterexample.\<close>
  "moore_oraclec_of mealy_orc \<equiv> (map_option butlast) \<circ> mealy_orc \<circ> mealy_of"

lemma mealy_vector_wf_moore_oraclem_of:
  "mealy_vector_wellformedp (set A) (moore_oraclem_of A morc s)"
  by(simp add: moore_oraclem_of_def mealy_vec_wf_mealy_vec_of)

lemma num_states_moore_of: "num_states A = Moore_Machine.num_states (moore_of A)"
  by(simp add: moore_of_def)

lemma iso_moore_of:
  "\<lbrakk>welldefined_mealy_p A; welldefined_mealy_p B; iso_betw_p f A B\<rbrakk>
   \<Longrightarrow> Moore_Machine.iso_betw_p f (moore_of A) (moore_of B)"
proof -
  assume wd_A: "welldefined_mealy_p A"
     and wd_b: "welldefined_mealy_p B"
     and iso: "iso_betw_p f A B"
  show "Moore_Machine.iso_betw_p f (moore_of A) (moore_of B)" proof(intro Moore_Machine.iso_betw_pI)
    \<comment> \<open> All goals are trivial by def, except the finalf.\<close>
    show "set (moore_machine.get_alphabet (moore_of A)) 
        = set (moore_machine.get_alphabet (moore_of B))"
      unfolding moore_of_def using iso Mealy_Machine.iso_betw_p_alph_equalE by auto
    show "bij_betw f (set (moore_machine.get_statespace (moore_of A)))
                     (set (moore_machine.get_statespace (moore_of B)))"
      unfolding moore_of_def using iso by auto
    show "f (moore_machine.get_initial (moore_of A))
          = moore_machine.get_initial (moore_of B)"
      unfolding moore_of_def using iso by auto
    show "\<And>s t a.
       \<lbrakk>s \<in> set (moore_machine.get_statespace (moore_of A)); t \<in> set (moore_machine.get_statespace (moore_of A));
        a \<in> set (moore_machine.get_alphabet (moore_of A))\<rbrakk>
       \<Longrightarrow> (moore_machine.get_transition (moore_of A) a s = t) =
            (moore_machine.get_transition (moore_of B) a (f s) = f t)"
      unfolding moore_of_def using iso
      by (simp add: Mealy_Machine.iso_betw_p_def)
    show "\<And>s. s \<in>set (moore_machine.get_statespace (moore_of A)) \<Longrightarrow>
       moore_machine.get_finalf (moore_of A) s = moore_machine.get_finalf (moore_of B) (f s)"
    proof -
      fix s
      assume defs: "s \<in> set (moore_machine.get_statespace (moore_of A))"
      have "moore_machine.get_finalf (moore_of A) s = get_finalf A s" by(simp add: moore_of_def)
      also from iso have "... = get_finalf B (f s)" proof(rule iso_betw_p_finalfE)
        have "moore_machine.get_statespace (moore_of A) = get_statespace A"
          unfolding moore_of_def by auto
        then show "s \<in> set (get_statespace A)" using defs by simp
      qed
      also have "... = moore_machine.get_finalf (moore_of B) (f s)" by(simp add: moore_of_def)
      finally show
        "moore_machine.get_finalf (moore_of A) s = moore_machine.get_finalf (moore_of B) (f s)" .
    qed
  qed
qed

lemma less_states_than_moore_of:
  "\<lbrakk>welldefined_mealy_p A; welldefined_mealy_p B; (Mealy_Machine.less_states_than A B)\<rbrakk>
  \<Longrightarrow> Moore_Machine.less_states_than (moore_of A) (moore_of B)"
  using iso_moore_of num_states_moore_of less_states_than_def Moore_Machine.less_states_than_def
  by metis

lemma delta_moore_of_cong: "Moore_Machine.delta (moore_of M) = delta M"
  unfolding Moore_Machine.delta_def moore_of_def
  by(auto simp add: delta_def)

context MAT_model_mealy begin

lemma output_on_moore_of:
  "\<lbrakk>welldefined_mealy_p M; set (get_alphabet M) = set \<Sigma>; s \<in> lists (set \<Sigma>)\<rbrakk>
   \<Longrightarrow> Moore_Machine.output_on (moore_of M) s = mealy_vector_of \<Sigma> (\<lambda>a. output_on M (s@[a]))"
proof(rule ext)
  fix x
  assume wdef: "welldefined_mealy_p M"
     and alph: "set (get_alphabet M) = set \<Sigma>"
     and defs: "s \<in> lists (set \<Sigma>)"

  from wdef have *: "delta M s (get_initial M) \<in> set (get_statespace M)"
  proof(rule welldefined_mealy_p_deltaE)
    show "mealy_machine.get_initial M \<in> set (mealy_machine.get_statespace M)"
      by (simp add: wdef welldefined_mealy_p_initialE)
    from defs alph show "s \<in> lists (set (mealy_machine.get_alphabet M))"
      by simp
  qed

  show "Moore_Machine.output_on (moore_of M) s x = mealy_vector_of \<Sigma> (\<lambda>a. output_on M (s@[a])) x"
  proof(cases "x \<in> set \<Sigma>")
    case True
    from wdef True have **: "((get_finalf M) (delta M s (get_initial M)) x) \<noteq> None" proof -
      let ?s = "delta M s (get_initial M)"
      from * and wdef have
        "dom (get_finalf M (delta M s (get_initial M))) = set (get_alphabet M)"
        by (simp add: welldefined_mealy_p_finalfE)
      then show ?thesis using True alph by auto
    qed

    have "Moore_Machine.output_on (moore_of M) s x
       = (Moore_Machine.get_finalf (moore_of M)) (Moore_Machine.delta (moore_of M) s (Moore_Machine.get_initial (moore_of M))) x"
      by(simp add: Moore_Machine.output_on_def)
    also have "... = (get_finalf M) (Moore_Machine.delta (moore_of M) s (get_initial M)) x"
      unfolding moore_of_def by simp
    also have "... = (get_finalf M) (delta M s (get_initial M)) x"
      by (simp add: delta_moore_of_cong)
    finally have one:
      "Moore_Machine.output_on (moore_of M) s x = (get_finalf M) (delta M s (get_initial M)) x" .

    have "mealy_vector_of \<Sigma> (\<lambda>a. output_on M (s@[a])) x = Some (output_on M (s@[x]))"
      using True by (simp add: mealy_vector_of_Some)
    also have "... = Some (the ((get_finalf M) (delta M s (get_initial M)) x))"
      by(simp add: output_on_def)
    also have "... = (get_finalf M) (delta M s (get_initial M)) x"
      using ** by auto
    finally have two: "mealy_vector_of \<Sigma> (\<lambda>a. output_on M (s@[a])) x
    = (get_finalf M) (delta M s (get_initial M)) x" .

    from one two[symmetric] show ?thesis by auto
  next
    case False
    have "Moore_Machine.output_on (moore_of M) s x
       = (Moore_Machine.get_finalf (moore_of M)) (Moore_Machine.delta (moore_of M) s (Moore_Machine.get_initial (moore_of M))) x"
      by(simp add: Moore_Machine.output_on_def)
    also have "... = (get_finalf M) (Moore_Machine.delta (moore_of M) s (get_initial M)) x"
      unfolding moore_of_def by simp
    also have "... = (get_finalf M) (delta M s (get_initial M)) x"
      by (simp add: delta_moore_of_cong)
    also have "... = None" proof -
      have "dom (get_finalf M (delta M s (get_initial M))) = set \<Sigma>"
        by (simp add: "*" alph wdef welldefined_mealy_p_finalfE)
      then show ?thesis using False by blast
    qed
    finally have one:
      "Moore_Machine.output_on (moore_of M) s x = None" .
    moreover have "mealy_vector_of \<Sigma> (\<lambda>a. output_on M (s@[a])) x = None"
      using False by (simp add: mealy_vector_of_None)
    ultimately show ?thesis by simp
  qed
qed                  
                                                                      
lemma output_on_moore_of_Some:
  "\<lbrakk>welldefined_mealy_p M; set (get_alphabet M) = set \<Sigma>; s \<in> lists (set \<Sigma>); x \<in> set \<Sigma>\<rbrakk>
    \<Longrightarrow> Moore_Machine.output_on (moore_of M) s x = Some (Mealy_Machine.output_on M (s@[x]))"
proof -
  assume assms: "welldefined_mealy_p M" "set (get_alphabet M) = set \<Sigma>" "s \<in> lists (set \<Sigma>)"
      and defx: "x \<in> set \<Sigma>" 
  then have "Moore_Machine.output_on (moore_of M) s x = mealy_vector_of \<Sigma> (\<lambda>a. output_on M (s@[a])) x"
    by (simp add: output_on_moore_of)
  also have "... = Some (output_on M (s@[x]))"
    using mealy_vector_of_Some[OF defx] by auto
  finally show ?thesis .
qed

lemma follows_prop_p_wdefE[elim]:
  assumes fprop: "follows_prop_p M (mealy_vector_wellformedp (set \<Sigma>))"
      and wdefM: "welldefined_dfa_p M" 
      and sameA: "(set (moore_machine.get_alphabet M) = set \<Sigma>)"
    shows "mealy_inv M" and "welldefined_mealy_p (mealy_of M)"
proof -
  show "mealy_inv M" by(metis (mono_tags, lifting) follows_prop_p_def fprop mealy_invI sameA)
  then show "welldefined_mealy_p (mealy_of M)" by(rule welldefined_mealy_of[OF wdefM])
qed

lemma moore_acc_lang_imp_mealy_acc_lang: 
  assumes
    langM: "Moore_Machine.accepted_language_eq_p M \<Sigma> (moore_oraclem_of \<Sigma> oracleM)" and
    wdefM: "welldefined_dfa_p M" and
    fprop: "follows_prop_p M (mealy_vector_wellformedp (set \<Sigma>))"
  shows "Mealy_Machine.accepted_language_eq_p (mealy_of M) \<Sigma> oracleM"
proof -
  have "mealy_inv M" and "welldefined_mealy_p (mealy_of M)" using follows_prop_p_wdefE[OF fprop]
    Moore_Machine.accepted_language_eq_p_alphE langM wdefM by auto
  show ?thesis proof(rule accepted_language_eq_pI)
    show alphM: "set (mealy_machine.get_alphabet (mealy_of M)) = set \<Sigma>"                          
      by (metis Moore_Machine.accepted_language_eq_p_alphE langM mealy_machine.simps(1) mealy_of_def)
    show "\<And>w. \<lbrakk>w \<in> lists (set (mealy_machine.get_alphabet (mealy_of M))); w \<noteq> []\<rbrakk>
         \<Longrightarrow> Mealy_Machine.output_on (mealy_of M) w = oracleM w" proof -
      fix w :: "'letter list"
      assume defw: "w \<in> lists (set (mealy_machine.get_alphabet (mealy_of M)))"
        and wne: "w \<noteq> []"
      from wne have "\<exists>s a. w = s@[a]" using rev_exhaust by blast
      then obtain s a where defsa: "w = s@[a]" by auto
      with defw have defs: "s \<in> lists (set (mealy_machine.get_alphabet (mealy_of M)))" by simp
      then have defs2: "s \<in> lists (set \<Sigma>)" by (simp add: alphM)
      from defsa defw have defa: "a \<in> set (mealy_machine.get_alphabet (mealy_of M))" by simp
      then have defa2: "a \<in> set \<Sigma>" by (simp add: alphM)
      have *: "Moore_Machine.output_on (moore_of (mealy_of M)) s a = Some (Mealy_Machine.output_on (mealy_of M) (s@[a]))"
        using output_on_moore_of_Some defa defs alphM
          \<open>welldefined_mealy_p (mealy_of M)\<close> by auto
      have "Mealy_Machine.output_on (mealy_of M) w = the (Some (Mealy_Machine.output_on (mealy_of M) (s@[a])))"
        by(simp add: defsa)
      also have "... = the (Moore_Machine.output_on (moore_of (mealy_of M)) s a)"
        by(simp add: *)
      also have "... = the (Moore_Machine.output_on M s a)" by (simp add: moore_of_mealy_of)
      also have "... = the ((moore_oraclem_of \<Sigma> oracleM) s a)" using langM
        using Moore_Machine.accepted_language_eq_pE Moore_Machine.accepted_language_eq_p_alphE defs2 
        by fastforce
      also have "... = the (Some (oracleM (s@[a])))" unfolding moore_oraclem_of_def using defs2 defa2
        by (simp add: mealy_vector_of_Some)
      also have "... = oracleM w" by(simp add: defsa)
      finally show "Mealy_Machine.output_on (mealy_of M) w = oracleM w" by auto
    qed
  qed
qed

lemma mealy_acc_lang_imp_moore_acc_lang: 
  assumes
    langM: "Mealy_Machine.accepted_language_eq_p (mealy_of M) \<Sigma> oracleM" and
    wdefM: "welldefined_dfa_p M" and
    fprop: "follows_prop_p M (mealy_vector_wellformedp (set \<Sigma>))"
  shows "Moore_Machine.accepted_language_eq_p M \<Sigma> (moore_oraclem_of \<Sigma> oracleM)"
proof -
  have alphM: "set (moore_machine.get_alphabet M) = set \<Sigma>"
    by (metis Mealy_Machine.accepted_language_eq_p_alphE langM mealy_machine.simps(1) mealy_of_def)
  then have "mealy_inv M" and "welldefined_mealy_p (mealy_of M)"
    using follows_prop_p_wdefE[OF fprop wdefM] by auto
  show ?thesis proof(rule Moore_Machine.accepted_language_eq_pI)
    from alphM show "set (moore_machine.get_alphabet M) = set \<Sigma>" .
    show "\<And>w. w \<in> lists (set (moore_machine.get_alphabet M))
         \<Longrightarrow> Moore_Machine.output_on M w = moore_oraclem_of \<Sigma> oracleM w" proof -
      fix w :: "'letter list"
      assume defw: "w \<in> lists (set (moore_machine.get_alphabet M))"
      show "Moore_Machine.output_on M w = moore_oraclem_of \<Sigma> oracleM w" proof(rule ext)
        fix x
        \<comment> \<open> We need a case distinction on Sigma, since x in Sigma is precondition for
             output-on-moore-of-Some lemma.\<close>
        show "Moore_Machine.output_on M w x = moore_oraclem_of \<Sigma> oracleM w x"
        proof(cases "x \<in> set \<Sigma>")
          case True
          have "Moore_Machine.output_on M w x = Moore_Machine.output_on (moore_of (mealy_of M)) w x" 
            by(simp add: moore_of_mealy_of)
          also have "... = Some (Mealy_Machine.output_on (mealy_of M) (w @ [x]))"
            using output_on_moore_of_Some True
            by (metis Mealy_Machine.accepted_language_eq_p_alphE \<open>welldefined_mealy_p (mealy_of M)\<close>
                alphM defw langM)
          also have "... = Some (oracleM (w @ [x]))" using langM True
            by (metis (no_types, lifting) Cons_in_lists_iff Mealy_Machine.accepted_language_eq_p_def
                 alphM append_in_lists_conv defw lists.Nil snoc_eq_iff_butlast)
          also have "... = moore_oraclem_of \<Sigma> oracleM w x"
            unfolding moore_oraclem_of_def using True by(simp add: mealy_vector_of_Some)
          finally show ?thesis .
        next
          case False
          then show ?thesis
            by (metis (no_types, lifting) MAT_model_mealy.output_on_moore_of
                 MAT_model_mealy_axioms Mealy_Machine.accepted_language_eq_p_alphE
                 \<open>welldefined_mealy_p (mealy_of M)\<close> alphM defw domIff langM
                 mealy_vec_wf_mealy_vec_of mealy_vector_wf_moore_oraclem_of moore_of_mealy_of)
        qed
      qed
    qed
  qed
qed

interpretation mtom: MAT_model_moore
  "\<Sigma>" (* Interpretation of Sigma *)
  "List.lists (set \<Sigma>)" (* Interpretation of SigmaStar *)
  \<comment> \<open> We interpret the moore language directly as follows. Correctness follows from the fact
       that oracleM follows original mealy-language L\<close>
  "moore_oraclem_of \<Sigma> oracleM"
  \<comment> \<open> The property on the output of L. Here, we use the mealy property to allow the quantifiers
       (for minimality of Ml, etc..) to be correctly bounded\<close>
  "mealy_vector_wellformedp (set \<Sigma>)"
  "moore_of M\<^sub>L" (* The minimal acceptor *)
  "moore_oraclem_of \<Sigma> oracleM"
  "moore_oraclec_of oracleC"
proof(unfold_locales)
  show "lists (set \<Sigma>) = lists (set \<Sigma>)" by simp
  show "set (moore_machine.get_alphabet (moore_of M\<^sub>L)) = set \<Sigma>"
    by (simp add: moore_of_def acceptor_alphabet)
  
  text \<open>Showing that we have L-prop\<close>
  show "\<And>w. mealy_vector_wellformedp (set \<Sigma>) (moore_oraclem_of \<Sigma> oracleM w)"
    by (simp add: mealy_vector_wf_moore_oraclem_of)

  text \<open> Trivial goal resulting from our instantiation of L\<close>
  show "\<And>w. moore_oraclem_of \<Sigma> oracleM w = moore_oraclem_of \<Sigma> oracleM w" by simp

  text \<open> Checking that moore-of Ml is the minimal acceptor for the modified language\<close>
  show "Moore_Machine.minimal_acceptor_p (moore_of M\<^sub>L) \<Sigma> (moore_oraclem_of \<Sigma> oracleM)
         (mealy_vector_wellformedp (set \<Sigma>))"
  proof(rule Moore_Machine.minimal_acceptor_pI)
    show "Moore_Machine.accepted_language_eq_p (moore_of M\<^sub>L) \<Sigma> (moore_oraclem_of \<Sigma> oracleM)"
    proof
      fix s :: "'letter list"
      assume defs: "s \<in> lists (set (moore_machine.get_alphabet (moore_of M\<^sub>L)))"
      have *: "accepted_language_eq_p M\<^sub>L \<Sigma> oracleM" 
        using oracleM_correct L_regular ext by blast
      have "moore_oraclem_of \<Sigma> oracleM s = mealy_vector_of \<Sigma> (\<lambda>a. oracleM (s@[a]))"
        by(simp add: moore_oraclem_of_def)
      also have "... = mealy_vector_of \<Sigma> (\<lambda>a. output_on M\<^sub>L (s@[a]))" proof -
        have cong: "\<And>a. a \<in> set \<Sigma> \<Longrightarrow> oracleM (s@[a]) = output_on M\<^sub>L (s@[a])" proof -
          fix a
          assume defa: "a \<in> set \<Sigma>"
          show "oracleM (s@[a]) = output_on M\<^sub>L (s@[a])"
          using accepted_language_eq_pE[OF *, where ?w="s@[a]"] defa defs
          by (simp add: \<open>set (moore_machine.get_alphabet (moore_of M\<^sub>L)) = set \<Sigma>\<close> acceptor_alphabet)
        qed
        show ?thesis by(rule mealy_vec_cong cong, auto simp add: cong)
      qed
      also have "... = Moore_Machine.output_on (moore_of M\<^sub>L) s"
      proof(intro output_on_moore_of[symmetric])
        have "set (moore_machine.get_alphabet (moore_of M\<^sub>L)) = set \<Sigma>" proof -
          have "set (moore_machine.get_alphabet (moore_of M\<^sub>L)) = set (get_alphabet M\<^sub>L)"
            by(simp add: moore_of_def)
          also have "... = set \<Sigma>" using acceptor_alphabet by auto
          finally show ?thesis .
        qed
        show "welldefined_mealy_p M\<^sub>L" using L_regular by blast
        from acceptor_alphabet show "set (mealy_machine.get_alphabet M\<^sub>L) = set \<Sigma>" .
        from this defs show "s \<in> lists (set \<Sigma>)" using 
            \<open>set (moore_machine.get_alphabet (moore_of M\<^sub>L)) = set \<Sigma>\<close> by auto
      qed
      finally show "Moore_Machine.output_on (moore_of M\<^sub>L) s = moore_oraclem_of \<Sigma> oracleM s"
       by simp
    next
     show "set (moore_machine.get_alphabet (moore_of M\<^sub>L)) = set \<Sigma>" unfolding moore_of_def
       by (simp add: acceptor_alphabet)
    qed
    show "welldefined_dfa_p (moore_of M\<^sub>L)" using welldefined_moore_of L_regular by auto
    text \<open>Because of the way the minimal-acceptor-p is defined, we need to restrict
          the 'other' machine M to have statespace 'letter list, but this is an infinite
          set (as long as sigma is not empty) so it should not impose restrictions.\<close>
    show "\<And>M :: ('letter, 'letter list, ('letter,'mealyo) mealy_vector) moore_machine.
             \<lbrakk>Moore_Machine.accepted_language_eq_p M \<Sigma> (moore_oraclem_of \<Sigma> oracleM);
               welldefined_dfa_p M; follows_prop_p M (mealy_vector_wellformedp (set \<Sigma>))\<rbrakk>
         \<Longrightarrow> Moore_Machine.less_states_than (moore_of M\<^sub>L) M" proof -
      fix M :: "('letter, 'letter list, ('letter,'mealyo) mealy_vector) moore_machine"
      assume
        langM: "Moore_Machine.accepted_language_eq_p M \<Sigma> (moore_oraclem_of \<Sigma> oracleM)" and
        wdefM: "welldefined_dfa_p M" and
        fprop: "follows_prop_p M (mealy_vector_wellformedp (set \<Sigma>))"
      from fprop have m_inv_M: "mealy_inv M" unfolding mealy_inv_def using langM
        by (simp add: Moore_Machine.accepted_language_eq_p_def)
      have "Moore_Machine.less_states_than (moore_of M\<^sub>L) (moore_of (mealy_of M))"
      proof(rule less_states_than_moore_of)
        from L_regular show "welldefined_mealy_p M\<^sub>L" by auto
        show "welldefined_mealy_p (mealy_of M)" by(rule welldefined_mealy_of[OF wdefM m_inv_M])
        show "Mealy_Machine.less_states_than M\<^sub>L (mealy_of M)" proof(rule minimal_acceptor_p_minD[OF L_regular])
          \<comment> \<open> mealy-of M accepts the same language as M \<close>
          have "Mealy_Machine.accepted_language_eq_p (mealy_of M) \<Sigma> oracleM" 
            using moore_acc_lang_imp_mealy_acc_lang fprop langM wdefM by blast
          then show "Mealy_Machine.accepted_language_eq_p (mealy_of M) \<Sigma> L"
            using oracleM_correct by (metis (full_types) Mealy_Machine.accepted_language_eq_pE 
                 Mealy_Machine.accepted_language_eq_pI Mealy_Machine.accepted_language_eq_p_alphE)
          from welldefined_mealy_of m_inv_M show "welldefined_mealy_p (mealy_of M)"
            using \<open>welldefined_mealy_p (mealy_of M)\<close> by auto
        qed
      qed
      then show "Moore_Machine.less_states_than (moore_of M\<^sub>L) M"
        by(simp add: moore_of_mealy_of)
    qed
  qed
  
  text \<open>The oracle returns bounded counter examples.\<close>
  show "\<And>conjecture x.
       \<lbrakk>welldefined_dfa_p conjecture; set (moore_machine.get_alphabet conjecture) = set \<Sigma>;
        follows_prop_p conjecture (mealy_vector_wellformedp (set \<Sigma>));
        Moore_Machine.num_states conjecture \<le> Moore_Machine.num_states (moore_of M\<^sub>L);
        moore_oraclec_of oracleC conjecture = Some x\<rbrakk>
       \<Longrightarrow> length x \<le> m_bound"
  proof -
    fix M x
    assume assms:
     "welldefined_dfa_p M" "set (moore_machine.get_alphabet M) = set \<Sigma>"
     "follows_prop_p M (mealy_vector_wellformedp (set \<Sigma>))"
     "Moore_Machine.num_states M \<le> Moore_Machine.num_states (moore_of M\<^sub>L)"
      and *: "moore_oraclec_of oracleC M = Some x"
    then have "\<exists>y. oracleC (mealy_of M) = Some y" unfolding moore_oraclec_of_def
      by auto
    then obtain y where defy: "oracleC (mealy_of M) = Some y" by auto
    have "length y \<le> m_bound" proof(rule oracleC_bounded)
      from \<open>oracleC (mealy_of M) = Some y\<close> show "oracleC (mealy_of M) = Some y" .
      show "welldefined_mealy_p (mealy_of M)"
        by (simp add: assms follows_prop_p_wdefE(2))
      show "set (mealy_machine.get_alphabet (mealy_of M)) = set \<Sigma>"
        by (simp add: assms(2) mealy_of_def)
      show "Mealy_Machine.num_states (mealy_of M) \<le> Mealy_Machine.num_states M\<^sub>L"
        by (simp add: assms(4) moore_of_mealy_of num_states_moore_of)
    qed
    then have "length (butlast y) \<le> m_bound" by simp
    moreover have "x = (butlast y)" using * defy
      by (simp add: moore_oraclec_of_def)
    ultimately show "length x \<le> m_bound" by simp 
  qed

  text \<open>Showing oracleC-correct-None\<close>
  show "\<And>conjecture.
       \<lbrakk>welldefined_dfa_p conjecture; set (moore_machine.get_alphabet conjecture) = set \<Sigma>;
        follows_prop_p conjecture (mealy_vector_wellformedp (set \<Sigma>))\<rbrakk>
       \<Longrightarrow> (moore_oraclec_of oracleC conjecture = None) =
        Moore_Machine.accepted_language_eq_p conjecture \<Sigma> (moore_oraclem_of \<Sigma> oracleM)"
  proof -
      fix M :: "('letter, 'letter list, ('letter,'mealyo) mealy_vector) moore_machine"
      assume
        wdefM: "welldefined_dfa_p M" and
        sameA: "set (moore_machine.get_alphabet M) = set \<Sigma>" and
        fprop: "follows_prop_p M (mealy_vector_wellformedp (set \<Sigma>))"
      from fprop have m_inv_M: "mealy_inv M" unfolding mealy_inv_def
        by (simp add: sameA)
      have *: "oracleC (mealy_of M) = None \<longleftrightarrow> accepted_language_eq_p (mealy_of M) \<Sigma> oracleM"
        by (simp add: Mealy_Machine.accepted_language_eq_p_def oracleC_correct_None oracleM_correct)
      have "(moore_oraclec_of oracleC M = None) \<longleftrightarrow> oracleC (mealy_of M) = None"
        by(simp add: moore_oraclec_of_def)
      also have "... \<longleftrightarrow> accepted_language_eq_p (mealy_of M) \<Sigma> oracleM" by(simp add: *)
      also have "... \<longleftrightarrow> Moore_Machine.accepted_language_eq_p M \<Sigma> (moore_oraclem_of \<Sigma> oracleM)"
      (is "?lhs \<longleftrightarrow> ?rhs") proof(rule iffI)
        show "?lhs \<Longrightarrow> ?rhs" using mealy_acc_lang_imp_moore_acc_lang using fprop wdefM by blast
        show "?rhs \<Longrightarrow> ?lhs" using moore_acc_lang_imp_mealy_acc_lang using fprop wdefM by blast
      qed
      finally show "(moore_oraclec_of oracleC M = None) =
        Moore_Machine.accepted_language_eq_p M \<Sigma> (moore_oraclem_of \<Sigma> oracleM)" .
    qed

  text \<open>Showing oracleC-correct-Some\<close>
  show "\<And>conjecture x.
       \<lbrakk>welldefined_dfa_p conjecture; set (moore_machine.get_alphabet conjecture) = set \<Sigma>;
        moore_oraclec_of oracleC conjecture = Some x;
        follows_prop_p conjecture (mealy_vector_wellformedp (set \<Sigma>))\<rbrakk>
       \<Longrightarrow> x \<in> lists (set \<Sigma>) \<and> moore_oraclem_of \<Sigma> oracleM x \<noteq> Moore_Machine.output_on conjecture x"
  proof -
    fix M :: "('letter, 'letter list, ('letter,'mealyo) mealy_vector) moore_machine"
    fix t
    assume
      wdefM: "welldefined_dfa_p M" and
      sameA: "set (moore_machine.get_alphabet M) = set \<Sigma>" and
      deft: "moore_oraclec_of oracleC M = Some t" and
      fprop: "follows_prop_p M (mealy_vector_wellformedp (set \<Sigma>))"
    have "mealy_inv M" and "welldefined_mealy_p (mealy_of M)"
      using follows_prop_p_wdefE[OF fprop wdefM] sameA by auto
    from deft have *: "\<exists>ta. oracleC (mealy_of M) = Some ta"
      by(auto simp add: moore_oraclec_of_def)
    then obtain ta where defta: "oracleC (mealy_of M) = Some ta" by auto
    then have "ta \<in> \<Sigma>star \<and> ta \<noteq> [] \<and> L ta \<noteq> Mealy_Machine.output_on (mealy_of M) ta" 
      using oracleC_correct_Some_correct \<open>welldefined_mealy_p (mealy_of M)\<close> * sameA deft
      by (simp add: mealy_of_def)
    then have "ta \<in> \<Sigma>star" and "ta \<noteq> []" and 
      mealy_diff: "L ta \<noteq> Mealy_Machine.output_on (mealy_of M) ta" by auto
    from defta deft have "t = (butlast ta)" by(simp add: moore_oraclec_of_def)
    from this and \<open>ta \<noteq> []\<close> have "\<exists>a. ta = t@[a]"
      by (metis append_butlast_last_id)
    then obtain a where defa: "ta = t@[a]" by auto
    from this \<open>ta \<in> \<Sigma>star\<close> have "t \<in> \<Sigma>star"
      by (simp add: def_sigmastar)
    from defa \<open>ta \<in> \<Sigma>star\<close> have "a \<in> set \<Sigma>"
      by (simp add: def_sigmastar)
    
    \<comment> \<open> We calculate from each side of the equality, and show the resulting terms are
         different\<close>

    have "Moore_Machine.output_on M t = Moore_Machine.output_on (moore_of (mealy_of M)) t"
      by(simp add: moore_of_mealy_of)
    also have "... = mealy_vector_of \<Sigma> (\<lambda>a. output_on (mealy_of M) (t@[a]))"
      using output_on_moore_of
      by (metis \<open>t \<in> \<Sigma>star\<close> \<open>welldefined_mealy_p (mealy_of M)\<close> def_sigmastar
          mealy_machine.simps(1) mealy_of_def sameA)
    finally have one:
      "Moore_Machine.output_on M t = mealy_vector_of \<Sigma> (\<lambda>a. output_on (mealy_of M) (t@[a]))"
      .

    have "moore_oraclem_of \<Sigma> oracleM t = mealy_vector_of \<Sigma> (\<lambda>a. oracleM (t@[a]))"
      by(simp add: moore_oraclem_of_def)
    also have "... = mealy_vector_of \<Sigma> (\<lambda>a. L (t@[a]))" using oracleM_correct by simp
    finally have two:
      "moore_oraclem_of \<Sigma> oracleM t = mealy_vector_of \<Sigma> (\<lambda>a. L (t@[a]))" .

    have diff:
      "mealy_vector_of \<Sigma> (\<lambda>a. output_on (mealy_of M) (t@[a])) \<noteq> mealy_vector_of \<Sigma> (\<lambda>a. L (t@[a]))"
    (is "?lhs \<noteq> ?rhs") proof -
      from mealy_diff have "?lhs a \<noteq> ?rhs a"
        by (simp add: \<open>a \<in> set \<Sigma>\<close> defa mealy_vector_of_Some)
      then show ?thesis by auto
    qed

    from one two diff have "moore_oraclem_of \<Sigma> oracleM t \<noteq> Moore_Machine.output_on M t"
      by simp
    moreover from \<open>t \<in> \<Sigma>star\<close> have "t \<in> \<Sigma>star" .
    ultimately show "t \<in> lists (set \<Sigma>) \<and> moore_oraclem_of \<Sigma> oracleM t \<noteq> Moore_Machine.output_on M t"
      by(simp add: def_sigmastar)
  qed
qed

(* We can lift the correctness lemma (any lemma) for mealy machines. *)
thm mtom.lstar_correct

end

section \<open>Lifting the L* algorithm for Mealy machines\<close>
text \<open>With the previous functions we can make a variant of the L* algorithm for
      mealy machines.\<close>
fun lstar_learner_conjecture_mealy ::
  "'letter fset \<Rightarrow> ('letter,'mealyo) membership_oracle \<Rightarrow>
   ('letter, 'letter list, 'mealyo) conjecture_oracle_mealy \<Rightarrow> ('letter, 'letter list, 'mealyo) mealy_machine"
  where
  "lstar_learner_conjecture_mealy alph orcM orcC =
     mealy_of ((make_conjecture \<circ> fst) (lstar_learner alph (moore_oraclem_of alph orcM) (moore_oraclec_of orcC)))"

export_code lstar_learner_conjecture_mealy moore_of in Haskell module_name ExportedMealy
  
end