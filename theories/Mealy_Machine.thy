section \<open> Mealy-Machine definitions \<close>

theory Mealy_Machine
  imports
    Main
    Finite_Sets
    Automata_Common
begin
 
text \<open> Contains basic definition of Mealy machines. \<close>

subsection \<open> Main definitions and lemmas \<close>

text \<open> Note that the Mealy-machine is defined over the fset type (finite set) as statespace. \<close>
record ('a,'s,'o) mealy_machine =
    get_alphabet :: "'a fset" \<comment> \<open> For flexibility, we make the alphabet known to the machine.\<close>
    get_statespace :: "'s fset "
    get_initial :: "'s"
    get_transition :: "'a \<Rightarrow> 's \<Rightarrow> 's"
    \<comment> \<open> The main difference between a dfa and a mealy machine is the output function !\<close>
    get_finalf :: "'s \<Rightarrow> ('a,'o) map"

text \<open> The transition function extended to work on words
       The mixfix annotation still allows for partial application \<close>
definition delta :: "('a,'s,'o) mealy_machine \<Rightarrow> 'a list \<Rightarrow> 's \<Rightarrow> 's" ("\<delta>\<langle>_\<rangle>") where
 " delta A = foldl2 (get_transition A) "

lemma delta_Nil[simp]: "\<delta>\<langle>A\<rangle> [] s = s"
  by(simp add:delta_def)

lemma delta_Cons[simp]: "\<delta>\<langle>A\<rangle> (w@[a]) s = (get_transition A) a (\<delta>\<langle>A\<rangle> w s)"
  by(simp add:delta_def foldl2_def)

lemma delta_append[simp]: "\<delta>\<langle>A\<rangle> (xs@ys) q = \<delta>\<langle>A\<rangle> ys (\<delta>\<langle>A\<rangle> xs q)"
  apply(induction xs)
  apply(simp_all add: delta_def foldl2_def)
  done

(* It's not a good simp rule, since we could keep decomposing *)
lemma delta_Cons_head: "\<delta>\<langle>A\<rangle> (x#xs) q = \<delta>\<langle>A\<rangle> xs (\<delta>\<langle>A\<rangle> [x] q)"
proof -
  have "\<delta>\<langle>A\<rangle> (x#xs) q = \<delta>\<langle>A\<rangle> ([x]@xs) q" by(simp)
  also have "    ... = \<delta>\<langle>A\<rangle> xs (\<delta>\<langle>A\<rangle> [x] q)" by(simp only: delta_append)
  finally show ?thesis .
qed

(* This is an attempt at making it easier to work with *)
lemma delta_ConsI: "\<lbrakk>\<delta>\<langle>A\<rangle> [x] q = r; \<delta>\<langle>A\<rangle> xs r = s\<rbrakk> \<Longrightarrow> \<delta>\<langle>A\<rangle> (x#xs) q = s"
  by (meson delta_Cons_head)

subsection \<open> Well-definiteness \<close>
text \<open> Note that nothing actually insures that the delta function stays in the statespace !
       We'll need this for further notions, thus we define a 'welldefiniteness' predicate. \<close>

definition welldefined_mealy_p :: " ('a, 's, 'o) mealy_machine \<Rightarrow> bool " where
  " welldefined_mealy_p machine \<equiv>
    \<comment> \<open> The initial state is in the statespace\<close>
    ((get_initial machine) \<in> set (get_statespace machine)) \<and> 
    \<comment> \<open> The transition function stays in the statespace \<close>
    (\<forall>s \<in> set (get_statespace machine). \<forall>a \<in> (set (get_alphabet machine)).
     (get_transition machine) a s \<in> set (get_statespace machine)) \<and>
    (\<forall>s \<in> set (get_statespace machine).
     dom (get_finalf machine s) = set (get_alphabet machine))"
               
lemma welldefined_mealy_pI[intro]: "\<lbrakk>((get_initial machine) \<in> set (get_statespace machine));
     \<And>s a. \<lbrakk>s \<in> set (get_statespace machine); a \<in> (set (get_alphabet machine))\<rbrakk>
      \<Longrightarrow> (get_transition machine) a s \<in> set (get_statespace machine);
     \<And>s. s \<in> set (get_statespace machine)
      \<Longrightarrow> dom (get_finalf machine s) = set (get_alphabet machine)\<rbrakk>
     \<Longrightarrow> welldefined_mealy_p machine"
  by(auto simp add: welldefined_mealy_p_def)

text \<open> If the transition function stays in the statespace, we can extend this to the 
       delta function as well. \<close>
lemma welldefined_mealy_p_imp_delta_in_statespace:
  "welldefined_mealy_p dfa 
    \<Longrightarrow> (\<forall>s \<in> set (get_statespace dfa). \<forall>w \<in> lists (set (get_alphabet dfa)).
     \<delta>\<langle>dfa\<rangle> w s \<in> set (get_statespace dfa))"
proof(rule ballI, rule ballI)
  fix s w
  assume wdef: "welldefined_mealy_p dfa" and
         defs: "s \<in> set (get_statespace dfa)" and
         defw: "w \<in> lists (set (get_alphabet dfa))"
  then show "\<delta>\<langle>dfa\<rangle> w s \<in> set (get_statespace dfa)" proof(induction w arbitrary: s)
  case Nil
    then show ?case by(simp)
  next
  case (Cons a w)
    then show ?case by (simp add: delta_def welldefined_mealy_p_def)
  qed
qed

text \<open> Elim rules for wdef dfa \<close>

lemma welldefined_mealy_p_initialE[elim]: "welldefined_mealy_p machine \<Longrightarrow> (get_initial machine) \<in> set (get_statespace machine)"
  by(simp add: welldefined_mealy_p_def)

lemma welldefined_mealy_p_deltaE[elim]: 
  "\<lbrakk>welldefined_mealy_p machine; s \<in> set (get_statespace machine); w \<in> lists (set (get_alphabet machine))\<rbrakk>
   \<Longrightarrow> \<delta>\<langle>machine\<rangle> w s \<in> set (get_statespace machine)"
  by(simp add: welldefined_mealy_p_imp_delta_in_statespace)

lemma welldefined_mealy_p_finalfE[elim]: "\<lbrakk>welldefined_mealy_p machine; s \<in> set (get_statespace machine)\<rbrakk>
  \<Longrightarrow> dom (get_finalf machine s) = set (get_alphabet machine)"
  by(simp add: welldefined_mealy_p_def)

subsection \<open> Output of the machine on words \<close>

value "butlast ([] :: nat list)"

text \<open> Gets the output of the moore machine on some word \<close>
definition output_on :: "('a,'s,'o) mealy_machine \<Rightarrow> 'a list \<Rightarrow> 'o" where
  " output_on m word = the ((get_finalf m) (\<delta>\<langle>m\<rangle> (butlast word) (get_initial m)) (last word))"         

text \<open> Predicate checking if the accepted language is equal to another language.
       We have to restrict the input words to non-empty (mealy) words in the alphabet !\<close>
definition accepted_language_eq_p ::
  " ('a,'s,'o) mealy_machine \<Rightarrow> 'a fset \<Rightarrow> ('a list \<Rightarrow> 'o) \<Rightarrow> bool" where
  " accepted_language_eq_p m \<Sigma> L \<equiv>
      (set (get_alphabet m) = set \<Sigma>) \<and>
      (\<forall>w \<in> lists (set (get_alphabet m)). w\<noteq>[] \<longrightarrow> output_on m w = L w)"

lemma accepted_language_eq_pI[intro]:
  "\<lbrakk>(set (get_alphabet m) = set \<Sigma>); \<And>w. w \<in> lists (set (get_alphabet m)) \<Longrightarrow> w \<noteq> []
     \<Longrightarrow> output_on m w = L w\<rbrakk>
    \<Longrightarrow> accepted_language_eq_p m \<Sigma> L"
  by(auto simp add: accepted_language_eq_p_def)

lemma accepted_language_eq_p_alphE[elim]:
  "accepted_language_eq_p M \<Sigma> L \<Longrightarrow> (set (get_alphabet M) = set \<Sigma>)"
  by(auto simp add: accepted_language_eq_p_def)

lemma accepted_language_eq_pE[elim]:
  "\<lbrakk>accepted_language_eq_p M \<Sigma> L; w \<in> lists (set (get_alphabet M)); w \<noteq> []\<rbrakk> \<Longrightarrow> 
     output_on M w = L w"
  by(auto simp add: accepted_language_eq_p_def)

subsection \<open> Number of states \<close>

abbreviation num_states :: " ('a,'s,'o) mealy_machine \<Rightarrow> nat " where
  " num_states machine \<equiv> card (set (get_statespace machine)) "

subsection \<open> Mealy-machine Isomorphisms \<close>

text \<open> An isomorphism between Mealy machines is simply a bijection between their statespace which also:
       0) maps the initials states to each other
       1) respects the transition function
       2) respects the output function
       We also require the input and output alphabets to match
       (for simplicity we don't model morphisms on alphabets)\<close>

definition iso_betw_p ::
  "('s \<Rightarrow> 't) \<Rightarrow> ('a,'s,'o) mealy_machine \<Rightarrow> ('a,'t,'o) mealy_machine \<Rightarrow> bool " where
  " iso_betw_p f M Q \<equiv>
    \<comment> \<open> The alphabets are equal \<close>
    (set (get_alphabet M) = set (get_alphabet Q)) \<and>
    \<comment> \<open> f is a bijection \<close>
    bij_betw f (set (get_statespace M)) (set (get_statespace Q)) \<and>
    \<comment> \<open> f maps q0 to q0' \<close>
    f (get_initial M) = get_initial Q \<and>
    \<comment> \<open> f respects the trans. function \<close>
   (\<forall>s \<in> set (get_statespace M). \<forall>t \<in> set (get_statespace M). \<forall>a \<in> set (get_alphabet M).
     (get_transition M a) s = t \<longleftrightarrow> (get_transition Q a) (f s) = f t) \<and>
    \<comment> \<open> f respects the output function \<close>
   (\<forall>s \<in> set (get_statespace M). (get_finalf M) s = (get_finalf Q) (f s))"

subsection \<open> Intro and elim rules. \<close>

lemma iso_betw_pI[intro]: 
  "\<lbrakk>(set (get_alphabet M) = set (get_alphabet Q));
    bij_betw f (set (get_statespace M)) (set (get_statespace Q));
    f (get_initial M) = get_initial Q;
    \<And>s t a. 
     \<lbrakk>s \<in> set (get_statespace M);
      t \<in> set (get_statespace M);
      a \<in> set (get_alphabet M)\<rbrakk> \<Longrightarrow>
     (get_transition M a) s = t \<longleftrightarrow> (get_transition Q a) (f s) = f t;
   (\<And>s. s \<in> set (get_statespace M) \<Longrightarrow> (get_finalf M) s = (get_finalf Q) (f s))\<rbrakk>
    \<Longrightarrow> iso_betw_p f M Q"
  by(auto simp add: iso_betw_p_def)

text \<open>We 'augment' the elimination rule to get that f respects the delta function by extending the statement to full words \<close>

lemma iso_betw_p_alph_equalE[elim]:
  "iso_betw_p f M Q \<Longrightarrow> set (get_alphabet M) = set (get_alphabet Q)"
  by(simp add: iso_betw_p_def)

lemma iso_betw_p_bijE[elim]:
  "iso_betw_p f M Q \<Longrightarrow> bij_betw f (set (get_statespace M)) (set (get_statespace Q))"
  by(simp add: iso_betw_p_def)

lemma iso_betw_p_initStatesE[elim]:
  "iso_betw_p f M Q \<Longrightarrow> f (get_initial M) = get_initial Q"
  by(simp add: iso_betw_p_def)

lemma iso_betw_p_finalfE[elim]:
  "\<lbrakk>iso_betw_p f M Q; s \<in> set (get_statespace M)\<rbrakk> \<Longrightarrow>
     (get_finalf M) s = (get_finalf Q) (f s)"
  by(simp add: iso_betw_p_def)

text \<open> This lemma "enhances" the fact by lifting it to the delta function \<close>
lemma iso_betw_p_imp_respects_delta: "welldefined_mealy_p M \<Longrightarrow> iso_betw_p f M Q \<Longrightarrow> 
(\<forall>s \<in> set (get_statespace M). \<forall>t \<in> set (get_statespace M). \<forall>w \<in> lists (set (get_alphabet M)).
     \<delta>\<langle>M\<rangle> w s = t \<longleftrightarrow> \<delta>\<langle>Q\<rangle> w (f s) = f t)"
proof(rule ballI, rule ballI, rule ballI)
  fix s t w
  assume wdef: "welldefined_mealy_p M" and
         iso: "iso_betw_p f M Q" and
         defs: "s \<in> set (get_statespace M)" and
         deft: "t \<in> set (get_statespace M)" and
         defw: "w \<in> lists (set (get_alphabet M))"
  \<comment> \<open> We have the statement, but only for 1 letter words \<close>
  from iso have respects1: 
    "(\<forall>s \<in> set (get_statespace M). \<forall>t \<in> set (get_statespace M). \<forall>a \<in> set (get_alphabet M).
     (get_transition M) a s = t \<longleftrightarrow> (get_transition Q) a (f s) = f t)" by(simp add: iso_betw_p_def)
  \<comment> \<open> We also know that f is a bijection \<close>
  from iso have fbij: "bij_betw f (set (get_statespace M)) (set (get_statespace Q))" by(rule iso_betw_p_bijE)
  from fbij defs deft defw show "(\<delta>\<langle>M\<rangle> w s = t) = (\<delta>\<langle>Q\<rangle> w (f s) = f t)"
  proof(induction w arbitrary: t rule: rev_induct)
  case Nil
    \<comment> \<open> This case follows from injectivity of f \<close>
    then show ?case proof(simp only: delta_Nil)
      qed(drule bij_betw_imp_inj_on, auto simp add: inj_onD)
  next
    case (snoc x xs)
    \<comment> \<open> The IH gives us: \<close>
    from snoc have ih: "\<And>t. t \<in> set (get_statespace M) \<Longrightarrow> (\<delta>\<langle>M\<rangle> xs s = t) = (\<delta>\<langle>Q\<rangle> xs (f s) = f t)" by(simp)
    show ?case
      using ih
      by (metis append_in_lists_conv delta_Cons listsE respects1 snoc.prems(2) snoc.prems(3) snoc.prems(4) wdef welldefined_mealy_p_deltaE)
  qed
qed

text \<open> An elimination rule giving the generalized fact \<close>
lemma iso_betw_p_respects_deltaE: 
  "\<lbrakk>welldefined_mealy_p M; iso_betw_p f M Q; s \<in> set (get_statespace M); t \<in> set (get_statespace M); w \<in> lists (set (get_alphabet M))\<rbrakk> \<Longrightarrow>
     \<delta>\<langle>M\<rangle> w s = t \<longleftrightarrow> \<delta>\<langle>Q\<rangle> w (f s) = f t"
  by(auto simp add: iso_betw_p_imp_respects_delta)

lemma iso_betw_p_finalpE[elim]:
  "\<lbrakk>iso_betw_p f M Q; s \<in> set (get_statespace M); a \<in> set (get_alphabet M)\<rbrakk>
    \<Longrightarrow> (get_finalf M) s a = (get_finalf Q) (f s) a"
  by(auto simp add: iso_betw_p_def)

subsection \<open> Relation to definition in the paper. \<close>

text \<open> In Angluin's paper, isomorphisms respecting the transition function is formalized as $f$ commutes
       with $\delta$. We show that the notion above is equivalent \<close>

text \<open> Note, for convenience we also generalized this to words (instead of single letters) \<close>
lemma iso_betw_p_imp_delta_commute[simp]:
  "\<lbrakk>welldefined_mealy_p M; iso_betw_p f M Q; s \<in> set (get_statespace M); w \<in> lists (set (get_alphabet M))\<rbrakk> \<Longrightarrow> 
   f (\<delta>\<langle>M\<rangle> w s) = \<delta>\<langle>Q\<rangle> w (f s)"
  using iso_betw_p_respects_deltaE welldefined_mealy_p_deltaE by fastforce

lemma delta_commute_imp_delta_respects: 
           "\<lbrakk>welldefined_mealy_p M; set (get_alphabet M) = set (get_alphabet Q); bij_betw f (set (get_statespace M)) (set (get_statespace Q))\<rbrakk> \<Longrightarrow>
           (\<forall>s \<in> set (get_statespace M). \<forall>a \<in> set (get_alphabet M). f (\<delta>\<langle>M\<rangle> [a] s) = \<delta>\<langle>Q\<rangle> [a] (f s)) \<Longrightarrow>
           (\<forall>s \<in> set (get_statespace M). \<forall>a \<in> set (get_alphabet M). \<forall>t \<in> set (get_statespace M). (get_transition M a) s = t \<longleftrightarrow> (get_transition Q a) (f s) = f t)"
proof -
  assume wdefined: "welldefined_mealy_p M" and
         sameAlph: "set (get_alphabet M) = set (get_alphabet Q)" and
         fbij: "bij_betw f (set (get_statespace M)) (set (get_statespace Q))" and
         fcommute: "(\<forall>s \<in> set (get_statespace M). \<forall>a \<in> set (get_alphabet M). f (\<delta>\<langle>M\<rangle> [a] s) = \<delta>\<langle>Q\<rangle> [a] (f s))"
  show "(\<forall>s \<in> set (get_statespace M). \<forall>a \<in> set (get_alphabet M). \<forall>t \<in> set (get_statespace M). (get_transition M a) s = t \<longleftrightarrow> (get_transition Q a) (f s) = f t)"
  proof (rule ballI, rule ballI, rule ballI)
    fix s a t
    assume defs: "s \<in> set (get_statespace M)" and defa: "a \<in> set (get_alphabet M)" and deft: "t \<in> set (get_statespace M)"
    from fbij have finj: "inj_on f (set (get_statespace M))" by(simp add: bij_betw_imp_inj_on)
    from wdefined defa defs have **: "(\<delta>\<langle>M\<rangle> [a] s) \<in> set (get_statespace M)" by(auto intro: welldefined_mealy_p_imp_delta_in_statespace)
    then show "(get_transition M a s = t) = (get_transition Q a (f s) = f t)" proof -
      from fcommute and defs defa have *: "f (\<delta>\<langle>M\<rangle> [a] s) = \<delta>\<langle>Q\<rangle> [a] (f s)" by(simp)
      \<comment> \<open> We replace get-transition by delta of one element for simplicity first \<close>
      then have "\<delta>\<langle>M\<rangle> [a] s = t \<longleftrightarrow> \<delta>\<langle>Q\<rangle> [a] (f s) = f t" proof -
        have "(\<delta>\<langle>Q\<rangle> [a] (f s) = f t) \<longleftrightarrow> f (\<delta>\<langle>M\<rangle> [a] s) = f t" using * by(simp)
        also have "... \<longleftrightarrow> (\<delta>\<langle>M\<rangle> [a] s) = t" using ** finj
          by (meson deft inj_onD)
        finally show ?thesis by(simp)
      qed
      then show ?thesis by (metis append_Nil delta_Cons delta_Nil)
    qed
  qed
qed

subsection \<open> Properties of dfa-Isomorphisms \<close>

text \<open> Having an isomorphism implies trivially that the number of states is equal \<close>
lemma iso_betw_p_imp_eq_numStates: "iso_betw_p f M Q \<Longrightarrow> num_states M = num_states Q"
  using bij_betw_same_card iso_betw_p_bijE by blast

text \<open> Most importantly; Isomorphic machines give the same outputs !
       Note that we *need* the machines(s) to be well defined (isomorphism carries welldefiniteness !) \<close>
lemma iso_betw_p_imp_eq_acceptsP:
  assumes iso:  " iso_betw_p f M Q " and
         wdef:  " welldefined_mealy_p M " and
         \<comment> \<open> For a mealy-machine, the output function is defined on sigma plus (non-empty sequences)\<close>
         defw1:  " w \<in> (lists (set (get_alphabet M))) " and
         defw2:  " w \<noteq> []"
       shows " output_on M w = output_on Q w "
using assms proof(induction w)
  case Nil
  \<comment> \<open> This case never happens for mealy machines !\<close>
  then show ?case using defw1 by auto
    next
  case (Cons a w)
  \<comment> \<open> We case-split again on w, to use simp rules for butlast\<close>
  then show ?case proof(cases w)
    case Nil
    then show ?thesis
      by (metis (no_types, lifting) Cons.prems(1) Cons.prems(3) Cons_in_lists_iff butlast.simps(2) delta_Nil iso_betw_p_finalpE iso_betw_p_initStatesE last_ConsL output_on_def wdef welldefined_mealy_p_initialE)
  next
    case (Cons wh ws)
    have *: "get_finalf M (\<delta>\<langle>M\<rangle> (butlast (a # w)) (get_initial M)) (last (a # w)) =
      get_finalf M (\<delta>\<langle>M\<rangle> (butlast w) (\<delta>\<langle>M\<rangle> [a] (get_initial M))) (last (a # w))"
      by (simp add: delta_def local.Cons)
    have **: "get_finalf Q (\<delta>\<langle>Q\<rangle> (butlast (a # w)) (get_initial Q)) (last (a # w)) =
      get_finalf Q(\<delta>\<langle>Q\<rangle> (butlast w) (\<delta>\<langle>Q\<rangle> [a] (get_initial Q))) (last (a # w)) "
      by (simp add: delta_def local.Cons)
    have "get_finalf M (\<delta>\<langle>M\<rangle> (butlast w) (\<delta>\<langle>M\<rangle> [a] (get_initial M))) (last (a # w)) =
      get_finalf Q (\<delta>\<langle>Q\<rangle> (butlast w) (\<delta>\<langle>Q\<rangle> [a] (get_initial Q))) (last (a # w))"
    proof -
      have "f (\<delta>\<langle>M\<rangle> [a] (get_initial M)) = (\<delta>\<langle>Q\<rangle> [a] (get_initial Q))"
        using Cons.prems(3) iso iso_betw_p_initStatesE wdef welldefined_mealy_p_initialE
        by fastforce
      then have "f (\<delta>\<langle>M\<rangle> (butlast w) (\<delta>\<langle>M\<rangle> [a] (get_initial M))) =
                   (\<delta>\<langle>Q\<rangle> (butlast w) (\<delta>\<langle>Q\<rangle> [a] (get_initial Q)))"
        by (metis (no_types, lifting) Cons.prems(3) append_butlast_last_id append_in_lists_conv
            butlast.simps(1) butlast.simps(2) delta_ConsI iso iso_betw_p_imp_delta_commute
            iso_betw_p_initStatesE wdef welldefined_mealy_p_initialE)
      then show ?thesis
        (* TODO: Fix this metis call, it works but it -very- long (~20 seconds) *)
        by (metis (no_types, lifting) Cons.prems(3) Cons_in_lists_iff append_butlast_last_id
            append_in_lists_conv butlast.simps(1) iso iso_betw_p_finalpE
            last.simps wdef welldefined_mealy_p_deltaE welldefined_mealy_p_initialE)
    qed
    then show ?thesis using * **
      by (simp add: output_on_def)
  qed
qed

lemma iso_betw_p_accepted_language_eq_p: 
  assumes iso:  "iso_betw_p f M Q" and 
          wdef: " welldefined_mealy_p M "
        shows  "accepted_language_eq_p M \<Sigma> L = accepted_language_eq_p Q \<Sigma> L"
proof -                    
  from assms iso_betw_p_imp_eq_acceptsP have
    *: "\<And>w. w \<in> lists (set (get_alphabet M)) \<Longrightarrow> w \<noteq> [] \<Longrightarrow> output_on M w = output_on Q w"
    by(auto)
  show ?thesis proof(rule iffI)
    assume as1: "accepted_language_eq_p M \<Sigma> L"
    show "accepted_language_eq_p Q \<Sigma> L" proof(rule accepted_language_eq_pI)
      show "\<And>w. \<lbrakk>w \<in> lists (set (get_alphabet Q)); w \<noteq> []\<rbrakk> \<Longrightarrow> output_on Q w = L w"
        using * as1 accepted_language_eq_pE iso iso_betw_p_alph_equalE by fastforce
      from iso as1 show "set (get_alphabet Q) = set \<Sigma>"
        using accepted_language_eq_p_def iso_betw_p_alph_equalE by fastforce
    qed
  next
    assume as2: "accepted_language_eq_p Q \<Sigma> L"
    show "accepted_language_eq_p M \<Sigma> L" proof(rule accepted_language_eq_pI)
      show "\<And>w. \<lbrakk>w \<in> lists (set (get_alphabet M)); w \<noteq> []\<rbrakk> \<Longrightarrow> output_on M w = L w"
        using * as2 accepted_language_eq_pE iso iso_betw_p_alph_equalE by fastforce
      from iso as2 show "set (get_alphabet M) = set \<Sigma>"
        using accepted_language_eq_p_def iso_betw_p_alph_equalE by fastforce
    qed
  qed
qed

subsection \<open> Order relation on DFAs \<close>
text \<open> With number of states and isomorphisms, we define 'smaller number of states; up to an isomorphism'
       as an order relation on DFAs \<close>

definition less_states_than :: " ('a,'s,'o) mealy_machine \<Rightarrow> ('a,'t,'o) mealy_machine \<Rightarrow> bool"
  ("(_) \<sqsubseteq> (_)" [64,64]69) where
  " less_states_than M Q \<equiv> (num_states M < num_states Q) \<or> (\<exists>f. iso_betw_p f M Q)"

subsection \<open> Minimal acceptors \<close>
text \<open> We have all the tools needed to define the concept of a 'minimal' acceptor; An acceptor
       for a weighted language L with minimal number of states (up to isomorphism). \<close>

text \<open> Note: Compared to a dfa, the language L is changed from a set to a weighted language with
       type ('a list => 'o).\<close>
definition minimal_acceptor_p :: " ('a,'s,'o) mealy_machine \<Rightarrow> 'a fset \<Rightarrow> ('a list \<Rightarrow> 'o) \<Rightarrow> bool" where
   " minimal_acceptor_p dfa \<Sigma> L \<equiv> accepted_language_eq_p dfa \<Sigma> L \<and>
                             (welldefined_mealy_p dfa) \<and>
                             (\<forall>M :: ('a,'a list,'o) mealy_machine.
                                accepted_language_eq_p M \<Sigma> L \<and> welldefined_mealy_p M \<longrightarrow> dfa \<sqsubseteq> M) "

lemma minimal_acceptor_p_languageD[dest]:
  "minimal_acceptor_p dfa \<Sigma> L \<Longrightarrow> accepted_language_eq_p dfa \<Sigma> L"
  by(simp add: minimal_acceptor_p_def)

lemma minimal_acceptor_p_wdefD[dest]:
  "minimal_acceptor_p dfa \<Sigma> L \<Longrightarrow> (welldefined_mealy_p dfa) "
  by(simp add: minimal_acceptor_p_def)

lemma minimal_acceptor_p_minD[dest]:
  "minimal_acceptor_p dfa \<Sigma> L \<Longrightarrow>
   (\<And>M :: ('a,'a list,'o) mealy_machine.
  accepted_language_eq_p M \<Sigma> L \<Longrightarrow> welldefined_mealy_p M \<Longrightarrow> dfa \<sqsubseteq> M)"
  by(simp add: minimal_acceptor_p_def)

lemma minimal_acceptor_pI[intro]:
  "\<lbrakk>accepted_language_eq_p dfa \<Sigma> L;
    welldefined_mealy_p dfa;
    \<And>M :: ('a,'a list,'o) mealy_machine.
    accepted_language_eq_p M \<Sigma> L \<Longrightarrow> welldefined_mealy_p M \<Longrightarrow> dfa \<sqsubseteq> M\<rbrakk>
    \<Longrightarrow> minimal_acceptor_p dfa \<Sigma> L "
  by(simp add: minimal_acceptor_p_def)

end