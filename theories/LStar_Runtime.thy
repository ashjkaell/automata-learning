theory LStar_Runtime
  imports
    LStar_Properties
begin

context MAT_model_moore begin

section \<open> Analysis of the time/space complexity \<close>

subsection \<open> Proofs about log access functions \<close>

lemma get_mkcl_upd_max_quantities[simp]: "get_mkcl (upd_max_quantities tbl log) = get_mkcl log"
  by(cases log, auto simp add: upd_max_quantities_def)

lemma get_mkco_upd_max_quantities[simp]: "get_mkco (upd_max_quantities tbl log) = get_mkco log"
  by(cases log, auto simp add: upd_max_quantities_def)

lemma get_lece_upd_max_quantities[simp]: "get_lece (upd_max_quantities tbl log) = get_lece log"
  by(cases log, auto simp add: upd_max_quantities_def)

lemma get_ncalls_log_mkcl_call[simp]:
  "get_mkcl (log_mkcl_call tbl log) = Suc (get_mkcl log)"
  "get_mkco (log_mkcl_call tbl log) = get_mkco log"
  "get_lece (log_mkcl_call tbl log) = get_lece log"
  by(auto simp add: log_mkcl_call_def)

lemma get_ncalls_log_mkco_call[simp]:
  "get_mkcl (log_mkco_call tbl log) = get_mkcl log"
  "get_mkco (log_mkco_call tbl log) = Suc (get_mkco log)"
  "get_lece (log_mkco_call tbl log) = get_lece log"
  by(auto simp add: log_mkco_call_def)

lemma get_ncalls_log_lece_call[simp]:
  "get_mkcl (log_lece_call tbl log) = get_mkcl log"
  "get_mkco (log_lece_call tbl log) = get_mkco log"
  "get_lece (log_lece_call tbl log) = Suc (get_lece log)"
  by(auto simp add: log_lece_call_def)

text \<open>Properties of maximum\<close>
lemma get_max_len_S_log_mkcl_call:
  "get_max_len_S (log_mkcl_call tbl log) \<ge> (length \<circ> get_S) tbl"
  by(auto simp add: log_mkcl_call_def upd_max_quantities_def)

lemma get_max_len_S_log_mkco_call:
  "get_max_len_S (log_mkco_call tbl log) \<ge> (length \<circ> get_S) tbl"
  by(auto simp add: log_mkco_call_def upd_max_quantities_def)

lemma get_max_len_S_log_lece_call:
  "get_max_len_S (log_lece_call tbl log) \<ge> (length \<circ> get_S) tbl"
  by(auto simp add: log_lece_call_def upd_max_quantities_def)

lemma get_max_str_len_S_log_mkcl_call:
  "get_max_str_len_S (log_mkcl_call tbl log) \<ge> (max_str_len \<circ> set \<circ> get_S) tbl"
  by(auto simp add: log_mkcl_call_def upd_max_quantities_def)

lemma get_max_str_len_S_log_mkco_call:
  "get_max_str_len_S (log_mkco_call tbl log) \<ge> (max_str_len \<circ> set \<circ> get_S) tbl"
  by(auto simp add: log_mkco_call_def upd_max_quantities_def)

lemma get_max_str_len_S_log_lece_call:
  "get_max_str_len_S (log_lece_call tbl log) \<ge> (max_str_len \<circ> set \<circ> get_S) tbl"
  by(auto simp add: log_lece_call_def upd_max_quantities_def)

text \<open> These rules allows to prove properties bounding the maximal size of S seen
       (returned by get-max-len-S).\<close>
lemma max_len_S_log_mkcl_call_leqI[intro]:
  "\<lbrakk>(length \<circ> get_S) tbl \<le> M; get_max_len_S log \<le> M\<rbrakk>
   \<Longrightarrow> get_max_len_S (log_mkcl_call tbl log) \<le> M"
  by(auto simp add: log_mkcl_call_def upd_max_quantities_def)

lemma max_len_S_log_mkco_call_leqI[intro]:
  "\<lbrakk>(length \<circ> get_S) tbl \<le> M; get_max_len_S log \<le> M\<rbrakk>
   \<Longrightarrow> get_max_len_S (log_mkco_call tbl log) \<le> M"
  by(auto simp add: log_mkco_call_def upd_max_quantities_def)

lemma max_len_S_log_lece_call_leqI[intro]:
  "\<lbrakk>(length \<circ> get_S) tbl \<le> M; get_max_len_S log \<le> M\<rbrakk>
   \<Longrightarrow> get_max_len_S (log_lece_call tbl log) \<le> M"
  by(auto simp add: log_lece_call_def upd_max_quantities_def)

text \<open> These rules allows to prove properties bounding the maximal size of E seen (returned by
       get-max-len-E).\<close>
lemma max_len_E_log_mkcl_call_leqI[intro]:
  "\<lbrakk>(length \<circ> get_E) tbl \<le> M; get_max_len_E log \<le> M\<rbrakk>
   \<Longrightarrow> get_max_len_E (log_mkcl_call tbl log) \<le> M"
  by(auto simp add: log_mkcl_call_def upd_max_quantities_def)

lemma max_len_E_log_mkco_call_leqI[intro]:
  "\<lbrakk>(length \<circ> get_E) tbl \<le> M; get_max_len_E log \<le> M\<rbrakk>
   \<Longrightarrow> get_max_len_E (log_mkco_call tbl log) \<le> M"
  by(auto simp add: log_mkco_call_def upd_max_quantities_def)

lemma max_len_E_log_lece_call_leqI[intro]:
  "\<lbrakk>(length \<circ> get_E) tbl \<le> M; get_max_len_E log \<le> M\<rbrakk>
   \<Longrightarrow> get_max_len_E (log_lece_call tbl log) \<le> M"
  by(auto simp add: log_lece_call_def upd_max_quantities_def)

text \<open> These rules allows to prove properties bounding the maximal string size seen in S 
       (returned by get-max-str-len-S).\<close>
lemma max_str_len_S_log_mkcl_call_leqI[intro]:
  "\<lbrakk>(max_str_len \<circ> set \<circ> get_S) tbl \<le> M; get_max_str_len_S log \<le> M\<rbrakk>
   \<Longrightarrow> get_max_str_len_S (log_mkcl_call tbl log) \<le> M"
  by(auto simp add: log_mkcl_call_def upd_max_quantities_def)

lemma max_str_len_S_log_mkco_call_leqI[intro]:
  "\<lbrakk>(max_str_len \<circ> set \<circ> get_S) tbl \<le> M; get_max_str_len_S log \<le> M\<rbrakk>
   \<Longrightarrow> get_max_str_len_S (log_mkco_call tbl log) \<le> M"
  by(auto simp add: log_mkco_call_def upd_max_quantities_def)

lemma max_str_len_S_log_lece_call_leqI[intro]:
  "\<lbrakk>(max_str_len \<circ> set \<circ> get_S) tbl \<le> M; get_max_str_len_S log \<le> M\<rbrakk>
   \<Longrightarrow> get_max_str_len_S (log_lece_call tbl log) \<le> M"
  by(auto simp add: log_lece_call_def upd_max_quantities_def)

text \<open> These rules allows to prove properties bounding the maximal string size seen in E
       (returned by get-max-len-E).\<close>
lemma max_str_len_E_log_mkcl_call_leqI[intro]:
  "\<lbrakk>(max_str_len \<circ> set \<circ> get_E) tbl \<le> M; get_max_str_len_E log \<le> M\<rbrakk>
   \<Longrightarrow> get_max_str_len_E (log_mkcl_call tbl log) \<le> M"
  by(auto simp add: log_mkcl_call_def upd_max_quantities_def)

lemma max_str_len_E_log_mkco_call_leqI[intro]:
  "\<lbrakk>(max_str_len \<circ> set \<circ> get_E) tbl \<le> M; get_max_str_len_E log \<le> M\<rbrakk>
   \<Longrightarrow> get_max_str_len_E (log_mkco_call tbl log) \<le> M"
  by(auto simp add: log_mkco_call_def upd_max_quantities_def)

lemma max_str_len_E_log_lece_call_leqI[intro]:
  "\<lbrakk>(max_str_len \<circ> set \<circ> get_E) tbl \<le> M; get_max_str_len_E log \<le> M\<rbrakk>
   \<Longrightarrow> get_max_str_len_E (log_lece_call tbl log) \<le> M"
  by(auto simp add: log_lece_call_def upd_max_quantities_def)

text \<open> Length is card given the invariant (has disinctiveness of list representation).\<close>

lemma len_eq_card_S: "(lstar_table_inv \<circ> fst) it
  \<Longrightarrow> (length \<circ> get_S \<circ> fst) it = (card \<circ> set \<circ> get_S \<circ> fst) it"
  by (simp add: lstar_table_invE distinct_card)

lemma len_eq_card_E: "(lstar_table_inv \<circ> fst) it
  \<Longrightarrow> (length \<circ> get_E \<circ> fst) it = (card \<circ> set \<circ> get_E \<circ> fst) it"
  by (simp add: lstar_table_invE distinct_card)

subsection \<open> Invariant for the logged iterations. \<close>
text \<open> We define an invariant on the log, carrying the properties we care about.
       It also carries some necessary properties for the parts of the log concerning
       'max seen' quantities.
       We do not enforce that these are indeed the maximum of every ... seen during execution
       because this would require storing keeping a trace of all the observation tables seen
       and is outside the scope of the simple log structure. Moreover, it would not add much
       precision to the model, since correctness of the log has to be 'assumed' either way
       (For instance: We can't enforce that the number of function calls is correctly incremented
        in the log each time the corresponding functions are called, this is a limitation of the
        model.)\<close>

text \<open> The invariant is defined with respect to the table the log is referring to. \<close>
definition iteration_log_inv :: "('l,'o) lstar_iteration \<Rightarrow> bool" where
  " iteration_log_inv s \<equiv>
    \<comment> \<open> We call make-closed and make-consistent at most N times \<close>
    (get_mkcl \<circ> snd) s + (get_mkco \<circ> snd) s < (num_diff_rows \<circ> fst) s \<and>

    \<comment> \<open> We call learn-from-cexample at most N times \<close>
    (get_lece \<circ> snd) s < (num_diff_rows \<circ> fst) s \<and>

    \<comment> \<open> The set S keeps growing. \<close>
    (length \<circ> get_S \<circ> fst) s \<ge> (get_max_len_S \<circ> snd) s \<and>

    \<comment> \<open> The size of S is related to the logged quantities \<close>
    (length \<circ> get_S \<circ> fst) s \<le>
      Suc ((get_mkcl \<circ> snd) s + m_bound * (get_lece \<circ> snd) s) \<and>

    \<comment> \<open> The set E keeps growing. \<close>
    (length \<circ> get_E \<circ> fst) s \<ge> (get_max_len_E \<circ> snd) s \<and>

    \<comment> \<open> The size of E is related to the logged quantities \<close>
    (length \<circ> get_E \<circ> fst) s \<le> Suc ((get_mkco \<circ> snd) s) \<and>

    \<comment> \<open> The max str len in S keeps growing. \<close>
    (max_str_len \<circ> set \<circ> get_S \<circ> fst) s \<ge> (get_max_str_len_S \<circ> snd) s \<and>

    \<comment> \<open> The max str len in S is related to logged quantities \<close>
    (max_str_len \<circ> set \<circ> get_S \<circ> fst) s \<le> m_bound + (get_mkcl \<circ> snd) s \<and>

    \<comment> \<open> The max str len in E keeps growing. \<close>
    (max_str_len \<circ> set \<circ> get_E \<circ> fst) s \<ge> (get_max_str_len_E \<circ> snd) s \<and>

    \<comment> \<open> The max str len in E is related to logged quantities \<close>
    (max_str_len \<circ> set \<circ> get_E \<circ> fst) s \<le> (get_mkco \<circ> snd) s"

lemma iteration_log_invI[intro]:
  "\<lbrakk>(get_mkcl \<circ> snd) s + (get_mkco \<circ> snd) s < (num_diff_rows \<circ> fst) s;
    (get_lece \<circ> snd) s < (num_diff_rows \<circ> fst) s;
    (length \<circ> get_S \<circ> fst) s \<ge> (get_max_len_S \<circ> snd) s;
    (length \<circ> get_S \<circ> fst) s \<le> Suc ((get_mkcl \<circ> snd) s + m_bound * (get_lece \<circ> snd) s);
    (length \<circ> get_E \<circ> fst) s \<ge> (get_max_len_E \<circ> snd) s;
    (length \<circ> get_E \<circ> fst) s \<le> Suc ((get_mkco \<circ> snd) s);
    (max_str_len \<circ> set \<circ> get_S \<circ> fst) s \<ge> (get_max_str_len_S \<circ> snd) s;
    (max_str_len \<circ> set \<circ> get_S \<circ> fst) s \<le> m_bound + (get_mkcl \<circ> snd) s;
    (max_str_len \<circ> set \<circ> get_E \<circ> fst) s \<ge> (get_max_str_len_E \<circ> snd) s;
    (max_str_len \<circ> set \<circ> get_E \<circ> fst) s \<le> (get_mkco \<circ> snd) s\<rbrakk>
  \<Longrightarrow> iteration_log_inv s"
  unfolding iteration_log_inv_def by auto

lemma iteration_log_inv_mkcl_mkcoD[dest]:
  "iteration_log_inv s \<Longrightarrow> (get_mkcl \<circ> snd) s + (get_mkco \<circ> snd) s < (num_diff_rows \<circ> fst) s"
  unfolding iteration_log_inv_def by auto

lemma iteration_log_inv_leceD[dest]:
  "iteration_log_inv s \<Longrightarrow> (get_lece \<circ> snd) s < (num_diff_rows \<circ> fst) s"
  unfolding iteration_log_inv_def by auto

lemma iteration_log_inv_get_max_lenSD[dest]:
  "iteration_log_inv s \<Longrightarrow> (length \<circ> get_S \<circ> fst) s \<ge> (get_max_len_S \<circ> snd) s"
  unfolding iteration_log_inv_def by auto

lemma iteration_log_inv_length_get_SD[dest]:
  "iteration_log_inv s \<Longrightarrow> (length \<circ> get_S \<circ> fst) s \<le> 
     Suc ((get_mkcl \<circ> snd) s + m_bound * (get_lece \<circ> snd) s)"
  unfolding iteration_log_inv_def by auto

lemma iteration_log_inv_get_max_lenED[dest]:
  "iteration_log_inv s \<Longrightarrow> (length \<circ> get_E \<circ> fst) s \<ge> (get_max_len_E \<circ> snd) s"
  unfolding iteration_log_inv_def by auto

lemma iteration_log_inv_length_get_ED[dest]:
  "iteration_log_inv s \<Longrightarrow> (length \<circ> get_E \<circ> fst) s \<le> Suc ((get_mkco \<circ> snd) s)"
  unfolding iteration_log_inv_def by auto

lemma iteration_log_inv_get_max_str_len_S_incrD[dest]:
  "iteration_log_inv s \<Longrightarrow> (max_str_len \<circ> set \<circ> get_S \<circ> fst) s \<ge> (get_max_str_len_S \<circ> snd) s"
  unfolding iteration_log_inv_def by auto

lemma iteration_log_inv_get_max_str_lenSD[dest]:
  "iteration_log_inv s \<Longrightarrow> (max_str_len \<circ> set \<circ> get_S \<circ> fst) s \<le>
    m_bound + (get_mkcl \<circ> snd) s"
  unfolding iteration_log_inv_def by auto

lemma iteration_log_inv_get_max_str_len_E_incrD[dest]: 
  "iteration_log_inv s \<Longrightarrow> (max_str_len \<circ> set \<circ> get_E \<circ> fst) s \<ge> (get_max_str_len_E \<circ> snd) s"
  unfolding iteration_log_inv_def by auto

lemma iteration_log_inv_get_max_str_lenED[dest]: 
  "iteration_log_inv s \<Longrightarrow> (max_str_len \<circ> set \<circ> get_E \<circ> fst) s \<le> (get_mkco \<circ> snd) s"
  unfolding iteration_log_inv_def by auto
  
subsection \<open> Counting the number of function calls via the log. \<close>

text \<open> In this section we count the number of calls of the important functions which fall in the 
       3 categories written in our log. \<close>

text \<open> After updating the log in one step, we have at most made one new function call (zero if no
       consistency/closedness problems to fix) \<close>
lemma make_cc_step_log_upd_plus_one:
  " (get_mkcl \<circ> snd) (make_cc_step orc iter) +  (get_mkco \<circ> snd) (make_cc_step orc iter)
     \<le> Suc ((get_mkcl \<circ> snd) iter + (get_mkco \<circ> snd) iter)"
  by(cases iter, simp)

text \<open> The number of fun calls of learn from counterexample has not been updated after the makeCC step \<close>
lemma make_cc_step_log_lece_not_upd:
  " (get_lece \<circ> snd) (make_cc_step orc iter) = (get_lece \<circ> snd) iter"
  by(cases iter, simp)

text \<open> The following is always true; "The total number of function calls of make-closed + make-consistent
       is always strictly less than the number of different rows of the table".
       This follows directly from the previous inequality and the invariant, if we assume it before
       make-cc-step is called (we'll then prove it in general by induction)\<close>
lemma make_cc_step_log_invariant:
  " lstar_table_inv table \<Longrightarrow>
   (get_mkcl \<circ> snd) (table, log) + (get_mkco \<circ> snd) (table, log) < (num_diff_rows \<circ> fst) (table, log) \<Longrightarrow>
   (get_mkcl \<circ> snd) (make_cc_step oracleM (table, log)) +
   (get_mkco \<circ> snd) (make_cc_step oracleM (table, log)) < (num_diff_rows \<circ> fst) (make_cc_step oracleM (table, log))"
proof(cases "(\<not> closedp table) \<or> (\<not> consistentp table)")
  case True
  assume inv_bef: "lstar_table_inv table"
  assume *: "(get_mkcl \<circ> snd) (table, log) + (get_mkco \<circ> snd) (table, log) < (num_diff_rows \<circ> fst) (table, log)"
  have "(get_mkcl \<circ> snd) (make_cc_step oracleM (table, log)) +
        (get_mkco \<circ> snd) (make_cc_step oracleM (table, log)) \<le>
          Suc ((get_mkcl \<circ> snd) (table, log) + (get_mkco \<circ> snd) (table, log))"
    using make_cc_step_log_upd_plus_one by blast
  also have "... < Suc ((num_diff_rows \<circ> fst) (table, log))" using * by(simp)
  also have "... \<le> (num_diff_rows \<circ> fst) (make_cc_step oracleM (table, log))"
    using make_cc_step_variant[OF inv_bef True] by(intro Suc_leI)
  finally show ?thesis .   
next
  \<comment> \<open> This is the trivial case where the make-cc-step doesn't do anything. \<close>
  case False
  assume inv_bef: "lstar_table_inv table"
  assume *: "(get_mkcl \<circ> snd) (table, log) + (get_mkco \<circ> snd) (table, log) < (num_diff_rows \<circ> fst) (table, log)"
  have "(get_mkcl \<circ> snd) (make_cc_step oracleM (table, log)) +
        (get_mkco \<circ> snd) (make_cc_step oracleM (table, log)) =
          (get_mkcl \<circ> snd) (table, log) + (get_mkco \<circ> snd) (table, log)"
    using False by simp
  also have "... < (num_diff_rows \<circ> fst) (table, log)" using * by(simp)
  also have "... = (num_diff_rows \<circ> fst) (make_cc_step oracleM (table, log))"
    using False by auto
  finally show ?thesis .
qed

text \<open> We generalize the previous lemma by induction to show that the number of calls of make-closed
       plus the number of calls of make-consistent is always less than the number of different rows.\<close>
lemma make_cc_log_num_diff_rows:
  assumes inv_bef: "(lstar_table_inv \<circ> fst) iter"
      and init: "(get_mkcl \<circ> snd) iter + (get_mkco \<circ> snd) iter < (num_diff_rows \<circ> fst) iter"
      and new: "(make_cc oracleM iter) = Some newIter" 
   shows "(get_mkcl \<circ> snd) newIter + (get_mkco \<circ> snd) newIter < (num_diff_rows \<circ> fst) newIter"
proof(rule make_cc_rule_with_invariant)
  from new show "make_cc oracleM iter = Some newIter" by(simp)
  show "\<And>s. \<lbrakk>(get_mkcl \<circ> snd) s + (get_mkco \<circ> snd) s < (num_diff_rows \<circ> fst) s;
          case s of (tbl, log) \<Rightarrow> \<not> closedp tbl \<or> \<not> consistentp tbl; (lstar_table_inv \<circ> fst) s\<rbrakk>
         \<Longrightarrow> (get_mkcl \<circ> snd) (make_cc_step oracleM s) +
             (get_mkco \<circ> snd) (make_cc_step oracleM s)
             < (num_diff_rows \<circ> fst) (make_cc_step oracleM s)"
    using make_cc_step_log_invariant by force
  show "(get_mkcl \<circ> snd) iter + (get_mkco \<circ> snd) iter < (num_diff_rows \<circ> fst) iter"
    using init by simp
  from inv_bef show "(lstar_table_inv \<circ> fst) iter" .
qed

text \<open> We'll also need the fact that the get-lece is not changed after makeCC.
       Note: The inv-bef assumption is superfluous here, but we'll have it and it allows us to use
       the simpler make-cc-rule-with-invariant rule. \<close>
lemma make_cc_log_lece:
  assumes inv_bef: "(lstar_table_inv \<circ> fst) iter" and
    new: "(make_cc oracleM iter) = Some newIter" 
   shows "(get_lece \<circ> snd) newIter = (get_lece \<circ> snd) iter"
proof(rule make_cc_rule_with_invariant
            [where P = "\<lambda>it. (get_lece \<circ> snd) it = (get_lece \<circ> snd) iter"])
  from new show "(make_cc oracleM iter) = Some newIter" .
  show "\<And>s. \<lbrakk>(get_lece \<circ> snd) s = (get_lece \<circ> snd) iter; case s of (tbl, log) \<Rightarrow> \<not> closedp tbl \<or> \<not> consistentp tbl;
          (lstar_table_inv \<circ> fst) s\<rbrakk>
         \<Longrightarrow> (get_lece \<circ> snd) (make_cc_step oracleM s) = (get_lece \<circ> snd) iter"
    by (metis make_cc_step_log_lece_not_upd)
  from inv_bef show "(lstar_table_inv \<circ> fst) iter" .
qed(simp)

text \<open> In the log, the fun calls of learn from counterexample has been increased by 
       exactly one after each lStarStep.
       However, we need to justify that the makeCC did not modify this number (proven before)
       Here the inv-bef and cexample-wformed assumptions are needed for termination of makeCC. \<close>
lemma lStarLearnerStep_log_upd_plus_one:
  assumes inv_bef: "lstar_table_inv tbl" and
          cexample_wformed: "t \<in> \<Sigma>star"
  shows "(get_lece \<circ> snd) (lstar_learner_step oracleM t (tbl,log)) = Suc ((get_lece \<circ> snd) (tbl,log))" proof -
  let ?iter_mid = "(learn_from_cexample t oracleM tbl, log_lece_call tbl log)"
  from inv_bef cexample_wformed have inv_mid: "(lstar_table_inv \<circ> fst) ?iter_mid"
    using learn_from_cexample_invariant by simp
  from inv_mid have "\<exists>newIter.(make_cc oracleM ?iter_mid) = Some newIter"
    using make_cc_termination by auto
  then obtain newIter where newIter_def: "(make_cc oracleM ?iter_mid) = Some newIter"
    by(auto)
  have "(get_lece \<circ> snd) ?iter_mid = Suc ((get_lece \<circ> snd) (tbl,log))" by(auto)
  also have "(get_lece \<circ> snd) ?iter_mid = (get_lece \<circ> snd) newIter" using newIter_def
    using inv_mid make_cc_log_lece by presburger
  finally show ?thesis using newIter_def by auto
qed

text \<open> The number of calls of learn-from-cexample so far is always smaller than the number of 
       distinct rows in the current table.\<close>
lemma lStarLearnerStep_log_invariant_lece:
  assumes inv_bef: "(lstar_table_inv \<circ> fst) iter" and
       closed_bef: "(closedp \<circ> fst) iter" and
   consistent_bef: "(consistentp \<circ> fst) iter" and
             t_wf: "t \<in> \<Sigma>star" and
             \<comment> \<open> t is indeed a counter-example\<close>
             t_ce: "L t \<noteq> (output_on (make_conjecture (fst iter))) t" and 
            start: "(get_lece \<circ> snd) iter < (num_diff_rows \<circ> fst) iter"
  shows "(get_lece \<circ> snd) (lstar_learner_step oracleM t iter) < (num_diff_rows \<circ> fst) (lstar_learner_step oracleM t iter)"
proof -
  let ?newIter = "(lstar_learner_step oracleM t iter)"
  \<comment> \<open> We'll need the invariant on the new iteration \<close>
  from inv_bef t_wf have inv_aft: "(lstar_table_inv \<circ> fst) ?newIter" 
    using lstar_learner_step_invariant 
    by (metis (mono_tags, lifting) comp_apply inv_bef prod.collapse t_wf)
  \<comment> \<open> We can now prove the inequality. \<close>
  from start have "(get_lece \<circ> snd) ?newIter = Suc ((get_lece \<circ> snd) iter)"
    using inv_bef t_wf lStarLearnerStep_log_upd_plus_one
    by (metis (mono_tags, lifting) comp_apply prod.collapse)
  also have "... < Suc ((num_diff_rows \<circ> fst) iter)" using start by simp
  also have "... \<le> (num_diff_rows \<circ> fst) (lstar_learner_step oracleM t iter)"
  proof -
    have "(num_states \<circ> make_conjecture \<circ> fst) (lstar_learner_step oracleM t iter) > 
          (num_states \<circ> make_conjecture \<circ> fst) iter"
    using lstar_learner_step_variant closed_bef consistent_bef inv_bef t_ce t_wf
    by(cases iter, simp)
    moreover have "(num_states \<circ> make_conjecture \<circ> fst) iter = (num_diff_rows \<circ> fst) iter "
      using num_states_eq_num_diff_rows inv_bef lstar_table_invE(4) by(auto dest: lstar_table_invE)
    moreover have "(num_states \<circ> make_conjecture \<circ> fst) ?newIter = (num_diff_rows \<circ> fst) ?newIter"
      using num_states_eq_num_diff_rows inv_aft lstar_table_invE(4) by(auto dest: lstar_table_invE)
    ultimately show ?thesis by auto
  qed
  finally show ?thesis .
qed

text \<open> We'll also need a version of the invariant for makeCC lifted after the lstar-learner-step.\<close>
lemma lStarLearnerStep_log_invariant_num_diff_rows:
  assumes inv_bef: "(lstar_table_inv \<circ> fst) iter"
      and init: "(get_mkcl \<circ> snd) iter + (get_mkco \<circ> snd) iter < (num_diff_rows \<circ> fst) iter"
      and t_wf: "t \<in> \<Sigma>star"
    shows "(get_mkcl \<circ> snd) (lstar_learner_step oracleM t iter) + (get_mkco \<circ> snd) (lstar_learner_step oracleM t iter)
            < (num_diff_rows \<circ> fst) (lstar_learner_step oracleM t iter)"
proof(cases iter)
  case (Pair tbl log)
    let ?midIter = "(learn_from_cexample t oracleM tbl, log_lece_call tbl log)"
    let ?newIter = "(lstar_learner_step oracleM t iter)"
    from inv_bef Pair t_wf have inv_mid: "(lstar_table_inv \<circ> fst) ?midIter"
      using learn_from_cexample_invariant by auto
    \<comment> \<open> We haven't changed anything in the first two counters after 'learn-from-cexample'\<close>
    have "(get_mkcl \<circ> snd) iter + (get_mkco \<circ> snd) iter =
          (get_mkcl \<circ> snd) ?midIter + (get_mkco \<circ> snd) ?midIter" using Pair by(cases log, auto)
    \<comment> \<open> We also have the following inequality (since learn-from-cexample extends the table).
         Note that the non strict inequality is why we couldn't use this directly as a variant
         before, and instead had to argue about the *whole* lStarLearnerStep. \<close>
    moreover have "(num_diff_rows \<circ> fst) (tbl,log) \<le> (num_diff_rows \<circ> fst) ?midIter"
      using learn_from_cexample_num_diff_rows inv_bef t_wf Pair by auto
    \<comment> \<open> From this we can push the 'init' inequality to after 'learn-from-cexample'\<close>
    ultimately have midinit:
      "(get_mkcl \<circ> snd) ?midIter + (get_mkco \<circ> snd) ?midIter < (num_diff_rows \<circ> fst) ?midIter"
      using Pair init by auto
    from inv_mid have "\<exists>newtbl newlog. make_cc oracleM ?midIter = Some (newtbl, newlog)"
      using make_cc_termination by simp
    then obtain newiter where miditer_def: "make_cc oracleM ?midIter = Some newiter"
      by(auto)
   from make_cc_log_num_diff_rows miditer_def inv_mid midinit
   have "(get_mkcl \<circ> snd) newiter + (get_mkco \<circ> snd) newiter < (num_diff_rows \<circ> fst) newiter"
     by blast
   then show ?thesis using miditer_def by (simp add: Pair)
qed

subsection \<open> Bounding the max size of sets S and E\<close>

text \<open> Now that we know how many function calls are performed from the log, we'll need
       to have a bound on the size of the observation table (since the running time of the
       accessor function is polynomial in the size of the observation table. )\<close>

text \<open> Important remark: In this section we consider S and E as sets for the proofs, and prove
       statements about their cardinalities. However, the algorithm represents these sets as lists.
       Thus, we need to argue that the length of the list representation is similar to the
       cardinality of the corresponding set (the size of the list could 'explode' while the set card.
       remains bounded). This will be possible because of the 'distinct' statements in the main
       invariant !\<close>

text \<open> Notation: From our locale, 'm' denotes the maximum size of a counterexample returned by the
       conjecture oracle.
       The letter N corresponds to the number of state of the minimal acceptor of L\<close>

text_raw
     \<open> Only the function make-consistent accesses E.
       We show that after make-consistent we have added (at most) one new element to E.
       Using this we'll show that the cardinality of E is smaller than the number \begin{equation}\mathit{mkco} + 1\end{equation}
       (referring to the number in the log) throughout the algorithm.
       This will allow us to conclude that the cardinality of E is smaller than N.\<close>

text \<open> We add at most one element to E after make-closed.\<close>
lemma card_E_make_consistent: 
  assumes not_consistent: "\<not> (consistentp tbl)"
  shows "(card \<circ> set) (get_E (make_consistent oracleM tbl)) \<le> Suc ((card \<circ> set) (get_E tbl))" and
        "(card \<circ> set) (get_E (make_consistent oracleM tbl)) \<ge> (card \<circ> set) (get_E tbl)"
proof -
  from not_consistent have "\<exists>s1 s2 a e. get_consistency_prob tbl = Some (s1,s2,a,e)"
    using get_consistency_prob_complete by simp
  then obtain s1 s2 a e where "get_consistency_prob tbl = Some (s1,s2,a,e)" by auto
  then have *: "set (get_E (make_consistent oracleM tbl)) = set (get_E tbl) \<union> {a#e}"
    using make_consistent_setE by simp
  then show "(card \<circ> set) (get_E (make_consistent oracleM tbl)) \<le> Suc ((card \<circ> set) (get_E tbl))"
    by (metis (no_types, lifting) List.finite_set Un_insert_right card.insert comp_apply eq_iff
        insert_absorb le_SucI sup_bot.right_neutral)
  from * show "(card \<circ> set) (get_E tbl) \<le> (card \<circ> set) (get_E (make_consistent oracleM tbl))"
    by (metis (no_types, lifting) List.finite_set Suc_leD Un_insert_right card.insert
        comp_apply eq_iff insert_absorb sup_bot.right_neutral)
qed

text \<open> The other functions don't change E\<close>

lemma card_E_make_closed:
  "(card \<circ> set) (get_E (make_closed oracleM tbl)) = (card \<circ> set) (get_E tbl)"
  using make_closed_setE by simp

lemma card_E_learn_from_cexample:
  "(card \<circ> set) (get_E (learn_from_cexample t oracleM tbl)) = (card \<circ> set) (get_E tbl)"
  using learn_from_cexample_setE by simp

text \<open> There are two functions accessing S; make-closed and learn-from-cexample.
       We show that after make-closed we have added (at most) one new element to S and
       have added at most 'm' different elements after learn-from-cexample (where m
       is the maximum size of a counterexample returned by oracleC).\<close>
text_raw
     \<open> Using this we'll show that the cardinality of S is smaller than: \begin{equation}\mathit{mkcl} + m*\mathit{lece} + 1\end{equation}
       (referring to numbers in the log) throughout the algorithm.
       This will allow us to conclude that the cardinality of S is smaller than $N + m(N-1)$.\<close>

text \<open> We add at most one element to S after make-closed.\<close>
lemma card_S_make_closed:
  assumes not_closed: "\<not> (closedp tbl)"
  shows "(card \<circ> set) (get_S (make_closed oracleM tbl)) \<le> Suc ((card \<circ> set) (get_S tbl))"
        "(card \<circ> set) (get_S (make_closed oracleM tbl)) \<ge> (card \<circ> set) (get_S tbl)"
proof -
  from not_closed have "\<exists>s a. get_closedness_prob tbl = Some (s,a)"
    using get_closedness_prob_complete by simp
  then obtain s a where "get_closedness_prob tbl = Some (s,a)" by auto
  then have *: "set (get_S (make_closed oracleM tbl)) = set (get_S tbl) \<union> {s@[a]}"
    using make_closed_setS by simp
  then show "(card \<circ> set) (get_S (make_closed oracleM tbl)) \<le> Suc ((card \<circ> set) (get_S tbl))"
    by (metis (no_types, lifting) List.finite_set Un_insert_right card.insert comp_apply eq_iff
        insert_absorb le_SucI sup_bot.right_neutral)
  from * show "(card \<circ> set) (get_S tbl) \<le> (card \<circ> set) (get_S (make_closed oracleM tbl))"
    by (metis (no_types, lifting) List.finite_set Suc_leD Un_insert_right card.insert
        comp_apply eq_iff insert_absorb sup_bot.right_neutral)
qed

text \<open> MakeConsistent doesn't change the set S\<close>
lemma card_S_make_consistent:
  shows "(card \<circ> set) (get_S (make_consistent oracleM tbl)) = ((card \<circ> set) (get_S tbl))"
  using make_consistent_setS by simp

text \<open> Learn from counterexample adds at most length t new elements to S\<close>
lemma card_S_learn_from_cexample:
  assumes inv_bef: "lstar_table_inv tbl"
  shows "(card \<circ> set) (get_S (learn_from_cexample t oracleM tbl)) \<le> length t + ((card \<circ> set) (get_S tbl))" and
        "(card \<circ> set) (get_S (learn_from_cexample t oracleM tbl)) \<ge> (card \<circ> set \<circ> get_S) tbl"
proof -
  have *: "card (set (all_prefixes t) - {[]}) \<le> length t" proof -
    have "card (set (all_prefixes t)) = length t + 1" by(rule card_all_prefixes)
    moreover have "[] \<in> set (all_prefixes t)" by (simp add: all_prefixes_complete prefixp_code(1))
    ultimately show ?thesis by auto
  qed
  have **: "set (get_S (learn_from_cexample t oracleM tbl)) = set (get_S tbl) \<union> (set (all_prefixes t) - {[]})"
    by(rule learn_from_cexample_setS[OF inv_bef])
  then have "(card \<circ> set) (get_S (learn_from_cexample t oracleM tbl))
               \<le> (card \<circ> set) (get_S tbl) + card (set (all_prefixes t) - {[]})"
    by (simp add: card_Un_le)
  then show "(card \<circ> set) (get_S (learn_from_cexample t oracleM tbl)) \<le> length t + ((card \<circ> set) (get_S tbl))"
    using * by auto 
  have "card ((set \<circ> get_S) tbl) \<le> card (set (get_S (learn_from_cexample t oracleM tbl)))"
  proof(rule card_mono)
    from ** show "(set \<circ> get_S) tbl \<subseteq> set (get_S (learn_from_cexample t oracleM tbl))" by simp
    show "finite (set (get_S (learn_from_cexample t oracleM tbl)))" by auto  
  qed
  then show "(card \<circ> set \<circ> get_S) tbl \<le> (card \<circ> set) (get_S (learn_from_cexample t oracleM tbl))"
    by simp
qed

lemma length_S_increasing_make_cc_step:
  assumes
   pbef: "(length \<circ> get_S \<circ> fst) (tbl,log) \<ge> (get_max_len_S \<circ> snd) (tbl,log)" and
   inv_bef: "(lstar_table_inv \<circ> fst) (tbl,log)"
  shows "(get_max_len_S \<circ> snd) (make_cc_step oracleM (tbl,log))
    \<le> (length \<circ> get_S \<circ> fst) (make_cc_step oracleM (tbl,log))"
proof(cases "\<not> closedp tbl \<or> \<not> consistentp tbl")
  assume true: "\<not> closedp tbl \<or> \<not> consistentp tbl"
  show "(get_max_len_S \<circ> snd) (make_cc_step oracleM (tbl,log))
    \<le> (length \<circ> get_S \<circ> fst) (make_cc_step oracleM (tbl,log))"
  proof(rule make_cc_step_cases)
    assume not_closed: "\<not> closedp tbl"
    have "(card \<circ> set \<circ> get_S \<circ> fst) (tbl,log) \<le> (card \<circ> set \<circ> get_S \<circ> fst) (make_closed oracleM tbl, log_mkcl_call tbl log)"
      using card_S_make_closed(2)[OF not_closed] by auto
    then have "(length \<circ> get_S \<circ> fst) (tbl,log) \<le> (length \<circ> get_S \<circ> fst) (make_closed oracleM tbl, log_mkcl_call tbl log)"
      using inv_bef len_eq_card_S make_cc_termination_step not_closed by fastforce
    from this and pbef show "(get_max_len_S \<circ> snd) (make_closed oracleM tbl, log_mkcl_call tbl log)
      \<le> (length \<circ> get_S \<circ> fst) (make_closed oracleM tbl, log_mkcl_call tbl log)" by auto
  next
    have "(card \<circ> set \<circ> get_S \<circ> fst) (tbl,log) \<le> (card \<circ> set \<circ> get_S \<circ> fst) (make_consistent oracleM tbl, log_mkco_call tbl log)"
      using card_S_make_consistent by auto
    then have "(length \<circ> get_S \<circ> fst) (tbl,log) \<le> (length \<circ> get_S \<circ> fst) (make_consistent oracleM tbl, log_mkco_call tbl log)"
      using inv_bef len_eq_card_S by (metis card_length comp_apply fstI make_consistent_setS)
    from this and pbef show "(get_max_len_S \<circ> snd) (make_consistent oracleM tbl, log_mkco_call tbl log)
      \<le> (length \<circ> get_S \<circ> fst) (make_consistent oracleM tbl, log_mkco_call tbl log)" by auto
  next
    from true show "\<not> closedp tbl \<or> \<not> consistentp tbl" .
  qed
next
  assume "\<not> (\<not> closedp tbl \<or> \<not> consistentp tbl)"
  then have nop: "(make_cc_step oracleM (tbl,log)) = (tbl,log)" by simp
  from this and pbef show "(get_max_len_S \<circ> snd) (make_cc_step oracleM (tbl,log))
    \<le> (length \<circ> get_S \<circ> fst) (make_cc_step oracleM (tbl,log))" by auto
qed

lemma length_E_increasing_make_cc_step:
  assumes
   pbef: "(length \<circ> get_E \<circ> fst) (tbl,log) \<ge> (get_max_len_E \<circ> snd) (tbl,log)" and
   inv_bef: "(lstar_table_inv \<circ> fst) (tbl,log)"
  shows "(get_max_len_E \<circ> snd) (make_cc_step oracleM (tbl,log))
    \<le> (length \<circ> get_E \<circ> fst) (make_cc_step oracleM (tbl,log))"
proof(cases "\<not> closedp tbl \<or> \<not> consistentp tbl")
  assume true: "\<not> closedp tbl \<or> \<not> consistentp tbl"
  show "(get_max_len_E \<circ> snd) (make_cc_step oracleM (tbl,log))
    \<le> (length \<circ> get_E \<circ> fst) (make_cc_step oracleM (tbl,log))"
  proof(rule make_cc_step_cases)
    assume not_closed: "\<not> closedp tbl"
    have "(card \<circ> set \<circ> get_E \<circ> fst) (tbl,log) \<le> (card \<circ> set \<circ> get_E \<circ> fst) (make_closed oracleM tbl, log_mkcl_call tbl log)"
      using card_E_make_closed by auto
    then have "(length \<circ> get_E \<circ> fst) (tbl,log) \<le> (length \<circ> get_E \<circ> fst) (make_closed oracleM tbl, log_mkcl_call tbl log)"
      using inv_bef len_eq_card_E make_cc_termination_step not_closed by fastforce
    from this and pbef show "(get_max_len_E \<circ> snd) (make_closed oracleM tbl, log_mkcl_call tbl log)
      \<le> (length \<circ> get_E \<circ> fst) (make_closed oracleM tbl, log_mkcl_call tbl log)" by auto
  next
    assume "closedp tbl \<and> \<not> consistentp tbl"
    then have not_consistent: "\<not> consistentp tbl" by simp
    have "(card \<circ> set \<circ> get_E \<circ> fst) (tbl,log) \<le> (card \<circ> set \<circ> get_E \<circ> fst) (make_consistent oracleM tbl, log_mkco_call tbl log)"
      using card_E_make_consistent(2)[OF not_consistent] by auto
    then have "(length \<circ> get_E \<circ> fst) (tbl,log) \<le> (length \<circ> get_E \<circ> fst) (make_consistent oracleM tbl, log_mkco_call tbl log)"
      using inv_bef len_eq_card_E \<open>closedp tbl \<and> \<not> consistentp tbl\<close> make_cc_termination_step by fastforce
    from this and pbef show "(get_max_len_E \<circ> snd) (make_consistent oracleM tbl, log_mkco_call tbl log)
      \<le> (length \<circ> get_E \<circ> fst) (make_consistent oracleM tbl, log_mkco_call tbl log)" by auto
  next
    from true show "\<not> closedp tbl \<or> \<not> consistentp tbl" .
  qed
next
  assume "\<not> (\<not> closedp tbl \<or> \<not> consistentp tbl)"
  then have nop: "(make_cc_step oracleM (tbl,log)) = (tbl,log)" by simp
  from this and pbef show "(get_max_len_E \<circ> snd) (make_cc_step oracleM (tbl,log))
    \<le> (length \<circ> get_E \<circ> fst) (make_cc_step oracleM (tbl,log))" by auto
qed

text \<open>It's required to do this proof separately, since the whole log invariant is not true after lece !\<close>


lemma length_S_increasing_make_cc:
  assumes inv_bef: "(lstar_table_inv \<circ> fst) iter"
       and init: "(get_max_len_S \<circ> snd) iter \<le> (length \<circ> get_S \<circ> fst) iter"
       and new: "(make_cc oracleM iter) = Some newiter"
  shows "(get_max_len_S \<circ> snd) newiter \<le> (length \<circ> get_S \<circ> fst) newiter"
proof(rule make_cc_rule_with_invariant)
  from new show "(make_cc oracleM iter) = Some newiter" .
  from init show "(get_max_len_S \<circ> snd) iter \<le> (length \<circ> get_S \<circ> fst) iter" .
  from inv_bef show "(lstar_table_inv \<circ> fst) iter" .
  show "\<And>s. \<lbrakk>(get_max_len_S \<circ> snd) s \<le> (length \<circ> get_S \<circ> fst) s; case s of (tbl, log) \<Rightarrow> \<not> closedp tbl \<or> \<not> consistentp tbl;
          (lstar_table_inv \<circ> fst) s\<rbrakk>
         \<Longrightarrow> (get_max_len_S \<circ> snd) (make_cc_step oracleM s)
             \<le> (length \<circ> get_S \<circ> fst) (make_cc_step oracleM s)"
    using length_S_increasing_make_cc_step by blast
qed

lemma length_E_increasing_make_cc:
  assumes inv_bef: "(lstar_table_inv \<circ> fst) iter"
       and init: "(get_max_len_E \<circ> snd) iter \<le> (length \<circ> get_E \<circ> fst) iter"
       and new: "(make_cc oracleM iter) = Some newiter"
  shows "(get_max_len_E \<circ> snd) newiter \<le> (length \<circ> get_E \<circ> fst) newiter"
proof(rule make_cc_rule_with_invariant)
  from new show "(make_cc oracleM iter) = Some newiter" .
  from init show "(get_max_len_E \<circ> snd) iter \<le> (length \<circ> get_E \<circ> fst) iter" .
  from inv_bef show "(lstar_table_inv \<circ> fst) iter" .
  show "\<And>s. \<lbrakk>(get_max_len_E \<circ> snd) s \<le> (length \<circ> get_E \<circ> fst) s; case s of (tbl, log) \<Rightarrow> \<not> closedp tbl \<or> \<not> consistentp tbl;
          (lstar_table_inv \<circ> fst) s\<rbrakk>
         \<Longrightarrow> (get_max_len_E \<circ> snd) (make_cc_step oracleM s)
             \<le> (length \<circ> get_E \<circ> fst) (make_cc_step oracleM s)"
    using length_E_increasing_make_cc_step by blast
qed

lemma length_S_make_cc_step:
  assumes
   pbef: "(length \<circ> get_S \<circ> fst) s \<le> Suc ((get_mkcl \<circ> snd) s + m_bound * (get_lece \<circ> snd) s)" and
   loop: "\<not> closedp (fst s) \<or> \<not> consistentp (fst s)" and
   inv_bef: "(lstar_table_inv \<circ> fst) s"
  shows "(length \<circ> get_S \<circ> fst) (make_cc_step oracleM s) \<le>
         Suc ((get_mkcl \<circ> snd) (make_cc_step oracleM s)
             + m_bound * (get_lece \<circ> snd) (make_cc_step oracleM s))"
proof(cases s)
case (Pair tbl log)
  let ?newiter = "(make_cc_step oracleM (tbl,log))"
  have inv_aft: "(lstar_table_inv \<circ> fst) ?newiter"
    using inv_bef Pair loop make_cc_termination_step by force 
  from pbef have pbef_weak: "(card \<circ> set \<circ> get_S \<circ> fst) s \<le> Suc ((get_mkcl \<circ> snd) s
    + m_bound * (get_lece \<circ> snd) s)"
   using len_eq_card_S[OF inv_bef] by simp
  have "(length \<circ> get_S \<circ> fst) ?newiter
          \<le> Suc ((get_mkcl \<circ> snd) ?newiter + m_bound * (get_lece \<circ> snd) ?newiter)"
  proof(rule make_cc_step_cases)
    from loop Pair show "\<not> closedp tbl \<or> \<not> consistentp tbl" by simp
  next
    assume not_closed: "\<not>closedp tbl"
    have "(card \<circ> set \<circ> get_S \<circ> fst) (make_closed oracleM tbl, log_mkcl_call tbl log)
    \<le> Suc ((get_mkcl \<circ> snd) (make_closed oracleM tbl, log_mkcl_call tbl log) +
            m_bound * (get_lece \<circ> snd) (make_closed oracleM tbl, log_mkcl_call tbl log))"
      using Pair inv_bef pbef_weak card_S_make_closed[OF not_closed] by auto
    then show "(length \<circ> get_S \<circ> fst) (make_closed oracleM tbl, log_mkcl_call tbl log)
    \<le> Suc ((get_mkcl \<circ> snd) (make_closed oracleM tbl, log_mkcl_call tbl log) +
            m_bound * (get_lece \<circ> snd) (make_closed oracleM tbl, log_mkcl_call tbl log))"
      using inv_aft len_eq_card_S not_closed by auto
  next
    assume closed_and_not_consistent: "closedp tbl \<and> \<not> consistentp tbl"
    then have "\<not> consistentp tbl" by simp
    then have "(card \<circ> set \<circ> get_S \<circ> fst) (make_consistent oracleM tbl, log_mkco_call tbl log)
     \<le> Suc ((get_mkcl \<circ> snd) (make_consistent oracleM tbl, log_mkco_call tbl log) +
            m_bound * (get_lece \<circ> snd) (make_consistent oracleM tbl, log_mkco_call tbl log))"
      using Pair inv_bef pbef_weak card_S_make_consistent by auto
    then show "(length \<circ> get_S \<circ> fst) (make_consistent oracleM tbl, log_mkco_call tbl log)
    \<le> Suc ((get_mkcl \<circ> snd) (make_consistent oracleM tbl, log_mkco_call tbl log) +
            m_bound * (get_lece \<circ> snd) (make_consistent oracleM tbl, log_mkco_call tbl log))"
      using inv_aft len_eq_card_S closed_and_not_consistent by auto
  qed
  then show "(length \<circ> get_S \<circ> fst) (make_cc_step oracleM s)
           \<le> Suc ((get_mkcl \<circ> snd) (make_cc_step oracleM s) +
                   m_bound * (get_lece \<circ> snd) (make_cc_step oracleM s))"
    by(simp add: Pair)
qed

lemma length_E_make_cc_step:
  assumes
   pbef: "(length \<circ> get_E \<circ> fst) s \<le> Suc ((get_mkco \<circ> snd) s)" and
   loop: "\<not> closedp (fst s) \<or> \<not> consistentp (fst s)" and
   inv_bef: "(lstar_table_inv \<circ> fst) s"
  shows "(length \<circ> get_E \<circ> fst) (make_cc_step oracleM s) \<le> Suc ((get_mkco \<circ> snd) (make_cc_step oracleM s))"
proof(cases s)
case (Pair tbl log)
  let ?newiter = "(make_cc_step oracleM (tbl,log))"
  have inv_aft: "(lstar_table_inv \<circ> fst) ?newiter"
    using inv_bef Pair loop make_cc_termination_step by force 
  from pbef have pbef_weak: "(card \<circ> set \<circ> get_E \<circ> fst) s \<le> Suc ((get_mkco \<circ> snd) s)"
   using len_eq_card_E[OF inv_bef] by simp
  have "(length \<circ> get_E \<circ> fst) ?newiter \<le> Suc ((get_mkco \<circ> snd) ?newiter)"
  proof(rule make_cc_step_cases)
    from loop Pair show "\<not> closedp tbl \<or> \<not> consistentp tbl" by simp
  next
    assume not_closed: "\<not>closedp tbl"
    have "(card \<circ> set \<circ> get_E \<circ> fst) (make_closed oracleM tbl, log_mkcl_call tbl log)
    \<le> Suc ((get_mkco \<circ> snd) (make_closed oracleM tbl, log_mkcl_call tbl log))"
      using Pair inv_bef pbef_weak card_E_make_closed by auto
    then show "(length \<circ> get_E \<circ> fst) (make_closed oracleM tbl, log_mkcl_call tbl log)
        \<le> Suc ((get_mkco \<circ> snd) (make_closed oracleM tbl, log_mkcl_call tbl log))"
      using inv_aft len_eq_card_E Pair not_closed by auto
  next
    assume closed_and_not_consistent: "closedp tbl \<and> \<not> consistentp tbl"
    then have not_consistent: "\<not> consistentp tbl" by simp
    then have "(card \<circ> set \<circ> get_E \<circ> fst) (make_consistent oracleM tbl, log_mkco_call tbl log)
     \<le> Suc ((get_mkco \<circ> snd) (make_consistent oracleM tbl, log_mkco_call tbl log))"
      using Pair inv_bef pbef_weak card_E_make_consistent(1)[OF not_consistent] by auto
    then show "(length \<circ> get_E \<circ> fst) (make_consistent oracleM tbl, log_mkco_call tbl log)
    \<le> Suc ((get_mkco \<circ> snd) (make_consistent oracleM tbl, log_mkco_call tbl log))"
      using inv_aft len_eq_card_E closed_and_not_consistent by auto
  qed
  then show "(length \<circ> get_E \<circ> fst) (make_cc_step oracleM s) \<le>
    Suc ((get_mkco \<circ> snd) (make_cc_step oracleM s))"
    using Pair by auto
qed

lemma length_S_make_cc:
  assumes inv_bef: "(lstar_table_inv \<circ> fst) iter" and \<comment> \<open> We don't need this, but simplifies proof\<close>
          init: "(length \<circ> get_S \<circ> fst) iter \<le> Suc ((get_mkcl \<circ> snd) iter + m_bound * (get_lece \<circ> snd) iter)"
       and new: "(make_cc oracleM iter) = Some newiter"
  shows "(length \<circ> get_S \<circ> fst) newiter
          \<le> Suc ((get_mkcl \<circ> snd) newiter + m_bound * (get_lece \<circ> snd) newiter)"
proof(rule make_cc_rule_with_invariant)
  from new show "(make_cc oracleM iter) = Some newiter" .
  from init show "(length \<circ> get_S \<circ> fst) iter \<le> Suc ((get_mkcl \<circ> snd) iter + m_bound * (get_lece \<circ> snd) iter)" . 
  from inv_bef show "(lstar_table_inv \<circ> fst) iter" .
  show "\<And>s. \<lbrakk>(length \<circ> get_S \<circ> fst) s \<le> Suc ((get_mkcl \<circ> snd) s + m_bound * (get_lece \<circ> snd) s);
          case s of (tbl, log) \<Rightarrow> \<not> closedp tbl \<or> \<not> consistentp tbl; (lstar_table_inv \<circ> fst) s\<rbrakk>
         \<Longrightarrow> (length \<circ> get_S \<circ> fst) (make_cc_step oracleM s)
             \<le> Suc ((get_mkcl \<circ> snd) (make_cc_step oracleM s) +
                     m_bound * (get_lece \<circ> snd) (make_cc_step oracleM s))"
    using length_S_make_cc_step by (simp add: case_prod_unfold)
qed

lemma length_E_make_cc:
  assumes inv_bef: "(lstar_table_inv \<circ> fst) iter" and \<comment> \<open> We don't need this, but simplifies proof\<close>
          init: "(card \<circ> set \<circ> get_E \<circ> fst) iter \<le> Suc ((get_mkco \<circ> snd) iter)"
       and new: "(make_cc oracleM iter) = Some newiter"
     shows "(card \<circ> set \<circ> get_E \<circ> fst) newiter \<le> Suc ((get_mkco \<circ> snd) newiter)"
proof(rule make_cc_rule_with_invariant)
  from new show "(make_cc oracleM iter) = Some newiter" .
  from init show "(card \<circ> set \<circ> get_E \<circ> fst) iter \<le> Suc ((get_mkco \<circ> snd) iter)" .
  from inv_bef show "(lstar_table_inv \<circ> fst) iter" .

  show "\<And>s. \<lbrakk>(card \<circ> set \<circ> get_E \<circ> fst) s \<le> Suc ((get_mkco \<circ> snd) s);
          case s of (tbl, log) \<Rightarrow> \<not> closedp tbl \<or> \<not> consistentp tbl; (lstar_table_inv \<circ> fst) s\<rbrakk>
         \<Longrightarrow> (card \<circ> set \<circ> get_E \<circ> fst) (make_cc_step oracleM s) \<le> Suc ((get_mkco \<circ> snd) (make_cc_step oracleM s))"
    using length_E_make_cc_step len_eq_card_E make_cc_termination_step by force
qed

lemma length_E_lstar_learner_step:
  assumes pbef: "(length \<circ> get_E \<circ> fst) s \<le> Suc ((get_mkco \<circ> snd) s)" and
          inv_bef: " (lstar_table_inv \<circ> fst) s" and
          closed: "(closedp \<circ> fst) s" and 
          consistent: "(consistentp \<circ> fst) s" and
          t_wf: "t \<in> \<Sigma>star" and
          \<comment> \<open> t is indeed a counter-example\<close>
          t_ce: "L t \<noteq> (output_on (make_conjecture (fst iter))) t" 
   shows "(length \<circ> get_E \<circ> fst) (lstar_learner_step oracleM t s)
             \<le> Suc ((get_mkco \<circ> snd) (lstar_learner_step oracleM t s))"
proof(rule lstar_learner_step_rule)
  let ?P = "\<lambda>iter. (card \<circ> set \<circ> get_E \<circ> fst) iter \<le> Suc ((get_mkco \<circ> snd) iter)"
  from inv_bef show "(lstar_table_inv \<circ> fst) s" by simp
  from t_wf show "t \<in> \<Sigma>star" .
  from pbef show "(length \<circ> get_E \<circ> fst) s \<le> Suc ((get_mkco \<circ> snd) s)" .
  show "\<And>tbl log.
       \<lbrakk>(lstar_table_inv \<circ> fst) (tbl, log); (length \<circ> get_E \<circ> fst) (tbl, log) \<le> Suc ((get_mkco \<circ> snd) (tbl, log))\<rbrakk>
       \<Longrightarrow> (length \<circ> get_E \<circ> fst) (learn_from_cexample t oracleM tbl, log_lece_call tbl log)
           \<le> Suc ((get_mkco \<circ> snd) (learn_from_cexample t oracleM tbl, log_lece_call tbl log))"
    by (simp add: extendS_listE)
  show "\<And>iter newiter.
       \<lbrakk>(lstar_table_inv \<circ> fst) iter; (length \<circ> get_E \<circ> fst) iter \<le> Suc ((get_mkco \<circ> snd) iter);
        make_cc oracleM iter = Some newiter\<rbrakk>
       \<Longrightarrow> (length \<circ> get_E \<circ> fst) newiter \<le> Suc ((get_mkco \<circ> snd) newiter)"
    by (metis (no_types, lifting) len_eq_card_E length_E_make_cc make_cc_invariant)
qed                                                      

lemma length_S_lstar_learner_step:
  assumes pbef: "(length \<circ> get_S \<circ> fst) s \<le> Suc ((get_mkcl \<circ> snd) s + m_bound * (get_lece \<circ> snd) s)" and
          inv_bef: " (lstar_table_inv \<circ> fst) s" and
          closed: "(closedp \<circ> fst) s" and 
          consistent: "(consistentp \<circ> fst) s" and
          t_wf: "t \<in> \<Sigma>star" and
          \<comment> \<open> t is indeed a counter-example\<close>
          t_ce: "L t \<noteq> (output_on (make_conjecture (fst iter))) t" and
          \<comment> \<open> m is a bound of the lenght of t\<close>
          t_bounded: "length t \<le> m_bound"
   shows "(length \<circ> get_S \<circ> fst) (lstar_learner_step oracleM t s)
          \<le> Suc ((get_mkcl \<circ> snd) (lstar_learner_step oracleM t s)
            + m_bound * (get_lece \<circ> snd) (lstar_learner_step oracleM t s))"
proof(rule lstar_learner_step_rule)
  let ?P = "\<lambda>s. (length \<circ> get_S \<circ> fst) s \<le> Suc ((get_mkcl \<circ> snd) s + m_bound * (get_lece \<circ> snd) s)"
  from inv_bef show "(lstar_table_inv \<circ> fst) s" by simp
  from pbef show "(length \<circ> get_S \<circ> fst) s \<le> Suc ((get_mkcl \<circ> snd) s + m_bound * (get_lece \<circ> snd) s)" .
  from t_wf show "t \<in> \<Sigma>star" .

  show "\<And>tbl log.
       \<lbrakk>(lstar_table_inv \<circ> fst) (tbl, log); ?P (tbl, log)\<rbrakk>
       \<Longrightarrow> ?P (learn_from_cexample t oracleM tbl, log_lece_call tbl log)"
  proof -
    fix tbl :: "('letter,'mlo) observation_table"
    fix log :: "log_type"
    assume inv: "(lstar_table_inv \<circ> fst) (tbl, log)" and pbef: "?P (tbl, log)"

    from card_S_learn_from_cexample have 
      "(card \<circ> set) (get_S (learn_from_cexample t oracleM tbl)) \<le> length t + ((card \<circ> set) (get_S tbl))"
      using inv by auto
    moreover have "length t \<le> m_bound" using t_bounded . 
    ultimately have
      *: "(card \<circ> set \<circ> get_S) (learn_from_cexample t oracleM tbl) \<le> m_bound+ ((card \<circ> set \<circ> get_S) tbl)"
      by auto
    then have "(card \<circ> set \<circ> get_S \<circ> fst) (learn_from_cexample t oracleM tbl, log_lece_call tbl log)
           \<le> Suc ((get_mkcl \<circ> snd) (learn_from_cexample t oracleM tbl, log_lece_call tbl log) +
                   m_bound* (get_lece \<circ> snd) (learn_from_cexample t oracleM tbl, log_lece_call tbl log))"
      (* TODO: Fix this slh-proof ! *)
    proof -
      have f1: "\<forall>n lss. n + (card \<circ> set) (lss::'letter list list) \<le> n + length lss"
        by (simp add: card_length)
      have "length t + length (get_S tbl) \<le> get_mkcl log + Suc (m_bound * Suc (get_lece log))"
        using pbef t_bounded by auto
      then have "(card \<circ> set) (get_S (learn_from_cexample t oracleM tbl)) \<le> get_mkcl log + Suc (m_bound * Suc (get_lece log))"
        using f1 by (meson \<open>(card \<circ> set) (get_S (learn_from_cexample t oracleM tbl)) \<le> length t + (card \<circ> set) (get_S tbl)\<close> order_trans)
      then show ?thesis
        by simp
    qed
    then show "?P (learn_from_cexample t oracleM tbl, log_lece_call tbl log)"
      using len_eq_card_S learn_from_cexample_invariant inv t_wf by auto
  qed
  show "\<And>iter newiter.
       \<lbrakk>(lstar_table_inv \<circ> fst) iter; (length \<circ> get_S \<circ> fst) iter \<le> Suc ((get_mkcl \<circ> snd) iter + m_bound* (get_lece \<circ> snd) iter);
        make_cc oracleM iter = Some newiter\<rbrakk>
       \<Longrightarrow> (length \<circ> get_S \<circ> fst) newiter \<le> Suc ((get_mkcl \<circ> snd) newiter + m_bound* (get_lece \<circ> snd) newiter)"
    using length_S_make_cc by auto
qed

lemma length_S_increasing_lstar_learner_step:
  assumes
      log_inv_bef: "iteration_log_inv iter" and
          inv_bef: "(lstar_table_inv \<circ> fst) iter" and
       closed_bef: "(closedp \<circ> fst) iter" and
   consistent_bef: "(consistentp \<circ> fst) iter" and
             t_wf: "t \<in> \<Sigma>star" and
             \<comment> \<open> t is indeed a counter-example\<close>
             t_ce: "L t \<noteq> (output_on (make_conjecture (fst iter))) t" 
  shows "(get_max_len_S \<circ> snd) (lstar_learner_step oracleM t iter)
         \<le> (length \<circ> get_S \<circ> fst) (lstar_learner_step oracleM t iter)" 
proof(rule lstar_learner_step_rule[OF inv_bef t_wf])
  from log_inv_bef show "(get_max_len_S \<circ> snd) iter \<le> (length \<circ> get_S \<circ> fst) iter" by auto
  show "\<And>tbl log.
       \<lbrakk>(lstar_table_inv \<circ> fst) (tbl, log); (get_max_len_S \<circ> snd) (tbl, log) \<le> (length \<circ> get_S \<circ> fst) (tbl, log)\<rbrakk>
       \<Longrightarrow> (get_max_len_S \<circ> snd) (learn_from_cexample t oracleM tbl, log_lece_call tbl log)
           \<le> (length \<circ> get_S \<circ> fst) (learn_from_cexample t oracleM tbl, log_lece_call tbl log)"
  proof -
    fix tbl :: "('letter,'mlo) observation_table"
    fix log :: "log_type"
    assume inv_bef: "(lstar_table_inv \<circ> fst) (tbl, log)" and
              pbef: "(get_max_len_S \<circ> snd) (tbl, log) \<le> (length \<circ> get_S \<circ> fst) (tbl, log)"
    from inv_bef have inv_tbl: "lstar_table_inv tbl" by simp
    have "(card \<circ> set \<circ> get_S) tbl \<le> (card \<circ> set) (get_S (learn_from_cexample t oracleM tbl))"
      using card_S_learn_from_cexample(2)[OF inv_tbl] .
    then have "(length \<circ> get_S) tbl \<le> length (get_S (learn_from_cexample t oracleM tbl))"
      using inv_bef MAT_model_moore.len_eq_card_S MAT_model_moore_axioms learn_from_cexample_invariant t_wf by fastforce
    then show "(get_max_len_S \<circ> snd) (learn_from_cexample t oracleM tbl, log_lece_call tbl log)
           \<le> (length \<circ> get_S \<circ> fst) (learn_from_cexample t oracleM tbl, log_lece_call tbl log)"
      using pbef by auto
  qed

  show "\<And>iter newiter.
       \<lbrakk>(lstar_table_inv \<circ> fst) iter; (get_max_len_S \<circ> snd) iter \<le> (length \<circ> get_S \<circ> fst) iter;
        make_cc oracleM iter = Some newiter\<rbrakk>
       \<Longrightarrow> (get_max_len_S \<circ> snd) newiter \<le> (length \<circ> get_S \<circ> fst) newiter" 
    using length_S_increasing_make_cc .
qed

lemma length_E_increasing_lstar_learner_step:
  assumes
      log_inv_bef: "iteration_log_inv iter" and
          inv_bef: "(lstar_table_inv \<circ> fst) iter" and
       closed_bef: "(closedp \<circ> fst) iter" and
   consistent_bef: "(consistentp \<circ> fst) iter" and
             t_wf: "t \<in> \<Sigma>star" and
             \<comment> \<open> t is indeed a counter-example\<close>
             t_ce: "L t \<noteq> (output_on (make_conjecture (fst iter))) t" 
  shows "(get_max_len_E \<circ> snd) (lstar_learner_step oracleM t iter)              
    \<le> (length \<circ> get_E \<circ> fst) (lstar_learner_step oracleM t iter)" 
proof(rule lstar_learner_step_rule[OF inv_bef t_wf])
  from log_inv_bef show "(get_max_len_E \<circ> snd) iter \<le> (length \<circ> get_E \<circ> fst) iter" by auto
  show "\<And>tbl log.
       \<lbrakk>(lstar_table_inv \<circ> fst) (tbl, log); (get_max_len_E \<circ> snd) (tbl, log) \<le> (length \<circ> get_E \<circ> fst) (tbl, log)\<rbrakk>
       \<Longrightarrow> (get_max_len_E \<circ> snd) (learn_from_cexample t oracleM tbl, log_lece_call tbl log)
           \<le> (length \<circ> get_E \<circ> fst) (learn_from_cexample t oracleM tbl, log_lece_call tbl log)"
    using card_E_learn_from_cexample by (simp add: extendS_listE max_len_E_log_lece_call_leqI)

  show "\<And>iter newiter. \<lbrakk>(lstar_table_inv \<circ> fst) iter;
        (get_max_len_E \<circ> snd) iter \<le> (length \<circ> get_E \<circ> fst) iter;
        make_cc oracleM iter = Some newiter\<rbrakk>
         \<Longrightarrow> (get_max_len_E \<circ> snd) newiter \<le> (length \<circ> get_E \<circ> fst) newiter" 
    using length_E_increasing_make_cc .
qed

subsection \<open> Bounding the max size of strings in S and E\<close>

(* TODO: Move these upwards *)
lemma max_str_len_Un:
  "\<lbrakk>finite A; A \<noteq> {}; finite B; B \<noteq> {}\<rbrakk> \<Longrightarrow> max_str_len (A \<union> B)
    = max (max_str_len A) (max_str_len B)"
proof -
  assume assms: "finite A" "A \<noteq> {}" "finite B" "B \<noteq> {}"
  have "max_str_len (A \<union> B) = Max (length ` (A \<union> B))" unfolding max_str_len_def by simp
  also have "... = Max ((length ` A) \<union> (length ` B))" by(simp add: image_Un)
  also have "... = max (Max (length ` A)) (Max (length ` B))" using assms Max_Un
    by (simp add: Max.union)
  finally show ?thesis by (simp add: max_str_len_def)
qed

lemma max_str_len_coboundedI[intro]: "\<lbrakk>finite A; x \<in> A\<rbrakk> \<Longrightarrow> length x \<le> max_str_len A" proof -
  assume "finite A" "x \<in> A"
  then have "length x \<in> length ` A" by simp
  then have "length x \<le> Max (length ` A)" using \<open>finite A\<close> by auto
  then show ?thesis by (simp add: max_str_len_def)
qed

lemma max_str_len_boundedI[intro]:
  "\<lbrakk>finite A; A \<noteq> {}; \<And>x. x \<in> A \<Longrightarrow> length x \<le> c\<rbrakk> \<Longrightarrow> max_str_len A \<le> c"
  using max_str_len_def Max.boundedI by (simp add: max_str_len_def image_iff)

text \<open> We add at most increased the max length of string in S by 1 after make-closed\<close>
lemma max_str_len_S_make_closed:
  assumes
    inv_bef: "lstar_table_inv tbl" and
    not_closed: "\<not> (closedp tbl)"
  shows "(max_str_len \<circ> set \<circ> get_S) (make_closed oracleM tbl) \<le> Suc ((max_str_len \<circ> set \<circ> get_S) tbl)"
        "(max_str_len \<circ> set \<circ> get_S) (make_closed oracleM tbl) \<ge> (max_str_len \<circ> set \<circ> get_S) tbl"
proof -
  let ?mlS = "(max_str_len \<circ> set \<circ> get_S) tbl"
  from not_closed have "\<exists>s a. get_closedness_prob tbl = Some (s,a)"
    using get_closedness_prob_complete by simp
  then obtain s a where cp: "get_closedness_prob tbl = Some (s,a)" by auto
  have f1: "finite (set (get_S tbl))" by simp
  have f2: "finite (set (get_S (make_closed oracleM tbl)))" by simp
  have ne1: "set (get_S tbl) \<noteq> {}" using inv_bef
    lambda_in_S lstar_table_invE(3) by fastforce
  have ne2: "set (get_S (make_closed oracleM tbl)) \<noteq> {}"
    using make_closed_invariant[OF inv_bef not_closed] lambda_in_S lstar_table_invE(3)
    by fastforce
  from cp have "s \<in> set (get_S tbl)" using inv_bef
    by (simp add: get_closedness_prob_correct(2) lstar_table_invE(3))
    from cp have *: "set (get_S (make_closed oracleM tbl)) = set (get_S tbl) \<union> {s@[a]}"
    using make_closed_setS by simp
  have "length s \<le> ?mlS"
    by (simp add: max_str_len_coboundedI \<open>s \<in> set (get_S tbl)\<close>)
  show
    "(max_str_len \<circ> set \<circ> get_S) (make_closed oracleM tbl)
     \<le> Suc ((max_str_len \<circ> set \<circ> get_S) tbl)" proof -
    have "(max_str_len \<circ> set \<circ> get_S) (make_closed oracleM tbl) = max_str_len ((set (get_S tbl)) \<union> {s@[a]})"
      using * by simp
    also have "... = max (max_str_len (set (get_S tbl))) (max_str_len {s@[a]})"
      using max_str_len_Un by(metis f1 finite.emptyI finite_insert insert_not_empty ne1)
    also have "... = max ((max_str_len \<circ> set \<circ> get_S) tbl) (length (s@[a]))"
      by(auto simp add: max_str_len_def)
    also have "... \<le> max ((max_str_len \<circ> set \<circ> get_S) tbl) (Suc ((max_str_len \<circ> set \<circ> get_S) tbl))"
      using \<open>length s \<le> ?mlS\<close> by auto
    also have "... \<le> Suc ((max_str_len \<circ> set \<circ> get_S) tbl)" by auto
    finally show ?thesis .
  qed
  show
    "(max_str_len \<circ> set \<circ> get_S) (make_closed oracleM tbl)
     \<ge> ((max_str_len \<circ> set \<circ> get_S) tbl)" proof -
    have "(max_str_len \<circ> set \<circ> get_S) (make_closed oracleM tbl)
         = max_str_len ((set (get_S tbl)) \<union> {s@[a]})"
      using * by simp
    also have "... = max (max_str_len (set (get_S tbl))) (max_str_len {s@[a]})"
      using max_str_len_Un by(metis f1 finite.emptyI finite_insert insert_not_empty ne1)
    also have "... = max ((max_str_len \<circ> set \<circ> get_S) tbl) (length (s@[a]))"
      by(auto simp add: max_str_len_def)
    also have "... \<ge> max ((max_str_len \<circ> set \<circ> get_S) tbl) ((max_str_len \<circ> set \<circ> get_S) tbl)"
      using \<open>length s \<le> ?mlS\<close> by auto
    finally show ?thesis by auto
  qed
qed

text \<open> MakeConsistent doesn't change the set S\<close>
lemma max_str_len_S_make_consistent:
  shows "(max_str_len \<circ> set \<circ> get_S) (make_consistent oracleM tbl) = (max_str_len \<circ> set \<circ> get_S) tbl"
  by(simp add: make_consistent_setS)

text \<open> Learn from counterexample can at most add a string of length m_bound to S\<close>
lemma max_str_len_S_learn_from_cexample:
  assumes inv_bef: "lstar_table_inv tbl" and
           t_size: "length t \<le> m"
  shows "(max_str_len \<circ> set \<circ> get_S) (learn_from_cexample t oracleM tbl)
          \<le> max ((max_str_len \<circ> set \<circ> get_S) tbl) m"
        "(max_str_len \<circ> set \<circ> get_S) (learn_from_cexample t oracleM tbl)
          \<ge> (max_str_len \<circ> set \<circ> get_S) tbl"
proof -
  from all_prefixes_complete have "set (all_prefixes t) = {y. prefixp y t}" .
  moreover from prefixp_length have "\<And>y t. prefixp y t \<Longrightarrow> length y \<le> length t" .
  ultimately have "\<And>y. y \<in> set (all_prefixes t) \<Longrightarrow> length y \<le> length t"
    by blast 
  then have "max_str_len (set (all_prefixes t)) \<le> length t"
    by (metis List.finite_set empty_iff max_str_len_boundedI t_in_all_prefixes_t)
      \<comment> \<open> Here we use the weak version of our lemma for learn-from-cexample\<close>
  from learn_from_cexample_setS_abstract have **:
    "set (get_S (learn_from_cexample t oracleM tbl))
     = set (get_S tbl) \<union> (set (all_prefixes t))" .
  have "max_str_len (set (get_S tbl) \<union> (set (all_prefixes t))) =
        max (max_str_len (set (get_S tbl))) (max_str_len (set (all_prefixes t)))"
  proof(intro max_str_len_Un)
    show "set (get_S tbl) \<noteq> {}" using inv_bef lambda_in_S lstar_table_invE(3) by fastforce
    show "set (all_prefixes t) \<noteq> {}"
      by (metis empty_iff t_in_all_prefixes_t)
  qed(auto)
  from this and ** have 
    ***: "(max_str_len \<circ> set) (get_S (learn_from_cexample t oracleM tbl))
         = max ((max_str_len \<circ> set) (get_S tbl)) (max_str_len (set (all_prefixes t)))"
    by auto
  moreover have "(max_str_len (set (all_prefixes t))) \<le> m"
    using \<open>max_str_len (set (all_prefixes t)) \<le> length t\<close> le_trans t_size by simp
  ultimately show "(max_str_len \<circ> set \<circ> get_S) (learn_from_cexample t oracleM tbl)
    \<le> max ((max_str_len \<circ> set \<circ> get_S) tbl) m"
    by auto
  from *** show "(max_str_len \<circ> set \<circ> get_S) tbl
    \<le> (max_str_len \<circ> set \<circ> get_S) (learn_from_cexample t oracleM tbl)"
    by auto
qed

text \<open> make-closed doesn't change the set E\<close>
lemma max_str_len_E_make_closed:
  shows "(max_str_len \<circ> set \<circ> get_E) (make_closed oracleM tbl)
         = (max_str_len \<circ> set \<circ> get_E) tbl"
  by(simp add: make_closed_setE)

text \<open> We add at most increased the max length of string in S by 1 after make-consistent\<close>
lemma max_str_len_E_make_consistent:
  assumes
    inv_bef: "lstar_table_inv tbl" and
    not_consistent: "\<not> (consistentp tbl)"
  shows "(max_str_len \<circ> set \<circ> get_E) (make_consistent oracleM tbl) \<le> Suc ((max_str_len \<circ> set \<circ> get_E) tbl)"
        "(max_str_len \<circ> set \<circ> get_E) (make_consistent oracleM tbl) \<ge> (max_str_len \<circ> set \<circ> get_E) tbl"
proof -
  let ?mlE = "(max_str_len \<circ> set \<circ> get_E) tbl"
  from not_consistent have "\<exists>s1 s2 a e. get_consistency_prob tbl = Some (s1,s2,a,e)"
    using get_consistency_prob_complete by simp
  then obtain s1 s2 a e where cp: "get_consistency_prob tbl = Some (s1,s2,a,e)" by auto
  then have *: "set (get_E (make_consistent oracleM tbl)) = set (get_E tbl) \<union> {a#e}"
    using make_consistent_setE by simp
  from cp have "e \<in> set (get_E tbl)" using inv_bef
    by (simp add: get_consistency_prob_correct lstar_table_invE)
  have f1: "finite (set (get_E tbl))" by simp
  have f2: "finite (set (get_E (make_consistent oracleM tbl)))" by simp
  have ne1: "set (get_E tbl) \<noteq> {}" using inv_bef
    lambda_in_E lstar_table_invE(3) by fastforce
  have ne2: "set (get_E (make_consistent oracleM tbl)) \<noteq> {}"
    using make_consistent_invariant[OF inv_bef not_consistent] lambda_in_E lstar_table_invE(3)
    by fastforce
  have **: "length e \<le> ?mlE"
    by (simp add: max_str_len_coboundedI \<open>e \<in> set (get_E tbl)\<close>)
  show "(max_str_len \<circ> set \<circ> get_E) (make_consistent oracleM tbl)
    \<le> Suc ((max_str_len \<circ> set \<circ> get_E) tbl)" proof -
    from * have "(max_str_len \<circ> set \<circ> get_E) (make_consistent oracleM tbl)
                   = max_str_len (set (get_E tbl) \<union> {a#e})" by simp
    also have "... = max (max_str_len (set (get_E tbl))) (max_str_len {a#e})"
     using max_str_len_Un by(metis f1 finite.emptyI finite_insert insert_not_empty ne1)
    also have "... \<le> max (max_str_len (set (get_E tbl))) (Suc ?mlE)" using ** by auto
    finally show ?thesis by auto
  qed
  show "(max_str_len \<circ> set \<circ> get_E) tbl
   \<le> (max_str_len \<circ> set \<circ> get_E) (make_consistent oracleM tbl)" proof -
    from * have "(max_str_len \<circ> set \<circ> get_E) (make_consistent oracleM tbl)
                   = max_str_len (set (get_E tbl) \<union> {a#e})" by simp
    also have "... = max (max_str_len (set (get_E tbl))) (max_str_len {a#e})"
     using max_str_len_Un by(metis f1 finite.emptyI finite_insert insert_not_empty ne1)
   finally show ?thesis by auto
 qed
qed

text \<open> make-closed doesn't change the set E\<close>
lemma max_str_len_E_learn_from_cexample:
  shows "(max_str_len \<circ> set \<circ> get_E) (learn_from_cexample t oracleM tbl) 
         = (max_str_len \<circ> set \<circ> get_E) tbl"
  by (simp add: extendS_listE)

lemma max_str_len_S_increasing_make_cc_step:
  assumes
   pbef: "(max_str_len \<circ> set \<circ> get_S \<circ> fst) (tbl,log) \<ge> (get_max_str_len_S \<circ> snd) (tbl,log)" and
   inv_bef: "(lstar_table_inv \<circ> fst) (tbl,log)"
  shows "(get_max_str_len_S \<circ> snd) (make_cc_step oracleM (tbl,log))
    \<le> (max_str_len \<circ> set \<circ> get_S \<circ> fst) (make_cc_step oracleM (tbl,log))"
proof(cases "\<not> closedp tbl \<or> \<not> consistentp tbl")
  assume true: "\<not> closedp tbl \<or> \<not> consistentp tbl"
  show "(get_max_str_len_S \<circ> snd) (make_cc_step oracleM (tbl,log))
    \<le> (max_str_len \<circ> set \<circ> get_S \<circ> fst) (make_cc_step oracleM (tbl,log))" 
  proof(rule make_cc_step_cases)
    assume not_closed: "\<not> closedp tbl"
    have "(max_str_len \<circ> set \<circ> get_S \<circ> fst) (tbl,log) \<le> (max_str_len \<circ> set \<circ> get_S \<circ> fst) (make_closed oracleM tbl, log_mkcl_call tbl log)"
      using inv_bef max_str_len_S_make_closed(2)[OF _ not_closed] by auto
    from this and pbef show
      "(get_max_str_len_S \<circ> snd) (make_closed oracleM tbl, log_mkcl_call tbl log)
       \<le> (max_str_len \<circ> set \<circ> get_S \<circ> fst) (make_closed oracleM tbl, log_mkcl_call tbl log)"
      by auto
  next
    have "(max_str_len \<circ> set \<circ> get_S \<circ> fst) (tbl,log) =
      (max_str_len \<circ> set \<circ> get_S \<circ> fst) (make_consistent oracleM tbl, log_mkco_call tbl log)"
      using max_str_len_S_make_consistent by auto
    from this and pbef show 
      "(get_max_str_len_S \<circ> snd) (make_consistent oracleM tbl, log_mkco_call tbl log) \<le>
       (max_str_len \<circ> set \<circ> get_S \<circ> fst) (make_consistent oracleM tbl, log_mkco_call tbl log)"
      by auto
  next
    from true show "\<not> closedp tbl \<or> \<not> consistentp tbl" .
  qed
next
  assume "\<not> (\<not> closedp tbl \<or> \<not> consistentp tbl)"
  then have nop: "(make_cc_step oracleM (tbl,log)) = (tbl,log)" by simp
  from this and pbef show ?thesis by auto
qed


lemma max_str_len_E_increasing_make_cc_step:
  assumes
   pbef: "(max_str_len \<circ> set \<circ> get_E \<circ> fst) (tbl,log) \<ge> (get_max_str_len_E \<circ> snd) (tbl,log)" and
   inv_bef: "(lstar_table_inv \<circ> fst) (tbl,log)"
  shows "(get_max_str_len_E \<circ> snd) (make_cc_step oracleM (tbl,log))
    \<le> (max_str_len \<circ> set \<circ> get_E \<circ> fst) (make_cc_step oracleM (tbl,log))"
proof(cases "\<not> closedp tbl \<or> \<not> consistentp tbl")
  assume true: "\<not> closedp tbl \<or> \<not> consistentp tbl"
  show "(get_max_str_len_E \<circ> snd) (make_cc_step oracleM (tbl,log))
    \<le> (max_str_len \<circ> set \<circ> get_E \<circ> fst) (make_cc_step oracleM (tbl,log))" 
  proof(rule make_cc_step_cases)
    have "(max_str_len \<circ> set \<circ> get_E \<circ> fst) (tbl,log) \<le> 
     (max_str_len \<circ> set \<circ> get_E \<circ> fst) (make_closed oracleM tbl, log_mkcl_call tbl log)"
      using max_str_len_E_make_closed by auto
    from this and pbef show
      "(get_max_str_len_E \<circ> snd) (make_closed oracleM tbl, log_mkcl_call tbl log)
       \<le> (max_str_len \<circ> set \<circ> get_E \<circ> fst) (make_closed oracleM tbl, log_mkcl_call tbl log)"
      by auto
  next
    assume "closedp tbl \<and> \<not> consistentp tbl"
    then have not_consistent: "\<not> consistentp tbl" by simp
    have "(max_str_len \<circ> set \<circ> get_E \<circ> fst) (tbl,log) \<le>
      (max_str_len \<circ> set \<circ> get_E \<circ> fst) (make_consistent oracleM tbl, log_mkco_call tbl log)"
      using inv_bef max_str_len_E_make_consistent(2)[OF _ not_consistent] by auto
    from this and pbef show 
      "(get_max_str_len_E \<circ> snd) (make_consistent oracleM tbl, log_mkco_call tbl log)
       \<le> (max_str_len \<circ> set \<circ> get_E \<circ> fst)
          (make_consistent oracleM tbl, log_mkco_call tbl log)"
      by auto
  next
    from true show "\<not> closedp tbl \<or> \<not> consistentp tbl" .
  qed
next
  assume "\<not> (\<not> closedp tbl \<or> \<not> consistentp tbl)"
  then have nop: "(make_cc_step oracleM (tbl,log)) = (tbl,log)" by simp
  from this and pbef show ?thesis by auto
qed

lemma max_str_len_S_increasing_make_cc:
  assumes inv_bef: "(lstar_table_inv \<circ> fst) iter"
       and init: "(get_max_str_len_S \<circ> snd) iter \<le> (max_str_len \<circ> set \<circ> get_S \<circ> fst) iter"
       and new: "(make_cc oracleM iter) = Some newiter"
  shows "(get_max_str_len_S \<circ> snd) newiter \<le> (max_str_len \<circ> set \<circ> get_S \<circ> fst) newiter"
proof(rule make_cc_rule_with_invariant)
  from new show "(make_cc oracleM iter) = Some newiter" .
  from init show "(get_max_str_len_S \<circ> snd) iter \<le> (max_str_len \<circ> set \<circ> get_S \<circ> fst) iter" .
  from inv_bef show "(lstar_table_inv \<circ> fst) iter" .
  show "\<And>s. \<lbrakk>(get_max_str_len_S \<circ> snd) s \<le> (max_str_len \<circ> set \<circ> get_S \<circ> fst) s;
      case s of (tbl, log) \<Rightarrow> \<not> closedp tbl \<or> \<not> consistentp tbl; (lstar_table_inv \<circ> fst) s\<rbrakk>
         \<Longrightarrow> (get_max_str_len_S \<circ> snd) (make_cc_step oracleM s) \<le> (max_str_len \<circ> set \<circ> get_S \<circ> fst) (make_cc_step oracleM s)"
    using max_str_len_S_increasing_make_cc_step by blast
qed

lemma max_str_len_E_increasing_make_cc:
  assumes inv_bef: "(lstar_table_inv \<circ> fst) iter"
       and init: "(get_max_str_len_E \<circ> snd) iter \<le> (max_str_len \<circ> set \<circ> get_E \<circ> fst) iter"
       and new: "(make_cc oracleM iter) = Some newiter"
  shows "(get_max_str_len_E \<circ> snd) newiter \<le> (max_str_len \<circ> set \<circ> get_E \<circ> fst) newiter"
proof(rule make_cc_rule_with_invariant)
  from new show "(make_cc oracleM iter) = Some newiter" .
  from init show "(get_max_str_len_E \<circ> snd) iter \<le> (max_str_len \<circ> set \<circ> get_E \<circ> fst) iter" .
  from inv_bef show "(lstar_table_inv \<circ> fst) iter" .
  show "\<And>s. \<lbrakk>(get_max_str_len_E \<circ> snd) s \<le> (max_str_len \<circ> set \<circ> get_E \<circ> fst) s;
      case s of (tbl, log) \<Rightarrow> \<not> closedp tbl \<or> \<not> consistentp tbl; (lstar_table_inv \<circ> fst) s\<rbrakk>
         \<Longrightarrow> (get_max_str_len_E \<circ> snd) (make_cc_step oracleM s) \<le> (max_str_len \<circ> set \<circ> get_E \<circ> fst) (make_cc_step oracleM s)"
    using max_str_len_E_increasing_make_cc_step by blast
qed

lemma max_str_len_S_make_cc_step:
  assumes
   pbef: "(max_str_len \<circ> set \<circ> get_S \<circ> fst) s \<le> m_bound + (get_mkcl \<circ> snd) s" and
   loop: "\<not> closedp (fst s) \<or> \<not> consistentp (fst s)" and
   inv_bef: "(lstar_table_inv \<circ> fst) s"
  shows "(max_str_len \<circ> set \<circ> get_S \<circ> fst) (make_cc_step oracleM s)
          \<le> m_bound + (get_mkcl \<circ> snd) (make_cc_step oracleM s)"
proof(cases s)
case (Pair tbl log)
  let ?newiter = "(make_cc_step oracleM (tbl,log))"
  have inv_aft: "(lstar_table_inv \<circ> fst) ?newiter"
    using inv_bef Pair loop make_cc_termination_step by force
  have "(max_str_len \<circ> set \<circ> get_S \<circ> fst) ?newiter \<le> m_bound + (get_mkcl \<circ> snd) ?newiter"
  proof(rule make_cc_step_cases)
    from loop Pair show "\<not> closedp tbl \<or> \<not> consistentp tbl" by simp
  next
    assume not_closed: "\<not>closedp tbl"
    show "(max_str_len \<circ> set \<circ> get_S \<circ> fst) (make_closed oracleM tbl, log_mkcl_call tbl log) \<le> m_bound + (get_mkcl \<circ> snd) (make_closed oracleM tbl, log_mkcl_call tbl log)"
      using inv_bef max_str_len_S_make_closed[OF _ not_closed] Pair pbef by auto
  next
    assume closed_and_not_consistent: "closedp tbl \<and> \<not> consistentp tbl"
    show "(max_str_len \<circ> set \<circ> get_S \<circ> fst) (make_consistent oracleM tbl, log_mkco_call tbl log) \<le> m_bound + (get_mkcl \<circ> snd) (make_consistent oracleM tbl, log_mkco_call tbl log)"
      using max_str_len_S_make_consistent Pair pbef by auto
  qed
  then show "(max_str_len \<circ> set \<circ> get_S \<circ> fst) (make_cc_step oracleM s) \<le> m_bound + (get_mkcl \<circ> snd) (make_cc_step oracleM s)"
   by(simp add: Pair)
qed

lemma max_str_len_E_make_cc_step:
  assumes
   pbef: "(max_str_len \<circ> set \<circ> get_E \<circ> fst) s \<le> (get_mkco \<circ> snd) s" and
   loop: "\<not> closedp (fst s) \<or> \<not> consistentp (fst s)" and
   inv_bef: "(lstar_table_inv \<circ> fst) s"
  shows "(max_str_len \<circ> set \<circ> get_E \<circ> fst) (make_cc_step oracleM s)
          \<le> (get_mkco \<circ> snd) (make_cc_step oracleM s)"
proof(cases s)
case (Pair tbl log)
  let ?newiter = "(make_cc_step oracleM (tbl,log))"
  have inv_aft: "(lstar_table_inv \<circ> fst) ?newiter"
    using inv_bef Pair loop make_cc_termination_step by force
  have "(max_str_len \<circ> set \<circ> get_E \<circ> fst) ?newiter \<le> (get_mkco \<circ> snd) ?newiter"
  proof(rule make_cc_step_cases)
    from loop Pair show "\<not> closedp tbl \<or> \<not> consistentp tbl" by simp
  next
    show "(max_str_len \<circ> set \<circ> get_E \<circ> fst) (make_closed oracleM tbl, log_mkcl_call tbl log)
    \<le> (get_mkco \<circ> snd) (make_closed oracleM tbl, log_mkcl_call tbl log)"
          using max_str_len_E_make_closed Pair pbef by auto
  next
    assume closed_and_not_consistent: "closedp tbl \<and> \<not> consistentp tbl"
    then have not_consistent: "\<not>consistentp tbl" by simp
    have "(max_str_len \<circ> set \<circ> get_E) (make_consistent oracleM tbl) \<le> Suc ((max_str_len \<circ> set \<circ> get_E) tbl)"
      using inv_bef max_str_len_E_make_consistent(1)[OF _ not_consistent] Pair by auto
    from this and pbef Pair show "(max_str_len \<circ> set \<circ> get_E \<circ> fst) (make_consistent oracleM tbl, log_mkco_call tbl log)
    \<le> (get_mkco \<circ> snd) (make_consistent oracleM tbl, log_mkco_call tbl log)"
      by auto
  qed
  then show "(max_str_len \<circ> set \<circ> get_E \<circ> fst) (make_cc_step oracleM s)
             \<le> (get_mkco \<circ> snd) (make_cc_step oracleM s)"
   by(simp add: Pair)
qed

lemma max_str_len_S_make_cc:
  assumes inv_bef: "(lstar_table_inv \<circ> fst) iter" and \<comment> \<open> We don't need this, but simplifies proof\<close>
          init: "(max_str_len \<circ> set \<circ> get_S \<circ> fst) iter \<le> m_bound + (get_mkcl \<circ> snd) iter"
       and new: "(make_cc oracleM iter) = Some newiter"
  shows "(max_str_len \<circ> set \<circ> get_S \<circ> fst) newiter \<le> m_bound + (get_mkcl \<circ> snd) newiter"
proof(rule make_cc_rule_with_invariant)
  from new show "(make_cc oracleM iter) = Some newiter" .
  from init show "(max_str_len \<circ> set \<circ> get_S \<circ> fst) iter \<le> m_bound + (get_mkcl \<circ> snd) iter" .
  from inv_bef show "(lstar_table_inv \<circ> fst) iter" .
  show "\<And>s. \<lbrakk>(max_str_len \<circ> set \<circ> get_S \<circ> fst) s \<le> m_bound + (get_mkcl \<circ> snd) s;
              case s of (tbl, log) \<Rightarrow> \<not> closedp tbl \<or> \<not> consistentp tbl;
             (lstar_table_inv \<circ> fst) s\<rbrakk>
         \<Longrightarrow> (max_str_len \<circ> set \<circ> get_S \<circ> fst) (make_cc_step oracleM s)
              \<le> m_bound + (get_mkcl \<circ> snd) (make_cc_step oracleM s)"
    using max_str_len_S_make_cc_step by (simp add: case_prod_unfold)
qed

lemma max_str_len_E_make_cc:
  assumes inv_bef: "(lstar_table_inv \<circ> fst) iter" and \<comment> \<open> We don't need this, but simplifies proof\<close>
          init: "(max_str_len \<circ> set \<circ> get_E \<circ> fst) iter \<le> (get_mkco \<circ> snd) iter"
       and new: "(make_cc oracleM iter) = Some newiter"
  shows "(max_str_len \<circ> set \<circ> get_E \<circ> fst) newiter \<le> (get_mkco \<circ> snd) newiter"
proof(rule make_cc_rule_with_invariant)
  from new show "(make_cc oracleM iter) = Some newiter" .
  from init show "(max_str_len \<circ> set \<circ> get_E \<circ> fst) iter \<le> (get_mkco \<circ> snd) iter" .
  from inv_bef show "(lstar_table_inv \<circ> fst) iter" .
  show "\<And>s. \<lbrakk>(max_str_len \<circ> set \<circ> get_E \<circ> fst) s \<le> (get_mkco \<circ> snd) s;
              case s of (tbl, log) \<Rightarrow> \<not> closedp tbl \<or> \<not> consistentp tbl;
             (lstar_table_inv \<circ> fst) s\<rbrakk>
         \<Longrightarrow> (max_str_len \<circ> set \<circ> get_E \<circ> fst) (make_cc_step oracleM s)
              \<le> (get_mkco \<circ> snd) (make_cc_step oracleM s)"
    using max_str_len_E_make_cc_step by (simp add: case_prod_unfold)
qed

lemma max_str_len_S_lstar_learner_step:
  assumes pbef: "(max_str_len \<circ> set \<circ> get_S \<circ> fst) s \<le> m_bound + (get_mkcl \<circ> snd) s" and
          inv_bef: " (lstar_table_inv \<circ> fst) s" and
          closed: "(closedp \<circ> fst) s" and 
          consistent: "(consistentp \<circ> fst) s" and
          t_wf: "t \<in> \<Sigma>star" and
          \<comment> \<open> t is indeed a counter-example\<close>
          t_ce: "L t \<noteq> (output_on (make_conjecture (fst iter))) t" and
          \<comment> \<open> m_bound is a bound of the lenght of t\<close>
          t_bounded: "length t \<le> m_bound"
   shows "(max_str_len \<circ> set \<circ> get_S \<circ> fst) (lstar_learner_step oracleM t s)
          \<le> m_bound + (get_mkcl \<circ> snd) (lstar_learner_step oracleM t s)"
proof(rule lstar_learner_step_rule)
  let ?P = "\<lambda>s. (max_str_len \<circ> set \<circ> get_S \<circ> fst) s \<le> m_bound + (get_mkcl \<circ> snd) s"
  from inv_bef show "(lstar_table_inv \<circ> fst) s" by simp
  from pbef show "?P s" .
  from t_wf show "t \<in> \<Sigma>star" .
  show "\<And>tbl log.
       \<lbrakk>(lstar_table_inv \<circ> fst) (tbl, log); ?P (tbl, log)\<rbrakk>
       \<Longrightarrow> ?P (learn_from_cexample t oracleM tbl, log_lece_call tbl log)"
  proof -
    fix tbl :: "('letter,'mlo) observation_table"
    fix log :: "log_type"
    assume inv: "(lstar_table_inv \<circ> fst) (tbl, log)" and pbef: "?P (tbl, log)"
    from max_str_len_S_learn_from_cexample have 
      *: "(max_str_len \<circ> set \<circ> get_S) (learn_from_cexample t oracleM tbl)
          \<le> max ((max_str_len \<circ> set \<circ> get_S) tbl) m_bound"
      using inv t_bounded by auto
    show "?P (learn_from_cexample t oracleM tbl, log_lece_call tbl log)"
      using "*" pbef by auto
  qed
  show " \<And>iter newiter. \<lbrakk>(lstar_table_inv \<circ> fst) iter; (max_str_len \<circ> set \<circ> get_S \<circ> fst) iter
           \<le> m_bound + (get_mkcl \<circ> snd) iter; make_cc oracleM iter = Some newiter\<rbrakk>
      \<Longrightarrow> (max_str_len \<circ> set \<circ> get_S \<circ> fst) newiter \<le> m_bound + (get_mkcl \<circ> snd) newiter"
    using max_str_len_S_make_cc by auto
qed

lemma max_str_len_E_lstar_learner_step:
  assumes pbef: "(max_str_len \<circ> set \<circ> get_E \<circ> fst) s \<le> (get_mkco \<circ> snd) s" and
          inv_bef: " (lstar_table_inv \<circ> fst) s" and
          closed: "(closedp \<circ> fst) s" and 
          consistent: "(consistentp \<circ> fst) s" and
          t_wf: "t \<in> \<Sigma>star" and
          \<comment> \<open> t is indeed a counter-example\<close>
          t_ce: "L t \<noteq> (output_on (make_conjecture (fst iter))) t" and
          \<comment> \<open> m_bound is a bound of the lenght of t\<close>
          t_bounded: "length t \<le> m"
   shows "(max_str_len \<circ> set \<circ> get_E \<circ> fst) (lstar_learner_step oracleM t s)
          \<le> (get_mkco \<circ> snd) (lstar_learner_step oracleM t s)"
proof(rule lstar_learner_step_rule)
  let ?P = "\<lambda>s. (max_str_len \<circ> set \<circ> get_E \<circ> fst) s \<le> (get_mkco \<circ> snd) s"
  from inv_bef show "(lstar_table_inv \<circ> fst) s" by simp
  from pbef show "?P s" .
  from t_wf show "t \<in> \<Sigma>star" .
  show "\<And>tbl log.
       \<lbrakk>(lstar_table_inv \<circ> fst) (tbl, log); ?P (tbl, log)\<rbrakk>
       \<Longrightarrow> ?P (learn_from_cexample t oracleM tbl, log_lece_call tbl log)"
  proof -
    fix tbl :: "('letter,'mlo) observation_table"
    fix log :: "log_type"
    assume inv: "(lstar_table_inv \<circ> fst) (tbl, log)" and pbef: "?P (tbl, log)"
    show "?P (learn_from_cexample t oracleM tbl, log_lece_call tbl log)"
      using max_str_len_E_learn_from_cexample pbef by auto
  qed
  show "\<And>iter newiter.
       \<lbrakk>(lstar_table_inv \<circ> fst) iter; (max_str_len \<circ> set \<circ> get_E \<circ> fst) iter \<le> (get_mkco \<circ> snd) iter;
        make_cc oracleM iter = Some newiter\<rbrakk>
       \<Longrightarrow> (max_str_len \<circ> set \<circ> get_E \<circ> fst) newiter \<le> (get_mkco \<circ> snd) newiter"
    using max_str_len_E_make_cc by auto
qed

lemma max_str_len_S_increasing_lstar_learner_step:
  assumes
      log_inv_bef: "iteration_log_inv iter" and
          inv_bef: "(lstar_table_inv \<circ> fst) iter" and
       closed_bef: "(closedp \<circ> fst) iter" and
   consistent_bef: "(consistentp \<circ> fst) iter" and
             t_wf: "t \<in> \<Sigma>star" and
             \<comment> \<open> t is indeed a counter-example\<close>
             t_ce: "L t \<noteq> (output_on (make_conjecture (fst iter))) t" and
             \<comment> \<open> m_bound is a bound of the lenght of t
                  (not needed, but precond of stronger lemma)\<close>
        t_bounded: "length t \<le> m"
  shows "(get_max_str_len_S \<circ> snd) (lstar_learner_step oracleM t iter)
         \<le> (max_str_len \<circ> set \<circ> get_S \<circ> fst) (lstar_learner_step oracleM t iter)"
proof(rule lstar_learner_step_rule[OF inv_bef t_wf])
  from log_inv_bef show "(get_max_str_len_S \<circ> snd) iter \<le> (max_str_len \<circ> set \<circ> get_S \<circ> fst) iter"
    by auto
  show "\<And>tbl log.
       \<lbrakk>(lstar_table_inv \<circ> fst) (tbl, log); (get_max_str_len_S \<circ> snd) (tbl, log)
        \<le> (max_str_len \<circ> set \<circ> get_S \<circ> fst) (tbl, log)\<rbrakk>
       \<Longrightarrow> (get_max_str_len_S \<circ> snd) (learn_from_cexample t oracleM tbl, log_lece_call tbl log)
        \<le> (max_str_len \<circ> set \<circ> get_S \<circ> fst)
             (learn_from_cexample t oracleM tbl, log_lece_call tbl log)"
  proof -
    fix tbl :: "('letter,'mlo) observation_table"
    fix log :: "log_type"
    assume inv_bef: "(lstar_table_inv \<circ> fst) (tbl, log)" and
              pbef: "(get_max_str_len_S \<circ> snd) (tbl, log) \<le> (max_str_len \<circ> set \<circ> get_S \<circ> fst) (tbl, log)"
    from inv_bef have inv_tbl: "lstar_table_inv tbl" by simp
    have "(max_str_len \<circ> set \<circ> get_S) (learn_from_cexample t oracleM tbl)
          \<ge> (max_str_len \<circ> set \<circ> get_S) tbl"
      using max_str_len_S_learn_from_cexample(2)[OF inv_tbl t_bounded] .
    then show
       "(get_max_str_len_S \<circ> snd) (learn_from_cexample t oracleM tbl, log_lece_call tbl log)
       \<le> (max_str_len \<circ> set \<circ> get_S \<circ> fst)
           (learn_from_cexample t oracleM tbl, log_lece_call tbl log)"
      using pbef by auto
  qed

  show "\<And>iter newiter.
       \<lbrakk>(lstar_table_inv \<circ> fst) iter; (get_max_str_len_S \<circ> snd) iter
         \<le> (max_str_len \<circ> set \<circ> get_S \<circ> fst) iter; make_cc oracleM iter = Some newiter\<rbrakk>
       \<Longrightarrow> (get_max_str_len_S \<circ> snd) newiter \<le> (max_str_len \<circ> set \<circ> get_S \<circ> fst) newiter"
    using max_str_len_S_increasing_make_cc .
qed

lemma max_str_len_E_increasing_lstar_learner_step:
  assumes
      log_inv_bef: "iteration_log_inv iter" and
          inv_bef: "(lstar_table_inv \<circ> fst) iter" and
       closed_bef: "(closedp \<circ> fst) iter" and
   consistent_bef: "(consistentp \<circ> fst) iter" and
             t_wf: "t \<in> \<Sigma>star" and
             \<comment> \<open> t is indeed a counter-example\<close>
             t_ce: "L t \<noteq> (output_on (make_conjecture (fst iter))) t" and
             \<comment> \<open> m_bound is a bound of the lenght of t
                  (not needed, but precond of stronger lemma)\<close>
        t_bounded: "length t \<le> m"
  shows "(get_max_str_len_E \<circ> snd) (lstar_learner_step oracleM t iter)
         \<le> (max_str_len \<circ> set \<circ> get_E \<circ> fst) (lstar_learner_step oracleM t iter)"
proof(rule lstar_learner_step_rule[OF inv_bef t_wf])
  from log_inv_bef show "(get_max_str_len_E \<circ> snd) iter \<le> (max_str_len \<circ> set \<circ> get_E \<circ> fst) iter"
    by auto
  show "\<And>tbl log.
       \<lbrakk>(lstar_table_inv \<circ> fst) (tbl, log); (get_max_str_len_E \<circ> snd) (tbl, log)
        \<le> (max_str_len \<circ> set \<circ> get_E \<circ> fst) (tbl, log)\<rbrakk>
       \<Longrightarrow> (get_max_str_len_E \<circ> snd) (learn_from_cexample t oracleM tbl, log_lece_call tbl log)
        \<le> (max_str_len \<circ> set \<circ> get_E \<circ> fst)
             (learn_from_cexample t oracleM tbl, log_lece_call tbl log)"
  proof -
    fix tbl :: "('letter,'mlo) observation_table"
    fix log :: "log_type"
    assume inv_bef: "(lstar_table_inv \<circ> fst) (tbl, log)" and
              pbef: "(get_max_str_len_E \<circ> snd) (tbl, log)
                      \<le> (max_str_len \<circ> set \<circ> get_E \<circ> fst) (tbl, log)"
    from inv_bef have inv_tbl: "lstar_table_inv tbl" by simp
    then show
       "(get_max_str_len_E \<circ> snd) (learn_from_cexample t oracleM tbl, log_lece_call tbl log)
       \<le> (max_str_len \<circ> set \<circ> get_E \<circ> fst)
           (learn_from_cexample t oracleM tbl, log_lece_call tbl log)"
      using pbef max_str_len_E_learn_from_cexample by auto
  qed
  show "\<And>iter newiter.
       \<lbrakk>(lstar_table_inv \<circ> fst) iter; (get_max_str_len_E \<circ> snd) iter 
        \<le> (max_str_len \<circ> set \<circ> get_E \<circ> fst) iter;
        make_cc oracleM iter = Some newiter\<rbrakk>
       \<Longrightarrow> (get_max_str_len_E \<circ> snd) newiter \<le> (max_str_len \<circ> set \<circ> get_E \<circ> fst) newiter"
    using max_str_len_E_increasing_make_cc .
qed

subsection \<open> Propagating the iteration log invariant \<close>

lemma iteration_log_inv_make_cc_step:
  assumes inv_bef: "(lstar_table_inv \<circ> fst) iter"
      and log_inv_bef: "iteration_log_inv iter"
      and loop: "(\<not> closedp (fst iter)) \<or> (\<not> consistentp (fst iter))"
    shows "iteration_log_inv (make_cc_step oracleM iter)"
proof -
  let ?body = "make_cc_step oracleM iter"
  have "(get_mkcl \<circ> snd) ?body + (get_mkco \<circ> snd) ?body \<le>
          Suc ((get_mkcl \<circ> snd) iter + (get_mkco \<circ> snd) iter)"
    using make_cc_step_log_upd_plus_one by blast
  also have "... < Suc ((num_diff_rows \<circ> fst) iter)" using log_inv_bef by auto
  also have "... \<le> (num_diff_rows \<circ> fst) (make_cc_step oracleM iter)"
    using make_cc_step_variant inv_bef loop
    by (metis Suc_leI comp_apply prod.collapse)
  finally have **:
    "(get_mkcl \<circ> snd) ?body + (get_mkco \<circ> snd) ?body < (num_diff_rows \<circ> fst) ?body" .

  \<comment> \<open> We show the various parts of the log-invariant \<close>
  show ?thesis proof(intro iteration_log_invI)

    \<comment> \<open> The number of calls of mkcl + mkco remains bounded \<close>
    from ** show "(get_mkcl \<circ> snd) ?body + (get_mkco \<circ> snd) ?body < (num_diff_rows \<circ> fst) ?body" .
    
    \<comment> \<open> The number of calls of lece remains bounded \<close>
    show "(get_lece \<circ> snd) ?body < (num_diff_rows \<circ> fst) ?body"
      by (metis (no_types, lifting) log_inv_bef Suc_le_lessD \<open>Suc ((num_diff_rows \<circ> fst) iter) \<le> (num_diff_rows \<circ> fst) ?body\<close> dual_order.strict_trans iteration_log_inv_leceD make_cc_step_log_lece_not_upd)

    \<comment> \<open> The max length of the set S is increasing \<close>
    show "(get_max_len_S \<circ> snd) ?body
    \<le> (length \<circ> get_S \<circ> fst) ?body"
      using length_S_increasing_make_cc_step
      by (metis inv_bef iteration_log_inv_get_max_lenSD log_inv_bef prod.collapse)
    
    \<comment> \<open> The length of S is related to the log qtys.\<close>
    show "(length \<circ> get_S \<circ> fst) ?body
    \<le> Suc ((get_mkcl \<circ> snd) ?body +
            m_bound * (get_lece \<circ> snd) ?body)"
      using length_S_make_cc_step
            inv_bef log_inv_bef loop by blast

    \<comment> \<open> The length of E is increasing\<close>
    show "(get_max_len_E \<circ> snd) ?body \<le>  (length \<circ> get_E \<circ> fst) ?body"
      using length_E_increasing_make_cc_step
      by (metis inv_bef iteration_log_inv_get_max_lenED log_inv_bef prod.collapse)

    \<comment> \<open> The length of E is related to the log qtys.\<close>
    show "(length \<circ> get_E \<circ> fst) ?body \<le>
      Suc ((get_mkco \<circ> snd) ?body)"
      using length_E_make_cc_step inv_bef log_inv_bef loop by blast

    \<comment> \<open> The max str len in S keeps growing. \<close>
    show "(max_str_len \<circ> set \<circ> get_S \<circ> fst) ?body \<ge> (get_max_str_len_S \<circ> snd) ?body"
      using max_str_len_S_increasing_make_cc_step inv_bef log_inv_bef loop
      by (metis iteration_log_inv_get_max_str_len_S_incrD prod.collapse)

    \<comment> \<open> The max str len in S is related to logged quantities \<close>
    show "(max_str_len \<circ> set \<circ> get_S \<circ> fst) ?body \<le> m_bound + (get_mkcl \<circ> snd) ?body"
      using max_str_len_S_make_cc_step inv_bef log_inv_bef loop by blast

    \<comment> \<open> The max str len in E keeps growing. \<close>
    show "(max_str_len \<circ> set \<circ> get_E \<circ> fst) ?body \<ge> (get_max_str_len_E \<circ> snd) ?body"
      using max_str_len_E_increasing_make_cc_step inv_bef log_inv_bef loop
      by (metis iteration_log_inv_get_max_str_len_E_incrD prod.collapse)

    \<comment> \<open> The max str len in E is related to logged quantities \<close>
    show "(max_str_len \<circ> set \<circ> get_E \<circ> fst) ?body \<le> (get_mkco \<circ> snd) ?body"
      using max_str_len_E_make_cc_step inv_bef log_inv_bef loop by blast

  qed
qed

\<comment> \<open> We loop the log invariant through makeCC. \<close>
lemma iteration_log_inv_make_cc:
  assumes inv_bef: "(lstar_table_inv \<circ> fst) iter" and \<comment> \<open> We don't need this, but simplifies proof\<close>
          init: "iteration_log_inv iter"
       and new: "(make_cc oracleM iter) = Some newiter"
  shows "iteration_log_inv newiter"
proof(rule make_cc_rule_with_invariant)
  from new show "(make_cc oracleM iter) = Some newiter" .
  from init show "iteration_log_inv iter" . 
  from inv_bef show "(lstar_table_inv \<circ> fst) iter" .
  show "\<And>s. \<lbrakk>iteration_log_inv s; case s of (tbl, log) \<Rightarrow> \<not> closedp tbl \<or> \<not> consistentp tbl; (lstar_table_inv \<circ> fst) s\<rbrakk>
         \<Longrightarrow> iteration_log_inv (make_cc_step oracleM s)"
    using iteration_log_inv_make_cc_step
    by (simp add: case_prod_unfold)
qed

lemma iteration_log_inv_lstar_learner_step:
  assumes inv_bef: "(lstar_table_inv \<circ> fst) iter" and
      log_inv_bef: "iteration_log_inv iter" and 
       closed_bef: "(closedp \<circ> fst) iter" and
   consistent_bef: "(consistentp \<circ> fst) iter" and
             t_wf: "t \<in> \<Sigma>star" and
                \<comment> \<open> t is indeed a counter-example\<close>
             t_ce: "L t \<noteq> (output_on (make_conjecture (fst iter))) t" and
                \<comment> \<open> m_bound is an upper bound for the lenght of t\<close>
        t_bounded: "length t \<le> m_bound"
  shows "iteration_log_inv (lstar_learner_step oracleM t iter)"
proof(intro iteration_log_invI)
  let ?body = "lstar_learner_step oracleM t iter"

  \<comment> \<open> The number of calls of mkcl + mkco remains bounded \<close>
  show "(get_mkcl \<circ> snd) ?body + (get_mkco \<circ> snd) ?body < (num_diff_rows \<circ> fst) ?body"
    using lStarLearnerStep_log_invariant_num_diff_rows MAT_model_moore_axioms inv_bef log_inv_bef t_wf by blast
  
  \<comment> \<open> The number of calls of lece remains bounded \<close>
  show "(get_lece \<circ> snd) ?body < (num_diff_rows \<circ> fst) ?body"
     using closed_bef consistent_bef inv_bef lStarLearnerStep_log_invariant_lece log_inv_bef t_ce t_wf by blast

  \<comment> \<open> The max length of the set S is increasing \<close>
   show "(get_max_len_S \<circ> snd) ?body \<le> (length \<circ> get_S \<circ> fst) ?body"
     using length_S_increasing_lstar_learner_step  closed_bef consistent_bef
       inv_bef log_inv_bef t_ce t_wf by blast
  
  \<comment> \<open> The length of S is related to the log qtys.\<close>
  show "(length \<circ> get_S \<circ> fst) ?body \<le> Suc ((get_mkcl \<circ> snd) ?body +
          m_bound * (get_lece \<circ> snd) ?body)"
    using length_S_lstar_learner_step log_inv_bef closed_bef consistent_bef inv_bef t_ce t_wf
      t_bounded by blast

  \<comment> \<open> The length of E is increasing\<close>
  show "(get_max_len_E \<circ> snd) ?body \<le> (length \<circ> get_E \<circ> fst) ?body"
   using length_E_lstar_learner_step closed_bef consistent_bef inv_bef
     length_E_increasing_lstar_learner_step log_inv_bef t_ce t_wf by blast

  \<comment> \<open> The length of E is related to the log qtys.\<close>
  show "(length \<circ> get_E \<circ> fst) ?body \<le> Suc ((get_mkco \<circ> snd) ?body)"
    using length_E_lstar_learner_step log_inv_bef closed_bef consistent_bef inv_bef t_ce t_wf
    by blast

  \<comment> \<open> The max str len in S keeps growing. \<close>
  show "(max_str_len \<circ> set \<circ> get_S \<circ> fst) ?body \<ge> (get_max_str_len_S \<circ> snd) ?body"
    using max_str_len_S_lstar_learner_step closed_bef consistent_bef inv_bef
      log_inv_bef max_str_len_S_increasing_lstar_learner_step t_bounded t_ce t_wf by blast

  \<comment> \<open> The max str len in S is related to logged quantities \<close>
  show "(max_str_len \<circ> set \<circ> get_S \<circ> fst) ?body \<le> m_bound + (get_mkcl \<circ> snd) ?body"
    using max_str_len_S_lstar_learner_step closed_bef consistent_bef inv_bef
      log_inv_bef t_bounded t_ce t_wf by blast
  \<comment> \<open> The max str len in E keeps growing. \<close>
  show "(max_str_len \<circ> set \<circ> get_E \<circ> fst) ?body \<ge> (get_max_str_len_E \<circ> snd) ?body"
    using max_str_len_E_lstar_learner_step closed_bef consistent_bef inv_bef
      log_inv_bef max_str_len_E_increasing_lstar_learner_step t_bounded t_ce t_wf by blast

  \<comment> \<open> The max str len in E is related to logged quantities \<close>
  show "(max_str_len \<circ> set \<circ> get_E \<circ> fst) ?body \<le> (get_mkco \<circ> snd) ?body"
    using max_str_len_E_lstar_learner_step closed_bef consistent_bef inv_bef
      log_inv_bef t_bounded t_ce t_wf by blast
qed

lemma lstar_learner_loop_iteration_log_inv:
  assumes inv_bef: "(lstar_table_inv \<circ> fst) iter"
      and log_inv_bef: "iteration_log_inv iter"
      and new: "(lstar_learner_loop oracleM oracleC iter) = Some newiter" 
    shows "iteration_log_inv newiter"
proof(rule lstar_learner_loop_rule_with_invariant)
  from new show "lstar_learner_loop oracleM oracleC iter = Some newiter" .
  from inv_bef show "(lstar_table_inv \<circ> fst) iter" .
  show "\<And>newiter. make_cc oracleM iter = Some newiter \<Longrightarrow> iteration_log_inv newiter"
    using inv_bef log_inv_bef iteration_log_inv_make_cc by auto
  show "\<And>s. \<lbrakk>iteration_log_inv s; (oracleC \<circ> make_conjecture \<circ> fst) s \<noteq> None;
             (lstar_table_inv \<circ> fst) s; (closedp \<circ> fst) s; (consistentp \<circ> fst) s\<rbrakk>
         \<Longrightarrow> iteration_log_inv (lstar_learner_step oracleM
               (the (oracleC (make_conjecture (fst s)))) s)" proof -
    fix s
    assume assms: "iteration_log_inv s" "(oracleC \<circ> make_conjecture \<circ> fst) s \<noteq> None"
           "(lstar_table_inv \<circ> fst) s" "(closedp \<circ> fst) s" "(consistentp \<circ> fst) s"
    then have "num_states (make_conjecture (fst s)) \<le> num_states M\<^sub>L"
      using num_states_make_conjecture
      by auto
    then have "length (the (oracleC (make_conjecture (fst s)))) \<le> m_bound"
      using assms oracleC_make_conjecture_properties by auto
    then show "iteration_log_inv
      (lstar_learner_step oracleM (the (oracleC (make_conjecture (fst s)))) s)" using assms
      by (simp add: iteration_log_inv_lstar_learner_step oracleC_make_conjecture_properties)
  qed
qed

lemma iteration_log_inv_initial_iter:
  "iteration_log_inv (make_initial_table \<Sigma> oracleM, initial_log)"
proof(intro iteration_log_invI)
  let ?body = "(make_initial_table \<Sigma> oracleM, initial_log)"

  have ndr_geq_1: "(num_diff_rows \<circ> fst) ?body \<ge> 1" proof -
      have "[] \<in> set (get_S (fst ?body))" by simp
      then have "set (get_S (fst ?body)) \<noteq> {}" by simp
      then have "card {get_row_of (fst ?body) s | s. s \<in> set (get_S (fst ?body))} \<ge> 1"
        by auto
      then show ?thesis by(simp add: num_diff_rows_def)
  qed
  show "(get_mkcl \<circ> snd) ?body + (get_mkco \<circ> snd) ?body < (num_diff_rows \<circ> fst) ?body"
    using ndr_geq_1 by auto
  show "(get_lece \<circ> snd) ?body < (num_diff_rows \<circ> fst) ?body"
    using ndr_geq_1 by auto
  show "(get_max_len_S \<circ> snd) ?body \<le> (length \<circ> get_S \<circ> fst) ?body"
     by auto
  show "(length \<circ> get_S \<circ> fst) ?body \<le> Suc ((get_mkcl \<circ> snd) ?body +
          m_bound * (get_lece \<circ> snd) ?body)"
    by auto
  show "(get_max_len_E \<circ> snd) ?body \<le> (length \<circ> get_E \<circ> fst) ?body"
    by auto
  show "(length \<circ> get_E \<circ> fst) ?body \<le> Suc ((get_mkco \<circ> snd) ?body)"
    by auto
  show "(max_str_len \<circ> set \<circ> get_S \<circ> fst) ?body \<ge> (get_max_str_len_S \<circ> snd) ?body"
    by auto
  show "(max_str_len \<circ> set \<circ> get_S \<circ> fst) ?body \<le> m_bound + (get_mkcl \<circ> snd) ?body"
    by auto
  show "(max_str_len \<circ> set \<circ> get_E \<circ> fst) ?body \<ge> (get_max_str_len_E \<circ> snd) ?body"
    by auto
  show "(max_str_len \<circ> set \<circ> get_E \<circ> fst) ?body \<le> (get_mkco \<circ> snd) ?body" proof -
    have "max_str_len {[]} = 0" by(simp add: max_str_len_def)
    then show ?thesis by auto
  qed
qed

text \<open> Finally, we get the log-invariant (carrying desired properties) after the whole
       algorithm execution.\<close>
lemma lstar_learner_runtime: 
  assumes no_dups_sigma: "distinct \<Sigma>"
  shows "iteration_log_inv (lstar_learner \<Sigma> oracleM oracleC)"
proof(rule lstar_learner_loop_iteration_log_inv
       [where ?iter="((make_initial_table \<Sigma> oracleM), initial_log)"])
  show "(lstar_table_inv \<circ> fst) (make_initial_table \<Sigma> oracleM, initial_log)"
    using make_initial_table_invariant by(simp add: no_dups_sigma)
  then show "lstar_learner_loop oracleM oracleC (make_initial_table \<Sigma> oracleM, initial_log)
    = Some (lstar_learner \<Sigma> oracleM oracleC)"
    using lstar_learner_loop_termination by auto
  show "iteration_log_inv (make_initial_table \<Sigma> oracleM, initial_log)"
    using iteration_log_inv_initial_iter .
qed

end

end