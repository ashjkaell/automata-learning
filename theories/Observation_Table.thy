section \<open> Datatypes for Observation Tables \<close>

theory Observation_Table
  imports Main
          Basic_Defs
          Finite_Sets
begin

subsection \<open> The main datatype \<close>

text \<open> We use a datatype of finite sets (simply represented as lists) for A, S, SA and E.
    The functions used will maintain their "set-like" qualities \<close>
record ('l, 'o) observation_table =
    \<comment> \<open>The alphabet over which this table is defined is a finite set\<close>
    get_A :: "'l fset" 
    \<comment> \<open> Sets S, SA and E as per Angluin's description.\<close>
    get_S :: "'l list fset "
    get_SA :: "'l list fset "
    get_E :: "'l list fset "
    \<comment> \<open> The table's finite function of observations.\<close>
    get_T :: "('l list, 'o) map"

abbreviation row_labels :: " ('l,'o) observation_table \<Rightarrow> 'l list fset" where
  " row_labels table \<equiv> (get_S table \<union>\<^sub>f get_SA table)"

lemma row_labels_S_I: "x \<in> set (get_S table) \<Longrightarrow> x \<in> set (row_labels table)"
  by auto  
lemma row_labels_SA_I: "x \<in> set (get_SA table) \<Longrightarrow> x \<in> set (row_labels table)"
  by auto

text \<open> This lemma eases case splits over t in @{const row_labels}" \<close>
lemma row_labels_cases[consumes 1, case_names S SA]: 
  "\<lbrakk>t \<in> set (row_labels table); 
    t \<in> set (get_S table) \<Longrightarrow> P;
    t \<in> set (get_SA table) \<Longrightarrow> P\<rbrakk>
     \<Longrightarrow> P"
  using set_of_fset_union by auto

paragraph \<open> Equality on rows \<close>

text \<open> We define a predicate to test if rows labelled by r1 and r2 are equal.
       Note, this definition is still executable.\<close>
definition rows_eq_p :: " ('l,'o) observation_table \<Rightarrow> 'l list \<Rightarrow> 'l list \<Rightarrow> bool "
  ("(_) \<turnstile> (_)/ \<equiv>/ (_)" [64,64,64]63) where
  " rows_eq_p table r1 r2 \<equiv>
     \<forall>x \<in> (set (get_E table)). get_T table (r1@x) = get_T table (r2@x) "

lemma rows_eq_pE[elim]:
  "\<lbrakk>rows_eq_p table r q; e \<in> set (get_E table); a = r@e; b = q@e\<rbrakk>
    \<Longrightarrow> ((get_T table) a = (get_T table) b)"
  by(auto simp add: rows_eq_p_def)

text \<open> @{const rows_eq_p} is an equivalence relation \<close>

lemma equivp_rows_eq_p[simp]: "equivp (rows_eq_p table)"
  apply(auto simp add: equivp_def)
  apply(auto simp add: rows_eq_p_def)
  done

lemma symp_rows_eq_p: "symp (rows_eq_p table)" proof -
  have "equivp (rows_eq_p table)" by(simp)
  then show ?thesis by(rule equivpE)
qed

lemma reflp_rows_eq_p: "reflp (rows_eq_p table)" proof -
  have "equivp (rows_eq_p table)" by(simp)
  then show ?thesis by(rule equivpE)
qed

lemma transp_rows_eq_p: "transp (rows_eq_p table)" proof -
  have "equivp (rows_eq_p table)" by(simp)
  then show ?thesis by(rule equivpE)
qed

subsection \<open> The "filled" invariant \<close>

text \<open> During the execution of the algorithm (and also the definitions in this file)
       we'll need to access the T function of an observation table. This is a finite
       function, only defined on the rows in $S \cup SA$ extended by the extensions in E.
       We define a predicate, that says that the mapping T has a value for all such
       inputs.

       We will show that this stays correct during the execution. Ex: After extending
       the table (adding new rows) and updating T the "filled" predicate still holds. \<close>

definition filledp :: " ('a,'o) observation_table \<Rightarrow> bool " where
  "filledp table \<equiv> dom (get_T table) = 
     {s@e | s e. s \<in> set (row_labels table) \<and> e \<in> set (get_E table)}"

lemma filledpI[intro]: 
  "\<lbrakk>\<And>s e. \<lbrakk>s \<in> set (row_labels table); e \<in> set (get_E table)\<rbrakk> \<Longrightarrow>
                     (s@e) \<in> dom (get_T table);
    \<And>x. x \<in> dom (get_T table)
      \<Longrightarrow> (\<exists>s \<in> set (row_labels table). \<exists>e \<in> set (get_E table). x = s@e)\<rbrakk>
    \<Longrightarrow> filledp table"
  unfolding filledp_def by blast

lemma filledpE[elim]:
  "\<lbrakk>filledp table; s \<in> set (row_labels table); e \<in> set (get_E table)\<rbrakk>
    \<Longrightarrow> (get_T table) (s@e) \<noteq> None"
  by(auto simp add: filledp_def)

lemma filledpE2[elim]:
  "\<lbrakk>filledp table; s \<in> set (row_labels table); e \<in> set (get_E table)\<rbrakk>
   \<Longrightarrow> \<exists>x. (get_T table) (s@e) = Some x"
proof -
  assume "filledp table" "s \<in> set (row_labels table)" "e \<in> set (get_E table)"
  then have "(get_T table) (s@e) \<noteq> None" by(rule filledpE)
  then show ?thesis by auto
qed

lemma filledpE3[elim]: "\<lbrakk>filledp table; (get_T table) x = Some k\<rbrakk> \<Longrightarrow> 
  \<exists>s \<in> set (row_labels table). \<exists>e \<in> set (get_E table). x = s@e"
  unfolding filledp_def by blast

subsection \<open> Wellformedness of Tables \<close>

text \<open> We will maintain the following "well-formedness" invariant during execution of the
       algorithm:
       \<^item> The set S is non-empty and "prefix-closed"
       \<^item> The set SA is indeed $S \cdot A$ (all words of S with all letters from A appended)
       \<^item> The set E is non-empty and "suffix-closed" \<close>

definition wellformedp :: " ('a,'o) observation_table \<Rightarrow> bool " where
  " wellformedp table \<equiv>
      \<comment> \<open> S is non empty and prefix closed \<close>
      set (get_S table) \<noteq> {} \<and> prefix_closed (set (get_S table)) \<and>
      \<comment> \<open> E is non empty and suffix closed \<close>
      set (get_E table) \<noteq> {} \<and> suffix_closed (set (get_E table)) \<and>
      \<comment> \<open> SA is what it claims to be \<close>
      (set (get_SA table)
       = {(s@[a]) | a s. a \<in> set (get_A table) \<and> s \<in> set (get_S table)}) \<and>
      \<comment> \<open> A is indeed the alphabet \<close>
      (\<forall>s \<in> set (row_labels table). s \<in> lists (set (get_A table))) \<and>
      (\<forall>s \<in> set (get_E table). s \<in> lists (set (get_A table)))"

lemma wellformedpI: "\<lbrakk>
     set (get_S table) \<noteq> {}; prefix_closed (set (get_S table));
     set (get_E table) \<noteq> {}; suffix_closed (set (get_E table));
      (set (get_SA table)
        = {(s@[a]) | a s. a \<in> set (get_A table) \<and> s \<in> set (get_S table)});
      (\<forall>s \<in> set (row_labels table). s \<in> lists (set (get_A table)));
      (\<forall>s \<in> set (get_E table). s \<in> lists (set (get_A table)))\<rbrakk>
  \<Longrightarrow> wellformedp table"
  by (simp add: wellformedp_def)

lemma wellformedp_alphabetE:
  "wellformedp table \<Longrightarrow> s \<in> set (row_labels table)
    \<Longrightarrow> s \<in> lists (set (get_A table))"
  by (simp add: wellformedp_def)

lemma wellformedp_alphabetE2:
  "wellformedp table \<Longrightarrow> s \<in> set (get_E table)
    \<Longrightarrow> s \<in> lists (set (get_A table))"
  by (simp add: wellformedp_def)

text \<open> A useful fact is the following, which explains why we want S to be prefix closed. \<close>
lemma wellformedpE:
  assumes wformed: "wellformedp table" and *: "(s@[a] \<in> set (row_labels table))"
  shows "s \<in> set (get_S table)" "a \<in> set (get_A table) "
proof -
  show "s \<in> set (get_S table)"
  proof(cases "s@[a] \<in> set (get_S table)")
    case True
    from wformed have "prefix_closed (set (get_S table))"
      using wellformedp_def by blast
    then show ?thesis using True prefix_closed_def prefixp_def by fastforce 
  next
    case False
    then have "s@[a] \<in> set (get_SA table)"
      using "*" set_of_fset_union by fastforce
    moreover from wformed have "(set (get_SA table)
       = {(s@[a]) | a s. a \<in> set (get_A table) \<and> s \<in> set (get_S table)})"
      using wellformedp_def Collect_cong by fastforce
    ultimately show ?thesis by(auto)
  qed
  moreover show "a \<in> set (get_A table)" using wformed *
    using wellformedp_def *
    by (meson in_lists_conv_set in_set_conv_decomp wellformedp_alphabetE)
qed

lemma wellformedpE2:
  assumes wformed: "wellformedp table" and *: "s \<in> set (get_S table)" 
                                       and **: "a \<in> set (get_A table) "
  shows "(s@[a] \<in> set (get_SA table))"
proof -
  from wformed have "(set (get_SA table)
    = {(s@[a]) | a s. a \<in> set (get_A table) \<and> s \<in> set (get_S table)})"
    unfolding wellformedp_def by(auto)
  then show ?thesis using * ** by(auto)
qed

lemma wellformedp_get_SA_E: "wellformedp table \<Longrightarrow> sa \<in> set (get_SA table)
  \<Longrightarrow> (\<exists>s \<in> set (get_S table). \<exists>a \<in> set (get_A table). sa = s@[a])"
  unfolding wellformedp_def by auto

text \<open> Some consequences of the invariant are: \<close>

\<comment> \<open> The empty word $\lambda$ is in E\<close>
lemma lambda_in_E[simp]:
  assumes wf: " wellformedp table "
  shows " [] \<in> set (get_E table)" proof -
  from wf have *: "set (get_E table) \<noteq> {}"
          and **: "suffix_closed (set (get_E table))"
    using wellformedp_def by auto
  from * show " [] \<in> set (get_E table)" proof -
    from * obtain x where xdef: "x \<in> set (get_E table)"
      by fastforce
    then show ?thesis proof(induction x)
      case Nil
      then show ?case by(auto)
    next
      case (Cons a x)
      assume IH: "x \<in> set (get_E table) \<Longrightarrow> [] \<in> set (get_E table)" 
         and "a # x \<in> set (get_E table)"
      then have "suffixp x (a # x)" by(simp add: suffixp_def)
      from this and \<open>a # x \<in> set (get_E table)\<close> have "x \<in> set (get_E table)"
        using ** by(simp add: suffix_closed_def)
      then show ?case using IH by(auto)
    qed
  qed
qed

\<comment> \<open> The empty word $\lambda$ is in S\<close>
lemma lambda_in_S[simp]:
  assumes wf: " wellformedp table "
  shows "[] \<in> set (get_S table)" proof -
  from wf have *: "set (get_S table) \<noteq> {}"
          and **: "prefix_closed (set (get_S table))"
    using wellformedp_def by auto
  from * show " [] \<in> set (get_S table)" proof -
    from * obtain x where xdef: "x \<in> set (get_S table)"
      using last_in_set by auto
    then have "prefixp [] x" by(simp add: prefixp_def)
    then show ?thesis using ** prefix_closed_def xdef by auto
  qed
qed

subsection \<open> Closedness and Consistency \<close>

text \<open> Here we define predicates for the two very important notions of closedness and consistency
       of observation tables.\<close>

paragraph \<open> Definition of Closedness \<close>

text \<open> A table is closed iff for every row in S, there is a matching row in SA\<close>

text \<open> We define a function to get the matching rows in S from a row in $S \cdot A$ \<close>
definition projectToRowS :: "('a,'o) observation_table \<Rightarrow> 'a list \<Rightarrow> 'a list fset "
  where
  "projectToRowS table rSA = List.filter (rows_eq_p table rSA) (get_S table) "

lemma projectToRowS_nil:
  "projectToRowS table rSA = [] \<longleftrightarrow> (\<forall>s \<in> set (get_S table). \<not> (table \<turnstile> s \<equiv> rSA))"
  by (metis equivp_rows_eq_p equivp_symp filter_empty_conv projectToRowS_def)

text \<open> A predicate testing if a table is closed \<close>
definition closedp :: " ('l,'o) observation_table \<Rightarrow> bool " where
  " closedp table = (\<forall>x \<in> (set (get_SA table)). \<exists>y \<in> (set (get_S table)).
   table \<turnstile> x \<equiv> y)"

lemma closedpE: "closedp table \<Longrightarrow> x \<in> set (get_SA table)
  \<Longrightarrow> \<exists>y \<in> set (get_S table). table \<turnstile> x \<equiv> y"
  using closedp_def by auto

paragraph \<open> Definition of Consistency \<close>

text \<open> A table is consistent, iff appending letters to two matching rows result in two matching
       rows. \<close>

text \<open> A predicate testing if a table is consistent \<close>
definition consistentp :: " ('l,'o) observation_table \<Rightarrow> bool " where
   "consistentp table =
    (\<forall>r1 \<in> (set (get_S table)). \<forall>r2 \<in> (set (get_S table)).
     \<forall>a \<in> (set (get_A table)).
      table \<turnstile> r1 \<equiv> r2 \<longrightarrow> table \<turnstile> (r1@[a]) \<equiv> (r2@[a])) "

lemma consistentpE[simp]:
  "\<lbrakk>consistentp table; r1 \<in> set (get_S table); r2 \<in> set (get_S table);
    a \<in> set (get_A table); table \<turnstile> r1 \<equiv> r2\<rbrakk> \<Longrightarrow> 
     table \<turnstile> (r1@[a]) \<equiv> (r2@[a])"
  using consistentp_def by auto

subsection \<open> Getting Closedness and Consistency Problems \<close>
text \<open> For the main algorithm's step, we'll need to "fix" the consistency and closedness problems.
       To do this, we require the problematic rows. We define functions to obtain them. \<close>

paragraph \<open> Closedness problems\<close>
\<comment> \<open> A closedness problem is simply a rowSA which cannot be projected to a rowS. We store the
     r and the a in a pair. \<close>
type_synonym 'l closedness_prob = "('l list) \<times> 'l"

\<comment> \<open> Returns some closedness-problem iif the table is not closed and none otherwise. \<close>
definition get_closedness_prob :: "('l,'o) observation_table \<Rightarrow> 'l closedness_prob option"
  where
  " get_closedness_prob table =
     safe_head [(butlast x, last x). x <- get_SA table, projectToRowS table x = [] ] "

lemma get_closedness_prob_sound: "closedp table \<Longrightarrow> get_closedness_prob table = None"
proof -
  assume "closedp table"
  from this and projectToRowS_nil have
    "\<forall>s \<in> set (get_SA table). projectToRowS table s \<noteq> []"
    by (simp add: closedpE filter_empty_conv projectToRowS_def)
  then have "[(butlast x, last x). x <- get_SA table, projectToRowS table x = [] ] = []"
    by(auto)
  then show "get_closedness_prob table = None"
    by(auto simp add: get_closedness_prob_def)
qed

lemma get_closedness_prob_correct:
  assumes wformed: "wellformedp table"
    and getCP_some: "get_closedness_prob table = Some (s,a)"
  shows "(\<forall>x \<in> set (get_S table). \<not> (table \<turnstile> s@[a] \<equiv> x))"
    and "s \<in> set (get_S table)" and "a \<in> set (get_A table)" 
proof -
  from getCP_some have
    "hd [(butlast x, last x). x <- get_SA table, projectToRowS table x = [] ] = (s,a)"
    unfolding get_closedness_prob_def by(simp)
  then have "(s,a) \<in> set [(butlast x, last x). x <- get_SA table, projectToRowS table x = [] ]"
    by (metis (mono_tags, lifting)\<open>get_closedness_prob table = Some (s, a)\<close>
        get_closedness_prob_def hd_in_set safe_head_Some)
  then have
    "\<exists>x \<in> set (get_SA table). projectToRowS table x = [] \<and> (s,a) = (butlast x, last x)"
    by(auto)
  then obtain x where "x \<in> set (get_SA table)" and "projectToRowS table x = []"
    and "(s,a) = (butlast x, last x)" by(auto)
  then have "s = butlast x" and "a = last x" by(auto)
  from wformed and \<open>x \<in> set (get_SA table)\<close> have "x \<noteq> []"
    using \<open>projectToRowS table x = []\<close> lambda_in_S projectToRowS_nil
          rows_eq_p_def by blast
  then have "x = s@[a]" by (simp add: \<open>a = last x\<close> \<open>s = butlast x\<close> \<open>x \<noteq> []\<close>)
  then have "projectToRowS table (s@[a]) = []"
    using \<open>projectToRowS table x = []\<close> by auto
  then show "(\<forall>x \<in> set (get_S table). \<not> (table \<turnstile> s@[a] \<equiv> x))"
    by (simp add: filter_empty_conv projectToRowS_def)
  from \<open>x = s@[a]\<close> and \<open>x \<in> set (get_SA table)\<close> have "s@[a] \<in> set (row_labels table)"
    by auto
  then show "s \<in> set (get_S table)" and "a \<in> set (get_A table)"
    by(auto intro: wellformedpE[OF wformed])
qed

lemma get_closedness_prob_complete:
  "\<not>closedp table \<Longrightarrow> \<exists>x. get_closedness_prob table = Some x"
proof -
  assume "\<not>closedp table"
  then have "(\<exists>x \<in> (set (get_SA table)). \<forall>y \<in> (set (get_S table)). \<not>(table \<turnstile> x \<equiv> y))"
    by(simp add: closedp_def)
  then obtain x where defx0: "x \<in> (set (get_SA table))"
    and defx1: "\<forall>y \<in> (set (get_S table)). \<not>(table \<turnstile> x \<equiv> y)" by(blast)
  then have "projectToRowS table x = []"
    by (meson equivp_rows_eq_p equivp_symp projectToRowS_nil)
  from this and defx0 have
    "(butlast x, last x) \<in> set [(butlast x, last x). x <- get_SA table, projectToRowS table x = [] ]"
    by(auto)
  then show ?thesis by(auto simp add: get_closedness_prob_def)
qed

paragraph \<open> Consistency problems \<close>

text \<open> A consistency problem is a pair of rowsS, and a and an e such that the rowsSA obtained by
     appending "a" differ on extension "e". \<close>
type_synonym 'l consistency_prob = "'l list \<times> 'l list \<times> 'l \<times> 'l list"

\<comment> \<open> Returns some consistency-problem iif the table is not consistent and none otherwise. \<close>
definition get_consistency_prob :: "('l,'o) observation_table \<Rightarrow> 'l consistency_prob option" where
  " get_consistency_prob table = safe_head 
    [(s1,s2,a,e).  s1 <- (get_S table), s2 <- (get_S table), a <- (get_A table), e <- (get_E table), rows_eq_p table s1 s2 \<and> (get_T table) (s1@[a]@e) \<noteq> (get_T table) (s2@[a]@e)] "

lemma get_consistency_prob_correct:
  assumes wformed: "wellformedp table"
    and getCP_some: "get_consistency_prob table = Some (s1,s2,a,e)"
  shows "table \<turnstile> s1 \<equiv> s2" and "\<not>(table \<turnstile> (s1@[a]) \<equiv> (s2@[a]))"
    and "(get_T) table (s1@[a]@e) \<noteq> get_T table (s2@[a]@e)"
    and "s1 \<in> set (get_S table)" and "s2 \<in> set (get_S table)"
    and "a \<in> set (get_A table)" and "e \<in> set (get_E table)"
  using assms
proof -
  from getCP_some have "hd [(s1,s2,a,e).  s1 <- (get_S table), s2 <- (get_S table), a <- (get_A table), 
     e <- (get_E table), rows_eq_p table s1 s2 \<and> (get_T table) (s1@[a]@e) \<noteq> (get_T table) (s2@[a]@e)] = (s1,s2,a,e)"
    (is "hd ?biglist = (s1,s2,a,e)")
    unfolding get_consistency_prob_def by(simp)
  moreover from getCP_some have "?biglist \<noteq> []" using safe_head_Some by(auto simp add: get_consistency_prob_def)
  ultimately have "(s1,s2,a,e) \<in> set ?biglist"
    by (metis (no_types, lifting) hd_in_set)
  then have "\<exists>os1 \<in> set (get_S table). \<exists>os2 \<in> set (get_S table). \<exists>oa \<in> set (get_A table).
     \<exists>oe \<in> set (get_E table). rows_eq_p table os1 os2 \<and> (get_T table) (os1@[oa]@oe) \<noteq> (get_T table) (os2@[oa]@oe)
     \<and> (s1,s2,a,e) = (os1,os2,oa,oe)" by(auto)
  then obtain os1 os2 oa oe where "os1 \<in> set (get_S table)" and "os2 \<in> set (get_S table)" and
                              "oa \<in> set (get_A table)" and "oe \<in> set (get_E table)" and
                              "rows_eq_p table os1 os2" and "(get_T table) (os1@[oa]@oe) \<noteq> (get_T table) (os2@[oa]@oe)"
                              and "(s1,s2,a,e) = (os1,os2,oa,oe)"
    by(blast)
  then have s1def: "s1 \<in> set (get_S table)" and s2def: "s2 \<in> set (get_S table)" and
                              adef: "a \<in> set (get_A table)" and edef: "e \<in> set (get_E table)" and
                              *: "rows_eq_p table s1 s2" and **: "(get_T table) (s1@[a]@e) \<noteq> (get_T table) (s2@[a]@e)"
    using \<open>(s1,s2,a,e) = (os1,os2,oa,oe)\<close> by(auto)
    then show 
      "table \<turnstile> s1 \<equiv> s2"
      "\<not> table \<turnstile> s1 @ [a] \<equiv> s2 @ [a]"
      "get_T table (s1 @ [a] @ e) \<noteq> get_T table (s2 @ [a] @ e)"
      "s1 \<in> set (get_S table)"
      "s2 \<in> set (get_S table)"
      "a \<in> set (get_A table)"
      "e \<in> set (get_E table)"
      by(auto simp add: rows_eq_p_def)
  qed

lemma get_consistency_prob_sound_converse:
  "\<exists>x. get_consistency_prob table = Some x \<Longrightarrow> \<not> (consistentp table)"
proof
  assume "\<exists>x. get_consistency_prob table = Some x" and "consistentp table"
  then obtain s1 s2 a e where xdef: "get_consistency_prob table = Some ((s1,s2,a,e))" by(auto)
  from xdef have *: "((s1,s2,a,e)) \<in> set [(s1,s2,a,e).  s1 <- (get_S table), s2 <- (get_S table), a <- (get_A table),
                     e <- (get_E table), rows_eq_p table s1 s2 \<and> (get_T table) (s1@[a]@e) \<noteq> (get_T table) (s2@[a]@e)]"
        (is "((s1,s2,a,e)) \<in> set ?lcomp") proof -
    from \<open>get_consistency_prob table = Some ((s1,s2,a,e))\<close> have "safe_head ?lcomp = Some ((s1,s2,a,e))"
      by(simp only: get_consistency_prob_def)
    then show ?thesis by (metis (no_types, lifting) hd_Cons_tl list.set_intros(1) safe_head_Some)
  qed
  from * have "e \<in> set (get_E table)" and "(get_T table) (s1@[a]@e) \<noteq> (get_T table) (s2@[a]@e)" 
    by(auto)
  then have "\<not> rows_eq_p table (s1@[a]) (s2@[a])" by(auto simp add: rows_eq_p_def)
  moreover from * have "s1 \<in> set (get_S table)" and  "s2 \<in> set (get_S table)" and 
            "a \<in> set (get_A table)" and "e \<in> set (get_E table)" and
            "rows_eq_p table s1 s2" by(auto)
  ultimately show "False" using \<open>consistentp table\<close> by (meson consistentp_def)
qed

lemma get_consistency_prob_sound: "(consistentp table) \<Longrightarrow> get_consistency_prob table = None"
  using get_consistency_prob_sound_converse
  by (metis option.exhaust)

lemma get_consistency_prob_complete_converse:
  "get_consistency_prob table = None \<Longrightarrow> consistentp table"
proof -
  assume "get_consistency_prob table = None"
  then have "set [(s1,s2,a,e).  s1 <- (get_S table), s2 <- (get_S table), a <- (get_A table), e <- (get_E table),
      rows_eq_p table s1 s2 \<and> (get_T table) (s1@[a]@e) \<noteq> (get_T table) (s2@[a]@e)] = {}"
    (is "(set ?lcomp) = {}") proof -
    from \<open>get_consistency_prob table = None\<close> have "safe_head ?lcomp = None"
      by(simp only: get_consistency_prob_def)
    then show ?thesis
      by (metis (no_types, lifting) list.set(1) safe_head_None)
  qed
  show ?thesis proof(rule ccontr)
    assume "\<not> consistentp table"
    then have "\<exists>r1 \<in> (set (get_S table)). \<exists>r2 \<in> (set (get_S table)).
               \<exists>a \<in> (set (get_A table)).
        rows_eq_p table r1 r2 \<and> \<not> rows_eq_p table (r1@[a]) (r2@[a])"
      using consistentp_def by blast
    then obtain r1 r2 a where f1: "r1 \<in> (set (get_S table))"
                          and f2: "r2 \<in> (set (get_S table))"
                          and f3: "a \<in> (set (get_A table))"
      and f4: "rows_eq_p table r1 r2 \<and> \<not> rows_eq_p table (r1@[a]) (r2@[a])"
      by(auto)
    then have "\<exists>e \<in> (set (get_E table)). (get_T table) (r1@[a]@e) \<noteq> (get_T table) (r2@[a]@e)"
      by(simp add: rows_eq_p_def)
    then obtain e where f5: "e \<in> (set (get_E table))"
                    and f6: "(get_T table) (r1@[a]@e) \<noteq> (get_T table) (r2@[a]@e) "
      by(auto)
    from f4 f6 have "rows_eq_p table r1 r2 \<and> (get_T table) (r1@[a]@e) \<noteq> (get_T table) (r2@[a]@e) "
      by blast
    from f1 f2 f3 f5 and this have "(r1,r2,a,e) \<in> set ?lcomp"
      by auto
    from this and \<open>(set ?lcomp) = {}\<close> show "False"
      by auto
  qed
qed

lemma get_consistency_prob_complete:
  "\<not>consistentp table \<Longrightarrow> \<exists>x. get_consistency_prob table = Some x"
  using get_consistency_prob_complete_converse
  by (metis option.exhaust)

end (* End of ObservationTableMoore.thy *)
