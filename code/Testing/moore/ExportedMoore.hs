{-# LANGUAGE RankNTypes #-}
-- Note: When exporting from ISABELLE we'll need to remove the finite
-- list after the module declaration

module
  ExportedMoore where {

import Prelude ((==), (/=), (<), (<=), (>=), (>), (+), (-), (*), (/), (**),
  (>>=), (>>), (=<<), (&&), (||), (^), (^^), (.), ($), ($!), (++), (!!), Eq,
  error, id, return, not, fst, snd, map, filter, concat, concatMap, reverse,
  zip, null, takeWhile, dropWhile, all, any, Integer, negate, abs, divMod,
  String, Bool(True, False), Maybe(Nothing, Just));
import qualified Prelude;

data Nat = Zero_nat | Suc Nat;

data LogType = Log Nat Nat Nat;

data Moore_machine_ext a b c d =
  Moore_machine_ext [a] [c] [b] b (a -> b -> b) (b -> c) d;

data Observation_table_ext a b c =
  Observation_table_ext [a] [b] [[a]] [[a]] [[a]] ([a] -> Maybe b) c;

find :: forall a. (a -> Bool) -> [a] -> Maybe a;
find uu [] = Nothing;
find p (x : xs) = (if p x then Just x else find p xs);

last :: forall a. [a] -> a;
last (x : xs) = (if null xs then x else last xs);

map_of :: forall a b. (Eq a) => [(a, b)] -> a -> Maybe b;
map_of ((l, v) : ps) k = (if l == k then Just v else map_of ps k);
map_of [] k = Nothing;

member :: forall a. (Eq a) => [a] -> a -> Bool;
member [] y = False;
member (x : xs) y = x == y || member xs y;

map_add :: forall a b. (a -> Maybe b) -> (a -> Maybe b) -> a -> Maybe b;
map_add m1 m2 = (\ x -> (case m2 x of {
                          Nothing -> m1 x;
                          Just a -> Just a;
                        }));

butlast :: forall a. [a] -> [a];
butlast [] = [];
butlast (x : xs) = (if null xs then [] else x : butlast xs);

remdups :: forall a. (Eq a) => [a] -> [a];
remdups [] = [];
remdups (x : xs) = (if member xs x then remdups xs else x : remdups xs);

map_upds :: forall a b. (Eq a) => (a -> Maybe b) -> [a] -> [b] -> a -> Maybe b;
map_upds m xs ys = map_add m (map_of (reverse (zip xs ys)));

is_none :: forall a. Maybe a -> Bool;
is_none (Just x) = False;
is_none Nothing = True;

listSome :: forall a. (a -> Bool) -> [a] -> Bool;
listSome p [] = False;
listSome p (x : xs) = p x || listSome p xs;

nubBy :: forall a. (a -> a -> Bool) -> [a] -> [a];
nubBy p [] = [];
nubBy p (x : xs) = (if listSome (p x) xs then nubBy p xs else x : nubBy p xs);

the :: forall a. Maybe a -> a;
the (Just x2) = x2;

safeHead :: forall a. [a] -> Maybe a;
safeHead [] = Nothing;
safeHead (x : xs) = Just x;

fset_union :: forall a. (Eq a) => [a] -> [a] -> [a];
fset_union [] ys = ys;
fset_union (x : xs) ys = let {
                           result = fset_union xs ys;
                         } in (if member result x then result else x : result);

get_SA :: forall a b c. Observation_table_ext a b c -> [[a]];
get_SA (Observation_table_ext get_A get_O get_S get_SA get_E get_T more) =
  get_SA;

get_S :: forall a b c. Observation_table_ext a b c -> [[a]];
get_S (Observation_table_ext get_A get_O get_S get_SA get_E get_T more) = get_S;

get_T :: forall a b c. Observation_table_ext a b c -> [a] -> Maybe b;
get_T (Observation_table_ext get_A get_O get_S get_SA get_E get_T more) = get_T;

get_E :: forall a b c. Observation_table_ext a b c -> [[a]];
get_E (Observation_table_ext get_A get_O get_S get_SA get_E get_T more) = get_E;

rowsEqP ::
  forall a b. (Eq b) => Observation_table_ext a b () -> [a] -> [a] -> Bool;
rowsEqP table r1 r2 =
  all (\ x -> get_T table (r1 ++ x) == get_T table (r2 ++ x)) (get_E table);

closedP :: forall a b. (Eq b) => Observation_table_ext a b () -> Bool;
closedP table = all (\ x -> any (rowsEqP table x) (get_S table)) (get_SA table);

while_option :: forall a. (a -> Bool) -> (a -> a) -> a -> Maybe a;
while_option b c s = (if b s then while_option b c (c s) else Just s);

get_A :: forall a b c. Observation_table_ext a b c -> [a];
get_A (Observation_table_ext get_A get_O get_S get_SA get_E get_T more) = get_A;

consistentP :: forall a b. (Eq b) => Observation_table_ext a b () -> Bool;
consistentP table =
  all (\ r1 ->
        all (\ r2 ->
              all (\ a ->
                    (if rowsEqP table r1 r2
                      then rowsEqP table (r1 ++ [a]) (r2 ++ [a]) else True))
                (get_A table))
          (get_S table))
    (get_S table);

projectToRowS ::
  forall a b. (Eq b) => Observation_table_ext a b () -> [a] -> [[a]];
projectToRowS table rSA = filter (rowsEqP table rSA) (get_S table);

extendE_queries ::
  forall a b. (Eq a) => Observation_table_ext a b () -> [[a]] -> [[a]];
extendE_queries tbl updE =
  concatMap (\ s -> map (\ a -> s ++ a) updE)
    (fset_union (get_S tbl) (get_SA tbl));

safe_lookup :: forall a b. (a -> Maybe b) -> (a -> b) -> a -> b;
safe_lookup map_f safe_f q =
  (if not (is_none (map_f q)) then the (map_f q) else safe_f q);

extendE_answers_optimized ::
  forall a b.
    (Eq a) => Observation_table_ext a b () -> ([a] -> b) -> [[a]] -> [b];
extendE_answers_optimized tbl orc updE =
  map (safe_lookup (get_T tbl) orc) (extendE_queries tbl updE);

get_O :: forall a b c. Observation_table_ext a b c -> [b];
get_O (Observation_table_ext get_A get_O get_S get_SA get_E get_T more) = get_O;

extendE ::
  forall a b.
    (Eq a) => ([a] -> b) ->
                Observation_table_ext a b () ->
                  [[a]] -> Observation_table_ext a b ();
extendE orc tbl updE =
  let {
    newE = fset_union (get_E tbl) updE;
    queries = extendE_queries tbl updE;
    answers = extendE_answers_optimized tbl orc updE;
  } in Observation_table_ext (get_A tbl) (get_O tbl) (get_S tbl) (get_SA tbl)
         newE (map_upds (get_T tbl) queries answers) ();

extendS_queries ::
  forall a b. (Eq a) => Observation_table_ext a b () -> [[a]] -> [[a]];
extendS_queries tbl updS =
  fset_union (concatMap (\ s -> map (\ a -> s ++ a) (get_E tbl)) updS)
    (concatMap
      (\ s ->
        concatMap (\ a -> map (\ e -> s ++ [a] ++ e) (get_E tbl)) (get_A tbl))
      updS);

extendS_answers_optimized ::
  forall a b.
    (Eq a) => Observation_table_ext a b () -> ([a] -> b) -> [[a]] -> [b];
extendS_answers_optimized tbl orc updS =
  map (safe_lookup (get_T tbl) orc) (extendS_queries tbl updS);

extendS ::
  forall a b.
    (Eq a) => ([a] -> b) ->
                Observation_table_ext a b () ->
                  [[a]] -> Observation_table_ext a b ();
extendS orc tbl updS =
  let {
    newS = fset_union (get_S tbl) updS;
    queries = extendS_queries tbl updS;
    answers = extendS_answers_optimized tbl orc updS;
  } in Observation_table_ext (get_A tbl) (get_O tbl) newS
         (remdups (concatMap (\ s -> map (\ a -> s ++ [a]) (get_A tbl)) newS))
         (get_E tbl) (map_upds (get_T tbl) queries answers) ();

getClosednessProblem ::
  forall a b. (Eq b) => Observation_table_ext a b () -> Maybe ([a], a);
getClosednessProblem table =
  safeHead
    (concatMap
      (\ x ->
        (if null (projectToRowS table x) then [(butlast x, last x)] else []))
      (get_SA table));

incr_lece :: LogType -> LogType;
incr_lece (Log a b c) = Log a b (Suc c);

incr_mkcl :: LogType -> LogType;
incr_mkcl (Log a b c) = Log (Suc a) b c;

incr_mkco :: LogType -> LogType;
incr_mkco (Log a b c) = Log a (Suc b) c;

stateRepsOf :: forall a b. (Eq b) => Observation_table_ext a b () -> [[a]];
stateRepsOf table = nubBy (rowsEqP table) (get_S table ++ [[]]);

rowToState ::
  forall a b. (Eq b) => Observation_table_ext a b () -> [a] -> Maybe [a];
rowToState table r = find (rowsEqP table r) (stateRepsOf table);

getConsistencyProblem ::
  forall a b.
    (Eq b) => Observation_table_ext a b () -> Maybe ([a], ([a], (a, [a])));
getConsistencyProblem table =
  safeHead
    (concatMap
      (\ s1 ->
        concatMap
          (\ s2 ->
            concatMap
              (\ a ->
                concatMap
                  (\ e ->
                    (if rowsEqP table s1 s2 &&
                          not (get_T table (s1 ++ [a] ++ e) ==
                                get_T table (s2 ++ [a] ++ e))
                      then [(s1, (s2, (a, e)))] else []))
                  (get_E table))
              (get_A table))
          (get_S table))
      (get_S table));

fixClosednessProblem ::
  forall a b.
    (Eq a) => ([a] -> b) ->
                ([a], a) ->
                  Observation_table_ext a b () -> Observation_table_ext a b ();
fixClosednessProblem orc (s, a) tbl = extendS orc tbl [s ++ [a]];

makeClosed ::
  forall a b.
    (Eq a,
      Eq b) => ([a] -> b) ->
                 Observation_table_ext a b () -> Observation_table_ext a b ();
makeClosed orc tbl = let {
                       problem = getClosednessProblem tbl;
                     } in fixClosednessProblem orc (the problem) tbl;

make_finalf :: forall a b. Observation_table_ext a b () -> [a] -> b;
make_finalf table s = the (get_T table s);

allPrefixes :: forall a. [a] -> [[a]];
allPrefixes [] = [[]];
allPrefixes (v : va) = [v : va] ++ allPrefixes (butlast (v : va));

make_initial :: forall a b. Observation_table_ext a b () -> [a];
make_initial table = [];

makeInitialTable ::
  forall a b.
    (Eq a) => [a] -> [b] -> ([a] -> b) -> Observation_table_ext a b ();
makeInitialTable alph out_alph orc =
  let {
    queries = [[]] ++ map (\ a -> [a]) alph;
    answers = map orc queries;
  } in Observation_table_ext alph out_alph [[]]
         (remdups (map (\ a -> [a]) alph)) [[]]
         (map_upds (\ _ -> Nothing) queries answers) ();

fixConsistencyProblem ::
  forall a b.
    (Eq a) => ([a] -> b) ->
                ([a], ([a], (a, [a]))) ->
                  Observation_table_ext a b () -> Observation_table_ext a b ();
fixConsistencyProblem orc (s1, (s2, (a, e))) tbl = extendE orc tbl [a : e];

makeConsistent ::
  forall a b.
    (Eq a,
      Eq b) => ([a] -> b) ->
                 Observation_table_ext a b () -> Observation_table_ext a b ();
makeConsistent orc tbl = let {
                           problem = getConsistencyProblem tbl;
                         } in fixConsistencyProblem orc (the problem) tbl;

makeClosedAndConsistentStep ::
  forall a b.
    (Eq a,
      Eq b) => ([a] -> b) ->
                 (Observation_table_ext a b (), LogType) ->
                   (Observation_table_ext a b (), LogType);
makeClosedAndConsistentStep orc (tbl, log) =
  (if not (closedP tbl) then (makeClosed orc tbl, incr_mkcl log)
    else (if not (consistentP tbl) then (makeConsistent orc tbl, incr_mkco log)
           else (tbl, log)));

makeClosedAndConsistent ::
  forall a b.
    (Eq a,
      Eq b) => ([a] -> b) ->
                 (Observation_table_ext a b (), LogType) ->
                   Maybe (Observation_table_ext a b (), LogType);
makeClosedAndConsistent orc initIter =
  while_option (\ (tbl, _) -> not (closedP tbl) || not (consistentP tbl))
    (makeClosedAndConsistentStep orc) initIter;

learnFromCounterexample ::
  forall a b.
    (Eq a) => [a] ->
                ([a] -> b) ->
                  Observation_table_ext a b () -> Observation_table_ext a b ();
learnFromCounterexample t orc tbl = extendS orc tbl (allPrefixes t);

lStarLearnerStep ::
  forall a b.
    (Eq a,
      Eq b) => ([a] -> b) ->
                 [a] ->
                   (Observation_table_ext a b (), LogType) ->
                     (Observation_table_ext a b (), LogType);
lStarLearnerStep orcM t (tbl, log) =
  the (makeClosedAndConsistent orcM
        (learnFromCounterexample t orcM tbl, incr_lece log));

my_while_option ::
  forall a b. (a -> Maybe b) -> (Maybe b -> a -> a) -> a -> Maybe a;
my_while_option b c s =
  let {
    bs = b s;
  } in (if not (is_none bs) then my_while_option b c (c bs s) else Just s);

make_transition ::
  forall a b. (Eq b) => Observation_table_ext a b () -> a -> [a] -> [a];
make_transition table a st = the (rowToState table (st ++ [a]));

make_statespace :: forall a b. (Eq b) => Observation_table_ext a b () -> [[a]];
make_statespace table = stateRepsOf table;

makeConjecture ::
  forall a b.
    (Eq b) => Observation_table_ext a b () -> Moore_machine_ext a [a] b ();
makeConjecture table =
  Moore_machine_ext (get_A table) (get_O table) (make_statespace table)
    (make_initial table) (make_transition table) (make_finalf table) ();

lStarLearnerLoop ::
  forall a b.
    (Eq a,
      Eq b) => ([a] -> b) ->
                 (Moore_machine_ext a [a] b () -> Maybe [a]) ->
                   (Observation_table_ext a b (), LogType) ->
                     Maybe (Observation_table_ext a b (), LogType);
lStarLearnerLoop orcM orcC initIter =
  my_while_option ((orcC . makeConjecture) . fst)
    (\ val (tbl, log) -> lStarLearnerStep orcM (the val) (tbl, log))
    (the (makeClosedAndConsistent orcM initIter));

lStarLearner ::
  forall a b.
    (Eq a,
      Eq b) => [a] ->
                 [b] ->
                   ([a] -> b) ->
                     (Moore_machine_ext a [a] b () -> Maybe [a]) ->
                       Moore_machine_ext a [a] b ();
lStarLearner alph out_alph orcM orcC =
  ((makeConjecture . fst) . the)
    (lStarLearnerLoop orcM orcC
      (makeInitialTable alph out_alph orcM, Log Zero_nat Zero_nat Zero_nat));

}
