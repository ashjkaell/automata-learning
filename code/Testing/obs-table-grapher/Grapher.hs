{-# LANGUAGE FlexibleInstances #-}

import qualified Data.GraphViz as G
import qualified Data.GraphViz.Attributes.Complete as G
import qualified Data.GraphViz.Types as G
import qualified Data.Text.Lazy as TL
import qualified Data.Text.Lazy.IO as TLIO
import qualified Data.List
import qualified Data.Maybe

-- Def of observation table type
-- Isabelle's code export adds this weird additional (useless ?) argument.
data Observation_table_ext a b c =
  Observation_table_ext [a] [b] [[a]] [[a]] [[a]] ([a] -> Maybe b) c;

instance Prelude.Show (Observation_table_ext Prelude.Char Prelude.Char c) where
  show (Observation_table_ext alph out_alph s sa cls f x) = let
    rls = s `Data.List.union` sa --The row labels
    psize = maximum (map length (cls ++ rls)) --maxRLSize = maximum (map length rls)
    appendBlanks int str = (take int $ repeat ' ') ++ str
    lpad size str = appendBlanks (size - length str) str
    sepSymbol = "|"
    innerSep = " " ++ sepSymbol ++ ""
    showLabel "" = "λ"
    showLabel x = x
    showColLabels cls = sepSymbol ++ (appendBlanks (psize + length innerSep - 1) $ sepSymbol ++ (Data.List.intercalate innerSep . map ((lpad psize) . showLabel)) cls)
    showRow rl = sepSymbol ++ (Data.List.intercalate innerSep $ map (lpad psize) $ (showLabel rl) : (map (\x -> maybe "?" (\v -> [v]) (f (rl++x)) ) cls)) in
      -- showColLabels cls ++ "\n" ++ unlines (map showRow rls) ++ "Set S: " ++ show s
      showColLabels cls ++ "\n" ++ unlines (map showRow s) ++ sepSymbol ++ "----\n" ++ unlines (map showRow sa) ++ "Set S: " ++ show s

testObsTable = Observation_table_ext alph out_alph s sa cls f ()
  where
    alph = "ab"
    out_alph = "012"
    s = ["","a","b","aa","bb"]
    sa = [x++[a] | x <- s, a <- alph]
    cls = ["","a", "b"]
    f x = Just (go x) where
      go x = (head . show) $ ((flip mod) 3 . length . filter (=='a')) x

-- Graph datatypes

data VLabel = VNormal | VNonClosed deriving (Show)
type V = (String, VLabel)

data ELabel = ENormal String | ENonClosed String deriving (Show)
type E = (String, String, ELabel)

type ObsTableGraph = ([V], [E])

-- Converting tables to graphs

drawLambda "" = "λ"
drawLambda x = x

--rowsEqP :: forall a b. (Eq b) => Observation_table_ext a b () -> [a] -> [a] -> Bool;
rowsEqP (Observation_table_ext alph out_alph s sa cls f _) r1 r2 = all (\x -> f (r1 ++ x) == f (r2 ++ x)) cls;

--This corresponds to the function we would use to convert an obs table to a DFA.
--The statespace is the eq. classes on S
stateRepsOnS table@(Observation_table_ext alph out_alph s sa cls f _) = Data.List.nubBy (rowsEqP table) s

--This is a supplementary function to show the "states" on SA. If the table is closed, the output of this
--function should be a subset of the previous one.
stateRepsOnSA table@(Observation_table_ext alph out_alph s sa cls f _) = Data.List.nubBy (rowsEqP table) sa

-- Given an element of SA finds the corresponding row representative in S if it exists.
findStateRepInS table saRow = Data.List.find (rowsEqP table saRow) (stateRepsOnS table)

-- To avoid having duplicated non closed nodes when jumping into unknown extension configuration
-- in the rowsSA, we pick a 'representative' for non closed transitions.
-- This should always return Just _ since we assume saRow is in SA
findStateRepInSA table saRow = Data.Maybe.fromJust $ Data.List.find (rowsEqP table saRow) (stateRepsOnSA table)

--This is a supplementary function to show the non-closed "states" on SA.
nonClosedInSA table@(Observation_table_ext alph out_alph s sa cls f _) = Data.List.filter (\x -> Data.Maybe.isNothing $ findStateRepInS table x) sa

-- Converts an observation table to a styled list of vertices (type V)
obsTableToVertices table@(Observation_table_ext _ _ s sa _ _ _) =
  let normalVerts = map (\x -> (drawLambda x, VNormal)) (stateRepsOnS table)
      nonClosedVerts = map (\x -> (drawLambda x, VNonClosed)) (nonClosedInSA table) in
    Data.List.nubBy (\x y -> fst x == fst y) $ normalVerts ++ nonClosedVerts

--Returns the list of edges coming out of a row row of the table table.
edgesOfRow table@(Observation_table_ext alph _ _ _ _ _ _) row = map edgeTo alph
  where edgeTo a = let
          rowsa = row++[a]
          rowSMaybe = findStateRepInS table rowsa in
            Data.Maybe.maybe (drawLambda row, (drawLambda . findStateRepInSA table) rowsa, ENonClosed [a]) (\rowS -> (drawLambda row, drawLambda rowS, ENormal [a])) rowSMaybe
                      

obsTableToEdges table = concatMap (edgesOfRow table) (stateRepsOnS table)

-- Graph plotting part

v1 = obsTableToVertices testObsTable
e1 = obsTableToEdges testObsTable
g1 = (v1,e1)

-- GraphVisParams vertexType vertexLabeltype edgeLabelType clusterType clusterLabelType
fileGraphParams :: G.GraphvizParams String VLabel ELabel () VLabel
fileGraphParams = G.defaultParams {
    G.fmtNode = (\(v, vl) -> case vl of
                VNormal       -> [colorAttribute $ G.RGB 0 0 0, G.Shape G.Circle]
                VNonClosed    -> [colorAttribute $ G.RGB 255 0 0 , G.Shape G.Circle, G.Style [G.SItem G.Dashed []]])
  , G.fmtEdge = (\(from, to, etype) -> case etype of
                ENormal el -> [colorAttribute $ G.RGB 0 0 0, G.toLabel el]
                ENonClosed el -> [colorAttribute $ G.RGB 255 0 0, G.toLabel el, G.Style [G.SItem G.Dashed []]])
      }
  where
    colorAttribute color = G.Color $ G.toColorList [ color ]

main :: IO ()
main = do
  putStrLn $ show testObsTable
  -- 1. Create our application-specific graph
  let (vs, es) = g1
  -- 2. Convert it into a DotGraph
  let dotGraph = G.graphElemsToDot fileGraphParams vs es :: G.DotGraph String
  -- 3. Render it into .dot text
      dotText = G.printDotGraph dotGraph :: TL.Text
  -- 4. Write the contents to a file
  TLIO.writeFile "obsTable.dot" dotText
