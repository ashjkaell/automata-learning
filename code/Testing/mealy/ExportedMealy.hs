{-# LANGUAGE RankNTypes #-}
module
  ExportedMealy
  where {

import Prelude ((==), (/=), (<), (<=), (>=), (>), (+), (-), (*), (/), (**),
  (>>=), (>>), (=<<), (&&), (||), (^), (^^), (.), ($), ($!), (++), (!!), Eq,
  error, id, return, not, fst, snd, map, filter, concat, concatMap, reverse,
  zip, null, takeWhile, dropWhile, all, any, Integer, negate, abs, divMod,
  String, Bool(True, False), Maybe(Nothing, Just));
import qualified Prelude;

class Finite a where {
};

class (Finite a) => Enum a where {
  enum :: [a];
  enum_all :: (a -> Bool) -> Bool;
  enum_ex :: (a -> Bool) -> Bool;
};

equal_fun :: forall a b. (Enum a, Eq b) => (a -> b) -> (a -> b) -> Bool;
equal_fun f g = enum_all (\ x -> f x == g x);

instance (Enum a, Eq b) => Eq (a -> b) where {
  a == b = equal_fun a b;
};

data Nat = Zero_nat | Suc Nat;

less_eq_nat :: Nat -> Nat -> Bool;
less_eq_nat (Suc m) n = less_nat m n;
less_eq_nat Zero_nat n = True;

less_nat :: Nat -> Nat -> Bool;
less_nat m (Suc n) = less_eq_nat m n;
less_nat n Zero_nat = False;

class Ord a where {
  less_eq :: a -> a -> Bool;
  less :: a -> a -> Bool;
};

instance Ord Nat where {
  less_eq = less_eq_nat;
  less = less_nat;
};

class (Ord a) => Preorder a where {
};

class (Preorder a) => Order a where {
};

instance Preorder Nat where {
};

instance Order Nat where {
};

class (Order a) => Linorder a where {
};

instance Linorder Nat where {
};

data Set a = Set [a] | Coset [a];

data Mealy_machine_ext a b c d =
  Mealy_machine_ext [a] [b] b (a -> b -> b) (b -> a -> Maybe c) d;

data Moore_machine_ext a b c d =
  Moore_machine_ext [a] [b] b (a -> b -> b) (b -> c) d;

data Log_type_ext a = Log_type_ext Nat Nat Nat Nat Nat Nat Nat a;

data Observation_table_ext a b c =
  Observation_table_ext [a] [[a]] [[a]] [[a]] ([a] -> Maybe b) c;

find :: forall a. (a -> Bool) -> [a] -> Maybe a;
find uu [] = Nothing;
find p (x : xs) = (if p x then Just x else find p xs);

fold :: forall a b. (a -> b -> b) -> [a] -> b -> b;
fold f (x : xs) s = fold f xs (f x s);
fold f [] s = s;

last :: forall a. [a] -> a;
last (x : xs) = (if null xs then x else last xs);

image :: forall a b. (a -> b) -> Set a -> Set b;
image f (Set xs) = Set (map f xs);

map_of :: forall a b. (Eq a) => [(a, b)] -> a -> Maybe b;
map_of ((l, v) : ps) k = (if l == k then Just v else map_of ps k);
map_of [] k = Nothing;

member :: forall a. (Eq a) => [a] -> a -> Bool;
member [] y = False;
member (x : xs) y = x == y || member xs y;

map_add :: forall a b. (a -> Maybe b) -> (a -> Maybe b) -> a -> Maybe b;
map_add m1 m2 = (\ x -> (case m2 x of {
                          Nothing -> m1 x;
                          Just a -> Just a;
                        }));

butlast :: forall a. [a] -> [a];
butlast [] = [];
butlast (x : xs) = (if null xs then [] else x : butlast xs);

remdups :: forall a. (Eq a) => [a] -> [a];
remdups [] = [];
remdups (x : xs) = (if member xs x then remdups xs else x : remdups xs);

map_upds :: forall a b. (Eq a) => (a -> Maybe b) -> [a] -> [b] -> a -> Maybe b;
map_upds m xs ys = map_add m (map_of (reverse (zip xs ys)));

is_none :: forall a. Maybe a -> Bool;
is_none (Just x) = False;
is_none Nothing = True;

gen_length :: forall a. Nat -> [a] -> Nat;
gen_length n (x : xs) = gen_length (Suc n) xs;
gen_length n [] = n;

list_some :: forall a. (a -> Bool) -> [a] -> Bool;
list_some p [] = False;
list_some p (x : xs) = p x || list_some p xs;

nubby :: forall a. (a -> a -> Bool) -> [a] -> [a];
nubby p [] = [];
nubby p (x : xs) = (if list_some (p x) xs then nubby p xs else x : nubby p xs);

the :: forall a. Maybe a -> a;
the (Just x2) = x2;

safe_head :: forall a. [a] -> Maybe a;
safe_head [] = Nothing;
safe_head (x : xs) = Just x;

fset_union :: forall a. (Eq a) => [a] -> [a] -> [a];
fset_union [] ys = ys;
fset_union (x : xs) ys = let {
                           result = fset_union xs ys;
                         } in (if member result x then result else x : result);

max :: forall a. (Ord a) => a -> a -> a;
max a b = (if less_eq a b then b else a);

one_nat :: Nat;
one_nat = Suc Zero_nat;

map_option :: forall a b. (a -> b) -> Maybe a -> Maybe b;
map_option f Nothing = Nothing;
map_option f (Just x2) = Just (f x2);

all_prefixes :: forall a. [a] -> [[a]];
all_prefixes [] = [[]];
all_prefixes (v : va) = [v : va] ++ all_prefixes (butlast (v : va));

get_transitiona :: forall a b c d. Moore_machine_ext a b c d -> a -> b -> b;
get_transitiona
  (Moore_machine_ext get_alphabet get_statespace get_initial get_transition
    get_finalf more)
  = get_transition;

get_statespacea :: forall a b c d. Moore_machine_ext a b c d -> [b];
get_statespacea
  (Moore_machine_ext get_alphabet get_statespace get_initial get_transition
    get_finalf more)
  = get_statespace;

get_alphabeta :: forall a b c d. Moore_machine_ext a b c d -> [a];
get_alphabeta
  (Moore_machine_ext get_alphabet get_statespace get_initial get_transition
    get_finalf more)
  = get_alphabet;

get_initiala :: forall a b c d. Moore_machine_ext a b c d -> b;
get_initiala
  (Moore_machine_ext get_alphabet get_statespace get_initial get_transition
    get_finalf more)
  = get_initial;

get_finalfa :: forall a b c d. Moore_machine_ext a b c d -> b -> c;
get_finalfa
  (Moore_machine_ext get_alphabet get_statespace get_initial get_transition
    get_finalf more)
  = get_finalf;

mealy_of ::
  forall a b c.
    Moore_machine_ext a b (a -> Maybe c) () -> Mealy_machine_ext a b c ();
mealy_of m =
  Mealy_machine_ext (get_alphabeta m) (get_statespacea m) (get_initiala m)
    (get_transitiona m) (get_finalfa m) ();

get_transition :: forall a b c d. Mealy_machine_ext a b c d -> a -> b -> b;
get_transition
  (Mealy_machine_ext get_alphabet get_statespace get_initial get_transition
    get_finalf more)
  = get_transition;

get_statespace :: forall a b c d. Mealy_machine_ext a b c d -> [b];
get_statespace
  (Mealy_machine_ext get_alphabet get_statespace get_initial get_transition
    get_finalf more)
  = get_statespace;

get_alphabet :: forall a b c d. Mealy_machine_ext a b c d -> [a];
get_alphabet
  (Mealy_machine_ext get_alphabet get_statespace get_initial get_transition
    get_finalf more)
  = get_alphabet;

get_initial :: forall a b c d. Mealy_machine_ext a b c d -> b;
get_initial
  (Mealy_machine_ext get_alphabet get_statespace get_initial get_transition
    get_finalf more)
  = get_initial;

get_finalf :: forall a b c d. Mealy_machine_ext a b c d -> b -> a -> Maybe c;
get_finalf
  (Mealy_machine_ext get_alphabet get_statespace get_initial get_transition
    get_finalf more)
  = get_finalf;

moore_of ::
  forall a b c.
    Mealy_machine_ext a b c () -> Moore_machine_ext a b (a -> Maybe c) ();
moore_of m =
  Moore_machine_ext (get_alphabet m) (get_statespace m) (get_initial m)
    (get_transition m) (get_finalf m) ();

size_list :: forall a. [a] -> Nat;
size_list = gen_length Zero_nat;

get_SA :: forall a b c. Observation_table_ext a b c -> [[a]];
get_SA (Observation_table_ext get_A get_S get_SA get_E get_T more) = get_SA;

get_S :: forall a b c. Observation_table_ext a b c -> [[a]];
get_S (Observation_table_ext get_A get_S get_SA get_E get_T more) = get_S;

get_T :: forall a b c. Observation_table_ext a b c -> [a] -> Maybe b;
get_T (Observation_table_ext get_A get_S get_SA get_E get_T more) = get_T;

get_E :: forall a b c. Observation_table_ext a b c -> [[a]];
get_E (Observation_table_ext get_A get_S get_SA get_E get_T more) = get_E;

rows_eq_p ::
  forall a b. (Eq b) => Observation_table_ext a b () -> [a] -> [a] -> Bool;
rows_eq_p table r1 r2 =
  all (\ x -> get_T table (r1 ++ x) == get_T table (r2 ++ x)) (get_E table);

closedp :: forall a b. (Eq b) => Observation_table_ext a b () -> Bool;
closedp table =
  all (\ x -> any (rows_eq_p table x) (get_S table)) (get_SA table);

while_option :: forall a. (a -> Bool) -> (a -> a) -> a -> Maybe a;
while_option b c s = (if b s then while_option b c (c s) else Just s);

maxa :: forall a. (Linorder a) => Set a -> a;
maxa (Set (x : xs)) = fold max xs x;

get_A :: forall a b c. Observation_table_ext a b c -> [a];
get_A (Observation_table_ext get_A get_S get_SA get_E get_T more) = get_A;

consistentp :: forall a b. (Eq b) => Observation_table_ext a b () -> Bool;
consistentp table =
  all (\ r1 ->
        all (\ r2 ->
              all (\ a ->
                    (if rows_eq_p table r1 r2
                      then rows_eq_p table (r1 ++ [a]) (r2 ++ [a]) else True))
                (get_A table))
          (get_S table))
    (get_S table);

mealy_vector_of :: forall a b. (Eq a) => [a] -> (a -> b) -> a -> Maybe b;
mealy_vector_of a f = map_upds (\ _ -> Nothing) a (map f a);

projectToRowS ::
  forall a b. (Eq b) => Observation_table_ext a b () -> [a] -> [[a]];
projectToRowS table rSA = filter (rows_eq_p table rSA) (get_S table);

moore_oraclec_of ::
  forall a b.
    (Mealy_machine_ext a [a] b () -> Maybe [a]) ->
      Moore_machine_ext a [a] (a -> Maybe b) () -> Maybe [a];
moore_oraclec_of mealy_orc = (map_option butlast . mealy_orc) . mealy_of;

moore_oraclem_of ::
  forall a b. (Eq a) => [a] -> ([a] -> b) -> [a] -> a -> Maybe b;
moore_oraclem_of a mealy_orc s =
  mealy_vector_of a (\ aa -> mealy_orc (s ++ [aa]));

extendE_queries ::
  forall a b. (Eq a) => Observation_table_ext a b () -> [[a]] -> [[a]];
extendE_queries tbl updE =
  concatMap (\ s -> map (\ a -> s ++ a) updE)
    (fset_union (get_S tbl) (get_SA tbl));

safe_lookup :: forall a b. (a -> Maybe b) -> (a -> b) -> a -> b;
safe_lookup map_f safe_f q =
  (if not (is_none (map_f q)) then the (map_f q) else safe_f q);

extendE_answers_optimized ::
  forall a b.
    (Eq a) => Observation_table_ext a b () -> ([a] -> b) -> [[a]] -> [b];
extendE_answers_optimized tbl orc updE =
  map (safe_lookup (get_T tbl) orc) (extendE_queries tbl updE);

extendE ::
  forall a b.
    (Eq a) => ([a] -> b) ->
                Observation_table_ext a b () ->
                  [[a]] -> Observation_table_ext a b ();
extendE orc tbl updE =
  let {
    newE = fset_union (get_E tbl) updE;
    queries = extendE_queries tbl updE;
    answers = extendE_answers_optimized tbl orc updE;
  } in Observation_table_ext (get_A tbl) (get_S tbl) (get_SA tbl) newE
         (map_upds (get_T tbl) queries answers) ();

extendS_queries ::
  forall a b. (Eq a) => Observation_table_ext a b () -> [[a]] -> [[a]];
extendS_queries tbl updS =
  fset_union (concatMap (\ s -> map (\ a -> s ++ a) (get_E tbl)) updS)
    (concatMap
      (\ s ->
        concatMap (\ a -> map (\ e -> s ++ [a] ++ e) (get_E tbl)) (get_A tbl))
      updS);

extendS_answers_optimized ::
  forall a b.
    (Eq a) => Observation_table_ext a b () -> ([a] -> b) -> [[a]] -> [b];
extendS_answers_optimized tbl orc updS =
  map (safe_lookup (get_T tbl) orc) (extendS_queries tbl updS);

extendS ::
  forall a b.
    (Eq a) => ([a] -> b) ->
                Observation_table_ext a b () ->
                  [[a]] -> Observation_table_ext a b ();
extendS orc tbl updS =
  let {
    newS = fset_union (get_S tbl) updS;
    queries = extendS_queries tbl updS;
    answers = extendS_answers_optimized tbl orc updS;
  } in Observation_table_ext (get_A tbl) newS
         (remdups (concatMap (\ s -> map (\ a -> s ++ [a]) (get_A tbl)) newS))
         (get_E tbl) (map_upds (get_T tbl) queries answers) ();

fix_consistency_prob ::
  forall a b.
    (Eq a) => ([a] -> b) ->
                ([a], ([a], (a, [a]))) ->
                  Observation_table_ext a b () -> Observation_table_ext a b ();
fix_consistency_prob orc (s1, (s2, (a, e))) tbl = extendE orc tbl [a : e];

get_consistency_prob ::
  forall a b.
    (Eq b) => Observation_table_ext a b () -> Maybe ([a], ([a], (a, [a])));
get_consistency_prob table =
  safe_head
    (concatMap
      (\ s1 ->
        concatMap
          (\ s2 ->
            concatMap
              (\ a ->
                concatMap
                  (\ e ->
                    (if rows_eq_p table s1 s2 &&
                          not (get_T table (s1 ++ [a] ++ e) ==
                                get_T table (s2 ++ [a] ++ e))
                      then [(s1, (s2, (a, e)))] else []))
                  (get_E table))
              (get_A table))
          (get_S table))
      (get_S table));

make_consistent ::
  forall a b.
    (Eq a,
      Eq b) => ([a] -> b) ->
                 Observation_table_ext a b () -> Observation_table_ext a b ();
make_consistent orc tbl = let {
                            problem = get_consistency_prob tbl;
                          } in fix_consistency_prob orc (the problem) tbl;

get_mkco_update :: forall a. (Nat -> Nat) -> Log_type_ext a -> Log_type_ext a;
get_mkco_update get_mkcoa
  (Log_type_ext get_mkcl get_mkco get_lece get_max_len_S get_max_len_E
    get_max_str_len_S get_max_str_len_E more)
  = Log_type_ext get_mkcl (get_mkcoa get_mkco) get_lece get_max_len_S
      get_max_len_E get_max_str_len_S get_max_str_len_E more;

get_max_str_len_S_update ::
  forall a. (Nat -> Nat) -> Log_type_ext a -> Log_type_ext a;
get_max_str_len_S_update get_max_str_len_Sa
  (Log_type_ext get_mkcl get_mkco get_lece get_max_len_S get_max_len_E
    get_max_str_len_S get_max_str_len_E more)
  = Log_type_ext get_mkcl get_mkco get_lece get_max_len_S get_max_len_E
      (get_max_str_len_Sa get_max_str_len_S) get_max_str_len_E more;

get_max_str_len_E_update ::
  forall a. (Nat -> Nat) -> Log_type_ext a -> Log_type_ext a;
get_max_str_len_E_update get_max_str_len_Ea
  (Log_type_ext get_mkcl get_mkco get_lece get_max_len_S get_max_len_E
    get_max_str_len_S get_max_str_len_E more)
  = Log_type_ext get_mkcl get_mkco get_lece get_max_len_S get_max_len_E
      get_max_str_len_S (get_max_str_len_Ea get_max_str_len_E) more;

get_max_len_S_update ::
  forall a. (Nat -> Nat) -> Log_type_ext a -> Log_type_ext a;
get_max_len_S_update get_max_len_Sa
  (Log_type_ext get_mkcl get_mkco get_lece get_max_len_S get_max_len_E
    get_max_str_len_S get_max_str_len_E more)
  = Log_type_ext get_mkcl get_mkco get_lece (get_max_len_Sa get_max_len_S)
      get_max_len_E get_max_str_len_S get_max_str_len_E more;

get_max_len_E_update ::
  forall a. (Nat -> Nat) -> Log_type_ext a -> Log_type_ext a;
get_max_len_E_update get_max_len_Ea
  (Log_type_ext get_mkcl get_mkco get_lece get_max_len_S get_max_len_E
    get_max_str_len_S get_max_str_len_E more)
  = Log_type_ext get_mkcl get_mkco get_lece get_max_len_S
      (get_max_len_Ea get_max_len_E) get_max_str_len_S get_max_str_len_E more;

get_max_str_len_S :: forall a. Log_type_ext a -> Nat;
get_max_str_len_S
  (Log_type_ext get_mkcl get_mkco get_lece get_max_len_S get_max_len_E
    get_max_str_len_S get_max_str_len_E more)
  = get_max_str_len_S;

get_max_str_len_E :: forall a. Log_type_ext a -> Nat;
get_max_str_len_E
  (Log_type_ext get_mkcl get_mkco get_lece get_max_len_S get_max_len_E
    get_max_str_len_S get_max_str_len_E more)
  = get_max_str_len_E;

get_max_len_S :: forall a. Log_type_ext a -> Nat;
get_max_len_S
  (Log_type_ext get_mkcl get_mkco get_lece get_max_len_S get_max_len_E
    get_max_str_len_S get_max_str_len_E more)
  = get_max_len_S;

get_max_len_E :: forall a. Log_type_ext a -> Nat;
get_max_len_E
  (Log_type_ext get_mkcl get_mkco get_lece get_max_len_S get_max_len_E
    get_max_str_len_S get_max_str_len_E more)
  = get_max_len_E;

max_str_len :: forall a. Set [a] -> Nat;
max_str_len xs = maxa (image size_list xs);

upd_max_quantities ::
  forall a b.
    Observation_table_ext a b () -> Log_type_ext () -> Log_type_ext ();
upd_max_quantities tbl log =
  get_max_str_len_E_update
    (\ _ -> max (((max_str_len . Set) . get_E) tbl) (get_max_str_len_E log))
    (get_max_str_len_S_update
      (\ _ -> max (((max_str_len . Set) . get_S) tbl) (get_max_str_len_S log))
      (get_max_len_E_update
        (\ _ -> max ((size_list . get_E) tbl) (get_max_len_E log))
        (get_max_len_S_update
          (\ _ -> max ((size_list . get_S) tbl) (get_max_len_S log)) log)));

get_mkco :: forall a. Log_type_ext a -> Nat;
get_mkco
  (Log_type_ext get_mkcl get_mkco get_lece get_max_len_S get_max_len_E
    get_max_str_len_S get_max_str_len_E more)
  = get_mkco;

log_mkco_call ::
  forall a b.
    Observation_table_ext a b () -> Log_type_ext () -> Log_type_ext ();
log_mkco_call tbl log =
  upd_max_quantities tbl (get_mkco_update (\ _ -> Suc (get_mkco log)) log);

get_mkcl_update :: forall a. (Nat -> Nat) -> Log_type_ext a -> Log_type_ext a;
get_mkcl_update get_mkcla
  (Log_type_ext get_mkcl get_mkco get_lece get_max_len_S get_max_len_E
    get_max_str_len_S get_max_str_len_E more)
  = Log_type_ext (get_mkcla get_mkcl) get_mkco get_lece get_max_len_S
      get_max_len_E get_max_str_len_S get_max_str_len_E more;

get_mkcl :: forall a. Log_type_ext a -> Nat;
get_mkcl
  (Log_type_ext get_mkcl get_mkco get_lece get_max_len_S get_max_len_E
    get_max_str_len_S get_max_str_len_E more)
  = get_mkcl;

log_mkcl_call ::
  forall a b.
    Observation_table_ext a b () -> Log_type_ext () -> Log_type_ext ();
log_mkcl_call tbl log =
  upd_max_quantities tbl (get_mkcl_update (\ _ -> Suc (get_mkcl log)) log);

fix_closedness_prob ::
  forall a b.
    (Eq a) => ([a] -> b) ->
                ([a], a) ->
                  Observation_table_ext a b () -> Observation_table_ext a b ();
fix_closedness_prob orc (s, a) tbl = extendS orc tbl [s ++ [a]];

get_closedness_prob ::
  forall a b. (Eq b) => Observation_table_ext a b () -> Maybe ([a], a);
get_closedness_prob table =
  safe_head
    (concatMap
      (\ x ->
        (if null (projectToRowS table x) then [(butlast x, last x)] else []))
      (get_SA table));

make_closed ::
  forall a b.
    (Eq a,
      Eq b) => ([a] -> b) ->
                 Observation_table_ext a b () -> Observation_table_ext a b ();
make_closed orc tbl = let {
                        problem = get_closedness_prob tbl;
                      } in fix_closedness_prob orc (the problem) tbl;

make_cc_step ::
  forall a b.
    (Eq a,
      Eq b) => ([a] -> b) ->
                 (Observation_table_ext a b (), Log_type_ext ()) ->
                   (Observation_table_ext a b (), Log_type_ext ());
make_cc_step orc (tbl, log) =
  (if not (closedp tbl) then (make_closed orc tbl, log_mkcl_call tbl log)
    else (if not (consistentp tbl)
           then (make_consistent orc tbl, log_mkco_call tbl log)
           else (tbl, log)));

make_cc ::
  forall a b.
    (Eq a,
      Eq b) => ([a] -> b) ->
                 (Observation_table_ext a b (), Log_type_ext ()) ->
                   Maybe (Observation_table_ext a b (), Log_type_ext ());
make_cc orc initIter =
  while_option (\ (tbl, _) -> not (closedp tbl) || not (consistentp tbl))
    (make_cc_step orc) initIter;

make_finalf :: forall a b. Observation_table_ext a b () -> [a] -> b;
make_finalf table s = the (get_T table s);

make_initial :: forall a b. Observation_table_ext a b () -> [a];
make_initial table = [];

state_reps_of :: forall a b. (Eq b) => Observation_table_ext a b () -> [[a]];
state_reps_of table = nubby (rows_eq_p table) (get_S table ++ [[]]);

row_to_state ::
  forall a b. (Eq b) => Observation_table_ext a b () -> [a] -> Maybe [a];
row_to_state table r = find (rows_eq_p table r) (state_reps_of table);

get_lece_update :: forall a. (Nat -> Nat) -> Log_type_ext a -> Log_type_ext a;
get_lece_update get_lecea
  (Log_type_ext get_mkcl get_mkco get_lece get_max_len_S get_max_len_E
    get_max_str_len_S get_max_str_len_E more)
  = Log_type_ext get_mkcl get_mkco (get_lecea get_lece) get_max_len_S
      get_max_len_E get_max_str_len_S get_max_str_len_E more;

get_lece :: forall a. Log_type_ext a -> Nat;
get_lece
  (Log_type_ext get_mkcl get_mkco get_lece get_max_len_S get_max_len_E
    get_max_str_len_S get_max_str_len_E more)
  = get_lece;

log_lece_call ::
  forall a b.
    Observation_table_ext a b () -> Log_type_ext () -> Log_type_ext ();
log_lece_call tbl log =
  upd_max_quantities tbl (get_lece_update (\ _ -> Suc (get_lece log)) log);

make_initial_table ::
  forall a b. (Eq a) => [a] -> ([a] -> b) -> Observation_table_ext a b ();
make_initial_table alph orc =
  let {
    queries = [[]] ++ map (\ a -> [a]) alph;
    answers = map orc queries;
  } in Observation_table_ext alph [[]] (remdups (map (\ a -> [a]) alph)) [[]]
         (map_upds (\ _ -> Nothing) queries answers) ();

memoized_while_option ::
  forall a b. (a -> Maybe b) -> (Maybe b -> a -> a) -> a -> Maybe a;
memoized_while_option b c s =
  let {
    bs = b s;
  } in (if not (is_none bs) then memoized_while_option b c (c bs s)
         else Just s);

learn_from_cexample ::
  forall a b.
    (Eq a) => [a] ->
                ([a] -> b) ->
                  Observation_table_ext a b () -> Observation_table_ext a b ();
learn_from_cexample t orc tbl = extendS orc tbl (all_prefixes t);

lstar_learner_step ::
  forall a b.
    (Eq a,
      Eq b) => ([a] -> b) ->
                 [a] ->
                   (Observation_table_ext a b (), Log_type_ext ()) ->
                     (Observation_table_ext a b (), Log_type_ext ());
lstar_learner_step orcM t (tbl, log) =
  the (make_cc orcM (learn_from_cexample t orcM tbl, log_lece_call tbl log));

make_transition ::
  forall a b. (Eq b) => Observation_table_ext a b () -> a -> [a] -> [a];
make_transition table a st = the (row_to_state table (st ++ [a]));

make_statespace :: forall a b. (Eq b) => Observation_table_ext a b () -> [[a]];
make_statespace table = state_reps_of table;

make_conjecture ::
  forall a b.
    (Eq b) => Observation_table_ext a b () -> Moore_machine_ext a [a] b ();
make_conjecture table =
  Moore_machine_ext (get_A table) (make_statespace table) (make_initial table)
    (make_transition table) (make_finalf table) ();

lstar_learner_loop ::
  forall a b.
    (Eq a,
      Eq b) => ([a] -> b) ->
                 (Moore_machine_ext a [a] b () -> Maybe [a]) ->
                   (Observation_table_ext a b (), Log_type_ext ()) ->
                     Maybe (Observation_table_ext a b (), Log_type_ext ());
lstar_learner_loop orcM orcC initIter =
  memoized_while_option ((orcC . make_conjecture) . fst)
    (\ val (tbl, log) -> lstar_learner_step orcM (the val) (tbl, log))
    (the (make_cc orcM initIter));

lstar_learner ::
  forall a b.
    (Eq a,
      Eq b) => [a] ->
                 ([a] -> b) ->
                   (Moore_machine_ext a [a] b () -> Maybe [a]) ->
                     (Observation_table_ext a b (), Log_type_ext ());
lstar_learner alph orcM orcC =
  the (lstar_learner_loop orcM orcC
        (make_initial_table alph orcM,
          Log_type_ext Zero_nat Zero_nat Zero_nat one_nat one_nat Zero_nat
            Zero_nat ()));

lstar_learner_conjecture_mealy ::
  forall a b.
    (Enum a, Eq a,
      Eq b) => [a] ->
                 ([a] -> b) ->
                   (Mealy_machine_ext a [a] b () -> Maybe [a]) ->
                     Mealy_machine_ext a [a] b ();
lstar_learner_conjecture_mealy alph orcM orcC =
  mealy_of
    ((make_conjecture . fst)
      (lstar_learner alph (moore_oraclem_of alph orcM)
        (moore_oraclec_of orcC)));

}
