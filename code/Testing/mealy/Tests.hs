{-# LANGUAGE FlexibleInstances #-}
module Tests where

import qualified Data.List
import           ExportedMealy
import           System.IO.Unsafe --To define unsafe user oracles

------------------------------------
--    We make Dfa_ext showable
------------------------------------

go trans s a = "Read: " ++ show a ++ (if (trans a s)/=s then " -> Go to state " else " -> Stay in state ") ++ (show (trans a s))

showTransFrom alph trans s = "-------------------------\n" ++  "Transitions from state: " ++ show s ++ "\n-------------------------\n" ++ (Data.List.intercalate "\n" $ map (go trans s) alph)

showAllTrans alph states trans = (Data.List.intercalate "\n" $ map (showTransFrom alph trans) states)

instance Prelude.Show (Moore_machine_ext Prelude.Char [Prelude.Char] Prelude.Char ()) where
  show (Moore_machine_ext alph states q0 trans finp x) = "Moore machine on states: " ++ show states ++ "\n" ++ "Final state function: " ++ (show $ map (\x -> (x, finp x)) states) ++ "\nNextTransitions:\n" ++ showAllTrans alph states trans

instance (Prelude.Show a, Prelude.Eq a, Prelude.Show b) => Prelude.Show (Mealy_machine_ext a [a] b ()) where
  show machine@(Mealy_machine_ext alph states q0 trans finp x) = "Mealy machine on states: " ++ show states ++ "\n" ++ "Final state function: " ++ (show $ map (\x -> (x, map (finp x) alph)) states) ++ "\nNextTransitions:\n" ++ showAllTrans alph states trans

instance Prelude.Show (Observation_table_ext Prelude.Char Prelude.Char c) where
  show (Observation_table_ext alph s sa cls f x) = let
    rls = s `Data.List.union` sa --The row labels
    psize = maximum (map length (cls ++ rls)) --maxRLSize = maximum (map length rls)
    appendBlanks int str = (take int $ repeat ' ') ++ str
    lpad size str = appendBlanks (size - length str) str
    sep = " | "
    showLabel "" = "λ"
    showLabel x = x
    showColLabels = appendBlanks (psize + length sep) . Data.List.intercalate sep . map ((lpad psize) . showLabel)
    showRow rl = Data.List.intercalate sep $ map (lpad psize) $ (showLabel rl) : (map (\x -> maybe "?" (\v -> [v]) (f (rl++x)) ) cls) in
      -- showColLabels cls ++ "\n" ++ unlines (map showRow rls) ++ "Set S: " ++ show s
      showColLabels cls ++ "\n" ++ unlines (map showRow s) ++ "----\n" ++ unlines (map showRow sa) ++ "Set S: " ++ show s

-------------------------------
-- Define an UNSAFE user oracle
-------------------------------

--Use the user as an oracle for membership queries
unsafeUserOracleM :: [Char] -> Char
unsafeUserOracleM query = unsafePerformIO $ do
  putStrLn $ "[Membership query]: What value does the word [" ++ query ++ "] have in the language ? (Output alphabet letter)"
  x <- getLine
  let answer = head x
  return answer   

--Use the user as an oracle for conjecture queries
unsafeUserOracleC conjDFA = unsafePerformIO $ do
  putStrLn $ show conjDFA
  putStrLn $ "[Conjecture query]: is the previous automaton correct (True/False) ?"
  x <- getLine
  let answer = read x :: Bool
  if answer then return Nothing else askForCounterexample
  where askForCounterexample :: IO (Maybe [Char])
        askForCounterexample = do
          putStrLn $ "Please enter a counterexample word for the previous automaton (ex: \"acba\") without quotes):"
          ex <- getLine
          return (Just ex)

-------------------------------
-- Define automatic oracles
-------------------------------

-- A simple automatic oracle; Returns true iif the query is in set
automaticFiniteOracleM set query = query `elem` set

------------------------------
-- Here we can write some tests
------------------------------
data AlphabetType = A1 | A2 | A3 deriving(Show, Eq)

--There is nothing to satisfy for the Finite typeclass
instance ExportedMealy.Finite AlphabetType
--Enum is not Haskell's enum typeclass
instance ExportedMealy.Enum AlphabetType where
  enum = [A1,A2,A3]
  enum_all p = all p enum
  enum_ex p = any p enum

testOut = lstar_learner_conjecture_mealy [A1, A2, A3] (\x -> if (length x) == 2 then 1 else 0) (const Nothing)
-- testOut2 = lstar_learner_conjecture_mealy [A1, A2, A3] unsafeUserOracleM unsafeUserOracleC
-- testOut3 = lStarLearner "abd" (automaticFiniteOracleM ["ab", "a", "b"]) unsafeUserOracleC
