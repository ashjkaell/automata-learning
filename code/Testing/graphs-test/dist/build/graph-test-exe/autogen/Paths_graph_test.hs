{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Paths_graph_test (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/home/gabriel/.cabal/bin"
libdir     = "/home/gabriel/.cabal/lib/x86_64-linux-ghc-8.6.5/graph-test-0.1.0.0-B03Mg8uLk5UBqBWufOMzrj-graph-test-exe"
dynlibdir  = "/home/gabriel/.cabal/lib/x86_64-linux-ghc-8.6.5"
datadir    = "/home/gabriel/.cabal/share/x86_64-linux-ghc-8.6.5/graph-test-0.1.0.0"
libexecdir = "/home/gabriel/.cabal/libexec/x86_64-linux-ghc-8.6.5/graph-test-0.1.0.0"
sysconfdir = "/home/gabriel/.cabal/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "graph_test_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "graph_test_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "graph_test_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "graph_test_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "graph_test_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "graph_test_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
