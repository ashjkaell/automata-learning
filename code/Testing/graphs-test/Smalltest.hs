import qualified Data.GraphViz as G
import qualified Data.GraphViz.Attributes.Complete as G
import qualified Data.GraphViz.Types as G
import qualified Data.Text.Lazy as TL
import qualified Data.Text.Lazy.IO as TLIO

data VLabel = VNormal | VRed deriving (Show)
type V = (String, VLabel)

data ELabel = ENormal deriving (Show)
type E = (String, String, ELabel)

type FileGraph = ([V], [E])

v1 = map (\x -> (x,VNormal)) ["lambda", "a", "b"]
e1 = map (\(a,b) -> (a,b,ENormal)) [("lambda","a"),("lambda","b"),("a","a")]
g1 = (v1,e1)

-- GraphVisParams vertexType vertexLabeltype edgeLabelType clusterType clusterLabelType
fileGraphParams :: G.GraphvizParams String VLabel ELabel () VLabel
fileGraphParams = G.defaultParams {
    G.fmtNode = (\(v, vl) -> case vl of
                VNormal -> colorAttribute $ G.RGB 0 0 0
                VRed    -> colorAttribute $ G.RGB 40 255 40)
  , G.fmtEdge = (\(from, to, el) -> case el of
                ENormal -> colorAttribute $ G.RGB 0 0 0)
      }  
  where
    colorAttribute color = [ G.Color $ G.toColorList [ color ] ]

main :: IO ()
main = do
  -- 1. Create our application-specific graph
  let (vs, es) = g1
  -- 2. Convert it into a DotGraph
  let dotGraph = G.graphElemsToDot fileGraphParams vs es :: G.DotGraph String
  -- 3. Render it into .dot text
      dotText = G.printDotGraph dotGraph :: TL.Text
  -- 4. Write the contents to a file
  TLIO.writeFile "files.dot" dotText
