{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}

module Tests where

import qualified Data.List
import           Exported
import System.IO.Unsafe --To define unsafe user oracles

------------------------------------
--    We make Dfa_ext showable
------------------------------------

go trans s a = "Read: " ++ show a ++ (if (trans a s)/=s then " -> Go to state " else " -> Stay in state ") ++ (show (trans a s))

showTransFrom alph trans s = "-------------------------\n" ++  "Transitions from state: " ++ show s ++ "\n-------------------------\n" ++ (Data.List.intercalate "\n" $ map (go trans s) alph)

showAllTrans alph states trans = (Data.List.intercalate "\n" $ map (showTransFrom alph trans) states)

instance Prelude.Show (Exported.Dfa_ext Prelude.Char [Prelude.Char] c) where
    show (Exported.Dfa_ext alph states q0 trans finp x) = "Automaton on states: " ++ show states ++ "\n" ++ "Accept states: " ++ (show $ filter (finp) states) ++ "\nNextTransitions:\n" ++ showAllTrans alph states trans

instance Prelude.Show (Observation_table_ext Prelude.Char c) where
  show (Observation_table_ext alph s sa cls f x) = let
    rls = s `Data.List.union` sa --The row labels
    psize = maximum (map length (cls ++ rls)) --maxRLSize = maximum (map length rls)
    appendBlanks int str = (take int $ repeat ' ') ++ str
    lpad size str = appendBlanks (size - length str) str
    sep = " | "
    showLabel "" = "L"
    showLabel x = x
    showColLabels = appendBlanks (psize + length sep) . Data.List.intercalate sep . map ((lpad psize) . showLabel)
    showRow rl = Data.List.intercalate sep $ map (lpad psize) $ (showLabel rl) : (map (\x -> maybe "?" (\v -> if v then "1" else "0") (f (rl++x)) ) cls) in
      -- showColLabels cls ++ "\n" ++ unlines (map showRow rls) ++ "Set S: " ++ show s
      showColLabels cls ++ "\n" ++ unlines (map showRow s) ++ "----\n" ++ unlines (map showRow sa) ++ "Set S: " ++ show s

-------------------------------
-- Define an UNSAFE user oracle
-------------------------------

--Use the user as an oracle for membership queries
unsafeUserOracleM query = unsafePerformIO $ do
  putStrLn $ "[Membership query]: is [" ++ query ++ "] a word of the language ? (True/False)"
  x <- getLine
  let answer = read x :: Bool
  return answer    

--Use the user as an oracle for conjecture queries
unsafeUserOracleC conjDFA = unsafePerformIO $ do
  putStrLn $ show conjDFA
  putStrLn $ "[Conjecture query]: is the previous automaton correct (True/False) ?"
  x <- getLine
  let answer = read x :: Bool
  if answer then return Nothing else askForCounterexample
  where askForCounterexample :: IO (Maybe [Char])
        askForCounterexample = do
          putStrLn $ "Please enter a counterexample word for the previous automaton (ex: \"acba\") without quotes):"
          ex <- getLine
          return (Just ex)

-------------------------------
-- Define automatic oracles
-------------------------------

-- A simple automatic oracle; Returns true iif the query is in set
automaticFiniteOracleM set query = query `elem` set

------------------------------
-- Here we can write some tests
------------------------------
testOut = lStarLearner "ab" (const False) (const Nothing)
testOut2 = lStarLearner "ab" unsafeUserOracleM unsafeUserOracleC
testOut3 = lStarLearner "abd" (automaticFiniteOracleM ["ab", "a", "b"]) unsafeUserOracleC
