module Oracle where

import Dfa

type OracleM = String -> (IO Bool)  --An oracle for membership queries
type OracleC = DFA -> (IO (Maybe [Char]))  --An oracle for conjecture queries
--Conjecture queries are by maybe a counterexample iff the automaton provided isn't correct.
type Oracle = (OracleM, OracleC)

--Use the user as an oracle for membership queries
userOracleM :: OracleM
userOracleM query = do
  putStrLn $ "[Membership query]: is [" ++ query ++ "] a word of the language ? (True/False)"
  x <- getLine
  let answer = read x :: Bool
  return answer
  
--Use the user as an oracle for conjecture queries
userOracleC :: OracleC
userOracleC conjDFA = do
  putStrLn $ show conjDFA
  putStrLn $ "[Conjecture query]: is the previous automaton correct (True/False) ?"
  x <- getLine
  let answer = read x :: Bool
  if answer then return Nothing else askForCounterexample
  where askForCounterexample :: IO (Maybe [Char])
        askForCounterexample = do
          putStrLn $ "Please enter a counterexample word for the previous automaton (ex: \"acba\") without quotes):"
          ex <- getLine
          return (Just ex)
  
userOracle = (userOracleM,userOracleC)
