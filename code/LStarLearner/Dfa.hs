module Dfa where

import Data.List(intercalate)

--{ Typedefs }--

type DfaAlphabet = [Char]
data DFA = DFA
  { alphabet :: DfaAlphabet                        -- alphabet
  , statespace :: [Int]
  , initial :: Int                                 -- initial state
  , transition :: Int -> Char -> Int               -- transition function
  , finalDfaState :: Int -> Bool                   -- test for final state
  }

--{ Show instantiation }--

instance Show DFA where
  show (DFA alph states q0 trans finP) = let
    go s a = "Read: " ++ show a ++ (if newS/=s then " -> Go to state " else " -> Stay in state ") ++ (show newS)
      where newS = trans s a
    showTransFrom s = "-------------------------\n" ++  "Transitions from state: " ++ show s ++ "\n-------------------------\n" ++ (intercalate "\n" $ map (go s) alph)
    showAllTrans = (intercalate "\n" $ map showTransFrom states) in
      --TODO: Make this one line.
      "Automaton on states: " ++ show states ++ "\n" ++ "Accept states: " ++ (show $ filter (finP) states) ++ "\nNextTransitions:\n" ++ showAllTrans

--{ Accepts functions }--

accepts :: DFA -> String -> Bool
accepts dfa w = finalDfaState dfa (foldl (transition dfa) (initial dfa) w)

lexicon :: DfaAlphabet -> Int -> [String]
lexicon sigma n = foldl (\ws _ -> [x : w | x <- sigma, w <- ws]) [[]] [1..n]

language :: DFA -> Int -> [String]
language dfa n = filter (accepts dfa) (lexicon (alphabet dfa) n)

