module Angluin where

{- Note:
   In the DFA module we have fixed:
     * DfaAlphabet = [Char]
     * All words are Strings
-}
import Dfa
import Oracle
import Data.List
import Data.Maybe

--{ Observation Tables }--

data ObsTable = ObsTable { tableAlphabet :: DfaAlphabet
                         , tableS :: [String]
                         , tableSdotA :: [String]
                     --  , tableRowLabels :: [String]   --We store the rows as S Union SdotA
                         , tableColLabels :: [String]   --Called E in the article
                         , tableFunction :: String -> String -> Maybe Bool
                         }

tableRowLabels :: ObsTable -> [String]
tableRowLabels (ObsTable _ s sa _ _) = s `union` sa

instance Show ObsTable where
  show (ObsTable alph s sa cls f) = let
    rls = s `union` sa --The row labels
    psize = maximum (map length (cls ++ rls)) --maxRLSize = maximum (map length rls)
    appendBlanks int str = (take int $ repeat ' ') ++ str
    lpad size str = appendBlanks (size - length str) str
    sep = " | "
    showLabel "" = "L"
    showLabel x = x
    showColLabels = appendBlanks (psize + length sep) . intercalate sep . map ((lpad psize) . showLabel)
    showRow rl = intercalate sep $ map (lpad psize) $ (showLabel rl) : (map (\x -> maybe "?" (\v -> if v then "1" else "0") (f rl x) ) cls) in
      -- showColLabels cls ++ "\n" ++ unlines (map showRow rls) ++ "Set S: " ++ show s
      showColLabels cls ++ "\n" ++ unlines (map showRow s) ++ "----\n" ++ unlines (map showRow sa) ++ "Set S: " ++ show s

--Tests whether rows labeled by r1 and r2 are equal
rowsEqP :: ObsTable -> String -> String -> Bool
rowsEqP (ObsTable _ _ _ cls f) r1 r2 = map (f r1) cls == map (f r2) cls

--Test whether a table is _closed_
closedP :: ObsTable -> Bool
closedP tbl@(ObsTable _ setS setSA cls f) = all go setSA
  where go t = (not . null) $ filter (rowsEqP tbl t) setS

--Test whether a table is _consistent_
--Consistency means that we'll be able to define a
--transition function based on the table.
consistentP :: ObsTable -> Bool
consistentP tbl@(ObsTable alph setS setSA cls f) = let
  matches = [(a,b) | a <- setS, b <- setS, (rowsEqP tbl a b)] in
    all go matches
  where go (s1,s2) = all (\a -> rowsEqP tbl (s1 ++ [a]) (s2 ++ [a])) alph


-- Given an old obsTable function, new elems of S, extend the function
-- using the oracle
extendS :: Oracle -> (String -> String -> Maybe Bool) -> [String] -> [String] -> IO (String -> String -> Maybe Bool)
extendS (oracle,_) oldF newS oldE = do
  let queries = filter (not . known) [s++e | s <- newS, e <- oldE]
  answers <- mapM oracle queries
  let newMappings = zip queries answers
-- return (\x y -> fromMaybe (oldF x y) (lookup (x++y) newMappings))
  return (newf newMappings)
  where
    known word = any (\(x,y) -> (oldF x y) /= Nothing) $ zip (inits word) (tails word)
    newf newMappings x y = let oldres = listToMaybe $ mapMaybe (uncurry oldF) $ zip (inits (x++y)) (tails (x++y)) in
      if (isJust oldres) then oldres else (lookup (x++y) newMappings)

-- Given an old obsTable function, new elems of E, extend the function
-- using the oracle
extendE :: Oracle -> (String -> String -> Maybe Bool) -> [String] -> [String] -> IO (String -> String -> Maybe Bool)
extendE (oracle,_) oldF newE oldS = do
  let queries = filter (not . known) [s++e | s <- oldS, e <- newE]
  answers <- mapM oracle queries
  let newMappings = zip queries answers
-- return (\x y -> fromMaybe (oldF x y) (lookup (x++y) newMappings))
  return (newf newMappings)
  where
    known word = any (\(x,y) -> oldF x y /= Nothing) $ zip (inits word) (tails word) 
    newf newMappings x y = let oldres = listToMaybe $ mapMaybe (uncurry oldF) $ zip (inits (x++y)) (tails (x++y)) in
      if (isJust oldres) then oldres else (lookup (x++y) newMappings)
      
makeInitialTable :: DfaAlphabet -> Oracle -> IO ObsTable
makeInitialTable alph oracle = let s = [""]
                                   sdota = [[a] | a <- alph]
                                   newS = s `union` sdota
                                   e = [""]
                                   f = const (const Nothing) in do
  --Since the set e only contains lambda, we can use the function for an
  --update of S.
  newf <- extendS oracle f newS e
  let tbl = (ObsTable alph s sdota e newf)
  putStrLn $ "Made initial table: " ++ "\n" ++ show tbl
  return tbl

makeConjecture :: ObsTable -> DFA
makeConjecture tbl@(ObsTable alph setS setSA setE f) = DFA alph [0..n-1] initState trans accepts
  -- TODO: I think it's a bug to union with setE, what was I thinking ?
  -- where states = nubBy (rowsEqP tbl) $ setS `union` setE
  where states = nubBy (rowsEqP tbl) $ setS
        n = length states
        rowToState :: String -> Int
        rowToState s = snd . head $ filter (rowsEqP tbl s . fst) (zip states [0..])
        initState = rowToState ""  --Row(lambda) is the start state
        acceptStates = [rowToState s | s <- setS, (fromJust $ f s "") == True]
        accepts x = elem x acceptStates
        trans q a = rowToState (states!!q ++ [a])

--The main algorithm
--makeCC stands for make closed and consistent
--Closed: /home/gabriel/Dropbox/Documents/ETH/bthesis/code/Angluin.hs:47
--Consistent: /home/gabriel/Dropbox/Documents/ETH/bthesis/code/Angluin.hs:54

--Given an obsTable, uses the oracle until it's closed and consistent.
lStarStep :: DfaAlphabet -> Oracle -> ObsTable -> IO ObsTable
lStarStep alph oracle tInit = makeCC tInit
  where go :: ObsTable -> IO ObsTable
        go t@(ObsTable alph setS setSA setE f)
         | (not . consistentP) t = let
           (s1,s2,a,e) = head [(s1,s2,a,e) | s1 <- setS, s2 <- setS, a <- alph, e <- setE, rowsEqP t s1 s2, not $ rowsEqP t (s1 ++ [a] ++ e) (s2 ++ [a] ++ e)]
           newE = setE `union` [a:e] in do
             putStrLn $ "The following table is *not consistent* on rows (" ++ (show s1) ++ "," ++ (show s2) ++ ") with ending a.e = " ++ show (a,e)
             putStrLn (show t)
             newf <- extendE oracle f newE (setS `union` setSA)
             return (ObsTable alph setS setSA newE newf)
         | (not . closedP) t = let
           (s1,a) = head [(s1,a) | s1 <- setS, a <- alph, all (not . rowsEqP t (s1 ++ [a])) setS]
           newS = setS `union` [s1 ++ [a]]
           newSA = [s++[a] | s <- newS, a <- alph] 
           sUpdate = [(s1 ++ [a] ++ [aa]) | aa <- alph] in do
             putStrLn $ "The following table is *not closed* on row " ++ (show s1) ++ " with ending a = " ++ show a
             putStrLn (show t)
             newf <- extendS oracle f sUpdate setE
             return (ObsTable alph newS newSA setE newf)
        makeCC :: ObsTable -> IO ObsTable
        makeCC tbl = do
          if (closedP tbl && consistentP tbl) then return tbl else go tbl
          
lStar :: DfaAlphabet -> Oracle -> IO DFA
lStar alph oracle = do
  finalTable <- loopUntilDone $ makeInitialTable alph oracle
  return (makeConjecture finalTable)
  where loopUntilDone :: IO ObsTable -> IO ObsTable
        loopUntilDone tbl = do
          proposal <- tbl >>= lStarStep alph oracle
          putStrLn $ "I have table:\n" ++ show proposal
          putStrLn $ "Making automaton..."
          let automaton = makeConjecture proposal
          --We use the oracle on the conjecture
          answer <- (snd oracle) automaton
          maybe (return proposal) (loopUntilDone . fix proposal) answer
        fix :: ObsTable -> String -> IO ObsTable
        fix tbl@(ObsTable alph setS setSA setE f) t = let
          newS = setS `union` (inits t)
          newSA = [s++[a] | s <- newS, a <- alph] in do
            newf <- extendS oracle f (newS `union` newSA) setE
            return (ObsTable alph newS newSA setE newf)