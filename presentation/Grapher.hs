{-# LANGUAGE FlexibleInstances #-}

import qualified Data.GraphViz as G
import qualified Data.GraphViz.Attributes.Complete as G
import qualified Data.GraphViz.Types as G
import qualified Data.List
import qualified Data.Maybe
import qualified Data.Text.Lazy as TL
import qualified Data.Text.Lazy.IO as TLIO
import           System.Environment

-- Def of observation table type
-- Isabelle's code export adds this weird additional (useless ?) argument.
data Observation_table_ext a b c =
  Observation_table_ext [a] [b] [[a]] [[a]] [[a]] ([a] -> Maybe b) c;

-- Pretty print function for observation tables
showTablePretty (Observation_table_ext alph out_alph s sa cls f x) = let
    rls = s `Data.List.union` sa --The row labels
    psize = maximum (map length (cls ++ rls)) --maxRLSize = maximum (map length rls)
    appendBlanks int str = (take int $ repeat ' ') ++ str
    lpad size str = appendBlanks (size - length str) str
    sepSymbol = "|"
    innerSep = " " ++ sepSymbol ++ ""
    showLabel "" = "λ"
    showLabel x = x
    showColLabels cls = sepSymbol ++ (appendBlanks (psize + length innerSep - 1) $ sepSymbol ++ (Data.List.intercalate innerSep . map ((lpad psize) . showLabel)) cls)
    showRow rl = sepSymbol ++ (Data.List.intercalate innerSep $ map (lpad psize) $ (showLabel rl) : (map (\x -> maybe "?" (\v -> [v]) (f (rl++x)) ) cls)) in
      -- showColLabels cls ++ "\n" ++ unlines (map showRow rls) ++ "Set S: " ++ show s
      showColLabels cls ++ "\n" ++ unlines (map showRow s) ++ sepSymbol ++ "----\n" ++ unlines (map showRow sa) ++ "Set S: " ++ show s

-- Prints an observation table into a format org can parse as an org-table
showTableOrg (Observation_table_ext alph out_alph s sa cls f x) = let
    rls = s `Data.List.union` sa --The row labels
    psize = maximum (map length (cls ++ rls)) --maxRLSize = maximum (map length rls)
    appendBlanks int str = (take int $ repeat ' ') ++ str
    lpad size str = appendBlanks (size - length str) str
    sepSymbol = ""
    innerSep = " " ++ sepSymbol ++ ""
    showLabel "" = "\\lambda" --for the latex export
    showLabel x = x
    showColLabels cls = sepSymbol ++ "\\emph{S}\\hspace{1cm}\\emph{E}" ++ (appendBlanks (psize + length innerSep - 1) $ sepSymbol ++ (Data.List.intercalate innerSep . map ((lpad psize) . showLabel)) cls)
    showRow rl = sepSymbol ++ (Data.List.intercalate innerSep $ map (lpad psize) $ (showLabel rl) : (map (\x -> maybe "?" (\v -> [v]) (f (rl++x)) ) cls)) in
      -- showColLabels cls ++ "\n" ++ unlines (map showRow rls) ++ "Set S: " ++ show s
      showColLabels cls ++ "\n" ++ unlines (map showRow s) ++ sepSymbol ++ "\\emph{SA}\n" ++ unlines (map showRow sa)

instance Prelude.Show (Observation_table_ext Prelude.Char Prelude.Char c) where show = showTablePretty
    
testObsTable = Observation_table_ext alph out_alph s sa cls f ()
  where
    alph = "ab"
    out_alph = "012"
    s = ["","a","b"]
    sa = [x++[a] | x <- s, a <- alph]
    cls = ["","a", "b"]
    f x = Just (go x) where
      go x = (head . show) $ ((flip mod) 3 . length . filter (=='a')) x

--Make an observation table with given parameters. f is the L(.)
--(f :: String -> Maybe a where out_alph :: a)
--function and sa will be automaticlly set.
makeObsTable alph out_alph f s cls = Observation_table_ext alph out_alph s sa cls f ()
  where
    sa = [x++[a] | x <- s, a <- alph]

-- Graph datatypes

data VLabel = VNormal | VNonClosed deriving (Eq,Show)
type V = (String, VLabel)

data ELabel = ENormal String | ENonClosed String | ENonConsistent String deriving (Eq,Show)
type E = (String, String, ELabel)

type ObsTableGraph = ([V], [E])

-- Converting tables to graphs

drawLambda "" = "λ"
drawLambda x = x

--rowsEqP :: forall a b. (Eq b) => Observation_table_ext a b () -> [a] -> [a] -> Bool;
rowsEqP (Observation_table_ext alph out_alph s sa cls f _) r1 r2 = all (\x -> f (r1 ++ x) == f (r2 ++ x)) cls;

--This corresponds to the function we would use to convert an obs table to a DFA.
--The statespace is the eq. classes on S
--We use the same hack as usual, to force lambda being chosen for clarity
stateRepsOnS table@(Observation_table_ext alph out_alph s sa cls f _) = Data.List.nubBy (rowsEqP table) s++[""]

--This is a supplementary function to show the "states" on SA. If the table is closed, the output of this
--function should be a subset of the previous one.
stateRepsOnSA table@(Observation_table_ext alph out_alph s sa cls f _) = Data.List.nubBy (rowsEqP table) sa

-- Given an element of SA finds the corresponding row representative in S if it exists.
findStateRepInS table saRow = Data.List.find (rowsEqP table saRow) (stateRepsOnS table)

-- To avoid having duplicated non closed nodes when jumping into unknown extension configuration
-- in the rowsSA, we pick a 'representative' for non closed transitions.
-- This should always return Just _ since we assume saRow is in SA
findStateRepInSA table saRow = Data.Maybe.fromJust $ Data.List.find (rowsEqP table saRow) (stateRepsOnSA table)

--This is a supplementary function to show the non-closed "states" on SA.
--We map findStateRepInSA, to uniquify the names of the non closed states.
nonClosedInSA table@(Observation_table_ext alph out_alph s sa cls f _) = map (findStateRepInSA table) $ Data.List.filter (\x -> Data.Maybe.isNothing $ findStateRepInS table x) sa

-- Converts an observation table to a styled list of vertices (type V)
obsTableToVertices table@(Observation_table_ext _ _ s sa _ _ _) =
  let normalVerts = map (\x -> (drawLambda x, VNormal)) (stateRepsOnS table)
      nonClosedVerts = map (\x -> (drawLambda x, VNonClosed)) (nonClosedInSA table) in
    Data.List.nubBy (\x y -> fst x == fst y) $ normalVerts ++ nonClosedVerts

--Returns the list of edges coming out of a row row of the table table.
--This asumes row is in S
edgesOfRow table@(Observation_table_ext alph _ _ _ _ _ _) row = map edgeTo alph
  where edgeTo a = let
          --We assume row in S, so this will always work. We draw
          --edges out of the representative node
          rowRep = Data.Maybe.fromJust $ findStateRepInS table row
          rowsa = row++[a]
          rowSMaybe = findStateRepInS table rowsa in
            Data.Maybe.maybe (drawLambda rowRep, (drawLambda . findStateRepInSA table) rowsa, ENonClosed [a]) (\rowS -> (drawLambda rowRep, drawLambda rowS, ENormal [a])) rowSMaybe

--Converts the ENormal edges from the previous function to
--ENonConsistent if multiple of them come out of a single node
colorNonConsistentEdges edgeList = map f edgeList
  where f (a, b, ENonClosed lb) = (a, b, ENonClosed lb) --We don't touch the nonClosed edges.
        f (a, b, ENormal lb) = if ((>1) . length . filter (\(xa,xb,xt) -> xa == a && xt == ENormal lb)) edgeList then (a, b, ENonConsistent lb)
                               else (a, b, ENormal lb)
                      
--obsTableToEdges table = concatMap (edgesOfRow table) (stateRepsOnS table)
--This also removes redundant (duplicated) edges.
--obsTableToEdges table = Data.List.nub $ concatMap (edgesOfRow table) (stateRepsOnS table)
--TODO: Still bug, we don't see the non consistent edges.
obsTableToEdges table@(Observation_table_ext alph _ s _ _ _ _) = colorNonConsistentEdges $ Data.List.nub $ concatMap (edgesOfRow table) s

-- Graph plotting part

v1 = obsTableToVertices testObsTable
e1 = obsTableToEdges testObsTable
g1 = (v1,e1)

-- GraphVisParams vertexType vertexLabeltype edgeLabelType clusterType clusterLabelType
--TODO: Try using LabelDistance or xLabel constructor
fileGraphParams :: G.GraphvizParams String VLabel ELabel () VLabel
fileGraphParams = G.defaultParams {
    G.fmtNode = (\(v, vl) -> case vl of
                VNormal       -> [colorAttribute $ G.RGB 0 0 0, G.Shape G.Circle]
                VNonClosed    -> [colorAttribute $ G.RGB 255 0 0 , G.Shape G.Circle, G.Style [G.SItem G.Dotted []]])
  , G.fmtEdge = (\(from, to, etype) -> case etype of
                ENormal el -> [colorAttribute $ G.RGB 0 0 0, G.toLabel el]
                ENonClosed el -> [colorAttribute $ G.RGB 255 0 0, G.toLabel el, G.Style [G.SItem G.Dotted []]]
                ENonConsistent el -> [colorAttribute $ G.RGB 0 0 255, G.toLabel el, G.Style [G.SItem G.Dashed []]])
      }
  where
    colorAttribute color = G.Color $ G.toColorList [ color ]

main :: IO ()
main = do
  --Read args
  --Note: This crashes if the program isn't supplied all args:
  --Call as: Grapher.hs ./images/out.dot '["a","b","ab"]' '["","a"]'
  --(Don't forget singe quotes '' !)
  args <- getArgs
  let (dotFileName:sSetString:eSetString:_) = args
  let sSet = (read sSetString) :: [String]
      eSet = (read eSetString) :: [String]
  --Default params
  let alph = "ab"
      oalph = "01"
      f x = Just (go x) where
        --go x = (head . show) $ ((flip mod) 2 . length . filter (=='a')) x
        go x = let na = (length . filter (=='a')) x
                   nb = (length . filter (=='b')) x in
          if (mod na 2 == 0) && (mod nb 2 == 0) then '1' else '0'
                   
  --Make obs table
  --putStrLn $ show sSet
  let obsTable = makeObsTable alph oalph f sSet eSet
  putStrLn $ showTableOrg obsTable
  let vs = obsTableToVertices obsTable
      es = obsTableToEdges obsTable
      g = (vs,es)
  --We plot the graph
  --Convert the graph to a dot graph
  let dotGraph = G.graphElemsToDot fileGraphParams vs es :: G.DotGraph String
      -- Render it into .dot text
      dotText = G.printDotGraph dotGraph :: TL.Text
  -- 4. Write the contents to a file
  TLIO.writeFile dotFileName dotText
